from icmsutils.permissions import AbstractRestrictedResource


class ToolkitRestrictedResource(AbstractRestrictedResource):
    def __init__(self, db_resource_id):
        self._db_resource_id = db_resource_id

    @property
    def db_id(self):
        return self._db_resource_id

    def _get_id(self):
        return self.db_id
