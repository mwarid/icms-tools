export function uniqueBy(array, key){
    var index = [];
    return array.filter(function (item) {
        var k = key(item);
        return index.indexOf(k) >= 0 ? false : index.push(k);
    });
}

export function sortAlphabeticallyBy(array, key){
  return [...array].sort((a, b) => {
      const aKey = key(a);
      const bKey = key(b);
      if(aKey < bKey) { return -1; }
      if(aKey > bKey) { return 1; }
      return 0;
  })
}

export function debounce(fn, delay){
    let timeoutID = null
    return function () {
      clearTimeout(timeoutID)
      let args = arguments
      let that = this
      timeoutID = setTimeout(function () {
        fn.apply(that, args)
      }, delay)
    }
  }

export class Debouncer {
  /**
   * 
   * @param {Function} callback 
   * @param {Number} timeout a negative value will defer countdown's onset until after reset is called
   */
  constructor(callback, timeout=100) {
    this.callback = callback;
    this.timeoutMs = timeout;
    this.timeoutId = null;
    if (timeout > 0){
      this.reset();
    }
  }
  reset(newTimeout=undefined){
    if (Number(newTimeout)){
      this.timeoutMs = Number(newTimeout);
    }
    if (this.timeoutId !== null){
      clearTimeout(this.timeoutId);
    }
    this.timeoutId = setTimeout(this.callback, this.timeoutMs);
  }
}