/* eslint-disable no-undef */
import store from '../store.js';

export default{

    defaultFailHandler(request, textStatus, errorThrown){
        var message = 'Problem contacting server: «' + textStatus + '»'
        if (typeof(request.responseJSON) != 'undefined'){
            message = 'AJAX error ' + request.status + ': ' + request.responseJSON.message;
        }
        store.commit('errorMessage', message);
        console.error(message);
    },

    defaultSuccessHandler(data, testStatus, request){
        console.debug('Data fetched well!');
        console.debug(data);
    },

    trimJson(data){
        for(var key in data){
            if (data[key] === null){
                delete data[key];
            }
        }
        return data;
    },

    expandParametersIntoUrl(endpoint, params){
        endpoint += (endpoint.endsWith('?') ? '' : '?');
        Object.entries(params).forEach(([key, value]) => {
            if (!endpoint.endsWith('?') && !endpoint.endsWith('&')){
                endpoint += '&';
            }
            if (Array.isArray(value)){
                value.forEach((e, idx, arr) => {
                    endpoint += key + '=' + e + (idx < arr.length -1 ? '&' : '');
                });
            } else {
                endpoint += key + '=' + value;
            }
        });
        return endpoint;
    },

    getJson(url, sentData={}, successCallback=this.defaultSuccessHandler, dataType='json', failureCallback=this.defaultFailHandler, headers={}){
        return $.ajax({
            url: url,
            type: 'GET',
            data: sentData,
            dataType: dataType,
            global: false,
        }).done(successCallback).fail(failureCallback);
    },

    post(url, data, successCallback=function(){}, failureCallback=function(){}, dataType='json', trimNullValues=true){
        // delete null values from post data prior to submission
        if (trimNullValues === true){
            this.trimJson(data);
        }
        console.debug('About to post some date of type ' + dataType);
        var jqxhr = $.post(url, data, function(){console.debug('POST request success');}, dataType).
        error(failureCallback).success(successCallback).
        complete(function(data, status){
            console.debug('generic ajax complete callback with status: ' + status);
        });
    },

    delete(url, data, successCallback=this.defaultSuccessHandler, failureCallback=this.defaultFailHandler){
        $.ajax({
            // headers: {'Content-Type': 'application/json'},
            type: 'DELETE',
            url: url,
            data: data,
            global: false,
        }).done(successCallback).fail(failureCallback);
    },

    postJson(url, data, successCallback=this.defaultSuccessHandler, failureCallback=this.defaultFailHandler, trimNullValues=true){
        if (trimNullValues){
            this.trimJson(data);
        }
        $.ajax({
            headers: {'Content-Type': 'application/json'},
            type: 'POST',
            url: url,
            data: JSON.stringify(data),
            dataType: 'json',
            global: false
        }
        ).done(successCallback).fail(failureCallback);
    },

    getUserInfo(cmsId, successCallback){
        if (cmsId != null){
            this.getJson($SCRIPT_ROOT + '/restplus/icms_public/user_info', {'cms_id': cmsId}, successCallback);
        }else{
            this.getJson($SCRIPT_ROOT + '/restplus/icms_public/user_info', {}, successCallback);
        }
    },

    getCareerEvents(cmsId, eventType, successCallback, failureCallback = this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/members/career_events', {cms_id: cmsId, event_type: eventType}, successCallback, 'json', failureCallback);
    },

    postCareerEvent(id, cmsId, date, eventType, successCallback, failureCallback = this.defaultFailHandler){
        this.postJson($SCRIPT_ROOT + '/restplus/members/career_event', this.trimJson({
            id: id, cms_id: cmsId, date: date, event_type: eventType
        }), successCallback, failureCallback);
    },

    getInstInfo(instCode, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/members/institute', {code: instCode}, successCallback, 'json', failureCallback);
    },

    getCurrentUserInfo(successCallback){
        this.getJson($SCRIPT_ROOT + '/restplus/icms_public/user_info', {}, successCallback);
    },

    getInstMembersData(instCode, successCallback){
        this.getJson($SCRIPT_ROOT + '/restplus/members/people', {'inst_code': instCode, 'ind_status': 'CMS'}, successCallback);
    },

    getAuthorListMentions(cmsId, successCallback){
        this.getJson($SCRIPT_ROOT + '/restplus/authorlists/al_mentions', {'cms_id': cmsId}, successCallback);
    },

    getAuthorListsInfo(code, status, successCallback){
        var params = {};
        if (code != null) params['code'] = code;
        if (status != null) params['al_status'] = status;
        this.getJson($SCRIPT_ROOT + '/restplus/authorlists/al_info', params, successCallback);
    },

    getPhdMoList(mode, year, successCallback){
        this.getJson($SCRIPT_ROOT + '/restplus/mo/people_list', {'year': year, 'mode': mode}, successCallback);
    },

    getInstCodes(purpose="list", successCallback){
        switch (purpose){
            case "manage-mo":
                this.getJson($SCRIPT_ROOT + '/restplus/mo/managedInsts', {}, successCallback);
                break;
            case "list-all":
                this.getJson($SCRIPT_ROOT + '/restplus/members/institutes', {}, successCallback);
                break;
            case "list-members":
            default:
                this.getJson($SCRIPT_ROOT + '/restplus/members/institutes', {'member_status': 'Yes'}, successCallback);
        }

    },

    getPeopleSuggestions(searchTerm, successCallback){
        this.getJson($SCRIPT_ROOT + '/restplus/icms_public/autocomplete_people/' + searchTerm, {}, successCallback);
    },

    getInstSuggestions(searchTerm, successCallback){
        this.getJson($SCRIPT_ROOT + '/restplus/icms_public/autocomplete_institutes/' + searchTerm, {}, successCallback);
    },

    getTimeLines(cms_id, year, instCode=null, successCallback=function(){}){
        this.getJson($SCRIPT_ROOT + '/restplus/epr/timeline', {'year': year, 'inst_code': instCode, 'cms_id': cms_id}, successCallback);
    },

    postEprCorrection(cms_id, year, correction, instCode=null, type='author', reason=null, remarks=null, successCallback=function(){}, failureCallback=function(){}){
        var endpoint = (type == 'author' ? '/restplus/epr/waive_author_due' : '/restplus/epr/rectify_app_due');
        this.post($SCRIPT_ROOT + endpoint, {cms_id: cms_id, year: year, correction: correction, inst_code: instCode, remarks: remarks, reason: reason}, successCallback, failureCallback, 'json');
    },

    getEprCorrections(successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/epr/due_corrections', {}, successCallback);
    },

    getOrgUnits(type=null, domain=null, active=true, successCallback=this.defaultSuccessHandler, minLevel=null, maxLevel=null){
        var data = this.trimJson({domain: domain, type: type, is_active: active, min_unit_level: minLevel, max_unit_level: maxLevel});
        this.getJson($SCRIPT_ROOT + '/restplus/org_chart/units', data, successCallback);
    },

    getOrgUnit(id=null, successCallback=this.defaultSuccessHandler, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/org_chart/unit', {id: id}, successCallback);
    },

    postOrgUnit(domain, type, isActive=true, superiorUnitId=null, enclosingUnitId=null, outermostUnitId=null, id=null, successCallback=this.defaultSuccessHandler, failureCallback=this.defaultFailHandler, trimNullValues=false){
        // we might not want to trim the null values if null is supposed to overwrite something on the server side
        var data = {
            id: id, domain: domain, type: type, is_active: isActive, superior_unit_id: superiorUnitId, eclosing_unit_id: enclosingUnitId, outermost_unit_id: outermostUnitId
        };
        console.debug('About to submit unit data: ' + JSON.stringify(data));
        // BUT: we need to trim null values if the id is null - just to be consistent with the approach used so far
        this.postJson($SCRIPT_ROOT + '/restplus/org_chart/units', data, successCallback, failureCallback, trimNullValues || id == null);
    },

    getOrgUnitTypes(id, name, level, successCallback=this.defaultFailHandler, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/org_chart/unit_types', this.trimJson({id: id, name:name, level: level}), successCallback, 'json', failureCallback);
    },

    getPositions(fnGood=this.defaultSuccessHandler, fnFail=this.defaultFailHandler, ids=[], names=[], unit_types=[], unit_ids=[]){
        var url = $SCRIPT_ROOT + '/restplus/org_chart/positions?';
        ids.forEach(function(val){url += 'id=' + val + '&'});
        names.forEach(function(val){url += 'name=' + val + '&'});
        unit_types.forEach(function(val){url += 'unit_type=' + val + '&'});
        unit_ids.forEach(function(val){url += 'unit_id=' + val + '&'});
        this.getJson(url, {}, fnGood);
    },

    postPosition(id, name, unitTypeId, level, isActive, successCallback, failureCallback=this.defaultFailHandler){
        this.postJson($SCRIPT_ROOT + '/restplus/org_chart/positions', {
            id: id, name: name, unit_type_id: unitTypeId, level: level, is_active: isActive
        }, successCallback, failureCallback, true);
    },

    getTenures(successCallback=function(){}, cmsIds=[], asOfDate=null, unit_types=[], domains=[], unit_ids=[]){
        var data = {exclude_past: false, as_of: asOfDate};
        this.trimJson(data);
        var url = $SCRIPT_ROOT + '/restplus/org_chart/tenures?';
        cmsIds.forEach(function(val){url += 'cms_id=' + val + '&'});
        unit_types.forEach(function(val){url += 'unit_type=' + val + '&'});
        domains.forEach(function(val){url += 'domain=' + val + '&'});
        unit_ids.forEach(function(val){url += 'unit_id=' + val + '&'});
        this.getJson(url, data, successCallback);
    },

    getTenure(id, successCallback=this.defaultSuccessHandler, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/org_chart/tenure?id='+id, {}, successCallback, 'json', failureCallback);
    },

    postTenure(successCallback=this.defaultSuccessHandler, failureCallback=this.defaultFailHandler,
                id=null, cmsId=null, unitId=null, positionId=null, startDate=null, endDate=null, designate=false)
    {
        var postData = {
            "id": id,
            "cms_id": cmsId,
            "position_id": positionId,
            "unit_id": unitId,
            "start_date": startDate,
            "end_date": endDate
        };
        var endpoint = $SCRIPT_ROOT + '/restplus/org_chart/' + (designate === true ? 'designations' : 'tenures');
        console.debug('Endpoint for POSTing: ' + endpoint);
        this.postJson(endpoint, postData, successCallback, failureCallback);
    },

    deleteTenure(id, successCallback, failureCallback=this.defaultFailHandler){
        var endpoint = $SCRIPT_ROOT + '/restplus/org_chart/' + 'tenures';
        this.delete(endpoint, {id: id}, successCallback, failureCallback);
    },

    postDesignation(successCallback=this.defaultSuccessHandler, failureCallback=this.defaultFailHandler, cmsId=null, unitId=null, positionId=null, startDate=null, endDate=null){
        startDate = startDate != null ? startDate : new Date().toISOString().substring(0, 10);
        this.postTenure(successCallback, failureCallback, null, cmsId, unitId, positionId, startDate, endDate, true);
    },

    deleteDesignation(id, fnGood=this.defaultSuccessHandler, fnFail=this.defaultSuccessHandler){
        this.delete($SCRIPT_ROOT + '/restplus/org_chart/designations', {id: id}, fnGood, fnFail);
    },

    getOrgChartPositions(successCallback=function(){}, failureCallback=function(){}, unitId=null){
        var data = {};
        console.debug('About to fetch positions for unit_id ' + unitId);
        if (unitId != null){
            data['unit_id'] = unitId;
        }
        this.getJson($SCRIPT_ROOT + '/restplus/org_chart/positions', data, successCallback);
    },

    getOrgChartUnits(successCallback=function(){}, failureCallback=function(){}){
        this.getJson($SCRIPT_ROOT + '/restplus/org_chart/units', {}, successCallback);
    },

    getPeopleData(successCallback, statuses=['CMS'], projects=[], authorFlags=[true]){
        var data = {};
        var url = $SCRIPT_ROOT + '/restplus/members/people?';
        (statuses || []).forEach(e => {url += ('ind_status=' + e + '&')});
        (projects || []).forEach(e => {url += ('project=' + e + '&')});
        (authorFlags || []).forEach(e => {url += ('is_author=' + e + '&')});
        this.getJson(url, {}, successCallback);
    },

    getKnownProjects(successCallback){
        successCallback(['','BRIL','CORE','DAQ','DI','EC','GEN','HC','HGCAL','HGCAL (CE)','L1TRG','MTD','MUCSC','MUDT','MUGEM','Muon','MURPC','OFF','OFFCOMP','PPD','PPS','RUN','TECH','TK','TRGCOORD','UPGRADE']);
    },

    getKnownPersonStatuses(successCallback){
        successCallback(['CMS', 'EXMEMBER', 'NOSHOW', 'NOTCMS', 'NOTNAME', 'RELATED', 'CMSEXTENDED', 'CMSEMERITUS', 'CMSVO', 'CMSASSOC', 'DECEASED', 'CMSAFFILIATE']);
    },

    getActiveAnnouncements(successCallback){
        this.getJson($SCRIPT_ROOT + '/restplus/icms_public/announcements', {'active_only': true}, successCallback);
    },

    getAppAssetData(assetName, successCallback){
        this.getJson($SCRIPT_ROOT + '/restplus/diagnostics/app_asset', {'asset_name': assetName}, successCallback);
    },

    getDeploymentInfo(successCallback){
        this.getJson($SCRIPT_ROOT + '/restplus/diagnostics/deployment_info', {}, successCallback);
    },

    getInstitutes(codes=[], statuses=[], country_codes=[], successCallback=this.defaultSuccessHandler, failureCallback=this.defaultSuccessHandler){
        var url = $SCRIPT_ROOT + '/restplus/members/institutes?';
        codes.forEach(function(code){url += 'code=' + code + '&'});
        statuses.forEach(function(status){url += 'status=' + status + '&'});
        this.getJson(url, {}, successCallback);
    },

    getMarkdownList(id, name, goodCbk, failCbk=this.defaultFailHandler){
        var data = this.trimJson({id: id, name: name});
        this.getJson($SCRIPT_ROOT + '/restplus/general/markdown_list', data, goodCbk, 'json', failCbk);
    },

    getMarkdown(id, name, goodCbk, failCbk=this.defaultFailHandler){
        var data = this.trimJson({id: id, name: name});
        this.getJson($SCRIPT_ROOT + '/restplus/general/markdown', data, goodCbk, 'json', failCbk);
    },

    postMarkdown(id, name, body, goodCbk, failCbk=this.defaultFailHandler){
        var data = this.trimJson({id: id, name: name, body: body});
        console.debug('posting a note with name: ' + name + ' and id ' + id + ' and maybe some body too.');
        console.debug(data);
        this.postJson($SCRIPT_ROOT + '/restplus/general/markdown', data, goodCbk, failCbk);
    },

    getImagesList(id, name, goodCbk, failCbk=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/general/images', this.trimJson({id: id, name: name}), goodCbk, 'json', failCbk);
    },

    getEprAggregates(successCallback, cmsId, instCode, projectCode, year, aggUnits=['pledges done', 'shifts done'], groupBy=['year', 'cms_id']){
        var params = this.trimJson({year: year, cms_id: cmsId, inst_code: instCode, project_code: projectCode, agregate_unit: aggUnits, grouping_columns: groupBy});
        var endpoint = this.expandParametersIntoUrl($SCRIPT_ROOT + '/restplus/statistics/epr_aggregates?', params);
        this.getJson(endpoint, null, successCallback);
    },

    getEprUserTimeLines(successCallback, failureCallback=this.defaultFailHandler, cmsId, year){
        this.getJson($SCRIPT_ROOT + '/restplus/epr/timeline', this.trimJson({cms_id: cmsId, year: year}), successCallback, 'json', failureCallback);
    },

    getUserLdapInfo(successCallback, hrId){
        this.getJson($SCRIPT_ROOT + '/restplus/members/ldap_person_info', {hr_id: hrId}, successCallback);
    },

    getUsersLiteLdapInfo(successCallback, failureCallback=this.defaultFailHandler, gid=1399){
        this.getJson($SCRIPT_ROOT + '/restplus/members/ldap_people_lite_info', {gid: gid}, successCallback, 'json', failureCallback);
    },

    getHistoryLines(cmsId, successCallback=this.defaultSuccessHandler, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/janitorial/text_history/' + cmsId, {}, successCallback, 'json', failureCallback);
    },

    getParsedHistoryLines(cmsId, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/icms_public/parsed_history', {cms_id: cmsId}, successCallback, 'json', failureCallback);
    },

    getRestrictedPersonInfo(cmsId, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/members/person_restricted_info', {cms_id: cmsId}, successCallback, 'json', failureCallback);
    },

    getPersonMoInfo(cmsId, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/mo/person_mo', {cms_id: cmsId}, successCallback, 'json', failureCallback);
    },

    getAuthorStats(cmsId, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/authorship/author_stats', {cms_id: cmsId}, successCallback, 'json', failureCallback);
    },

    getOldDbPersonInfo(cmsId, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/members/person', {cms_id: cmsId}, successCallback, 'json', failureCallback);
    },

    getAccountStatusCheck(hrId, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/janitorial/account_status', {hr_id: hrId, action: 'check'}, successCallback, 'json', failureCallback);
    },

    getEprInstStats(code, year, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/epr/inst/stats', this.trimJson({code: code, year: year}), successCallback, 'json', failureCallback);
    },

    getEprInstTimeLines(code, year, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/epr/inst/timelines', this.trimJson({code: code, year: year}), successCallback, 'json', failureCallback);
    },

    getMoList(year, faId, instCode, cmsId, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/mo/list', this.trimJson({year: year, fa_id: faId, inst_code: instCode, cms_id: cmsId}), successCallback, 'json', failureCallback);
    },

    getMoSteps(cmsId, year, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/mo/steps', {cms_id: cmsId, year: year}, successCallback, 'json', failureCallback);
    },

    postMoStep(cmsId, year, status, successCallback, failureCallback=this.defaultFailHandler){
        this.postJson($SCRIPT_ROOT + '/restplus/mo/steps', {cms_id: cmsId, year: year, status: status}, successCallback, failureCallback);
    },

    getNoteProcesses(statuses=['Sec_Completed' , 'Submitted', 'SubEditor_Accepted' , 'Editor_Accepted', 'SubEditor_Completed', 'CoRefEditor_Accepted', 'RefEditor_Accepted'],
        successCbk=this.defaultSuccessHandler, failureCbk=this.defaultFailHandler){
        var url = $SCRIPT_ROOT + '/restplus/old_notes/processes?';
        statuses.forEach((e, idx, arr) => url += 'status=' + e + (idx < arr.length - 1 ? '&' : ''));
        this.getJson(url, {}, successCbk, 'json', failureCbk);
    },

    getNoteProcessSteps(processId, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/old_notes/process_steps', {process: processId}, successCallback, 'json', failureCallback);
    },

    getExOfficioMandates(successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/org_chart/ex_officio_mandates', {}, successCallback, 'json', failureCallback);
    },

    getExOfficioMandate(id, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/org_chart/ex_officio_mandate', {id: id}, successCallback, 'json', failureCallback);
    },

    postExOfficioMandate(id, srcUnitId, dstUnitId, srcLevel, dstLevel, srcPosition, dstPosition, startDate, endDate, successCallback, failureCallback=this.defaultFailHandler){
        var data = {
            id: id,
            start_date: startDate,
            end_date: endDate,
            src_unit_id: srcUnitId,
            dst_unit_id: dstUnitId,
            src_position_level: srcLevel,
            dst_position_level: dstLevel,
            src_position_name: srcPosition,
            dst_position_name: dstPosition,
        };
        this.postJson($SCRIPT_ROOT + '/restplus/org_chart/ex_officio_mandates', data, successCallback, failureCallback, id==null);
    },

    getVotingParticipants(code, nearlyVoting, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/voting/participants', {code: code, include_nearly_voting: nearlyVoting}, successCallback, 'json', failureCallback);
    },

    getVotingEventsList(successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/voting/events_list', {}, successCallback, 'json', failureCallback);
    },

    getVotingEvent(id, code, successCallback, failureCallback=this.defaultFailHandler){
        this.getJson($SCRIPT_ROOT + '/restplus/voting/event', this.trimJson({id: id, code: code}), successCallback, 'json', failureCallback);
    },

    postVotingEvent(id, code, type, title, closingDate, startTime, endTime, delegationDeadline, moYear, successCallback, failureCallback=this.defaultFailHandler){
        var data = {
            id: id, code: code, type: type, title: title, list_closting_date: closingDate, start_time: startTime, end_time: endTime, delegation_deadline: delegationDeadline, applicable_mo_year: moYear
        };
        this.postJson($SCRIPT_ROOT + '/restplus/voting/event', data, successCallback, failureCallback, id==null);
    },

    getMembersCount(groupBy=['nationality'], cmsStatuses=['CMS'], authorFlags=[], successCallback=this.defaultSuccessHandler, failureCallback=this.defaultFailHandler){
        var url = $SCRIPT_ROOT + '/restplus/statistics/members_count?';
        authorFlags.forEach((e) => url += 'is_author=' + e + '&');
        cmsStatuses.forEach((e) => url += 'status=' + e + '&');
        groupBy.forEach((e) => url += 'grouping_column=' + e + '&');
        url = url.substring(0, url.length - 1);
        this.getJson(url, {}, successCallback, 'json', failureCallback);
    }
};

