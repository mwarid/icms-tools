import AjaxCalls from "./AjaxCalls";
import { IcmsPiggybackHalCall, IcmsPiggybackCall } from './PiggybackCalls'


export class CadiLinesCall extends IcmsPiggybackCall {
    constructor() {
        super("/cadi/analyses");
    }
    setAwg(value) { return this.set('awg', value); }
    setYear(value) { return this.set('year', value); }
}

export class ArcsCall extends IcmsPiggybackCall {
    constructor() {
        super("/cadi/arcs");
    }
}

export class AwgsCall extends IcmsPiggybackCall {
    constructor() {
        super("/cadi/awgs");
    }
}