import Vue from 'vue'
import VueFlashMessage from 'vue-flash-message'
Vue.use(VueFlashMessage);

import router from './router'
import App from './App.vue'
import store from './store'
// import Element from 'element-ui'
// Vue.use(Element)
import Vuetify from 'vuetify'

Vue.use(Vuetify)

new Vue({
    router: router,
    vuetify: new Vuetify(),
    store: store,
    el: '#vueApp',
    render: h => h(App)
})