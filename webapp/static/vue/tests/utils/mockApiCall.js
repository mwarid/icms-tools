export default (AjaxCall, {requestParams = {}, response = null}) => {
    if(!response) response = requestParams;
    
    const fields = Object.keys(requestParams);
    const methodNames = fields.map(field => snakeToCamel(`set_${field}`));
    const mocks = methodNames.reduce((acc, methodName) => 
        Object.assign({}, acc, { 
            [methodName] : jest.fn().mockImplementation(function(){
                return this;
            })
        })
    , {});
    
    const initialize = () => {
        AjaxCall.mockClear();
        Object.values(mocks).forEach(mock => mock.mockClear());

        AjaxCall.mockImplementation(() => Object.assign({}, mocks, {
            setSuccessCallback(callback){
                this.callback = callback
                return this;
            },
            get(){ this.callback(response); },
            put(){ this.callback(response); },
            post(){ this.callback(response); },
            delete(){ this.callback(response); },
        }));
    };

    const assertionMap = Object.values(mocks).map((mock, index) => ({
        mock, 
        value: requestParams[fields[index]]
    }));

    return {
        initialize,
        assertionMap
    }
}

const snakeToCamel = (str) => str.replace(
    /([-_][a-z])/g,
    (group) => group.toUpperCase()
                    .replace('-', '')
                    .replace('_', '')
);