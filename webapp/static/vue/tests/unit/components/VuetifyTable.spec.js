import { mount } from '../../utils/testCreators';
import VuetifyTable from '../../../src/components/VuetifyTable.vue'
import { sortAlphabeticallyBy } from '../../../src/utils/helpers';

describe('VuetifyTable.vue', () => {
    const headers = [
        { text: 'Name', value: 'name', sortable: true },
        { text: 'Category', value: 'category', sortable: true },
        { text: 'Color', value: 'color', sortable: true },
        { text: 'Taste', value: 'taste', sortable: true },
    ]
    const items = [
        { name: 'Apple', category: 'Fruit', color: 'Red', taste: 3},
        { name: 'Lettuce', category: 'Vegetable', color: 'Green', taste: 1},
        { name: 'Orange', category: 'Fruit', color: 'Orange', taste: 4},
        { name: 'Banana', category: 'Fruit', color: 'Yellow', taste: 3},
        { name: 'Milk', category: 'Dairy', color: 'White', taste: 2},
        { name: 'Carrot', category: 'Vegetable', color: 'Orange', taste: 3},
    ]
    //Items get sorted by the first column by default
    const sortedItems = sortAlphabeticallyBy(items, item => item.name);

    it('Displays the items', () => {
        const wrapper = mount(VuetifyTable, {
            propsData: { headers, items }
        });

        const columns = wrapper.findAll('tr td');
        
        sortedItems.forEach((item, rowIndex) => {
            headers.forEach(({value}, colIndex) => {
                const col = columns.at(rowIndex * 4 + colIndex).text();
                expect(col).toEqual(String(item[value]))
            });
        });
    });

    it('Filters items by one column', async () => {
        const wrapper = mount(VuetifyTable, {
            propsData: { headers, items }
        });

        wrapper.setData({ searchMap : {
            category: 'Fruit'
        }});
        await wrapper.vm.$nextTick();
        const columns = wrapper.findAll('tr td');
    
        sortedItems.filter(item => item.category === 'Fruit' )
        .forEach((item, rowIndex) => {
            headers.forEach(({value}, colIndex) => {
                const col = columns.at(rowIndex * 4 + colIndex).text();
                expect(col).toEqual(String(item[value]))
            });
        });
    
    });

    it('Filters items by multiple columns', async () => {
        const wrapper = mount(VuetifyTable, {
            propsData: { headers, items }
        });
        wrapper.setData({ searchMap : {
            category: 'Vegetable',
            taste: '3'
        }})
        await wrapper.vm.$nextTick();
        const columns = wrapper.findAll('tr td');
    
        sortedItems
        .filter(item => item.category === 'Vegetable')
        .filter(item => item.taste === 3)
        .forEach((item, rowIndex) => {
            headers.forEach(({value}, colIndex) => {
                const col = columns.at(rowIndex * 4 + colIndex).text();
                expect(col).toEqual(String(item[value]))
            });
        });
    });
});