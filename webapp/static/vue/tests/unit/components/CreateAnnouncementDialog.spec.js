import mockApiCall from '../../utils/mockApiCall';
import { mount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import CreateAnnouncementDialog from '../../../src/components/CreateAnnouncementDialog.vue'

describe('CreateAnnouncementDialog.vue',() => {
    
    it('Initializes some fields on load', () => {
        const wrapper = mount(CreateAnnouncementDialog,{
            propsData: {
                value: true
            }
        });
        
        expect(wrapper.vm.type).toEqual("INFO");
        expect(wrapper.vm.startDatetime).toContain(new Date().toISOString().split('T')[0]);
        expect(wrapper.vm.application).toEqual('epr');
        
        expect(wrapper).toMatchSnapshot()
    });

    const newAnnouncement = {
        application: "common",
        content: 'This is new',
        end_datetime: "2018-03-22 08:00:00", 
        start_datetime: "2018-03-20 17:09:35",
        subject: "Service Disruption",
        type: "WARNING",
    }; 
    const { initialize, assertionMap } = mockApiCall(AjaxCalls.AnnouncementCall, { requestParams : newAnnouncement });

    it("Doesn't allow submitting when errors exist", async () => {
        initialize();
        const wrapper = mount(CreateAnnouncementDialog,{
            propsData: {
                value: true
            }
        });
        
        //Click Submit
        wrapper.findAllComponents({name: 'v-btn'}).at(1).trigger('click');
        await wrapper.vm.$nextTick()
        //Create announcement was never called
        expect(AjaxCalls.AnnouncementCall).toHaveBeenCalledTimes(0);
        //Dialog is still open
        expect(wrapper.emitted().input).toBeFalsy();
        
    });

    it('Closes when the X button is clicked', async () => {
        const wrapper = mount(CreateAnnouncementDialog,{
            propsData: {
                value: true
            }
        });

        //Click close
        await wrapper.findAllComponents({name: 'v-btn'}).at(0).trigger('click');
        expect(wrapper.emitted().input[0]).toEqual([false]);
    })

    it("Creates an announcement when the input is valid",async () => {
        initialize();
        const wrapper = mount(CreateAnnouncementDialog,{
            propsData: {
                value: true
            }
        });

        wrapper.setData({
            type: newAnnouncement.type,
            subject: newAnnouncement.subject,
            content: newAnnouncement.content,
            startDatetime: newAnnouncement.start_datetime,
            endDatetime: newAnnouncement.end_datetime,
            application: newAnnouncement.application
        });
        await wrapper.vm.$nextTick();
        //Click Submit button
        await wrapper.findAllComponents({name: 'v-btn'}).at(1).trigger('click');
        assertionMap.forEach(({mock, value}) => {
            expect(mock).toHaveBeenCalledWith(value);
        });
        expect(wrapper.emitted().created[0]).toEqual([newAnnouncement]);
        expect(wrapper.emitted().input[0]).toEqual([false]);
    });

});