import { shallowMount } from '../../utils/testCreators';
import Timeline from '../../../src/components/scheduler/Timeline.vue'
import moment from 'moment';

describe('Timeline.vue', () => {
    const propsData = {
        bookings: [
            {
                title: "booking_1",
                from : '2020-01-01 08:00:00',
                to : '2020-01-01 12:00:00',
                type: 'type_1',
                clickable: true,
            },
            {
                title: "booking_2",
                from : '2020-01-01 12:00:00',
                to : '2020-01-01 14:00:00',
                type: 'type_2',
                clickable: false,
            },
            {
                title: "booking_3",
                from : '2020-01-01 09:00:00',
                to : '2020-01-01 14:00:00',
                type: 'type_1',
                clickable: false,
            },
            {
                title: "booking_4",
                from : '2020-01-01 11:00:00',
                to : '2020-01-01 21:00:00',
                type: 'type_2',
                clickable: true,
            },
        ],
        types: [
            { name: 'type_1', color: 'green' }, 
            { name: 'type_2', color: 'red' }
        ],
        dayStartTime: '08:00:00',
        dayEndTime: '16:00:00',
        showGridLines: false
    }

    it('Assigns overlapping bookings to different rows', () => {
        const wrapper = shallowMount(Timeline, { propsData });

        const rows = wrapper.findAll('.timeline-row');
        expect(rows.length).toBe(3);

        const firstRowBookings = rows.at(0).findAll('.booking-title');
        expect(firstRowBookings.length).toBe(2);
        expect(firstRowBookings.at(0).text()).toEqual('booking_1');
        expect(firstRowBookings.at(1).text()).toEqual('booking_2');

        const secondRowBookings = rows.at(1).findAll('.booking-title');
        expect(secondRowBookings.length).toBe(1);
        expect(secondRowBookings.at(0).text()).toEqual('booking_3');

        const thirdRowBookings = rows.at(2).findAll('.booking-title');
        expect(thirdRowBookings.length).toBe(1);
        expect(thirdRowBookings.at(0).text()).toEqual('booking_4');
    });

    it('Trims the time range of bookings to match the assigned starting and ending time', () => {
        const wrapper = shallowMount(Timeline, { propsData });
        
        const trimmedDuration = moment.duration(moment('2020-01-01 16:00:00').diff(moment('2020-01-01 11:00:00'))).asMinutes()
        expect(wrapper.findAll('.timeline-row').at(2).find('.booking').element.style.width).toEqual(
            wrapper.vm.durationToWidth(trimmedDuration) + '%'
        );
    });

    it('Allows clicking only on clickable bookings', async () => {
        const wrapper = shallowMount(Timeline, { propsData });

        const bookings = wrapper.find('.timeline-row').findAll('.booking');
        // booking_1 is clickable
        await bookings.at(0).trigger('click');
        expect(wrapper.emitted().bookingselect[0]).toMatchObject([{ booking: { title: 'booking_1'}}]);
        // booking_2 is not clickable
        await bookings.at(1).trigger('click');
        expect(wrapper.emitted().bookingselect).toHaveLength(1);

    });

    it('Set the correct color for each booking type', () => {
        const wrapper = shallowMount(Timeline, { propsData });
        const bookings = wrapper.find('.timeline-row').findAll('.booking');
        // booking_1 is type_1 => green
        expect(bookings.at(0).element.style.background).toEqual('green');
        // booking_2 is type_2 => red
        expect(bookings.at(1).element.style.background).toEqual('red');
    });

    it('Renders bookings correctly', () => {
        const wrapper = shallowMount(Timeline, { propsData });
        expect(wrapper).toMatchSnapshot();
    });
})