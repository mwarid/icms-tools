import { shallowMount } from '../../utils/testCreators';
import DayPicker from '../../../src/components/scheduler/DayPicker.vue'
import moment from 'moment';

describe('DayPicker.vue',() => {
    it('Formats the selected day', () => {
        const wrapper = shallowMount(DayPicker, { propsData : {
            days: ['2020-01-01', '2020-01-02', '2020-01-03'],
            value: '2020-01-01'
        }});

        expect(wrapper.find('.day').text()).toEqual('Wed 01 January 2020');
    });

    it('Select the next day on button click', async () => {
        const wrapper = shallowMount(DayPicker, { propsData : {
            days: ['2020-01-01', '2020-01-02', '2020-01-03'],
            value: '2020-01-01'
        }})

        await wrapper.findAllComponents({name: 'v-btn'}).at(1).vm.$emit('click');
        expect(wrapper.emitted().input[0]).toEqual(['2020-01-02']);
    });

    it('Select the prev day on button click', async () => {
        const wrapper = shallowMount(DayPicker, { propsData : {
            days: ['2020-01-01', '2020-01-02', '2020-01-03'],
            value: '2020-01-02'
        }})

        await wrapper.findAllComponents({name: 'v-btn'}).at(0).vm.$emit('click');
        expect(wrapper.emitted().input[0]).toEqual(['2020-01-01']);
    });

    it("Next button is unavailable if the last day is already selected", async () => {
        const wrapper = shallowMount(DayPicker, { propsData : {
            days: ['2020-01-01', '2020-01-02', '2020-01-03'],
            value: '2020-01-03'
        }})

        await wrapper.findAllComponents({name: 'v-btn'}).at(1).vm.$emit('click');
        expect(wrapper.emitted().input).toBeFalsy();
    });
    
    it("Prev button is unavailable if the first day is already selected", async () => {
        const wrapper = shallowMount(DayPicker, { propsData : {
            days: ['2020-01-01', '2020-01-02', '2020-01-03'],
            value: '2020-01-01'
        }})

        await wrapper.findAllComponents({name: 'v-btn'}).at(0).vm.$emit('click');
        expect(wrapper.emitted().input).toBeFalsy();
    });
});