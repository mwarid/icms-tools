import mockApiCall from '../../utils/mockApiCall';
import { mount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import WeeksFormDialog from '../../../src/components/WeeksFormDialog.vue'

describe('WeeksFormDialog.vue', () => {
    it('Shows create week form if no id is provided', async () => {
        const wrapper = mount(WeeksFormDialog, {
            propsData: {
                value: true,
                item: null
            }
        });
        await wrapper.vm.$nextTick();
        
        expect(wrapper.find('.v-card__title').text()).toEqual('Create CMS Week');
    });

    it('Shows edit week form if no id is provided', async () => {
        const wrapper = mount(WeeksFormDialog, {
            propsData: {
                value: true,
                item: { id : 1 }
            }
        });
        await wrapper.vm.$nextTick();
        
        expect(wrapper.find('.v-card__title').text()).toEqual('Edit CMS Week');
    });

    it('Closes when the close button is clicked', async () => {
        const wrapper = mount(WeeksFormDialog, {
            propsData: {
                value: true,
                item: null
            }
        });
        await wrapper.vm.$nextTick();

        //Click close
        await wrapper.findAllComponents({name: 'v-btn'}).at(0).trigger('click');
        expect(wrapper.emitted().input[0]).toEqual([false]);
    });

    it('Validates that starting time is before ending time', async () => {
        const createWeekCall = mockApiCall(AjaxCalls.BookingWeeksCall, {});
        createWeekCall.initialize();

        const wrapper = mount(WeeksFormDialog,{
            propsData: {
                value: true
            }
        });  
        await wrapper.vm.$nextTick();

        wrapper.setData({
            formState: {
                title: 'Week',
                date: '2020-01-07',
                date_end: '2020-01-01'
            }
        });
        await wrapper.vm.$nextTick();

        await wrapper.findAllComponents({name: 'v-btn'}).at(1).trigger('click');
        
        expect(wrapper.find('.v-messages__message').text()).toEqual('The starting date must be before the ending end.');
    });

    it('Makes an api call to create a week on submit', async () => {
        const createBookingCall = mockApiCall(AjaxCalls.BookingWeeksCall, {
            requestParams: {
                title: 'Week',
                date: '2020-01-01',
                date_end: '2020-01-07',
                is_external: false
            }
        });
        createBookingCall.initialize();

        const wrapper = mount(WeeksFormDialog,{
            propsData: {
                value: true,
                item: null
            }
        });  
        await wrapper.vm.$nextTick();

        wrapper.setData({
            formState: {
                title: 'Week',
                date: '2020-01-01',
                date_end: '2020-01-07',
                is_external: true
            }
        });
        await wrapper.vm.$nextTick();

        await wrapper.findAllComponents({name: 'v-btn'}).at(1).trigger('click');
        
        expect(wrapper.emitted().itemcreated[0]).toBeTruthy();
        expect(wrapper.emitted().input[0]).toEqual([false]);
    });

    it('Makes an api call to edit a week on submit', async () => {
        const createBookingCall = mockApiCall(AjaxCalls.BookingWeeksCall, {
            requestParams: {
                id: 1,
                title: 'Week',
                date: '2020-01-01',
                date_end: '2020-01-07',
                is_external: false
            }
        });
        createBookingCall.initialize();

        const wrapper = mount(WeeksFormDialog,{
            propsData: {
                value: true,
                item: { id: 1 }
            }
        });  
        await wrapper.vm.$nextTick();

        wrapper.setData({
            formState: {
                title: 'Week',
                date: '2020-01-01',
                date_end: '2020-01-07',
                is_external: true
            }
        });
        await wrapper.vm.$nextTick();

        await wrapper.findAllComponents({name: 'v-btn'}).at(1).trigger('click');
        
        expect(wrapper.emitted().itemupdated[0]).toBeTruthy();
        expect(wrapper.emitted().input[0]).toEqual([false]);
    });
});