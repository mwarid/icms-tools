import mockApiCall from '../../utils/mockApiCall';
import { mount, shallowMount } from '../../utils/testCreators';

import AjaxCalls from '../../../src/utils/AjaxCalls';
import PendingAuthors from '../../../src/views/PendingAuthors.vue';

describe('MemberAuthors.vue', () => {
    const response = [
        {
            cmsId: 1,
            firstName: 'Ralph',
            lastName: 'Moore',
            instCode: 'CERN',
            name: 'Physist',
            moFullfilled: true,
            isAuthorBlock: true,
        },
        {
            cmsId: 2,
            firstName: 'Brian',
            lastName: 'Sanford',
            instCode: 'UCLA',
            name: 'Mountain Climber',
            moFullfilled: false,
            isAuthorBlock: true,
        },
        {
            cmsId: 3,
            firstName: 'Greta',
            lastName: 'Kris',
            instCode: 'CERN',
            name: 'Juggler',
            moFullfilled: true,
            isAuthorBlock: true,
        },
    ];
    const { initialize } = mockApiCall(AjaxCalls.PendingAuthorsCall, { response });
    beforeEach(() => {
        initialize();
    });

    it('Loads results from the api call', async () => {
        const wrapper = shallowMount(PendingAuthors);
        expect(AjaxCalls.PendingAuthorsCall).toHaveBeenCalled();

        await wrapper.vm.$nextTick();
        expect(wrapper.vm.data).toBe(response);
    });

    it('transforms the data correctly', async () => {
        const wrapper = shallowMount(PendingAuthors);
        
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.tableData).toMatchObject([
            {
                cmsId: 1,
                moFullfilled: 'OK',
                isAuthorBlock: 'YES',
            },
            {
                cmsId: 2,
                moFullfilled: 'Missing',
                isAuthorBlock: 'YES',
            },
            {
                cmsId: 3,
                moFullfilled: 'OK',
                isAuthorBlock: 'YES',
            }
        ]);
    });

    it('Displays the data on the table', async () => {
        const wrapper = mount(PendingAuthors);
        
        await wrapper.vm.$nextTick();
        const table = wrapper.findComponent({ name: "VuetifyTable"});
        
        expect(table.html()).toMatchSnapshot();
    });

    it('Filters by institute', async () => {
        const wrapper = mount(PendingAuthors);
        wrapper.setData({institute: 'CERN'});
    
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.tableData).toMatchObject([{ cmsId: 1 },{ cmsId: 3 }]);
    });

    it('Opens an Informational Dialog when prompted', async () => {
        const wrapper = mount(PendingAuthors);
        
        expect(wrapper.vm.isInfoDialogOpen).toBe(false);

        await wrapper.find('.v-icon--link').trigger('click');
        expect(wrapper.vm.isInfoDialogOpen).toBe(true);
    });

    it('Summons a warning modal when an action is selected', async () => {
        const wrapper = mount(PendingAuthors);
        
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.selectedPerson).toBe(null);
        expect(wrapper.vm.selectedAction).toBe(null);
        expect(wrapper.vm.isConfirmationDialogOpen).toBe(false);
        
        //Click the actions button to open the menu
        await wrapper.findAll('button').at(2).trigger('click');
        const menu = wrapper.find('.menuable__content__active');

        //Select the "Allow signing" option
        await menu.findAll('.v-list-item__title').at(0).trigger('click');
        expect(wrapper.vm.selectedPerson).toBe(response[0]);
        expect(wrapper.vm.selectedAction).toBe('admit');
        expect(wrapper.vm.isConfirmationDialogOpen).toBe(true);

        //Select the "Put on hold" option
        await menu.findAll('.v-list-item__title').at(1).trigger('click');
        expect(wrapper.vm.selectedPerson).toBe(response[0]);
        expect(wrapper.vm.selectedAction).toBe('freeze');
        expect(wrapper.vm.isConfirmationDialogOpen).toBe(true);
    });

    it('Removes the person from the list when the action is confirmed', async () => {
        const wrapper = mount(PendingAuthors);
        
        await wrapper.vm.$nextTick();
        
        await wrapper.findComponent({name: 'PendingAuthorConfirmationDialog'}).vm.$emit('actioncompleted', 2);
        expect(wrapper.vm.data).toMatchObject([{ cmsId: 1 }, { cmsId: 3 }]);
    })
});