import { mount, shallowMount } from '../../utils/testCreators';
import mockApiCall from '../../utils/mockApiCall';

import Suspensions from '../../../src/views/Suspensions.vue';
import AjaxCalls from '../../../src/utils/AjaxCalls';

describe('Suspension.vue',() => {
    const response = [
        {
            cmsId: 1,
            firstName: 'Ralph',
            lastName: 'Moore',
            instCode: 'CERN',
            name: 'Physist',
            isAuthor: false,
            isAuthorSuspended: true,
        },
        {
            cmsId: 2,
            firstName: 'Brian',
            lastName: 'Sanford',
            instCode: 'UCLA',
            name: 'Mountain Climber',
            isAuthor: true,
            isAuthorSuspended: true,
        },
        {
            cmsId: 3,
            firstName: 'Greta',
            lastName: 'Kris',
            instCode: 'CERN',
            name: 'Juggler',
            isAuthor: false,
            isAuthorSuspended: false,
        },
    ];
    const { initialize } = mockApiCall(AjaxCalls.SuspensionsCall, { response });
    beforeEach(() => {
        initialize();
    });

    it('Loads results from the api call', async () => {
        const wrapper = shallowMount(Suspensions);
        expect(AjaxCalls.SuspensionsCall).toHaveBeenCalled();

        await wrapper.vm.$nextTick()
        expect(wrapper.vm.data).toBe(response);
    });

    it('Displays the correct actions', async () => {
        const wrapper = mount(Suspensions);
        await wrapper.vm.$nextTick();
        // Find the table's columns 
        const columns = wrapper.findAll('tr td');
        // row 1, col 8 (Action)
        expect(columns.at(0 * 8 + 7).text()).toContain('Unsuspend')
        // row 2, col 8 (Action)
        expect(columns.at(1 * 8 + 7).text()).toContain('Active author')
        // row 3, col 8 (Action)
        expect(columns.at(2 * 8 + 7).text()).toContain('Suspend')
    });

    it('Transforms response data correctly', async () => {
        const wrapper = shallowMount(Suspensions);
        
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.tableData).toEqual([
            {
                cmsId: 1,
                firstName: 'Ralph',
                lastName: 'Moore',
                instCode: 'CERN',
                name: 'Physist',
                isAuthor: 'NO',
                isAuthorSuspended: 'YES'
            },
            {
                cmsId: 2,
                firstName: 'Brian',
                lastName: 'Sanford',
                instCode: 'UCLA',
                name: 'Mountain Climber',
                isAuthor: 'YES',
                isAuthorSuspended: 'YES'
            },
            {
                cmsId: 3,
                firstName: 'Greta',
                lastName: 'Kris',
                instCode: 'CERN',
                name: 'Juggler',
                isAuthor: 'NO',
                isAuthorSuspended: 'NO'
            }
        ]);
    });
    
    it('Filters by institute', async () => {
        const wrapper = mount(Suspensions);
        wrapper.setData({institute: 'CERN'});
    
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.tableData).toMatchObject([{ cmsId: 1 },{ cmsId: 3 }]);
    });
    
    it('Summons a warning modal when an action is selected', async () => {
        const wrapper = mount(Suspensions);
        
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.selectedPerson).toBe(null);
        expect(wrapper.vm.isDialogOpen).toBe(false);
        
        // Click the Unsuspend button
        await wrapper.findAll('button').at(1).trigger('click');
        expect(wrapper.vm.selectedPerson).toBe(response[0]);
        expect(wrapper.vm.isDialogOpen).toBe(true);
        
    });
  
    it('Displays the new status if the action is confirmed', async () => {
        const wrapper = mount(Suspensions);
        
        await wrapper.vm.$nextTick();
        //Select the first person (Suspended)
        wrapper.setData({ selectedPerson: wrapper.vm.data[0] })

        //Lift suspension
        await wrapper.findComponent({name: 'SuspensionConfirmationDialog'}).vm.$emit('suspensionupdate', false);
        //Status should change to unsuspended 
        expect(wrapper.vm.data[0].isAuthorSuspended).toBe(false);

        //Select the third person (Unsuspended)
        wrapper.setData({ selectedPerson: wrapper.vm.data[2] })

        //Confirm suspension
        await wrapper.findComponent({name: 'SuspensionConfirmationDialog'}).vm.$emit('suspensionupdate', true);
        //Status should change to suspended 
        expect(wrapper.vm.data[0].isAuthorSuspended).toBe(true);
    });
})