import instituteSelectMixin from '../../../src/mixins/instituteSelectMixin'
import {mount} from '@vue/test-utils';

describe('instituteSelectMixin', () => {
    const Component = {
        render() {},
        mixins: [instituteSelectMixin]
    };
    it('Creates a list of institute options ordered alphabetically', () => {
        const wrapper = mount(Component);

        expect(wrapper.vm.instituteSelectIsVisible).toBe(false);
        wrapper.vm.createInstituteOptions([
            {instCode: 'INST C'},
            {instCode: 'INST A'},
            {instCode: 'INST D'},
            {instCode: 'INST A'},
            {instCode: 'INST B'},
            {instCode: 'INST B'},
        ]);

        wrapper.vm.$nextTick(() => {
            expect(wrapper.vm.instituteOptions).toEqual([
                {value: 'INST A', text: 'INST A'},
                {value: 'INST B', text: 'INST B'},
                {value: 'INST C', text: 'INST C'},
                {value: 'INST D', text: 'INST D'}
            ])
            expect(wrapper.vm.instituteSelectIsVisible).toBe(true);
        });
    });

    it('Makes the institute select visible only if more than one options exist', () => {
        const wrapper = mount(Component);

        expect(wrapper.vm.instituteSelectIsVisible).toBe(false);
        wrapper.vm.createInstituteOptions([
            {instCode: 'INST A'},
            {instCode: 'INST A'},
            {instCode: 'INST A'},
        ]);

        wrapper.vm.$nextTick(() => {
            expect(wrapper.vm.instituteOptions).toEqual([
                {value: 'INST A', text: 'INST A'}
            ])
            expect(wrapper.vm.instituteSelectIsVisible).toBe(false);
        });
    })
});