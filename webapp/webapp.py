from typing import Any, Dict, List, Optional
import flask
import traceback
from flask.wrappers import Request

from icms_orm.orm_interface.orm_manager import IcmsOrmSession
from icmsutils.ldaputils import LdapProxy
from werkzeug.routing import Rule, MapAdapter
from util.ReverseProxied import ReverseProxied
import logging
import os
from util import constants
import flask_login
import hashlib
import subprocess
import app_profiles
import socket
import re
from itsdangerous import URLSafeTimedSerializer
from icmscommon.governors import ConfigGovernor, DbGovernor
from util.webapp_governors import WebappConfigGovernor
from util.database.orm_interface import WebappOrmManager
from flask_bootstrap import Bootstrap
from flask_debugtoolbar import DebugToolbarExtension
from flask_migrate import Migrate
from flask_compress import Compress
from util.glossary import demystify
from blueprints.api import api_blueprint
from blueprints.author_lists import al_blueprint
from icmsutils.businesslogic.requests import PersonStatusChangeRequestDefinition
from util.ldap import DevLdapProxy

log: logging.Logger = logging.getLogger(__name__)


class Webapp(flask.Flask):
    """
    Encapsulating the Flask application object with all the quirks
    necessary to run the app in our setup.
    """

    def __init__(self, import_name, root_path, profile):
        super().__init__(import_name=import_name,
                         static_url_path='/static',
                         static_folder='static', instance_relative_config=True,
                         root_path=root_path)
        self.__profile = None
        self.__validate_and_set_profile(profile)
        self.__set_up_app_config()
        self.__set_up_database_connections()
        self.__set_up_db_sessions_management()
        self.__set_up_extensions()
        self.__set_up_error_handlers()
        self.__set_up_cwd_recovery()
        self.__set_up_context_processor()
        self.__set_up_before_request_hooks()
        self.__set_up_cors_headers_support()
        self.__set_up_route_interceptions()
        self.__register_jinja_globals()
        self.__attach_blueprints()
        self.__register_request_types()
        self.__attach_wildcard_route()
        ConfigGovernor.set_instance(WebappConfigGovernor(self))
        self.__ldap_proxy: Optional[LdapProxy] = None
        self.init_ldap_proxy()

    @classmethod
    def current_app(cls) -> 'Webapp':
        app = flask.current_app._get_current_object()
        if isinstance(app, Webapp):
            return app
        raise ValueError(f'Current app is of unknown type {type(app)}')

    @classmethod
    def db_session(cls) -> IcmsOrmSession:
        return cls.current_app().db.session()

    @property
    def db(self):
        return self._db

    @db.setter
    def db(self, db):
        self._db = db
        DbGovernor.set_db_manager_getter(lambda: self.db)

    def init_ldap_proxy(self):
        '''To be called by Webapp\'s constructor. Can be overridden in test/dev environment to mock LDAP access'''
        if self.config.get('LDAP_MOCKING_ENABLED', False) is True:
            log.info('Using the DevLdapProxy')
            self.__ldap_proxy = DevLdapProxy()
            self.__ldap_proxy.register_mocks(
                self.config.get('LDAP_MOCK_ENTRIES', []))
        else:
            log.info('Using the default LdapProxy')
            self.__ldap_proxy = LdapProxy(
                ldap_uri_override=self.config['LDAP_URI'])

    def get_ldap_proxy(self) -> LdapProxy:
        '''Centralising access to LdapProxies so that they can be stubbed if need be (dev/test)'''
        assert self.__ldap_proxy is not None, 'LDAP proxy should have been initialized by now'
        return self.__ldap_proxy

    def is_running_on_production(self):
        return socket.gethostname() in app_profiles.ProfileProd.PRODUCTION_HOSTNAMES

    def is_running_on_local_dev_machine(self):
        return 'cms' not in (socket.gethostname() or '').lower()

    def get_routing_rule_matching_path(self, resource_path: str) -> Optional[Rule]:
        # Getting rid of the script SCRIPT_NAME
        script_name = flask.request.environ.get('SCRIPT_NAME', '')
        if script_name and resource_path.startswith(script_name):
            log.debug(f'Removing {script_name} from {resource_path}')
            resource_path = resource_path[len(script_name):]

        # And an arcane path-construction recipe borrowed from the depths of werkzeug's guts
        url_adapter: MapAdapter = flask.current_app.create_url_adapter(
            flask.request)
        path = u"%s|%s" % (
            url_adapter.map.host_matching and url_adapter.server_name or url_adapter.subdomain,
            resource_path and "/%s" % resource_path.lstrip("/"),
        )

        matching_rules: List[Rule] = [
            r for r in self.url_map.iter_rules() if r.match(path) is not None]
        if len(matching_rules) > 0:
            # the longer the rule, the more specific the match is assumed to be - thus it goes first
            matching_rules = sorted(
                matching_rules, key=lambda x: len(x.rule), reverse=True)
            url_rule: Rule = matching_rules[0]
            log.debug(
                f'Rule {url_rule.rule} matches {resource_path}. Matching rules count: {len(matching_rules)}')
            return url_rule
        return None

    def __validate_and_set_profile(self, profile):
        if profile == app_profiles.ProfileProd:
            if self.is_running_on_production():
                self.__profile = profile
            else:
                log.warning('Production profile specified but corrent hostname %s not in %s',
                            socket.gethostname(), ', '.join(profile.PRODUCTION_HOSTNAMES))
                self.__profile = app_profiles.ProfilePreProd
        else:
            self.__profile = profile

    def __set_up_extensions(self):
        self.wsgi_app = ReverseProxied(self.wsgi_app)
        compress = Compress()
        Migrate(app=self, db=self.db)
        compress.init_app(self)
        Bootstrap(self)
        DebugToolbarExtension(self)

    def __set_up_error_handlers(self):
        @self.errorhandler(403)
        @self.errorhandler(404)
        @self.errorhandler(410)
        @self.errorhandler(500)
        def route_error_page(e):
            request: Request = flask.request
            log.warning(
                f'Trapped an error {e.code}, looking for path {request.path} (full: {request.full_path}) at url_root {request.url_root} on host {request.host} (URL: {request.url})')
            return flask.render_template('error.html', error=e, stack=traceback.format_exc()), e.code if hasattr(e, 'code') else 500

    def __set_up_cwd_recovery(self):
        _startup_wd = os.getcwd()
        log.info('Working directory at startup: {0}'.format(_startup_wd))

        @self.after_request
        def check_cwd(r):
            cd_needed = False
            try:
                if os.getcwd() != _startup_wd:
                    cd_needed = True
            except FileNotFoundError as _:
                cd_needed = True
                log.warning(
                    'Caught a FileNotFound while trying to get CWD - the directory might have been deleted!')
            finally:
                if cd_needed:
                    log.warning(
                        'Changing working directory back to {0}, as observed at startup.'.format(_startup_wd))
                    os.chdir(_startup_wd)
            return r

    def __set_up_before_request_hooks(self):
        app = self

        @app.before_request
        def catch_deprecated_endpoints():
            request: Request = flask.request
            path: str = request.path
            deprecation_map: Dict[str, Any] = self.config.get(
                'DEPRECATED_ROUTES', dict())

            if path not in deprecation_map.keys():
                return
            alternative = deprecation_map.get(path)
            if alternative is None:
                alternative = f'{request.url}'
                swaptokens = app.config.get(
                    'DEPRECATION_FALLBACK_SWAPTOKENS', dict())
                for outbound, inbound in swaptokens.items():
                    alternative = alternative.replace(
                        outbound, inbound)
            else:
                alternative = alternative[1:] if alternative[0] == '/' else alternative
                alternative = f'{request.url_root}{alternative}'

            return flask.jsonify({"message": f'Endpoint {path} has been deprecated in favor of {alternative}'}), 410

        @app.before_request
        def flask_wtf_csrf_antidote():
            # now some sorcery. let's beat flaky wtf-csrf to setting the global variable (app crashes otheriwse)
            field_name = app.config.get('WTF_CSRF_FIELD_NAME', 'csrf_token')
            secret_key = app.config['SECRET_KEY']
            if field_name not in flask.g:
                if field_name not in flask.session:
                    flask.session[field_name] = hashlib.sha1(
                        os.urandom(64)).hexdigest()
                s = URLSafeTimedSerializer(secret_key, salt='wtf-csrf-token')
                # here the .decode() makes all the difference
                setattr(flask.g, field_name, s.dumps(
                    (lambda x: x.decode() if isinstance(x, bytes) else x)(flask.session[field_name])))

    def __set_up_cors_headers_support(self):
        @self.after_request
        def add_cors_header(response):
            pattern = self.config.get('CORS_ALLOWED_ORIGINS_REGEX')
            if pattern == '*':
                response.headers['Access-Control-Allow-Origin'] = pattern
            else:
                headers = flask.request.headers
                origin = headers.get('Origin') or headers.get(
                    'Referer') or None
                if origin and re.match(pattern, origin):
                    log.debug(f'Origin {origin} matched the pattern!')
                    response.headers['Access-Control-Allow-Origin'] = origin
                    response.headers['Access-Control-Allow-Credentials'] = 'true'
                    response.headers['Access-Control-Allow-Headers'] = self.config.get(
                        'CORS_ALLOWED_HEADERS')
                    response.headers['Access-Control-Allow-Methods'] = self.config.get(
                        'CORS_ALLOWED_METHODS')
            return response

    def __set_up_context_processor(self):
        app = self

        @app.context_processor
        def populate_template_context():
            # Ugly import but it imports other stuff that the poor interpreter should not see too early
            from webapp.menus import TopMenuFactory
            return {
                'const': constants,
                'is_prod': lambda: app.is_running_on_production(),
                'top_menus': TopMenuFactory.produce_top_menus(),
                'is_dev': lambda: app.is_running_on_local_dev_machine() and 'dev' in app.config.get('ENV', ''),
                'is_admin': lambda: flask_login.current_user.is_admin()
            }

    def __set_up_route_interceptions(self):
        """
        TODO: remove it once Jinja's phased-out
        This was used once upon a time to render Jinja-based confirmation screens.
        Can be safely discarded once Jinja is phased out.
        """
        @self.route('/intercept')
        def route_intercept():
            log.debug('Intercepting some request!')
            real_endpoint = flask.request.args[constants.REQUEST_ARG_ENDPOINT]
            title = flask.request.args.get(
                constants.REQUEST_ARG_TITLE, 'Confirmation')
            message = flask.request.args.get(
                constants.REQUEST_ARG_MESSAGE, 'Are you sure?')
            return flask.render_template('dialog_intercept.html', real_endpoint=real_endpoint, title=title, message=message)

        constants.ROUTE_NAME_INTERCEPT_AND_CONFIRM = route_intercept.__name__

    def __attach_wildcard_route(self):
        """
        TODO: remove this along with the corresponding config option to disable Vue FE in this application.
        Wildcard route fallback was used to have Vue work alongside Jinja. 
        """
        wildcard_route_template = self.config['WILDCARD_ROUTE_TEMPLATE']

        @self.route('/', methods=['GET'])
        @self.route('/<path:vue_path>', methods=['GET'])
        @flask_login.login_required
        def route_vue(vue_path=None):
            if not wildcard_route_template:
                flask.abort(404)
            log.debug(
                f'vue path is {vue_path} and wildcard template is {wildcard_route_template}')
            return flask.render_template(wildcard_route_template)

    def __attach_blueprints(self):
        """
        Attaches the blueprints once the app is safely created so that the circular dependencies are broken up.
        Respective blueprints should however be restructured not to make premature db calls so that the imports can be moved to the top level.
        """

        from blueprints.cadi_viewer import cadi_viewer_blueprint
        from blueprints.dashboard import dashboard_blueprint
        from blueprints.admin import admin_blueprint
        from blueprints.api_restplus import restplus_blueprint
        from blueprints import users
        from blueprints import identity_provider as id_provider

        self.register_blueprint(dashboard_blueprint, url_prefix='/')
        self.register_blueprint(api_blueprint, url_prefix='/api')
        self.register_blueprint(cadi_viewer_blueprint, url_prefix='/cadi')
        self.register_blueprint(al_blueprint, url_prefix='/al')
        self.register_blueprint(admin_blueprint, url_prefix='/admin')
        self.register_blueprint(restplus_blueprint, url_prefix='/restplus')
        self.register_blueprint(users.users_blueprint, url_prefix='/users')
        self.register_blueprint(id_provider.blueprint,
                                url_prefix='/idProvider')
        users.login_manager.init_app(self)

    def __register_jinja_globals(self):
        self.jinja_env.globals.update(const=constants)
        self.jinja_env.globals.update(demystify=demystify)

    def __set_up_database_connections(self):
        self.db = WebappOrmManager(self)

    def __set_up_app_config(self):
        self.config.from_object(self.__profile)
        log.debug('Using instance-specific config override: %s' %
                  self.__profile.CONFIG_OVERRIDE_FILENAME)
        self.config.from_pyfile(self.__profile.CONFIG_OVERRIDE_FILENAME)
        app_logging_level = self.config['LOGGING_LEVEL']
        logging.getLogger().setLevel(app_logging_level)
        log.info('Logging level set to {0}'.format(app_logging_level))
        # allow automatic restart if code is changed:
        if self.config['DEBUG']:
            self.debug = True
        # set ENVIRONMENT_VARIABLES as specified in app's config
        for var in self.config[constants.OPT_NAME_ENVIRONMENT_VARIABLES]:
            os.environ[var] = self.config[constants.OPT_NAME_ENVIRONMENT_VARIABLES][var]

    def remount_eos_if_needed(self):
        if self.config.get('AL_FILES_DIR').startswith('/eos'):
            # according to Jan Iven on MatterMost (2017-06-26 morning)
            subprocess.call(['/bin/eosfusebind', '-g'])

    def __set_up_db_sessions_management(self):
        @self.teardown_request
        def _rollback_transactions(error=None):
            if error is None:
                log.debug("teardown_request: committing a DB transaction")
                self.db.session.commit()
            else:
                log.debug("teardown_request: rolling back a DB transaction")
                self.db.session.rollback()

    def __register_request_types(self):
        PersonStatusChangeRequestDefinition.register()
