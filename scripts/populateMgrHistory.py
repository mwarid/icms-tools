
import os
import sys
import datetime
import time
import logging
import pyexcel as pe
from pyexcel.ext import xlsx

import json

from flask import Flask
from sqlalchemy.orm.exc import  NoResultFound, MultipleResultsFound

from icms_orm.common import DeclarativeBase
from icms_orm.common import ManagerHistory
from icms_orm.epr import EprUser, EprInstitute

from util.factory import WebappOrmManager

sys.path.append( '.' )
scrPath = './scripts'
if scrPath not in sys.path:
    sys.path.append(scrPath)

theLogger = None

def timing_val(func):
    def wrapper(*arg, **kw):
        '''source: http://www.daniweb.com/code/snippet368.html'''
        t1 = time.time()
        res = func(*arg, **kw)
        t2 = time.time()
        theLogger.debug( '%s ran in %s sec.' % (func.__name__, (t2 - t1)) )
        return (t2 - t1), res, func.__name__
    return wrapper

def getExcelInfo( fileName ) :

    book_dict = pe.get_book_dict( file_name=fileName )

    sheetStarts = { 'POG': 7,
                    'PAG': 8,
                    'Generator' : 8,
                    'L1-DSP' : 6
                    }
    mgrHistory = { }
    for k in sheetStarts.keys(): mgrHistory[k] = []

    year = 2007
    for key, item in book_dict.items( ) :
        if  key not in sheetStarts.keys(): continue
        count = 0
        print( "processing sheet %s " % key )
        for row in item:
            count += 1
            if count < sheetStarts[key]: continue
            if key != 'L1-DSP':
                if type(row[0]) != type(1) and row[0].startswith("Total"): continue
                try:
                    year = int(row[0])
                except ValueError:
                    pass
                mgrHistory[key].append( [year, '%s | %s' % (key, row[1]), row[2]] )
            else:
                if type(row[0]) == type(1):
                    year = int(row[0])
                    level = 'DeputySP'
                else:
                    level = 'L1-%s' % row[0]
                mgrHistory[ key ].append( [ year, level, row[ 1 ] ] )

    # for k in sheetStarts.keys():
    #     print( '\n%s : %s' % (k, json.dumps( mgrHistory[k][:20], indent=4, sort_keys=True )) )

    return mgrHistory

class UserFinder(object):

    def __init__(self):
        self.allUsers = { }
        allUsersDB = EprUser.query.all( )
        for item in allUsersDB :
            if item.name in self.allUsers.keys( ) : continue
            self.allUsers[ item.name ] = item

        print( "UserFinder> got %d users from DB" % len( self.allUsers.keys() ) )

    def findUser(self, partial):

        if partial.strip( ) == '' :
            return None, None

        if ' ' in partial.strip() :
            partial = partial.strip().split( )[ -1 ]

        if partial.strip( ) == '' :
            print( "ERROR: no valid lastname found in %s " % partial )
            return None, None

        for k, user in self.allUsers.items():
            if partial in k:
                instCode = user.institute.code
                if not instCode:
                    instCode = 'N/A'

                return user, instCode

        return None, None

def importMgrHistoryData(inFileName, db):

    mgrHistInfo = getExcelInfo( inFileName )
    uf = UserFinder()

    if len( ManagerHistory.query.all() ) > 0:
        print( "Table has already data, skipping import ! " )
        return

    for k, rows in mgrHistInfo.items():
        for row in rows:

            if not row[2].strip() :
                # print( "no name in row:", row )
                continue # ignore empties ...

            user, instCode = uf.findUser(row[2])
            if not user:
                print( 'No user info for', row[2], 'skipping ...', row )
                continue

            mh = ManagerHistory(username = user.username,
                                role = row[1],
                                level = 0 if "DeputySP" in row[1] else 1, # at present we only have L1 and (Deputy-)SP
                                name = '%s' % (user.name,),
                                cmsId = user.cmsId,
                                instCode = instCode,
                                year = row[0])
            db.session.add( mh )
    db.session.commit()

def main():

    global theLogger

    theApp = Flask( __name__, instance_relative_config=True )
    theApp.config.from_pyfile( 'config.py' )

    with theApp.app_context() :
        db = WebappOrmManager(theApp)
        db.create_all(bind='icms_common_db')

        dbUrl = str(db.engine.url)
        theLogger = theApp.logger
        theLogger.setLevel( logging.INFO )
        theLogger.info( "\n==> DB at: %s \n" % dbUrl)#[:15] )

        importMgrHistoryData( './levelNmanagers-lm_historica_2018.xlsx', db)

        del theLogger

    return 0

if __name__ == '__main__':
    retCode = main()
