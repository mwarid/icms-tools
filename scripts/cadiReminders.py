import datetime
from flask import Flask, current_app
from cadi_reminders import CadiReminder


def main(db=None):
    """
    Warning: app context will be needed if no db object is supplied
    """
    db = db or current_app.db
    reminder = CadiReminder()
    reminder.getInfoFromDB()
    reminder.showInfo()
    reminder.sendEmails(what='greenLights', db=db)
    if datetime.datetime.today().day == 1:
        print("\n ... going to send monthly mails ... ")
        reminder.sendEmails(what='monthly', db=db)
        print("\n ... going to send monthly summary to PC ... ")
        reminder.sendEmails(what='PCsummary', db=db)
