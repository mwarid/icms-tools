from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from icms_orm.cmsanalysis import PaperAuthorHistory as PAH, PaperAuthorHistoryActions as PAHActions
import logging
from icms_orm.toolkit import AuthorListFiles


def get_al_status_map_from_old_db():
    al_statuses = {}
    pah_list = PAH.query.filter(PAH.action.in_([PAHActions.LIST_GEN, PAHActions.LIST_CLOSE])).all()
    for item in pah_list:
        # preserve whatever is already there, unless there's nothing - then set to open
        al_statuses[item.code] = al_statuses.get(item.code, 'open')
        # and overwrite that only if it can be set as closed (never to overwrite closed with open!)
        if item.action == PAHActions.LIST_CLOSE:
            al_statuses[item.code] = 'closed'
    logging.debug('Found %d closed AL entries on the other side' % sum([v == 'closed' for v in al_statuses.values()]))
    return al_statuses


def update_al_statuses_in_new_db(db):
    al_statuses = get_al_status_map_from_old_db()
    for code, stat in al_statuses.items():
        try:
            items = AuthorListFiles.query.filter_by(paper_code=code).all()
            for item in items:
                item.status = stat
                db.session.add(item)
                db.session.commit()
        except NoResultFound as e:
            # logging.info( 'No item found for %s' % code )
            pass
        except MultipleResultsFound as e:
            logging.error('Multiple items found for %s' % code)
            pass
