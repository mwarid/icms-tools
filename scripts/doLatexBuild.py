#!/usr/bin/env python

# A rewrite of /afs/cern.ch/cms/PAS/do-pas-svn-upload in python
#
# Author: Andreas.Pfeiffer@cern.ch
#
#
# Options:
#     --style: type of cadiLine: paper or PAS
#     --wrap: forces generation of the tar-ball; also creates the upload info (figures and manifest.xml) for CDS
#     --nodraft: to strip off the draft label
#     --data: indicates based on data (default) or on MonteCarlo. Typically now only GEN is on MC
#     --mc|--nodata: , help='MonteCarlo based (typically only for GEN)
#     --upload: push to CDS: only for PAS
#     --preview: do the standard checks
#     -r, --reload': re-upload to CDS. needs CDS record-id of originally uploaded doc.
# Args:
#     cadiLine', help='The cadiLine to be processed (e.g. FOO-18-042)' )
#

import sys
import argparse
import os
import time
import re
import socket
import glob

from tempfile import mkdtemp
from shutil import rmtree
import subprocess
from collections import  OrderedDict

def doCmd(cmd, log, verbose=False, dryRun=False):

    msg = ''
    if verbose: msg += '\n[verbose] going to execute: "%s"' % cmd

    out = ''
    err = ''
    returnCode = 0

    if dryRun:
        msg += 'dryRun requested. No action ... '
    else:
        process = subprocess.Popen( cmd, shell=True, stdin=subprocess.PIPE, stderr=subprocess.STDOUT )
        out, err = process.communicate()
        returnCode = process.returncode

    if out: msg += '\n%s' % out
    if err: msg += '\n%s' % err

    if msg:
        # we need a unique string for the key here, so add the command (strip it when showing it in the html)
        log[ 'doCmd [%s]' % cmd ] = msg
    if returnCode != 0:
        raise subprocess.CalledProcessError(returnCode, cmd)

    return returnCode, out, err


class CMSLatexBuildChecker(object):

    def __init__(self):
        self.origDir = os.getcwd()
        self.tmpDir = None
        self.topDir = None
        self.log = OrderedDict()

        self.status = 'OK'
        self.tdrTool = None

        self.cadiLine = None
        self.cadiStyle = None
        self.options = None

        # self.cadiStyleDirMap = { 'pas' : 'note',  'paper' : 'paper' }
        self.cadiStyleDirMap  = { 'pas' : 'notes', 'paper' : 'papers' }

        self.verbose = False
        self.keepFiles = False

        return

    def _mkTmpDir( self ):

        if not self.cadiLine:
            self.log[ 'Creating self.tmpDir' ] = " ERROR no cadiLine given to make temp dir ! "
            return

        self.tmpDir = None
        try:
            self.tmpDir = mkdtemp( prefix='tmp-%s-' % self.cadiLine )
            self.log[ 'Creating self.tmpDir' ] = " OK -- going to work in %s" % self.tmpDir
            self.tmpDir = self.tmpDir
        except Exception as e:
            self.log[ 'Creating self.tmpDir' ] = "ERROR creating self.tmpDir (%s), got %s - aborting." % (self.tmpDir, str(e))
            if self.tmpDir and not self.keepFiles:
                rmtree(self.tmpDir, ignore_errors=True)
            self.status = "ERROR creating self.tmpDir"

        os.chdir( self.tmpDir )

        return

    def doGitlabCheckout( self ):
        gitRoot = 'https://:@gitlab.cern.ch:8443/tdr'

        if self.verbose:
            self.log[ 'gitlab checkout' ] = '[verbose] in %s, going to clone %s %s from gitlab ... ' % ( os.getcwd(), self.cadiStyle, self.cadiLine)

        gitRet = 0
        try:
            cmd = 'rm /data/tmp/%s.gitlog;' % self.cadiLine
            if 'Darwin' not in os.uname():
                cmd += "source /opt/rh/git19/enable > /data/tmp/%s.gitlog 2>&1 || " % self.cadiLine
            cmd += "git --version >>/data/tmp/%s.gitlog 2>&1;" % self.cadiLine
            cmd += "git clone --recurse-submodules %s/%s/%s.git "  % (gitRoot, self.cadiStyle, self.cadiLine)
            cmd += " >>/data/tmp/%s.gitlog 2>&1 " % self.cadiLine
            gitRet = doCmd(cmd, self.log, verbose=self.verbose)
            # we get here only if the processing of the command was successful, otherwise an exception is raised
            os.chdir( self.cadiLine )
            self.topDir = os.getcwd()

        except Exception as e :
            if self.verbose:
                msg = "\nFATAL ERROR while processing %s %s from gitlab -- got: \n'%s'\n\t Aborting.\n" % (
                self.cadiStyle, self.cadiLine, str( e ))
                self.log[ 'Exception1 caught' ] = '"%s"' % msg
                self.showLogFile( '/data/tmp/%s.gitlog' % self.cadiLine )
            self.status = "ERROR when cloning from gitlab"

        # if requested (verbose), show the log file even if things went OK
        if self.verbose and self.status == 'OK': # don't show it twice in case of errors
            self.showLogFile( '/data/tmp/%s.gitlog' % self.cadiLine )

        self.tdrTool = './utils/tdr'

        if self.verbose:
            self.log['gitlab checkout dir - content'] = '[verbose] %s - %s' % (os.getcwd(), '\n'.join( os.listdir('.') ) )

        if self.status == 'OK':
            self.log[ 'gitlab checkout' ] = '%s %s successfully cloned from gitlab ... ' % (self.cadiStyle, self.cadiLine)

        return gitRet, self.log

    def showLogFile( self, lfPath ) :
        # first ensure the log file doesn't contain any traces of Latin-1 chars, so convert it:
        cmd = "/bin/iconv -f LATIN1 -t 'UTF-8//TRANSLIT' %s -o %s-utf8" % (lfPath, lfPath)
        doCmd(cmd, self.log, verbose=self.verbose)
        # then open and show
        with open( '%s-utf8' % lfPath, 'r', encoding='utf-8', errors='replace' ) as glFile :
            lines = glFile.readlines()
            # print( lines )
            self.log[ 'logfile [%s]' % lfPath ] = ''.join( lines )

    def doSVNCheckout( self ) :

        svnRoot = "svn+ssh://svn.cern.ch/reps/tdr2"

        if self.verbose:
            self.log[ 'SVN checkout' ] = 'in %s, going to check out %s %s from SVN ... ' % (os.getcwd(), self.cadiStyle, self.cadiLine)

        try :
            doCmd( "svn co     -N --depth empty    %s                >svn-co-utils.log 2>&1" % svnRoot, self.log, verbose=self.verbose )
            doCmd( "svn update    --depth empty    tdr2/utils       >>svn-co-utils.log 2>&1", self.log, verbose=self.verbose )
            doCmd( "svn update    --depth infinity tdr2/utils/trunk >>svn-co-utils.log 2>&1", self.log, verbose=self.verbose )
        except Exception as e :
            msg = "\nFATAL ERROR while checking out from SVN -- got: \n'%s'\n\t got nothing to process - aborting.\n" % str(
                e )
            self.log[ 'Exception2a caught' ] = '"%s"' % msg
            self.status = "ERROR checking out from SVN"

        # this one is OK (nothing found) if there is an exception:
        try:
            doCmd( "grep -v -e '^A ' svn-co-utils.log               >>svn-co-utils-grep.log 2>&1", self.log, verbose=self.verbose )
        except:
            pass

        try:
            doCmd( "svn update -N --depth empty    tdr2/%s          >>svn-co-utils.log 2>&1" % self.cadiStyle, self.log, verbose=self.verbose )
            doCmd( "svn update -N --depth empty    tdr2/%s/%s       >>svn-co-utils.log 2>&1" % (self.cadiStyle, self.cadiLine), self.log, verbose=self.verbose )
            doCmd( "svn update    --depth infinity tdr2/%s/%s/trunk >>svn-co-utils.log 2>&1" % (self.cadiStyle, self.cadiLine), self.log, verbose=self.verbose )
            self.log[ 'SVN checkout ' ] = '%s %s successfully checked out from SVN ... ' % ( self.cadiStyle, self.cadiLine )
            self.topDir = os.path.join( self.tmpDir, 'tdr2', self.cadiStyle )
            self.status = 'OK'
        except Exception as e :
            msg = "\nFATAL ERROR while checking out from SVN -- got: \n'%s'\n\t got nothing to process - aborting.\n" % str( e )
            self.log[ 'Exception2b caught' ] = '"%s"' % msg
            self.status = "ERROR checking out from SVN"

        self.tdrTool = './tdr'

        return self.log

    def checkConsistency( self ):

        if self.options.data and self.options.mc:
            msg = "Invalid args: --data can not be specified simultaneously with --nodata or --mc"
            self.log[ 'Invalid args' ] = msg
            self.status = 'ERROR: %s' % msg
        #-toDo: should we also check that only GEN has MC ??

        if self.options.style and self.options.style.lower() not in self.cadiStyleDirMap.keys():
            msg = "Invalid args: --style has to be 'pas' or 'paper', found: %s " % self.options.style
            self.log[ 'Invalid args' ] = msg
            self.status = 'ERROR: %s' % msg

        if not self.options.cadiLine:
            msg = "Invalid args: no cadiLine specified "
            self.log[ 'Invalid args' ] = msg
            self.status = 'ERROR: %s' % msg

        validCadiLineRe = re.compile(r'^[A-Z][A-Z0-9][A-Z0-9]-\d{2}-\d{3}$')
        if not validCadiLineRe.match( self.options.cadiLine):
            msg = "Invalid args: invalid cadiLine specified: '%s' should look like 'XYZ-dd-ddd' " % self.options.cadiLine
            self.log[ 'Invalid args' ] = msg
            self.status = 'ERROR: %s' % msg

        self.cadiLine = self.options.cadiLine
        self.cadiStyle  = self.cadiStyleDirMap[self.options.style.lower()]

        return self.log

    def processOne( self, argvIn ):

        startTime = time.time()

        print( 'doLatexBuild.py::processOne> argvIn:', argvIn )
        print( 'doLatexBuild.py::processOne> verbose/keepFiles:', self.verbose, self.keepFiles ) 
        
        self.parseArgs( argvIn )

        print( 'doLatexBuild.py::processOne> options:', self.options )

        if 'vocms0215' not in socket.gethostname() and 'vocms0890' not in socket.gethostname() and 'vocms0190' not in socket.gethostname():
            msg = "Refusing to run on non-production system %s ! " % socket.gethostname()
            self.log[ 'nonProdSys' ] = msg
            print( msg )
            return self.log, (time.time()-startTime, 0.)

        self.checkConsistency( )
        if self.status != 'OK' : return self.log, (time.time()-startTime, 0.)

        self._mkTmpDir()
        if self.status != 'OK' : return self.log, (time.time()-startTime, 0.)

        self.doCheckOut()
        if self.status != 'OK' : return self.log, (time.time()-startTime, 0.)

        buildTime = self.doBuild( )
        if self.status != 'OK' : return self.log, (time.time()-startTime, buildTime)

        self.check8bit()
        if self.status != 'OK' : return self.log, (time.time()-startTime, 0.)

        now = time.time()
        self.log[ 'Overall ' ] = 'Processing %s %s took %4.1f sec (latex build: %4.1f)' % ( self.cadiStyle, self.cadiLine, now-startTime, buildTime )

        return self.log, (now-startTime, buildTime)

    def parseArgs( self, argsIn ):

        parser = argparse.ArgumentParser( description='A rewrite of /afs/cern.ch/cms/PAS/do-pas-svn-upload in python' )
        parser.add_argument( 'style', help='Type of cadiLine: paper or PAS', type=str, choices=[ 'pas', 'paper' ] )
        parser.add_argument( 'cadiLine', help='The cadiLine to be processed (e.g. FOO-18-042)' )
        parser.add_argument( '--wrap', help='Forces generation of the tar-ball', action="store_true" )
        parser.add_argument( '--nodraft', help='To strip off the draft label', action="store_true" )
        parser.add_argument( '--data',
                             help='Indicates based on data (default) or on MonteCarlo. Typically now only GEN is on MC',
                             action="store_true" )
        parser.add_argument( '--mc', help='MonteCarlo based (typically only for GEN)', action="store_true" )
        parser.add_argument( '--upload', help='Push to CDS: only for PAS', action="store_true" )
        parser.add_argument( '--preview', help='Do the standard checks', action="store_true" )
        parser.add_argument( '--export', help='???', action="store_true" )
        parser.add_argument( '--reload', help='Re-upload. Needs id of originally uploaded doc (CDS-recordID).', action='store', dest='cdsRecId', type=int )

        self.options = parser.parse_args( argsIn )

        return


    def check8bit( self ):
        # make sure we're in the right dir:
        os.chdir( self.topDir )

        logCheckMsg = 'Checking for potential 8-bit issues ... '
        scriptName = './utils/general/find-8bit.py'
        if not os.path.exists(scriptName) :
            logCheckMsg += 'No script to check 8bit issues found in util/general ... '
            self.status = 'ERROR when checking 8bit issues'

            # make sure the temp dir is always removed ...
            if not self.keepFiles : rmtree( self.tmpDir, ignore_errors=True )

            return

        cmd = 'python3 %s *.tex *.bib ' % (scriptName,)
        cmd += ' >/data/tmp/%s.chk8bit.log 2>&1' % self.cadiLine

        try:
            doCmd( cmd, self.log, verbose=self.verbose )
        except subprocess.CalledProcessError as e:
            logCheckMsg += 'Checking 8bit issues of %s %s failed. \n%s\n' % (self.cadiStyle, self.cadiLine, str(e))
            self.status = 'ERROR when checking 8bit issues'

        if self.status == 'OK':
            self.showLogFile( '/data/tmp/%s.chk8bit.log' % self.cadiLine )
            self.log[ 'Successful chk8bit' ] = 'Checking 8bit issues of %s %s successful -- no issues found - Congratulations ! ' % (self.cadiStyle, self.cadiLine)
        else:
            self.showLogFile( '/data/tmp/%s.chk8bit.log' % self.cadiLine )
            self.log[ 'Checking 8bit issues log' ] = logCheckMsg
            self.log[ 'Checking 8bit issues summary' ] = 'Checking 8bit issues of %s %s failed. See log above for details.' % (self.cadiStyle, self.cadiLine)

        # make sure the temp dir is always removed ...
        if not self.keepFiles : rmtree( self.tmpDir, ignore_errors=True )

        os.chdir( self.origDir )
        return

    def doBuild( self ) :

        buildStart = time.time()

        # make sure we're in the right dir:
        os.chdir( self.topDir )

        cmdOpts = ''
        if self.options.data: cmdOpts += ' --data '
        else:                 cmdOpts += ' --no-data '

        if self.options.cdsRecId: cmdOpts += ' --reload %s ' % self.options.cdsRecId
        if self.options.wrap    : cmdOpts += ' --wrap '
        if self.options.export  : cmdOpts += ' --export '
        if self.options.nodraft : cmdOpts += ' --nodraft '
        if self.options.preview : cmdOpts += ' --preview '
        if self.options.upload  : cmdOpts += ' --upload '

        # tdrStyleMap = { 'notes' : 'note', 'papers' : 'paper' }
        cmd = 'export PATH=/usr/local/texlive/2018/bin/x86_64-linux/:$PATH; %s --style %s %s ' % (self.tdrTool, self.options.style.lower(), cmdOpts)
        cmd += ' b %s' % self.cadiLine
        cmd += ' >/data/tmp/%s.buildlog 2>&1' % self.cadiLine

        logCheckMsg = 'Building using "%s"\n' % cmd
        try:
            doCmd( cmd, self.log, verbose=self.verbose )
        except subprocess.CalledProcessError as e:
            logCheckMsg += 'Build of %s %s failed. \n%s\n' % (self.cadiStyle, self.cadiLine, str(e))
            self.status = 'ERROR in build'

        logChecks = [ " grep -A2 -e '^!' ",
                      " grep -C2 -e '^I couldn.t open file' ",
                      " grep 'contains Unicode characters (typically quote marks or ligatures from cut and paste from Word). These are not allowed with the standard BibTex (requires BibTeX8).' ",
                      " grep -A5 -e '^Traceback (most recent call last):' ",
                      ]

        for logChkCmd in logChecks:
            try:
                doCmd( "%s /data/tmp/%s.buildlog >/dev/null 2>&1" % (logChkCmd, self.cadiLine), self.log, verbose=self.verbose )
                logCheckMsg += 'Some issues found in build log (see above)'
                self.status = 'ERROR(s) found in build log'
            except subprocess.CalledProcessError as e :
                pass # nothing found, all OK

        self.checkFigures(  )

        if self.status == 'OK':
            self.showLogFile( '/data/tmp/%s.buildlog' % self.cadiLine )
            self.log[ 'Successful build' ] = 'Build of %s %s successful -- no issues found in build log - Congratulations ! ' % (self.cadiStyle, self.cadiLine)
        else:
            self.showLogFile( '/data/tmp/%s.buildlog' % self.cadiLine )
            self.log[ 'Checking build log' ] = logCheckMsg
            self.log[ 'Build summary' ] = 'Build of %s %s failed. See log above for details.' % (self.cadiStyle, self.cadiLine)

        os.chdir( self.origDir )

        return time.time()-buildStart

    def checkFigures( self ):
        figTempDir = None
        figureTmpDirRe = re.compile( r'^\s*Made temporary directory (/tmp/tdr_.+) for --wrap\s*$' )
        
        cmd = f"/bin/grep 'Made temporary directory' /data/tmp/{self.cadiLine}.buildlog"
        out = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE).stdout
        figureTempDirMatch = figureTmpDirRe.match(out.decode('utf-8').strip()) if out else None
        if figureTempDirMatch:
            figTempDir = figureTempDirMatch.group(1)
        self.log[ 'Figure Check' ] = f'found tempdir for figures at {figTempDir}'

        figFiles = glob.glob( f'{figTempDir}/Figure*' )
        pfdFiles = [ x for x in figFiles if x.endswith('.pdf') ]
        thumbs = [ x for x in figFiles if 'thumb' in x ]
        pngs  = [ x for x in figFiles if x.endswith('.png') ]
        self.log[ 'Figure Check' ] = f'found {len(figFiles)} files for figures at {figTempDir}; {len(pfdFiles)} PDFs, {len(thumbs)} thumbs ({len(pngs)} pngs)'

        for figFile in pfdFiles:
            expectedThumbName = figFile.replace('.pdf', '-thumb.png')
            if not os.path.exists( expectedThumbName ):
                self.log[ 'Figure Check' ] = f'ERROR: thumbnail {expectedThumbName} not found for {figFile} '
                self.status = 'ERROR'

        return

    def doCheckOut( self ) :

        # first try to find the PAS in gitlab:
        self.doGitlabCheckout(  )

        # check if we got something, if not, try to check out from SVN
        if not os.path.exists( os.path.join( self.tmpDir, self.cadiLine, '.git' ) ) :
            # msg = "\nERROR when trying to clone %s %s from gitlab (retcode: %d)\n" % (self.cadiStyle, self.cadiLine, gitRet)
            # self.log[ 'ERROR from git clone' ] = msg
            self.log[
                'Fallback to SVN ' ] = 'Git clone returned %s -- Falling back to checkout from SVN for %s %s ... ' % ( self.status, self.cadiStyle, self.cadiLine )
            self.log = self.doSVNCheckout( )

        if self.status != 'OK' :
            os.chdir( self.origDir )
            # make sure the temp dir is always removed ...
            if not self.keepFiles : rmtree( self.tmpDir, ignore_errors=True )

        return

    def showBuildLog(self):
        self.showLogFile( '/data/tmp/%s.buildlog' % self.cadiLine )

def main():

    lbc = CMSLatexBuildChecker()
    log, (runTime, buildTime) = lbc.processOne( sys.argv[1:] )
    for cmd, res in log.items():
        print( '[%s]\n%s\n' % (cmd, res.encode('utf-8', errors='xmlcharrefreplace')) )
    lbc.showBuildLog()

if __name__ == '__main__':
    main()
    sys.exit(0)

