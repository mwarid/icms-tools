
import os
import sys
import datetime
import time
import logging
import json

from flask import Flask
from sqlalchemy.orm.exc import  NoResultFound, MultipleResultsFound

from icms_orm.common import UserAuthorData
from icms_orm.cmspeople import Person as PeopleData

sys.path.append( '.' )
scrPath = './scripts'
if scrPath not in sys.path:
    sys.path.append(scrPath)

import webapp
import app_profiles

def getHRdbInfoFromJson( jsonFileName ) :

    idMap = {}
    noOrcid = 0
    nEx = 0
    with open (jsonFileName, 'r') as jF:

        data = json.load( jF )
        key = list(data.keys())[0]
        itemList = data[ key ]
        
        for item in itemList:
            # print( f'item: {item}')
            hrId = item['PERSON_ID']
            orcId = item['ORCID']
            endDate = datetime.datetime.strptime( item['CONTRACT_END_DATE'][:19], '%Y-%m-%dT%H:%M:%S' ) if item['CONTRACT_END_DATE'] else None  # 2022-12-01T11:01:49.000Z
            if endDate is None or endDate > datetime.datetime.now():
                if not orcId or orcId.strip() == '' :
                    noOrcid += 1
                else:
                    idMap[ hrId ] = orcId
            else:
                nEx += 1

    print( f'found {len(idMap)} valid entries with OrcIDs -- {noOrcid} w/o OrcIDs -- a total of {nEx} are exMembers (endDate in the past)' )

    return idMap

class UserFinder(object):

    def __init__(self, db):
        self.db = db

        self.idMap = {}
        ids = (self.db.session.query( PeopleData.hrId, PeopleData.cmsId).all())
        for h, c in ids:
            self.idMap[ int(h) ] = int(c)


        self.allUsers = { }
        allUsersDB = (self.db.session.query( UserAuthorData.hrid, UserAuthorData.cmsid, UserAuthorData.orcid, UserAuthorData.lastupdate)
                   .filter( UserAuthorData.orcid != '' )
                   .all())

        for hrid, cmsid, orcid, lastupdate in allUsersDB :
            if orcid in self.allUsers.keys( ) : continue
            self.allUsers[ orcid ] = (hrid, cmsid, orcid, lastupdate)

        print( "UserFinder> got %d users with orcids from CMS DB" % len( self.allUsers.keys() ) )

    def findUserByOrcId(self, orcid):

        if orcid.strip( ) == '' :
            return None

        return self.allUsers[ orcid ] if orcid in self.allUsers.keys() else None

    def findUserByHRid(self, hrId):

        for orcId, info in self.allUsers.items():
            if int(hrId) == int(info[0]):
                return info
        return None

def updateOrcIDsFromHRdbInfo(inFileName, db):

    orcIdMap = getHRdbInfoFromJson( inFileName )
    uf = UserFinder(db)

    index = 0
    nNoInfo = 0
    nDone = 0
    nKnown = 0
    for hrId, orcId in orcIdMap.items():
        uInfo = uf.findUserByOrcId( orcId )
        if uInfo: # orcId already in DB, check consistency
            index += 1
            if index<5: print( hrId, orcId, uInfo )
            if orcId != uInfo[2]:
                print( "inconsistency: HRDB orcId %s maps to %s in CMS DB " % (orcId, uInfo) )
        else:
            if uf.findUserByHRid( hrId ) is None:
                if int(hrId) not in uf.idMap.keys():
                    print ( f'user for hrId {hrId} not found in CMS DB ... skipping ')
                    nNoInfo += 1
                    continue
                uad = UserAuthorData(hrid=hrId, cmsid=uf.idMap[ int(hrId) ], inspireid='', orcid=orcId)
                db.session.add( uad )
                nDone += 1
            else:
                nKnown += 1
            
    db.session.commit()

    print( "found %d users w/o inspId " % nNoInfo )
    print( f"added {nDone} users to CMS DB ({nKnown} already known)" )

def main():

    theApp = webapp.Webapp(__name__, '.', app_profiles.ProfileDev)
    with theApp.app_context():
        db = theApp.db

        jsonFileName = 'HRdb-OrcIDs-20230220.json' # './HRdb-20221215.json'
        updateOrcIDsFromHRdbInfo( jsonFileName, db)

    return 0

if __name__ == '__main__':
    retCode = main()
