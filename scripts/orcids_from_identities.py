"""Fetch CMS ORCIDs from the Authorization Service API Identities.

After fetching, compare against the ones stored in our db, detect conflicts
and update the db with any new ones.

Run this script as such:
TOOLKIT_CONFIG=DEV python manage.py script orcids-from-identities

Replace DEV with PREPROD or PROD as necessary.

The logs will be at logs/orcids_from_identities.log, rotated daily
and backups kept for 14 days.

In case of issues (duplicates, conflicts, exceptions), an email is
sent to icms-logs@cern.ch .
"""

import functools
import logging
import sys
import time
from collections import defaultdict
from datetime import datetime
from logging.handlers import TimedRotatingFileHandler
from netrc import netrc
from pathlib import Path
from typing import NamedTuple

import requests
from icms_orm.common import Person, UserAuthorData
from icms_orm.toolkit import EmailMessage
from requests.auth import AuthBase

REQUEST_TIMEOUT = 5  # sec


def log_elapsed_time(func):
    @functools.wraps(func)
    def time_logger_wrapper(*args, **kwargs):
        time_start = time.perf_counter()
        return_val = func(*args, **kwargs)
        time_stop = time.perf_counter()
        elapsed_time = time_stop - time_start
        ScriptLogger().info("Elapsed time: %.2f seconds", elapsed_time)
        return return_val

    return time_logger_wrapper


def log_exceptions(func):
    @functools.wraps(func)
    def exception_logger_wrapper(*args, **kwargs):
        try:
            return_val = func(*args, **kwargs)
        except Exception as exc:
            ScriptLogger().exception("Exception while running %s", __file__)
            email_icms_logs(f"Exception {exc} occured.")
            sys.exit(-1)
        return return_val

    return exception_logger_wrapper


class AuthorizationServiceApiAuth(AuthBase):
    """Bearer token auth for API access"""

    def __init__(self, token):
        self.token = token

    def __call__(self, request):
        request.headers["Authorization"] = f"Bearer {self.token}"
        return request


class AuthorizationServiceApiSession(requests.Session):
    """Authorization service API session handling auth, timeouts and errors"""

    def __init__(self, *, api_token, timeout=5):
        self.timeout = timeout
        super().__init__()
        self.auth = AuthorizationServiceApiAuth(api_token)
        self.hooks["response"].append(lambda r, *args, **kwargs: r.raise_for_status())

    def request(self, method, url, **kwargs):
        if "timeout" not in kwargs:
            kwargs["timeout"] = self.timeout
        return super().request(method, url, **kwargs)


class IdentityOrcidHandler:
    """Fetches ORCIDs from Authorization Service API Identities"""

    @staticmethod
    def get_application_credentials():
        grappa_netrc_host = "grappa.cern.ch"
        data_netrc_file = Path("/data/secrets/.netrc")
        netrc_entry = None
        if data_netrc_file.exists():
            netrc_entry = netrc(data_netrc_file).authenticators(grappa_netrc_host)
        if not netrc_entry:
            home_netrc_file = Path.home() / ".netrc"
            netrc_entry = netrc(home_netrc_file).authenticators(grappa_netrc_host)
        client_id, _, client_secret = netrc_entry
        return client_id, client_secret

    def get_token(self):
        api_token_endpoint = "https://auth.cern.ch/auth/realms/cern/api-access/token"
        client_id, client_secret = self.get_application_credentials()
        token_resp = requests.post(
            api_token_endpoint,
            data={
                "grant_type": "client_credentials",
                "client_id": client_id,
                "client_secret": client_secret,
                "audience": "authorization-service-api",
            },
            headers={"Content-Type": "application/x-www-form-urlencoded"},
            timeout=REQUEST_TIMEOUT,
        )
        token_resp.raise_for_status()
        api_token = token_resp.json()["access_token"]
        return api_token

    def __init__(self):
        self.token = self.get_token()

    @staticmethod
    def extract_orcids_by_person_id(json_resp):
        return {
            identity["personId"]: identity["orcid"]
            for identity in json_resp["data"]
            if "orcid" in identity
        }

    def get_cms_orcids_from_identities(self):
        auth_service_api_base_url = "https://authorization-service-api.web.cern.ch"
        group = "cms-web-access"
        group_identity_endpoint = (
            f"{auth_service_api_base_url}/api/v1.0/Group/{group}/memberidentities"
        )
        with AuthorizationServiceApiSession(
            api_token=self.token, timeout=REQUEST_TIMEOUT
        ) as session:
            first_page = session.get(
                group_identity_endpoint,
                params={
                    "limit": 500,
                    "field": ["personId", "orcid"],
                },
            )
            json_resp = first_page.json()
            orcids_by_person = self.extract_orcids_by_person_id(json_resp)
            total_num_identities = json_resp["pagination"]["total"]
            while json_resp["pagination"]["next"] is not None:
                json_resp = session.get(
                    f"{auth_service_api_base_url}{json_resp['pagination']['next']}"
                ).json()
                orcids_by_person.update(**self.extract_orcids_by_person_id(json_resp))
        return orcids_by_person, total_num_identities


class AuthorDataOrcidHandler:
    """Handles ORCIDs stored in the author_data table"""

    _orcids_by_hr_id = None

    def __init__(self):
        self.session = UserAuthorData.session()
        pg_entries = self.session.query(UserAuthorData.hrid, UserAuthorData.orcid).all()
        self._orcids_by_hr_id = {
            str(hr_id): orcid.strip() for hr_id, orcid in pg_entries
        }

    @property
    def orcids_by_hr_id(self):
        return self._orcids_by_hr_id

    def update_orcid(self, hr_id, orcid):
        hr_id = int(hr_id)
        cms_id = (
            self.session.query(Person.cms_id).filter(Person.hr_id == hr_id).scalar()
        )
        if not cms_id:
            ScriptLogger().warning("HR ID %s has no matching CMS ID!", hr_id)
            return
        new_entry = UserAuthorData(hrid=hr_id, cmsid=cms_id, inspireid="", orcid=orcid)
        self.session.add(new_entry)
        self.session.commit()

    def detect_duplicates(self):
        hr_ids_by_orcid = defaultdict(list)
        for hr_id, orcid in self._orcids_by_hr_id.items():
            hr_ids_by_orcid[orcid].append(hr_id)
        return {
            orcid: hr_ids
            for orcid, hr_ids in hr_ids_by_orcid.items()
            if len(hr_ids) > 1
        }


class ScriptLogger:
    """Helper class to ease logging."""

    _logger = None

    @classmethod
    def setup_logger(cls):
        script_name = __name__.rsplit(".", maxsplit=1)[-1]
        logger = logging.getLogger(script_name)
        logger.propagate = False
        logger.setLevel(logging.INFO)
        file_name = Path("logs") / f"{script_name}.log"
        file_handler = TimedRotatingFileHandler(
            filename=file_name, when="d", interval=1, backupCount=14
        )
        console_handler = logging.StreamHandler()
        formatter = logging.Formatter(
            "%(asctime)s %(name)s [%(levelname)s] %(message)s"
        )
        file_handler.setFormatter(formatter)
        console_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
        logger.addHandler(console_handler)
        logger.abs_file_path = file_name.absolute()
        cls._logger = logger

    def __init__(self):
        if self._logger is None:
            self.setup_logger()

    @property
    def logger(self):
        return self._logger

    @property
    def debug(self):
        return self._logger.debug

    @property
    def info(self):
        return self._logger.info

    @property
    def warning(self):
        return self._logger.warning

    @property
    def error(self):
        return self._logger.error

    @property
    def exception(self):
        return self._logger.exception


class OrcidStats(NamedTuple):
    """Helper to group stats related to the ORCID processing"""

    postgres_duplicates: dict
    num_identities: int
    num_identity_orcids: int
    num_new_orcids: int
    num_existing_orcids: int
    conflicting_orcids: dict


def fetch_update_orcids():
    (
        identity_orcids,
        num_identities,
    ) = IdentityOrcidHandler().get_cms_orcids_from_identities()
    author_data_orcid_handler = AuthorDataOrcidHandler()
    postgres_orcids = author_data_orcid_handler.orcids_by_hr_id
    postgres_duplicates = author_data_orcid_handler.detect_duplicates()
    new_orcids = {}
    num_existing_orcids = 0
    conflicting_orcids = {}
    for person_id, identity_orcid in identity_orcids.items():
        postgres_orcid = postgres_orcids.get(person_id)
        if postgres_orcid is None:
            new_orcids[person_id] = identity_orcid
        elif postgres_orcid == identity_orcid:
            num_existing_orcids += 1
        else:
            conflicting_orcids[person_id] = (identity_orcid, postgres_orcid)
    for hr_id, orcid in new_orcids.items():
        author_data_orcid_handler.update_orcid(hr_id, orcid)
    return OrcidStats(
        postgres_duplicates=postgres_duplicates,
        num_identities=num_identities,
        num_identity_orcids=len(identity_orcids),
        num_new_orcids=len(new_orcids),
        num_existing_orcids=num_existing_orcids,
        conflicting_orcids=conflicting_orcids,
    )


def compose_body_from_stats(orcid_stats: OrcidStats) -> str:
    duplicates = orcid_stats.postgres_duplicates
    conflicts = orcid_stats.conflicting_orcids
    body_list = []
    for orcid, hr_ids in duplicates.items():
        body_list.append(f"Duplicate ORCID {orcid} shared among HR IDs {hr_ids}")
    for hr_id, (identity_orcid, postgres_orcid) in conflicts.items():
        body_list.append(
            f"Conflicting ORCIDs for {hr_id};"
            f" identity: {identity_orcid}; postgres: {postgres_orcid}"
        )
    body = "\n".join(body_list)
    return body


def email_icms_logs(body: str):
    if not body:
        return
    intro_msg = f"The ORCID updater detected the following issues at {datetime.now()}:"
    logs_msg = f"P.S. Logs can be found at {ScriptLogger().logger.abs_file_path}\n"
    full_body = f"{intro_msg}\n\n{body}\n\n{logs_msg}"
    EmailMessage.compose_message(
        sender="icms-support@cern.ch",
        subject="[ORCID Updater] Action needed",
        source_app="toolkit",
        to="icms-logs@cern.ch",
        body=full_body,
        db_session=EmailMessage.session(),
    )
    ScriptLogger().info("Scheduled email for icms-logs with body: %r", full_body)


@log_elapsed_time
@log_exceptions
def main():
    ScriptLogger().info("Starting %s", __file__)
    orcid_stats = fetch_update_orcids()
    for orcid, hr_ids in orcid_stats.postgres_duplicates.items():
        ScriptLogger().warning(
            "Duplicate ORCID %s shared among HR IDs %s", orcid, hr_ids
        )
    for person_id, (
        identity_orcid,
        postgres_orcid,
    ) in orcid_stats.conflicting_orcids.items():
        ScriptLogger().warning(
            "Conflicting ORCIDs detected for person id %s; identity: %s, postgres: %s",
            person_id,
            identity_orcid,
            postgres_orcid,
        )
    ScriptLogger().info(
        "%s / %s identities have ORCIDs: %s new, %s conflicting, %s already known",
        orcid_stats.num_identity_orcids,
        orcid_stats.num_identities,
        orcid_stats.num_new_orcids,
        len(orcid_stats.conflicting_orcids),
        orcid_stats.num_existing_orcids,
    )
    email_icms_logs(compose_body_from_stats(orcid_stats))
