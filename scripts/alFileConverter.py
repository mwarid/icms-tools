#!/usr/bin/env python3

import sys
import os
import shutil
import glob
import subprocess
import logging
import socket
import re
import datetime

logging.basicConfig(format='%(levelname)s:%(funcName)s:%(message)s', level=logging.INFO)

def doCmd(cmd, verbose=False):
    res = None
    ret = -1
    timeOut = 60
    try:
        logging.info('doCmd> going to execute:"%s"' % cmd)
        # res = subprocess.check_output( cmd, shell=True, stderr=subprocess.STDOUT)
        # py38 only: 
        # res = subprocess.run( cmd.split(), capture_output=True, text=True, input='', timeout=timeOut)
        proc = subprocess.Popen( cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE  ) 
        res = proc.communicate(timeout=timeOut)
        # logging.info( f'doCmd> res: {res}' )
        ret = proc.returncode
        if ret != 0:
            logging.error( f'==> retcode from proc: {ret} ' )
    except subprocess.TimeoutExpired:
        proc.kill()
        res = proc.communicate()
        ret = proc.returncode
        logging.error( f'==> cmd {cmd} TIMED OUT after {timeOut} sec -- res: {res[-2500:]} ' )
        return res, ret
    except Exception as e:
        logging.error( f'==> res: {res} ' )
        logging.error("doCmd> ERROR: got: %s" % (str(e),))
    logging.info( f'==> cmd {cmd} returned {ret} ' )

    return res, ret

def processPaperFile( path, code ):
    
    logFileName = '/tmp/alfc.log'
    
    startDir = os.getcwd()
    alfConvPath = '../icms-al-file-converter'
    if not os.path.exists( alfConvPath ):
        cmd = 'cd ..; git clone https://gitlab.cern.ch/cms-icmsweb/icms-al-file-converter.git'
        res = doCmd(cmd, verbose=True)
        logging.info( f'==> cmd {cmd} returned {res} ' )

    os.chdir( alfConvPath )
    logging.info( f'now in {os.getcwd()}' )
    # cmd = f'git pull >{logFileName} 2>&1'
    # res = 'SKIPPED' # doCmd(cmd, verbose=True)
    # logging.info( f'==> cmd {cmd} returned {res} ' )

    for dir in ['xml', 'tex', 'pdf', 'out', 'logs']:
        if not os.path.exists( f'{dir}' ):
            os.makedirs( f'{dir}' )
            logging.info( f'in {os.getcwd()} - created {dir}' )

    cmd = f'cp {path}/{code}-journal-internal-authorlistN.xml xml/{code}-journal-internal-authorlistN.xml'
    res, ret = doCmd(cmd, verbose=True)
    
    scenario = 'C'
    cmd = f'./make_authorlists.sh {code} {scenario}'
    res, ret = doCmd(cmd, verbose=True)
    logging.info( f'==> cmd {cmd} returned {ret} ' )
    if ret == -9:
        logging.error( f'==> cmd {cmd} TIMED OUT !' )

    # INFO:processPaperFile:+=+> found 2350 authors of which 1943 have orchids ( 0.827)
    orcIdRe = re.compile( r'^.*\+=\+> found (\d+) authors of which (\d+) have orchids \( (0\.\d+)\).*$' )
    orcIdInfo = (-1, -1, -1)
    orcIdLines = []
    with open( f'{path}/{code}.log', 'w') as lF:
        for line in res[0].decode().split('\n'):
            if 'ORCIDiD_iconvector.pdf' in line: continue
            lF.write( f'{line.strip()}\n' )
            if '+=+> found ' in line: 
                orcIdLines.append( line.strip() )
                orcIdMatch = orcIdRe.match( line.strip() )
                if orcIdMatch:
                    orcIdInfo = orcIdMatch.groups()
            # logging.info( f'{line.strip()}' )

        # prepare and write out a new row for the web page for this paper:
        msg = '''
      <tr>
        <td> <a href="files/pdf/{code}_C_PLB.pdf" target="blank">{code}</a> </td>
        <td> {nAuth} </td> <td> {nOrcID} </td> <td> {fracOrcID:.2f} </td>
        <td class="prep"> under prep </td>
        <td> {stamp} </td>
      </tr>
'''.format( code=code, nAuth=orcIdInfo[0], nOrcID=orcIdInfo[1], fracOrcID=float(orcIdInfo[2]), stamp=datetime.date.today() )
        logging.info( f'\n{msg}\n' )

        logging.info( f'log written to: {path}/{code}.log')
        for orcIdLine in orcIdLines:
            logging.info( f'{orcIdLine} ' )

    if b'!  ==> Fatal error occurred' in res[0]:
        with open( f'{path}/{code}-error.log', 'w') as lF:
            lF.write( f'{res[1]}' )
            lF.write( '\n================================================================\n')
            lF.write( f'{res[0]}' )
        logging.error( f'ERROR from pdfTeX ! Find the log at: {path}/{code}-error.log' )

    # check for non-ascii chars in the tex file:
    cmd = f'./find_8bit.py ./tex/{code}-authorlist.tex'
    res8, ret8 = doCmd(cmd, verbose=True)
    logging.info( f'==> cmd {cmd} returned {ret8} ' )

    if ret8 != 0 or b'No Unicode characters (x80-xFF) found' not in res8[0]:
        with open( f'{path}/{code}-check8bit-error.log', 'w') as lF:
            lF.write( f'{res8[1]}' )
            lF.write( '\n================================================================\n')
            lF.write( f'{res8[0]}' )
        logging.error( f'ERROR from find_8bit.py ! Find the log at: {path}/{code}-check8bit-error.log' )


    # copy files to final destination
    fqdn = socket.getfqdn()
    if 'cern.ch' in fqdn and 'dyndns' not in fqdn:
        destDir = '/eos/project-c/cmsweb/www/eoswebtest/secr/authorlistParser/authorListCMS-new'
        if 'vocms0190' in fqdn: # update path for -dev
            destDir += 'Dev'
    else:
        destDir = '/tmp/al-files/'
    logging.info( f'Copying files over from {os.getcwd()} to {destDir}')
    shutil.copy( f'xml/{code}-journal-internal-authorlistN.xml', f'{destDir}/{code}-journal-internal-authorlist.xml' )
    shutil.copy( f'xml/{code}-public-authorlistN.xml', f'{destDir}/{code}-public-authorlist.xml')
    shutil.copy( f'tex/{code}-authorlist.html', f'{destDir}/{code}-public-authorlist.html')
    shutil.copy( f'tex/{code}-authorlist.tex', f'{destDir}/{code}-public-authorlist.tex')
    shutil.copy( f'tex/{code}-authorlist.revtex', f'{destDir}/{code}-public-authorlist.revtex')
    shutil.copy( 'pdf/PLB_main.pdf', f'{destDir}/{code}-PLB.pdf' )
    # shutil.copy( 'pdf/APS_main.pdf', f'{destDir}/{code}-APS.pdf' )


    pdfTexOK = ( # (f'Output written on APS_main.pdf' in str(res[0]) ) and 
                 (f'Output written on PLB_main.pdf' in str(res[0]) ) )
    if ( pdfTexOK and (ret8 == 0) ):
        logging.info( f'\n==> pdfTeX run was OK.\n')
        return (0, 'OK')
    else:
        if not pdfTexOK:
            logging.error( f'\n==> pdfTeX run was NOT OK -- errors found: \n')
            msg = [x for x in str(res[0]).split('\n') if "! LaTeX Error: " in x]
            logging.error( f'{msg}\n' )
        else:
            logging.info( f'\n==> pdfTeX run was OK.')
        if ret8 != 0:
            logging.error( f'\n==> NON-ascii characters found \n')
        return (-1, "FAIL")
    
if __name__ == '__main__':
    
    path = sys.argv[1]
    code = sys.argv[2]
    res = processPaperFile( path, code )
    sys.exit( int(res[0]) )
