import hashlib
import hmac
import urllib
import collections
import time
from util import constants as const


IndicoRoom = collections.namedtuple('IndicoRoom', ['id', 'label'])


def indico_get_rooms_info(room_ids, api_key, api_secret):
    # todo: implement the actual fetching
    rooms = []
    for room_id in const.INDICO_ROOM_IDS:
        rooms.append(IndicoRoom(id=room_id, label='Room with id %d' % room_id))
    return rooms


def build_indico_request(path, params, api_key=None, secret_key=None, only_public=False, persistent=False):
    items = list(params.items()) if hasattr(params, 'items') else list(params)
    if api_key:
        items.append(('apikey', api_key))
    if only_public:
        items.append(('onlypublic', 'yes'))
    if secret_key:
        if not persistent:
            items.append(('timestamp', str(int(time.time()))))
        items = sorted(items, key=lambda x: x[0].lower())
        url = '%s?%s' % (path, urllib.parse.urlencode(items))
        signature = hmac.new(bytes(secret_key, 'utf8'), url.encode('utf8'), hashlib.sha1).hexdigest()
        items.append(('signature', signature))  
    if not items:
        return path
    return '%s?%s' % (path, urllib.parse.urlencode(items))