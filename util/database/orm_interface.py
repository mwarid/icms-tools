from icms_orm.orm_interface import IcmsAlchemySession, OrmManager, EngineOption
from util.database.changelog import DbChangesListener
from sqlalchemy import event
import flask


class WebappOrmManager(OrmManager):
    """
    A very thin wrapper around the OrmManager, only extracting the config from the app and adding the necessary engine options.
    Things left to examine: 
    - driver hacks from SQLAlchemy 
    """

    def __init__(self, app=None, session_class=IcmsAlchemySession):
        config = app is not None and app.config or dict()
        OrmManager.__init__(self, config=config, session_class=session_class)
        self.register_engine_option(EngineOption(
            'json_serializer', flask.json.dumps))
        if config.get('DB_CHANGELOG_ENABLED', False) is True:
            self.__register_change_tracking()

    def __register_change_tracking(self):
        @event.listens_for(self.session(), 'before_flush')
        def _anonymous_session_event_listener(session, flush_context, instances=None):
            DbChangesListener.event_listener(session, flush_context)
