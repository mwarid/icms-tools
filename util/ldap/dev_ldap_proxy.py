from typing import Any, Dict, List
from icms_orm.common.common_schema_people_tables import Person
from icms_orm.common.common_schema_views import PersonView
from icmsutils.ldaputils import LdapPerson, LdapProxy
import logging
import sqlalchemy as sa
from sqlalchemy.orm.query import Query


log: logging.Logger = logging.getLogger(__name__)


class DevLdapPerson(LdapPerson):
    def __init__(self, ldap_entry=None):
        super().__init__(ldap_entry or {})

    @classmethod
    def from_person_view(cls, person: PersonView) -> LdapPerson:
        '''
        Skipped fields:
        self.institute
        self.division
        self.section
        self.group
        self.unixHome
        self.officePhone
        self.mobilePhone
        self.postOfficeBox
        '''
        retval: DevLdapPerson = DevLdapPerson()
        retval.cmsEgroups = ['cms-web-access', 'cms-zh']
        retval.accountStatus = 'Active'
        retval.cernEgroups = ['zh']
        retval.hrId = person.hr_id
        retval.familyName = person.last_name
        retval.firstName = person.first_name
        retval.login = person.login
        retval.mail = f'{person.login}@cern.ch'
        retval.fullName = f'{person.first_name} {person.last_name}'
        retval.institute = list(person.affiliations.keys())[0] if person.affiliations is not None else "CERN"
        retval.department = "EP"
        retval.gid = 1399
        retval.office = "40 3-A12"
        return retval

    @classmethod
    def from_dict(cls, data: Dict[str, Any]) -> 'DevLdapPerson':
        retval: DevLdapPerson = DevLdapPerson()
        for key, value in data.items():
            setattr(retval, key, value)
        return retval


class DevLdapProxy(LdapProxy):
    '''
    LdapProxy subclass to be used in DEV/TEST environment.
    It will return a copy of the data already stored in this app's DB
    plus the LDAP_MOCK_ENTRIES from the app configuration
    '''

    def __init__(self, ldap_uri_override=None, retry_max_override=None, retry_delay_override=None, wrapper_reconnect_attempts=None):
        try:
            super().__init__(ldap_uri_override=ldap_uri_override, retry_max_override=0,
                             retry_delay_override=0, wrapper_reconnect_attempts=0)
        except Exception as e:
            log.warning(f'LdapProxy init raised an exception: {e}')
        self._mocks: List[LdapPerson] = []

    def find_people_by_filter(self, filter, attrlist):
        raise NotImplementedError(
            "find_people_by_filter is not implemented by DevLdapProxy")

    def find_people_by_family_name(self, family_name, attrlist=None):
        people: List[PersonView] = PersonView.query.filter(
            PersonView.last_name == family_name).all()
        return [DevLdapPerson.from_person_view(p) for p in people] + [m for m in self._mocks if m.familyName == family_name]

    def find_people_by_gid(self, gid, attrlist=None):
        '''Making the assumption that all our test users have the GID of 1399'''
        if gid == 1399:
            people: List[PersonView] = PersonView.query.all()
            return [DevLdapPerson.from_person_view(p) for p in people] + [m for m in self._mocks if m.gid == gid]
        return []

    def find_people_by_hrids(self, hrids, account_types=LdapProxy.AccountType.PRIMARY) -> List[LdapPerson]:
        people: List[PersonView] = PersonView.query.filter(
            PersonView.hr_id.in_(hrids)).all()
        return [DevLdapPerson.from_person_view(p) for p in people] + [m for m in self._mocks if m.hrId in hrids]

    def get_person_by_hrid(self, hrid, account_types=LdapProxy.AccountType.PRIMARY):
        return self.find_people_by_hrids([hrid], account_types)[0]

    def get_person_by_login(self, login, account_types=LdapProxy.AccountType.PRIMARY):
        for m in self._mocks:
            if m.login == login:
                return m
        return DevLdapPerson.from_person_view(PersonView.query.filter(PersonView.login == login).one())

    def get_person_by_mail(self, email, account_types=LdapProxy.AccountType.PRIMARY):
        for m in self._mocks:
            if m.mail == email:
                return m
        log.debug(f'Looking for a user going by email: "{email}"')
        query: Query = PersonView.query.join(Person, Person.cms_id == PersonView.cms_id).\
            filter(sa.or_(
                Person.email == email,
                Person.login == email.replace('@cern.ch', '')))
        return DevLdapPerson.from_person_view(query.one())

    def register_mocks(self, data: List[Dict[str, Any]]):
        for entry in data:
            self._mocks.append(DevLdapPerson.from_dict(entry))
