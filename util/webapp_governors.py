from icmscommon import ConfigGovernor
import flask
from util import constants as const


class WebappConfigGovernor(ConfigGovernor):

    def __init__(self, app: flask.Flask):
        self._app = app
        self._config = app.config

    def _check_test_mode(self) -> bool:
        return self._config.get(const.OPT_NAME_TESTING)

    def _get_db_owner_pass(self):
        pass

    def _get_db_owner_user(self):
        pass

    def _get_epr_db_string(self):
        return self._get_db_string('epr')

    def _get_db_string(self, db: str) -> str:
        _binds = self._config.get(const.OPT_NAME_BINDS)
        if 'epr' in db.lower():
            return _binds.get(const.OPT_NAME_BIND_EPR)
        elif 'common' in db.lower():
            return _binds.get(const.OPT_NAME_BIND_COMMON)
        elif 'legacy' in db.lower():
            return _binds.get(const.OPT_NAME_BIND_LEGACY)
        elif 'toolkit' in db.lower():
            return _binds.get(const.OPT_NAME_BIND_TOOLKIT)
        raise ValueError('Failed to determine the DB string for %s DB' % db)
