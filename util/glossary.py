from icms_orm.cmspeople import Person, PeopleFlagsAssociation, MemberActivity, User, MoData, Flag
from icms_orm.cmsanalysis import CadiAnalysis, CadiHistory, CadiAnalysisAnalysts as Analyst, AWG, AWGConvener
from icms_orm.cmsanalysis import Note as AnalysisNote
from icms_orm.old_notes import Note as OldNote
from icms_orm.toolkit import Voting
from icms_orm.epr import TimeLineInst as TLI
from icms_orm import c2k
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.sql.elements import Label, ColumnClause


CMS_ID = 'CMS id'
USER_FULL_NAME = 'Full name'
INST_CODE = 'Institute'
USER_CMS_ACTIVITY = 'Activity'
VOTING = 'Voting'
DATE = 'Date'
DELEGATION_DEADLINE = 'Deadline'
DELEGATE = 'Delegate'
ACTIONS = 'Actions'
STATUS = 'Status'

APPLICATION_PASSED = 'Passed'
APPLICATION_FAILED = 'Failed'
APPLICATION_REASON = 'Reason'
APPLICATION_DAYS = 'Days'
APPLICATION_USER_WORK_DONE = 'Personal work done'
APPLICATION_INST_WORK_DONE = 'Institute work done'
APPLICATION_INST_WORK_REQUIRED = 'Institute work needed'
APPLICATION_SLIPTHROUGH = 'Possible slipthrough'

MSG_INSUFFICIENT_PRIVILEGES = 'Your privileges were insufficient to see the requested page. You have been redirected here instead.'
MSG_LOGIN_FAILED = 'Wrong credentials provided!'
MSG_LOGOUT_SUCCESSFUL = 'Logout successful.'
MSG_DELEGATION_CANCELLED = 'Your vote delegation to %s for %s has been cancelled.'
MSG_DELEGATION_CANNOT_CANCEL = 'Cannot cancel this delegation. Reason: %s'

VALIDATION_ERROR_PERSON_NOT_FOUND = 'Specified person could not be found in the database.'
VALIDATION_ERROR_INST_NOT_FOUND = 'Specified institute could not be found in the database.'
VALIDATION_ERROR_NOTE_NOT_FOUND = 'Specified note could not be found in the database'
VALIDATION_ERROR_DELEGATED_ALREADY = 'You have no vote to delegate (you might have delegated your vote already or you might not be eligible to vote).'
VALIDATION_ERROR_DELEGATE_A_CBI = 'Specified delegate happens to be a CBI and is therefore unavailable to delegate to.'
VALIDATION_ERROR_DELEGATE_BUSY = 'Specified delegate is already casting a vote.'
VALIDATION_ERROR_DELEGATE_LONG_TERM_PROXY = 'Specified delegate acts as a long-term proxy and is therefore unavailable to delegate to.'
VALIDATION_ERROR_DELEGATE_LONG_TERM_PROXY_ALREADY_IN_PLACE = 'You have already designated a long-term proxy. Please cancel your existing delegation and try again.'


def demystify(key):
    """
    Attempts to translate some commonly used dictionary keys into more user-friendly terms
    :return:
    """
    if isinstance(key, InstrumentedAttribute):
        return demystify(c2k(key))
    elif isinstance(key, Label):
        return key.name
    elif isinstance(key, ColumnClause):
        return demystify._data.get(key.name, key.name.replace('_', ''))
    return demystify._data.get(key, key)

demystify._data = {
    'notes_count': 'Notes Count',
}


for k, v in [
    (Person.cmsId, 'CMS ID'), (PeopleFlagsAssociation.flagId, 'Flag'), (Person.lastName, 'Last Name'),
    (Person.firstName, 'First Name'), (Person.status, 'CMS Status'), (Person.isAuthor, 'Author'),
    (MemberActivity.name, 'Activity'), (User.email1, 'Email'), (User.email2, 'Email'), (User.emailCern, 'Email'),
    (Person.instCode, 'Institute'), (CadiHistory.updateDate, 'Update Time'), (CadiAnalysis.code, 'CadiLine'),
    (CadiHistory.newValue, 'New Value'), (CadiHistory.updaterName, 'Updated By'), (CadiHistory.updaterId, 'Updater CMS ID'),
    (Voting.code, 'Voting Code'), (Voting.title, 'Voting Subject'), (Voting.type, 'Voting Type'),
    (Voting.start_time, 'Voting Start'), (Voting.end_time, 'Voting End Time'),
    (Voting.delegation_deadline, 'Deadline for Delegations'),
    (TLI.year, 'Year'), (TLI.timestamp, 'Date'), (TLI.code, 'Inst Code'), (TLI.cmsStatus, 'CMS Membership'),
    (Flag.id, 'Flag Name'), (Flag.desc, 'Flag Description'), (Analyst.cmsid, 'CMS ID'), (Analyst.status, 'Status'),
    (AWG.status, 'AWG Status'), (AWG.fullName, 'Full Name'), (AWG.name, 'Name'), (AWG.type, 'Type'),
    (AWG.createDate, 'Creation date'), (AWG.creatorName, 'Creator'), (AWGConvener.status, 'Status'),
    (AnalysisNote.cmsNoteId, 'CMS Note ID'), (OldNote.cmsNoteId, 'CMS Note ID'), (OldNote.title, 'Title'),
    (CadiAnalysis.name, 'Title'), (CadiAnalysis.status, 'Status'),
    (Person.isAuthorSuspended, 'EPR Suspended'), (Person.project, 'Project')
]:
    demystify._data[c2k(k)] = v


for year in range(2015, 2021):
    mo_col = getattr(MoData, MoData.mo2020.key.replace('2020', '%d' % year))
    free_mo_col = getattr(MoData, MoData.freeMo2020.key.replace('2020', '%d' % year))

    demystify._data[c2k(mo_col)] = 'MO %d' % year
    demystify._data[c2k(free_mo_col)] = 'Free MO %d' % year