"""
Objects in this module are used throughout the project as simple data containers to facilitate passing the information
around.
"""

import abc
from typing import Optional
import flask

from util import constants
from webapp.routing import VueRoutes
from dataclasses import dataclass


@dataclass
class Credentials:
    username: Optional[str]
    password: Optional[str]
    email: Optional[str]

    def extract_username_from_email(self):
        return self.email.split('@')[0]

    def is_empty(self):
        return self.username is None and self.email is None

    @classmethod
    def from_string(cls, identifier: str):
        return '@' in identifier and cls.for_email(identifier) or cls.for_username_password(identifier, None)

    @classmethod
    def for_email(cls, email: str):
        return Credentials(None, None, email)

    @classmethod
    def for_username_password(cls, username: str, password: Optional[str] = None):
        return Credentials(username, password, None)


class Table(object):
    """
    A simple class used to pass the collections of data to the views
    """
    def __init__(self, keys=None, vals=None, tips=None, urls=None, actions=None, form_action=None, styles=None, html_class=None,
                 value_processors=None):
        """
        :param keys: a single row containing the column headers in the order they are to be displayed
        :param vals: array of dictionaries, each dictionary is one table row
        :param tips: array of tooltips for corresponding rows, ignored when empty
        :param urls: array of urls used to create links from each row, ignored when empty
        :param styles: array of style keywords (classes?) used to decorate cells
        :param value_processors: a mapping of table keys to lambdas (taking 2 args: cell value and whole row) meant to convert the value into something derived from it, like a link

        """
        self.keys = keys or []
        self.vals = vals or []
        self.tips = tips or []
        self.urls = urls or []
        self.styles = styles or []
        self.actions = actions or []
        self.form_action = form_action
        self.column_classes = {}
        self.html_class = html_class
        self.processors = value_processors or {}

    def __del__(self):
        del self.keys
        del self.vals
        del self.tips
        del self.urls
        del self.actions
        del self.column_classes

    def row_sets(self):
        for i in range(0, len(self.vals)):
            val = self.vals[i]
            tip = self.tips[i] if self.tips else None
            url = self.urls[i] if self.urls else None
            actions = self.actions[i] if self.actions else None
            styles = self.styles[i] if self.styles else None
            yield val, tip, url, actions, styles

    def set_col_class(self, col, the_class):
        self.column_classes[col] = the_class

    def get_col_class(self, col):
        return self.column_classes.get(col, None)

    @staticmethod
    def get_empty(keys=None):
        return Table(keys or [], [], [], [], [])

    def append_values_from_db_row(self, db_row):
        self.vals.append({key: self.processors.get(key, lambda x, y: x)(db_row[i], db_row) for i, key in enumerate(self.keys)})


class HtmlElement(abc.ABC):
    def to_html(self, *args, **kwargs):
        return self.render(*args, **kwargs)

    @abc.abstractmethod
    def render(self, *args, **kwargs):
        ...

    def _classes_substr(self, extra_classes=[]):
        classes = (hasattr(self, 'classes') and getattr(self, 'classes') or []) + extra_classes
        if classes:
            return ' class="%s" ' % (' '.join(classes))
        return ''


class Endpoint(object):
    """
    Wrapper for endpoint descriptions
    """

    def __init__(self, view_method, blueprint=None, **kwargs):
        self.view_method = view_method.__name__ if callable(view_method) else view_method
        self.blueprint = blueprint
        self.kwargs = kwargs

    def url(self):
        endpoint = ''
        if self.blueprint:
            endpoint = '%s.%s' % (self.blueprint, self.view_method)
        else:
            endpoint = self.view_method
        return flask.url_for(endpoint, **self.kwargs)

    def __str__(self):
        return self.url()


class Link(HtmlElement):
    """
    Wrapper for a link to be rendered in Jinja
    There are 3 distinct types:
    1. And ordinary link - user clicks and gets redirected to the target location
    2. A link to "passive" content to be displayed in a modal - it gets fetched,
    pumped into some container and then the container gets displayed
    3. A link to "active" (including JS) content that needs to be loaded into a placeholder element an from there on
    it's "self-displaying" thanks to the JS fetched with the request
    The difference between 2 and 3 is that the former needs to be wrapped in some call, like onCLick="loadAndShowSomething()"
    whereas the latter merely needs to be fetched and put into a predetermined div. Well, that could also be wrapped in a function
    like "loadAndStoreSomething()" so the difference boils down to the content of that function and whether or not it triggers the modal
    to popup or leaves it up to the fetched content to do so.
    """

    class Type(object):
        REDIRECT = 'LINK_TYPE_REDIRECT'
        MODAL_PASSIVE = 'LINK_TYPE_MODAL_PASSIVE'
        MODAL_ACTIVE = 'LINK_TYPE_MODAL_ACTIVE'
        REVEAL_HIDDEN = 'LINK_TYPE_REVEAL_HIDDEN'

    def __init__(self, href, label, type=None, **kwargs):
        self.href = href
        self.label = label
        self.type = type if type is not None else Link.Type.REDIRECT
        self.options = kwargs or {}

    def render(self, extra_classes=[]):
        class_substr = HtmlElement._classes_substr(self, extra_classes=extra_classes)
        if self.type == Link.Type.REDIRECT:
            return '<a %s href="%s">%s</a>' % (class_substr, self.href, self.label)
        elif self.type == Link.Type.MODAL_PASSIVE:
            return '<a {classes} href="#" onClick="{onclick}">{label}</a>'. \
                format(classes=class_substr, label=self.label, onclick='%s(\'%s\', \'%s\')' % (constants.JS_FN_NAME_MODAL_LOADER, self.label, self.href))
        elif self.type == Link.Type.MODAL_ACTIVE:
            return '<a {classes} href="#" onClick="{onclick}">{label}</a>'. \
                format(label=self.label, classes=class_substr,
                       onclick="$.ajax({url: '%s', success: function(data){$('#%s').html(data);}})" % (self.href, constants.HTML_ID_MODAL_CONTAINER))
        elif self.type == Link.Type.REVEAL_HIDDEN:
            return '<a {classes} href="#" onClick="{onclick}">{label}</a>'.format(classes=class_substr,
                onclick="%s('%s', '%s');" % (constants.JS_FN_NAME_REVEAL_HIDDEN, self.options.get('title', self.label), self.href), label=self.label)

        return 'Unsupported link type "%s"!' % self.type

    @classmethod
    def link_to_user_profile(cls, cms_id, label=None):
        from blueprints.users import views as user_views
        href = VueRoutes.user_profile(cms_id)
        label = label or cms_id
        return cls(href, label)

    @classmethod
    def link_to_cadi_line(cls, code, label=None):
        code = code.startswith('d') and code[1:] or code
        return cls('http://cms.cern.ch/iCMS/analysisadmin/cadilines?line=%s' % code, label or code)

    @classmethod
    def link_reveal_hidden(cls, hidden_element_id, label=None, **kwargs):
        return cls(href=hidden_element_id, label=(label or hidden_element_id), type=Link.Type.REVEAL_HIDDEN, **kwargs)

    @classmethod
    def link_to_user_history(cls, cms_id, label=None):
        from blueprints.users import views as user_views
        return cls(label=label or cms_id, href=flask.url_for(
            '%s.%s' % (constants.BP_NAME_USERS, user_views.route_modal_history.__name__), cms_id=cms_id),
             type=Link.Type.MODAL_PASSIVE)

    @classmethod
    def intercepted(cls, href, label, title, message=None):
        return Link(label=label, href=flask.url_for(constants.ROUTE_NAME_INTERCEPT_AND_CONFIRM, **{
            constants.REQUEST_ARG_ENDPOINT: href,
            constants.REQUEST_ARG_TITLE: title,
            constants.REQUEST_ARG_MESSAGE: message
        }), type=Link.Type.MODAL_ACTIVE)

    @classmethod
    def link_to_inst_info(cls, inst_code, label=None):
        href = VueRoutes.inst_profile(inst_code)
        label = label or inst_code
        return cls(href, label)

    @classmethod
    def link_to_account_status(cls, hrId, label=None):
        href = flask.url_for('%s.route_account_status' % constants.BP_NAME_USERS, hr_id=hrId)
        label = label or hrId
        return cls(href, label)

    def __lt__(self, other):
        return self.label < other.label

    def __gt__(self, other):
        return self.label > other.label


class Checkbox(HtmlElement):
    def __init__(self, name, label='', checked=False, id=None):
        self.label = label
        self.name = name
        self.is_checked = checked
        self.id = id or 'checkbox_%s' % name

    def render(self):
        return '<label><input type="checkbox" name="{name}" id="{id}" {checked} value=""> {label}</label></div>' \
            .format(label=self.label, checked=(self.is_checked and 'checked' or ''), name=self.name, id=self.id)


class InputField(HtmlElement):
    def __init__(self, name, label='', value=None, type='text'):
        self.type = type
        self.value = value
        # not supported as of now
        self.label = label
        self.name = name

    def render(self):
        return '<input name="{name}" type="{type}" value="{value}">'.format(type=self.type, value=self.value, name=self.name)


class PreformatedText(HtmlElement):
    def __init__(self, text):
        self.text = text

    def render(self):
        return '<pre>%s</pre>' % self.text
