from enum import Enum

FLASK_FLASH_SUCCESS = "success"
FLASK_FLASH_INFO = "info"
FLASK_FLASH_WARNING = "warning"
FLASK_FLASH_DANGER = "danger"

FLASK_FLASH_TYPES = [
    FLASK_FLASH_SUCCESS,
    FLASK_FLASH_INFO,
    FLASK_FLASH_WARNING,
    FLASK_FLASH_DANGER,
]

# these roles map to their corresponding flags in the iCMS DB
ROLE_ADMIN = 0x1
ROLE_CBI = 0x2
ROLE_CBD = 0x4
ROLE_SECRETARIAT = 0x8
ROLE_MGT_BOARD = 0x10

# Some iCMS flags
FLAG_EXECUTIVE_BOARD = "COM_eb"
FLAG_FINANCE_BOARD = "COM_fb"
FLAG_FINANCE_BOARD_EXTENDED = "COM_fball"
FLAG_AUTHORSHIP_BOARD = "COM_ab"
FLAG_AUTHORSHIB_BOARD_AUB = "COM_aub"
FLAG_MANAGEMENT_BOARD = "COM_mb"
FLAG_FINANCE_BOARD = "COM_fb"
FLAG_SPOKESPERSON_AND_DEPUTIES = "COM_sp"
FLAG_ICMS_ADMIN = "ICMS_admin"
FLAG_ICMS_GEN_ADMIN = "ICMS_genadmin"
FLAG_PROJECT_MANAGER = "MISC_pm"
FLAG_AUTHOR_NO = "MISC_authorno"
FLAG_INST_REP = "CB_cbi"
FLAG_INST_DEP = "CB_cbd"


# Commonly used directory name
DIR_NAME_DATA = "data"
DIR_NAME_AUTHOR_APPLICATION_CHECKS = "author_app_check"
DIR_NAME_LOGS = "logs"
DIR_NAME_TEST = "test"
SUBDIR_NAME_UNIT_TESTS = "unit_cases"
DIR_NAME_RSC = "rsc"

# Useful file names
FILE_NAME_CMSPEOPLE_DUMP = "CMSPEOPLE.sql"
FILE_NAME_CMSPEOPLE_BZIP = "CMSPEOPLE.sql.bz2"
SUBDIR_NAME_EPR = "epr_db"
FILE_NAME_EPR_BZIP = "icms-epr.db_dumps.tar.bz2"


# Bind-keys
# OPT_NAME_BIND_PEOPLE = 'icms_people_db'
OPT_NAME_BIND_LEGACY = "icms_legacy_db"
OPT_NAME_BIND_TOOLKIT = "icms_toolkit_db"
OPT_NAME_BIND_EPR = "icms_epr_db"
OPT_NAME_BIND_COMMON = "icms_common_db"
# OPT_NAME_BIND_CADI = 'icms_cadi_db'

# Names of top-level configuration options
OPT_NAME_BINDS = "SQLALCHEMY_BINDS"
OPT_NAME_TESTING = "TESTING"
OPT_NAME_ENV = "ENV"
OPT_NAME_ADMIN_LOGINS = "ADMIN_LOGINS"
OPT_NAME_ADMIN_CMSIDS = "ADMIN_CMS_IDS"
OPT_NAME_IGNORE_PASSWORD = "LOGIN_IGNORE_PASSWORD"
OPT_NAME_LOGGING_LEVEL = "LOGGING_LEVEL"
OPT_NAME_INDICO_TOKEN = "INDICO_TOKEN"
OPT_NAME_INDICO_SECRET = "INDICO_SECRET"
OPT_NAME_ENVIRONMENT_VARIABLES = "ENVIRONMENT_VARIABLES"
OPT_NAME_PRODUCTION_HOSTNAMES = "PRODUCTION_HOSTNAMES"
OPT_NAME_SCHEMA_NAMES = "SCHEMA_NAMES"

# Default development database parameters. Others may well be used, but why?
DB_DEFAULT_DEV_USERNAME = "icms_dev"
DB_DEFAULT_DEV_HOSTNAME = "localhost"
DB_DEFAULT_DEV_NAME_PEOPLE = "CMSPEOPLE"
DB_DEFAULT_DEV_NAME_ADDONS = "ICMS_ADDONS"
DB_DEFAULT_DEV_NAME_EPR = "ICMS_EPR"

# Possible values for the top-level ENV configuration option
ENV_NAME_TEST = "testenv"
ENV_NAME_PROD = "prodenv"
ENV_NAME_DEVEL = "develenv"

# Names that the blueprints are registered with
BP_NAME_ADMIN = "admin"
BP_NAME_API = "api"
BP_NAME_API_RESTPLUS = "api_restplus"
BP_NAME_USERS = "users"
BP_NAME_CADI_VIEWER = "cadi_viewer"
BP_NAME_DASHBOARD = "dashboard"
BP_NAME_AUTHOR_LISTS = "author_lists"
BP_NAME_GENERAL_MANAGEMENT = "general_management"
BP_NAME_INSTITUTE = "institute"

# Names and values of some app-wide routes

# Row actions & titles
ROW_ACTION_DELETE = "ROW_ACTION_DELETE"
ROW_ACTION_EDIT = "ROW_ACTION_EDIT"
ROW_ACTION_INFO = "ROW_ACTION_INFO"
ROW_ACTION_EMAIL = "ROW_ACTION_EMAIL"
ROW_ACTION_EMAIL_REMINDER = "ROW_ACTION_EMAIL_REMINDER"
ROW_HEADER_DELETE = "ROW_HEADER_DELETE"
ROW_HEADER_EDIT = "ROW_HEADER_EDIT"
ROW_HEADER_INFO = "ROW_HEADER_INFO"
ROW_HEADER_EMAIL = "ROW_HEADER_EMAIL"
ROW_HEADER_EMAIL_REMINDER = "ROW_HEADER_EMAIL_REMINDER"

# HTML CLASS\ID FOR TYPICAL ELEMENTS
HTML_CLASS_CMS_MEMBER_SEARCH = "CmsMemberSearchField"
HTML_CLASS_CMS_MULTI_MEMBER_SEARCH = "CmsMultiMemberSearchField"
HTML_CLASS_CMS_INST_SEARCH = "CmsInstSearchField"
HTML_ID_MODAL_CONTAINER = "modal_content_container"
HTML_KEY_AUTOCOMPLETE_ENDPOINT = "ICMS_TOOLKIT_AUTOCOMPLETE_OPTS_ENDPT"
HTML_KEY_DELIMITER = "ICMS_TOOLKIT_DELIMITER"

# SOME DATABASE STRINGS
DB_STR_LONG_TERM_PROXY = "Long-Term proxy"


# EXTERNAL URLS
LINK_ICMS_PRJLIST = "http://cms.cern.ch/iCMS/jsp/gen/admin/prjlist.jsp"


# MAIN MENU ENTRIES
MENU_TOP_HOME = "iCMS toolkit"

MENU_GROUP_AUTHOR_LISTS = "Author Lists"
MENU_ITEM_EXT_AL = "External Authors List"
MENU_ITEM_GEN_AL_DB = "Generate DB Author List"
MENU_ITEM_GEN_AL_FILES = "Generate Author List Files"
MENU_ITEM_EXT_AL_FLAGS = "Ext AL: flags"
MENU_ITEM_EXT_AL_PROJECTS = "Ext AL: projects"
MENU_ITEM_PENDING_AUTHORS = "Pending Authors"

MENU_GROUP_GENERAL_MANAGEMENT = "General Management"
MENU_SUBGROUP_BOOKING = "Room Booking"
MENU_ITEM_BOOKING_INDICO = "Reserve with Indico"
MENU_ITEM_BOOKING_OVERVIEW = "Room Booking"
MENU_ITEM_CMS_WEEKS = "CMS Weeks"
MENU_ITEM_BOOKING_REQUEST = "Room Request"
MENU_ITEM_PROJECT_MANAGERS = "Project Managers"
MENU_ITEM_TIMELINES_DOCTOR = "Timelines Doctor"

MENU_GROUP_DATA_VIEWS = "Data Views"
MENU_GROUP_CADI_VIEWS = "CADI Views"
MENU_ITEM_SEARCH = "Search"
MENU_GROUP_USER = "User"
MENU_GROUP_VOTING = "Voting"


# SESSION_KEY
SESSION_KEY_REAL_CMS_ID = "REAL_CMS_ID"

# ROUTES
ROUTE_NAME_HOME = "route_vue"
ROUTE_NAME_INTERCEPT_AND_CONFIRM = (
    "THIS_SHOULD_BE_SET_WHERE_THE_INTERCEPTION_ROUTE_IS_DEFINED"
)

# REQUEST_ARGS (sometimes passed around to dynamically render some intermediate page (like a confirmation dialog)
REQUEST_ARG_ENDPOINT = "arg_real_endpoint"
REQUEST_ARG_TITLE = "arg_title"
REQUEST_ARG_MESSAGE = "arg_message"


INDICO_ROOM_IDS = tuple(
    sorted(
        (
            51,
            52,
            47,
            48,
            43,
            46,
            210,
            176,
            290,
            291,
            142,
            146,
            23,
            318,
            177,
            57,
            184,
            28,
            15,
            10,
            33,
            49,
            36,
            220,
            201,
            282,
            263,
            35,
            58,
            50,
            54,
            295,
            8,
            207,
        )
    )
)
NUMBER_OF_SUGGESTED_RESULTS = 10


# Profiles are called configs here - not great
ENV_VAR_NAME_CONFIG = "TOOLKIT_CONFIG"
ENV_VAR_CONFIG_DEV = "DEV"
ENV_VAR_CONFIG_TEST = "TEST"
ENV_VAR_CONFIG_PREPROD = "PREPROD"
ENV_VAR_CONFIG_PROD = "PROD"


class ScriptJobName(Enum):
    CHECK_APPS = "check_applications"
    DB_DEV_SETUP_TOOLKIT = "toolkit_db_devdata"
    DB_DEV_SETUP_EPR = "epr_db_devdata"
    GENERATE_PAPER_AUTHOR_LISTS = "generatePaperAuthorLists"
    UPDATE_AL_STATUS = "update_al_status"
    TEST_UPDATE_AL_STATUS = "test_update_al_status"
    CHECK_NEW_MEMBER_ACCOUNTS = "checkNewCMSmemberAccounts"
    CADI_REMINDERS = "cadi_reminders"
    FILL_FOOH = "fill_fora_only_on_hypernews"
    GENERATE_PLWJALP = "genPeopleListWhoJoinAndLeftProject"
    ORCIDS_FROM_IDENTITIES = "orcids-from-identities"
    PROJECT_MEMBER_STATS = "project-member-stats"
    RENAME_INSTITUTE_SQL = "rename-institute-sql"
    SEND_REMINDER_TO_NOMINEE_RESPONSE = "send-reminder-to-nominee-response"


#
JS_FN_NAME_MODAL_LOADER = "fillAndShowModal"
JS_FN_NAME_REVEAL_HIDDEN = "revealHidden"

# DATA TABLE CLASSES
DATA_TABLE_CLASS_LIGHT = "lightDataTable"

# JOB OPENING STATUSES
JOB_STATUS_ACTIVE = "active"
JOB_STATUS_CLOSED = "closed"

# JOB NOMINATION STATUSES
JOB_NOMINATION_ACCEPTED = "accepted"
JOB_NOMINATION_REJECTED = "rejected"
JOB_NOMINATED = "nominated"
