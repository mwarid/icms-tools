#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys

import logging
import argparse

from icmsutils.ldaputils import LdapProxy
from icms_orm.cmspeople import Person
try:
    from util.EgroupHandler import EgroupHandler
except ModuleNotFoundError:
    from EgroupHandler import EgroupHandler

from icms_orm.common import EmailMessage, EmailLog

# create logger
logger = logging.getLogger('cms_account_checker_logger')
logger.setLevel(logging.INFO)

class CMSAccountCheckerBase(object):

    def __init__(self, hrId, verbose):
        
        self.gidMap = { 'zh' : 1399, # CMS
                        'zj' : 1468, # Totem
                        'zp' : 1307, # Atlas
                        'z5' : 1470, # LHCb
                        'z2' : 1395, # Alice
                      }

        self.exptMap = { 1399 : 'CMS',
                         1468 : 'Totem',
                         1307 : 'Atlas',
                         1470 : 'LHCb',
                         1395 : 'Alice',
                       }
        self.otherExptGroups = [ 'zp', 'z5', 'z2' ] 

        self.issues = { 'info' : [], 'error' : [] }
        
        self.hrId = hrId
        self.verbose = verbose

        self.account = None
        self.secAccounts = []

        logger.debug('\nset up for check of hrId %s account %s - verbose: %s\n' % (self.hrId, self.account, self.verbose) )

        # default: primary account in zh/zj ...
        self.zhPrimaryOK = False

        # ... and no secondary account in zh/zj ...
        self.zhSecondaryOK = False

        # ... and no accounts in other experiments:
        self.otherExptOK = False
        self.otherExptMemberList = []
        return

    def check(self):
        self.checkPrimaryInZH()
        self.checkSecondaryInZH()

        self.otherExptOK = True
        self.checkPrimaryInOther()
        self.checkSecondaryInOther()

    def checkPrimaryInZH(self):
        try:
            res, login = self._checkIdInGroup('zh')
            if res:
                self.zhPrimaryOK = True
        except KeyError:
            raise
        except Exception as e:
            logger.error('unknown exception when checking if primary for hrId %s account %s in zh - got : %s' % (self.hrId, self.account, str(e)) )
            raise

    def checkSecondaryInZH(self):
        self.zhSecondaryOK = True
        for secondary in self.secAccounts:
            try:
                res, login = self._checkSecAccountInGroup(secondary, 'zh')
                logger.debug("check for %s in zh returns: %s" % (secondary, str(res) ) )
                if res:
                    self.zhSecondaryOK = False
                    # logger.debug("set zhSecOK to false for check for %s in zh returns: %s" % (secondary, str(res) ) )
            except KeyError:
                raise
            except Exception as e:
                logger.error('unknown exception when checking if secondary account %s for hrId %s in zh - got : %s' % (secondary, self.hrId, str(e)) )
                raise
   
    def checkPrimaryInOther(self):

        for otherGrp in self.otherExptGroups:
            try:
                res, login = self._checkIdInGroup(otherGrp)
                if res:
                    self.otherExptOK = False
                    self.otherExptMemberList.append( self.exptMap[ self.gidMap[otherGrp] ] )
            except KeyError:
                raise
            except Exception as e:
                logger.error('unknown exception when checking if primary account for hrId %s account %s in other expt (%s) - got : %s' % (self.hrId, self.account, otherGrp, str(e)) )
                raise

    def checkSecondaryInOther(self):
        for otherGrp in self.otherExptGroups:
            for secondary in self.secAccounts:
                try:
                    res, login = self._checkSecAccountInGroup(secondary, otherGrp)
                    if res:
                        self.otherExptOK = False
                        self.otherExptMemberList.append( self.exptMap[ self.gidMap[otherGrp] ] )
                except KeyError:
                    raise
                except Exception as e:
                    logger.error('unknown exception when checking if secondary account %s for hrId %s in other expt (%s)  - got : %s' % (secondary, self.hrId, otherGrp, str(e)) )
                    raise
   
    def _checkIdInGroup( self, group ):
        # dummy implementation in base class ... 
        pass

    def _checkSecAccountInGroup( self, account, group ):
        # dummy implementation in base class ... 
        pass

class CMSLdapAccountChecker(CMSAccountCheckerBase):

    def __init__(self, hrId, verbose):
        super().__init__(hrId, verbose )
        self.accountEnabled = False
        self.ldp = LdapProxy.get_instance()
        self.results = None

    def getLdapInfo(self):
        if not self.results:
            self.results = self.ldp.find_people_by_hrids( [self.hrId] )
        if len(self.results) > 1:
            logger.warning("more than one result found in LDAP for hrId %s" % self.hrId )
        
        if len(self.results) == 0:
            logger.error("NO result found in LDAP for hrId %s" % self.hrId )
            return 

        # cut to first one:
        self.results = self.results[0]

        self.account = self.results.login
        return

    def getSecondaryAccounts(self):
        filter = '(&(employeeID=%s)(employeeType=Secondary))' % self.hrId
        self.secAccounts = [ x.login for x in self.ldp.find_people_by_filter( filter ) ]
        if self.verbose : 
            logger.info('found %d secondary accounts for hrId %s: %s ' % (len(self.secAccounts), self.hrId, ', '.join(self.secAccounts) ) )

        return

    def checkaccountEnabled(self):
        self.getLdapInfo()
        
        if not self.results: 
            logger.warning('no account found, can not check ... ')
            return

        # if no account set yet, set it
        if not self.account:
            self.account = self.results[0].login
        
        self.accountEnabled = self.results.accountStatus == 'Active'
        return 

    def _checkIdInGroup( self, eGroupName ):
        logger.debug('going to check LDAP for hrId %s... ' % self.hrId )

        self.results = self.ldp.find_people_by_hrids( [self.hrId] )
        
        if len(self.results) > 1:
            logger.warning("more than 1 entry (%d) found for %s: %s" % (len(self.results), self.hrId, str(self.results)) )
        
        if not self.results:
            logger.debug('search found no result in LDAP for primary account with hrId %s... ' % self.hrId )
            return False, None

        gid = self.results[0].gid
        logger.debug('found gid %s (%s) for hrId %s in LDAP info ' % (gid, type(gid), self.hrId) )
        if gid != self.gidMap[eGroupName]:
            return False, self.results[0].login
        
        return True, self.results[0].login

    def _checkSecAccountInGroup( self, account, eGroupName ):
        logger.debug('going to check LDAP for hrId %s... ' % self.hrId )

        self.results = self.ldp.get_person_by_login( account, account_types=self.ldp.AccountType.SECONDARY )
        if not self.results:
            logger.debug('search found no result in LDAP for primary account with hrId %s... ' % self.hrId )
            return False, None

        gid = self.results.gid
        logger.debug('found gid %s for account %s for hrId %s in LDAP info ' % (gid, account, self.hrId) )
        if gid != self.gidMap[eGroupName]:
            return False, self.results.login
        
        return True, self.results.login

class CMSEgroupAccountChecker(CMSAccountCheckerBase):

    def __init__(self, hrId, verbose):
        super().__init__(hrId, verbose )
        self.egh = EgroupHandler()

        return

    def _checkIdInGroup( self, eGroupName ):
        logger.debug('going to check eGroups for hrId %s... ' % self.hrId )

        if eGroupName not in ['zh', 'zj', 'zp', 'z2', 'z5' ]: return False, None
        if not self.account or self.account is None: 
            logger.error("can not check eGroup w/o account ... ")
            return False, None

        try:
            isIn, member = self.egh.accountInEgroup( self.account, eGroupName )
            logger.debug( "isInEgroup(%s, %s) = %s (%s)" % (self.account, eGroupName, isIn, member) )
        except Exception as e:
            logger.error( " error when checking account %s in group %s " % (self.account, eGroupName) )
            raise(e)

        return isIn, self.account

    def _checkSecAccountInGroup( self, account, eGroupName ):
        logger.debug('going to check eGroups for hrId %s... ' % self.hrId )

        if eGroupName not in ['zh', 'zj', 'zp', 'z2', 'z5' ]: return False, None
        if not self.account: return False, None

        try:
            isIn, member = self.egh.accountInEgroup( account, eGroupName )
            logger.debug( "isInEgroup(%s, %s) = %s (%s)" % (account, eGroupName, isIn, member) )
        except Exception as e:
            logger.error( " error when checking account %s in group %s " % (ccount, eGroupName) )
            raise(e)

        return isIn, account

class AccountChecker(object):

    def __init__(self, hrId, verbose):
        
        self.ldapChkr = CMSLdapAccountChecker(hrId, verbose)
        self.ldapChkr.checkaccountEnabled()
        self.ldapChkr.getSecondaryAccounts()

        self.eGrpChkr = CMSEgroupAccountChecker(hrId, verbose)
        self.eGrpChkr.account = self.ldapChkr.account
        self.eGrpChkr.secAccounts = self.ldapChkr.secAccounts

        return
    
    def check( self ):

        self.ldapChkr.check()
        # if not self.ldapChkr.zhPrimaryOK:
        self.eGrpChkr.check()

        return {
            'accountExists': bool(self.ldapChkr.account), 
            "accountEnabled": self.ldapChkr.accountEnabled,
            "zhPrimaryOK": self.ldapChkr.zhPrimaryOK and self.eGrpChkr.zhPrimaryOK,
            "zhSecondaryOK": self.ldapChkr.zhSecondaryOK and self.eGrpChkr.zhSecondaryOK,
            "otherExptOK": self.ldapChkr.otherExptOK and self.eGrpChkr.otherExptOK,
        }

    def show( self ):

        print( '\n', '-'*42, '\n' )
        print( 'Results for check of hrId %d - primary account %s ' % (self.ldapChkr.hrId, self.ldapChkr.account) )
        print( 'LDAP: ' )
        print( '      account enabled             : %s ' % self.ldapChkr.accountEnabled )
        print( '      primary IS in ZH or ZJ      : %s ' % self.ldapChkr.zhPrimaryOK )
        print( '      NO secondary in ZH or ZJ    : %s ' % self.ldapChkr.zhSecondaryOK )
        print( '      NO account(s) in other expts: %s ' % self.ldapChkr.otherExptOK )
        if not self.ldapChkr.otherExptOK:
            print( '      account(s) in other expts   : "%s" ' % ','.join(self.ldapChkr.otherExptMemberList) )

        print( '\n', '-'*42, '\n' )
        print( 'eGroup: ' )
        print( '      primary IS in ZH or ZJ       : %s ' % self.eGrpChkr.zhPrimaryOK )
        print( '      NO secondary in ZH or ZJ     : %s ' % self.eGrpChkr.zhSecondaryOK )
        print( '      NO account(s) in other expts : %s ' % self.eGrpChkr.otherExptOK )
        if not self.eGrpChkr.otherExptOK:
            print( '      account(s) in other expts   : "%s" ' % ','.join(self.ldapChkr.otherExptMemberList) )

        print( '\n', '-'*42, '\n' )

        return 

def test():
    for hrId in [ 422405, 744616, 829143, 42240500 ]:
        check(hrId)

def check(hrId, verbose=True):
    
    if verbose : logger.info( " " )
    if verbose : logger.info( "------- checking hrId: %s" % hrId )
    ac = AccountChecker(hrId, verbose)
    ac.check()
    ac.show()

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument('-t', '--testOnly',
                        action='store_true', # set to true if flag is given, default: false
                        help="only run the tests" )

    parser.add_argument('--hrId', nargs=1, type=int,
                        help="the HR ID of the user to check" )

    parser.add_argument('--mail', action='store_true',
                        help="send confirmation mail to the user" )

    args = parser.parse_args()

    if args.testOnly:
        test()
        sys.exit(0)

    check( args.hrId[0] )

if __name__ == '__main__':
    # run as (adapt the tunnel-port for LDAP as needed):
    # LDAP_URI=ldaps://localhost:8636 PYTONPATH=. python ./CmsAccountChecker.py --test
    main()

