import flask


def redirect_unauthorized():
    if flask.request.referrer and flask.request.referrer.startswith(flask.request.url_root):
        return flask.redirect(flask.request.referrer)
    else:
        return flask.redirect(flask.request.url_root)


def quick_url(view_fn, blueprint_name=None, **kwargs):
    """
    Proxy to flask.url_for() assuming target blueprint to be the same as current flask.request.blueprint.
    Optional parameter allows specifying the blueprint name should the link be a cross-blueprint one.
    """
    return flask.url_for('%s.%s' % (blueprint_name or flask.request.blueprint, view_fn.__name__), **kwargs)


def render_generic_template(template_name="generic_page.html", title='Untitled', forms=None, data_maps=None,
                            table_data=None, hidden_data=None, action_links=None, **kwargs):
    """
    A function that knows all the parameters accepted by the template, can and does suggest them, saves typing.
    Can be used for templates inheriting from generic_page, too.
    """
    return flask.render_template(template_name, title=title, forms=forms, data_maps=data_maps, table_data=table_data,
                                 hidden_data=hidden_data, action_links=action_links, **kwargs)


def render_generic_dialog_details_template(template_name='dialog_details.html', title='Details', details={}, actions=[],
                                           autonomous=False):
    return flask.render_template(template_name, title=title, details=details, actions=actions)