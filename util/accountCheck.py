#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys

import logging
import argparse

from icmsutils.ldaputils import LdapProxy
from icms_orm.cmspeople import Person
from util.EgroupHandler import EgroupHandler

from icms_orm.common import EmailMessage, EmailLog

logFmt = "%(asctime)s %(levelname)s: '%(message)s'"
logging.basicConfig(format = logFmt, level=logging.WARNING )


msgOK = '''Dear %s,

your account "%s" is now in the CMS (zh) group.

It will still take a while for this info to propagate for the services needed, so please be patient.

Some services, like the ones which will give you access to the protected CMS twiki and CMS indico pages, will only be updated over night, so this will work only tomorrow (CERN time).

You can manage some properties of the account, as well as pass the obligatory security course and sign the Cern computing usage rules via the web:

   https://account.cern.ch/account

Thanks,
  cheers, andreas
'''

msgInButDisabled = '''Dear %s,

Your account "%s" was created and added to the CMS (zh) group, however at present the account is disabled.  

Please contact the CERN Service desk (service-desk@cern.ch, tel.Cern-77777) to get it activated, you will also get your initial password then.

You can manage some properties of the account, as well as pass the obligatory security course and sign the Cern computing usage rules via the web at: 

	https://account.cern.ch/account 

It will still take a while for this info to propagate for the services needed, so please be patient. Some services (like the ones which will give you access to the protected twiki and indico pages) will only be updated over night, so this could work on the following working day.

Thanks,
  cheers, andreas
'''


msgWhySecondary = '''Dear %s,

   why do you need this %s account (%s) to be in the "zh" group ?

In CMS we have the policy to only have one account per user in the "zh" group to ensure fair use of the resources.

If you need it as a librarian account for a new private repository in SVN or the like, please simply use your main account; if it is for a group-wide repository, a "service" account would probably better, as this can be transferred to other people as needed.

Thanks,
   cheers, andreas
'''

msgInOther = '''Dear %s,

your account is presently registered with %s. By common policy agreed between the major LHC experiments, everybody can only have an account in one of the experiments.

If you agree, I'll ask the %s admins to remove you from their computing group (if not yet done), before I can add you to the CMS one.

Please let me know.

Thanks,
   cheers, andreas
'''

helpInfo = { 'account disabled' : '''Your account was created and added to the CMS (zh) group, however at present the account is disabled. Please contact the CERN Service desk (service-desk@cern.ch, tel.Cern-77777) to get it activated, you will also get your initial password then.''',
             'user in CMS DB' : '''You first need to become a member of the CMS collaboration - which did not yet happen. Your supervisor/team-leader needs to contact the CMS secretariat to register you as a member of CMS.
             ''',
            'primary account in zh' : '''Action on this can only be taken if all other issues are resolved''',
            'secondary account in zh' : '''This should only exist in exceptional circumstances.'''
#              '' : ''' ''',
             }

def send_mail_db(db, fromAddr, toAddrs, ccAddrs, subject, msgBody, bccAddrs=None, reply_to=''):

    email, log = EmailMessage.compose_message(sender=fromAddr,
                                              to = ','.join(toAddrs),
                                              cc=','.join(ccAddrs) if ccAddrs else '',
                                              bcc=','.join(bccAddrs) if bccAddrs else '',
                                              reply_to='icms-support@cern.ch',
                                              subject=subject,
                                              body=msgBody,
                                              source_app='toolkit',
                                              db_session=None, # do not yet send, we'll need to check if it's already done ...
                                             )

    res = EmailMessage.query.filter_by(hash=email.hash).all()
    msg =  "found %s existing mail(s) with hash %s: %s " % (len(res), email.hash, '\n'.join([str(x) for x in res]) )
    if res:
        oldLog = EmailLog.query.filter_by(email_id=res[0].id).first()
        msg += 'A mail was already sent to (%s) on %s' % (toAddrs, oldLog.timestamp)
        logging.warning( msg )
        return { 'status': 'Warning', 'msg': msg }

    try:
        db.session.add( email )
        db.session.add( log )
        db.session.commit()
    except Exception as e:
        msg = "Exception caught when trying to add email to DB: %s" % str(e)
        logging.error(msg)
        return { 'status': 'Error', 'msg': msg }

    msg = 'Message (id: %s) successfully sent to %s' % (email.id, toAddrs)
    logging.info( msg )

    return { 'status': 'OK', 'msg': msg }


class AccountChecker(object):

    def __init__(self, infoOK = False, verbose=True):
        self.ldapCkr = LdapChecker( infoOK, verbose )
        # self.cmsDbCkr = CMSdbChecker()
        self.issues = { 'info' : [], 'error' : [] }
        self.verbose = verbose

    def checkLdap( self, hrId ) :

        resPrim = self.ldapCkr.checkPrimaryAccount( hrId )
        if self.verbose : logging.info( 'ldap: primary account for hrId %s checked: %s' % (hrId, resPrim) )

        resSec  = self.ldapCkr.checkSecondaryAccounts( hrId )
        if self.verbose : logging.info( 'ldap: secondary accounts for hrId %s checked: %s' % (hrId, resSec) )

        return resPrim or resSec

    def checkDB( self, hrId ) :
        res, entry = self.isActiveMember( hrId )
        if res :
            if self.verbose : logging.debug( 'checkDB> hrId %s IS  found in CMS peopleDB: %s' % (hrId, entry) )
        else :
            if self.verbose : logging.debug( 'checkDB> hrId %s NOT found in CMS peopleDB' % hrId )

        return res

    def check( self, hrId ):

        self.checkDB( hrId )

        self.checkLdap( hrId )
        self.issues['info'] += self.ldapCkr.issues['info']
        self.issues['error'] += self.ldapCkr.issues['error']

        if self.verbose and self.issues:
            sys.stdout.flush()
            sys.stderr.flush()
            logging.debug( '\n', '='*80 )
            from pprint import pprint
            pprint( self.issues )


    def isActiveMember( self, hrId ):

        res = Person.query.filter_by(hrId=hrId).all()
        if not res:
            msg = 'user with hrID "%s" not found in CMS DB' % hrId
            if self.verbose : logging.debug( msg )
            self.issues['error'].append( { 'check'  : 'user NOT in CMS DB',
                                           'status' : 'error',
                                           'msg'    : msg } )
            return False, None

        if len( res ) > 1 :
            msg = 'wrong number of users (%s) with hrID "%s" found in CMS DB' % (len(res), hrId)
            if self.verbose : logging.warning( msg )
            self.issues['error'].append( { 'check'  : 'wrong number of users in DB',
                                           'status' : 'error',
                                           'msg'    : msg } )

        if 'CMS' == res[0].status:
            msg = 'user with hrID "%s" was found in CMS DB' % hrId
            self.issues['info'].append( { 'check'  : 'user in CMS DB',
                                           'status' : 'OK',
                                           'msg'    : msg } )
            return True, res[0]
        return False, None



def isInEgroup( account, eGroupName, verbose=True ):

    # only check for CMS, Totem, Atlas, Alice and LHCb
    if eGroupName not in ['zh', 'zj', 'zp', 'z2', 'z5' ]: return False

    if not account: return False

    egh = EgroupHandler()
    isIn, member = egh.accountInEgroup( account, eGroupName )
    if verbose : logging.debug( "isInEgroup(%s, %s) = %s (%s)" % (account, eGroupName, isIn, member) )

    return isIn


class LdapChecker(object):

    def __init__(self, infoOK = False, verbose=True ):

        self.gidMap = { 'zh' : 1399, # CMS
                        'zj' : 1468, # Totem
                        'zp' : 1307, # Atlas
                        'z5' : 1470, # LHCb
                        'z2' : 1395, # Alice
                      }

        self.exptMap = { 1399 : 'CMS',
                         1468 : 'Totem',
                         1307 : 'Atlas',
                         1470 : 'LHCb',
                         1395 : 'Alice',
                       }

        self.ldp = LdapProxy.get_instance()

        self.issues = { 'info' : [], 'error' : [] }

        self.infoOK = infoOK
        self.verbose = verbose

        return

    def primaryInGroupLDAP( self, hrId, group ):

        try:
            results = self.ldp.find_people_by_hrids( [hrId] )
            if self.verbose : logging.debug("results = %s" % str(results) )
            if not results:
                if self.verbose : logging.debug('search found no result in LDAP for primary account with hrId %s... ' % hrId )
                return False, None

            gid = results[0].gid
            if self.verbose : logging.debug('found gid %s (%s) for hrId %s in LDAP info ' % (gid, type(gid), hrId) )
            if gid != self.gidMap[group]:
                return False, results[0].login

            return True, results[0].login

        except KeyError:
            raise
        except Exception as e:
            if self.verbose : logging.error('unknown exception when checking if primary for hrId %s in zh - got : %s' % (hrId, str(e)) )
            raise

    def getPersonFromHRid( self, hrId ):
        results = self.ldp.find_people_by_hrids( [ hrId ] )
        if not results:
            return None

        return results[0]

    def checkPrimaryAccount( self, hrId ):

        resTot = True
        eGroup = 'zh'
        isInEg, pa = self.primaryInGroupLDAP( hrId, eGroup )
        if not isInEg: # not in CMS/'zh', try Totem/'zj'
            eGroup = 'zj'
            isInEg, pa = self.primaryInGroupLDAP( hrId, eGroup )
        if isInEg:
            msg = 'primary account "%s" for hrId %s already in %s' % (pa, hrId, eGroup)
            if self.verbose : logging.warning( msg )
            self.issues['info'].append( { 'check' : 'primary account in %s' % eGroup,
                                   'status':'info',
                                   'msg' : msg } )
        else: # not in ZH or ZJ- error
            # no membership found in ldap, now check eGroup directly.
            # This is expensive, so we only do it if ldap did not find it
            if isInEgroup( pa, 'zh' ) :
                eGroup = 'zh'
                msg = 'primary account %s IS in eGroup %s - though not yet synced with LDAP' % (pa, eGroup)
                if self.verbose : logging.info( msg )
                self.issues[ 'info' ].append( { 'check'  : 'primary account in %s' % eGroup,
                                                'status' : 'info',
                                                'msg'    : msg } )
            elif isInEgroup( pa, 'zj' ) :
                eGroup = 'zj'
                msg = 'primary account %s IS in eGroup %s - though not yet synced with LDAP' % (pa, eGroup)
                if self.verbose : logging.info( msg )
                self.issues[ 'info' ].append( { 'check'  : 'primary account in %s' % eGroup,
                                                'status' : 'info',
                                                'msg'    : msg } )
            else:
                msg = 'primary account "%s" for hrId %s NOT in zh or zj' % (pa, hrId)
                if self.verbose : logging.warning( msg )
                self.issues['error'].append( { 'check' : 'primary account in zh/zj',
                                              'status':'error',
                                              'msg' : msg } )

        # flag this as error only if not just for info:
        if not self.infoOK: resTot &= not isInEg

        for grp in [ 'zp', 'z5', 'z2' ] :
            resLdap, pa = self.primaryInGroupLDAP( hrId, grp )
            resEG = None
            if resLdap :
                # membership found in ldap, to make sure, check eGroup directly.
                # This is expensive, so we only do it if ldap found it
                resEG = isInEgroup( pa, grp, self.verbose )
                if resEG :
                    msg = 'primary account "%s" for hrId %s is also in eGroup %s (%s)' % (pa, hrId, grp, self.exptMap[self.gidMap[grp]])
                    if self.verbose : logging.warning( msg )
                    self.issues['error'].append( { 'check' : 'primary account in other expt (eGroup)',
                                                   'status':'error',
                                                   'msg' : msg,
                                                   'otherExp' : (grp, self.exptMap[self.gidMap[grp]])
                                                  } )
                else:
                    msg = 'primary account "%s" for hrId %s is also in LDAP group %s (%s)' % (pa, hrId, grp, self.exptMap[self.gidMap[grp]])
                    if self.verbose : logging.warning( msg )
                    self.issues['error'].append( { 'check' : 'primary account in other expt (LDAP)',
                                                   'status':'error',
                                                   'msg' : msg,
                                                   'otherExp' : (grp, self.exptMap[self.gidMap[grp]])
                                                 } )

            resTot &= not resLdap # only OK if hrId is NOT in grp

            # resEG = isInEgroup( pa, grp, self.verbose )
            # if resEG:
            #     msg = 'primary account "%s" for hrId %s is also in LDAP group %s (%s)' % (pa, hrId, grp, self.exptMap[self.gidMap[grp]])
            #     if self.verbose : logging.warning( msg )
            #     self.issues['error'].append( { 'check' : 'primary account in other expt (eGroup)',
            #                                    'status':'error',
            #                                    'msg' : msg } )
            # resTot &= not resEG # only OK if hrId is NOT in grp

            if resLdap: logging.info(  "grp: %s resTot: %s, resLdap: %s, resEG: %s, issues: %s" % (grp, resTot, resLdap, resEG, str(self.issues) ) )

        person = self.getPersonFromHRid(hrId)
        if person is None:
            msg = 'user with hrId %s (%s) not found in LDAP' % (hrId, person.login if person else 'None')
            if self.verbose : logging.warning( msg )
            self.issues['error'].append( { 'check'  : 'user in LDAP',
                                  'status' : 'error',
                                  'msg'    : msg } )
            resTot &= False
        else: # don't try to use None object ....

            if 'active' != person.accountStatus.lower():
                msg = 'account %s for hrId %s exists, but is presently disabled' % (person.login, hrId)
                if self.verbose : logging.warning( msg )
                self.issues[ 'error' ].append( { 'check'  : 'account disabled',
                                                 'status' : 'error',
                                                 'msg'    : msg } )
                resTot &= False

            for eG in [ 'cms-web-access', 'cms-physics-access' ]:
                res = eG in person.cmsEgroups
                if res:
                    msg = 'user with hrId %s (%s) already in %s' % (hrId, person.login if person else 'None', eG)
                    if self.verbose : logging.warning( msg )
                    self.issues['info'].append( { 'check'  : 'user in general CMS eGroups',
                                                  'status' : 'info',
                                                  'msg'    : msg } )
                # flag this as error only if not just for info:
                if not self.infoOK :
                    resTot &= not res # user should NOT YET be in these groups -- may need to separate that out ...

        return resTot

    def checkSecondaryAccounts( self, hrId ):

        filter = '(&(employeeID=%s)(employeeType=Secondary))' % hrId
        userList = self.ldp.find_people_by_filter( filter )

        resTot = True
        for user in userList:
            for grp in [ 'zp', 'z5', 'z2' ] :
                res = ( user.gid == self.gidMap[grp] )
                if res:
                    msg = 'secondary account "%s" found to be in group %s (%s)' % (user.login, grp, self.exptMap[self.gidMap[grp]])
                    if self.verbose : logging.warning( '%s -- not allowed' % msg )
                    self.issues['error'].append( { 'check'  : 'secondary account in another experiment',
                                                   'status' : 'error',
                                                   'msg'    : msg } )
                resTot &= not res # OK if account is NOT in one of these groups
            for grp in ['zh', 'zj']:
                res = (user.gid == self.gidMap[ grp ])
                if res :
                    msg = 'secondary account "%s" already in group %s' % (user.login, grp)
                    if self.verbose : logging.warning( '%s -- not allowed' % msg )
                    self.issues['error'].append( { 'check'  : 'secondary account in %s' % grp,
                                                   'status' : 'error',
                                                   'msg'    : msg } )
                resTot &= not (user.gid == self.gidMap[ grp ])  # only one account in 'zh'/'zj'

        return resTot


def sendMailOK(db, account, email, userFirstName):

    send_mail_db(db=db,
                 fromAddr='cms.computing@cern.ch',
                 toAddrs=[ email ],
                 ccAddrs=['zh-admins@cern.ch'],
                 bccAddrs=['cms.people@cern.ch'],
                 subject='[zh-admin] account %s added to the CMS computing group' % account,
                 msgBody=msgOK % (userFirstName, account),
                )

    return

def sendMailInOther(db, account, email, userFirstName, otherExp):

    send_mail_db(db=db,
                 fromAddr='cms.computing@cern.ch',
                 toAddrs=[ email ],
                 ccAddrs=['zh-admins@cern.ch'],
                 bccAddrs=['cms.people@cern.ch'],
                 subject='[zh-admin] problem found when trying to add your account %s to the CMS computing group' % account,
                 msgBody=msgInOther % (userFirstName, account, otherExp),
                )

    return

def sendMailInButDisabled(db, account, email, userFirstName):

    send_mail_db(db=db,
                 fromAddr='cms.computing@cern.ch',
                 toAddrs=[ email ],
                 ccAddrs=['zh-admins@cern.ch'],
                 bccAddrs=['cms.people@cern.ch'],
                 subject='[zh-admin] problem found when trying to add your account %s to the CMS computing group' % account,
                 msgBody = msgInButDisabled % (userFirstName, account),
                )
    return

def sendMailWhy(db, account, email, userFirstName, acctType):

    #-ap: for the time being, send this only to myself, so I can double-check and re-send if needed
    
    send_mail_db(db=db,
                 fromAddr='cms.computing@cern.ch',
                 # toAddrs = [ email ],
                 # ccAddrs=['zh-admins@cern.ch']
                 toAddrs= [ 'andreas.pfeiffer@cern.ch' ],
                 subject='[zh-admin] your request to add %s to the CMS computing group' % account,
                 msgBody=msgWhySecondary % (userFirstName, acctType, account),
                 # ccAddrs=['zh-admins@cern.ch']
                )

    return

def test():
    for hrId in [ 422405, 744616, 829143, 42240500 ]:
        check(hrId)

def check(hrId, sendMail=False, infoOK=False, verbose=True):
    if verbose : logging.info( " " )
    if verbose : logging.info( "------- checking hrId: %s" % hrId )
    ac = AccountChecker(infoOK)
    ac.check( hrId, verbose=False )
    # testEGH( hrId )
    if verbose : logging.info( "------- analyzing result: %s" % analyse(ac.issues) )

    if ac.issues['error']:
        person = LdapProxy.get_instance().get_person_by_hrid(hrId)
        for item in ac.issues['error']:
            if 'account disabled' in item['check']:
                if verbose : logging.debug( "found account disabled, mail is:\n", msgInButDisabled % (person.fullName, person.login) )


def analyse(issues, infoFails=False):
    if infoFails: # request to fail if info issues are there:
        if issues['info']:
            return False

    if issues['error']:
        return False

    return True

def main():

    parser = argparse.ArgumentParser()

    parser.add_argument('-t', '--testOnly',
                        action='store_true', # set to true if flag is given, default: false
                        help="only run the tests" )

    parser.add_argument('--hrId', nargs=1, type=int,
                        help="the HR ID of the user to add" )

    parser.add_argument('--mail', action='store_true',
                        help="send confirmation mail to the user" )

    args = parser.parse_args()

    if args.testOnly:
        test()
        sys.exit(0)

    check( args.hrId[0], sendMail=args.mail )

if __name__ == '__main__':
    main()

