#!/bin/bash
# expected args: job_name, [prod/dev, venv_path]
cd "$(dirname "$0")"

#-todo: find a better way than hardcoding the hostname ...
if [ `hostname` != 'vocms0890.cern.ch' ] ; then
    echo "not running on non-production host " `hostname`
    exit 0
fi

JOB=$1
CONFIG=PROD
VNV=./venv-py3/bin/activate
if [ ! -z "$2" ]; then CONFIG=$2; fi
if [ ! -z "$3" ]; then VNV=$3; fi

if [ "$CONFIG" = "PROD" ]; then
    source /opt/rh/rh-postgresql95/enable
fi

source ${VNV}
TOOLKIT_CONFIG=${CONFIG} python manage.py script $JOB
