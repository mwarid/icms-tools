#!/usr/bin/env python
"""
Implemented according to: http://flask.pocoo.org/docs/0.11/cli/
Please mind setting environment variables FLASK_DEBUG and TOOLKIT_CONFIG as needed.
"""
import logging
import os
import traceback

import click
import flask
from flask.cli import FlaskGroup

import app_profiles as profiles
import webapp
from util import constants as const
from util import trivial


def factory_proxy(info):
    app_config = __get_app_config()
    app = None
    try:
        logging.debug(
            "Active config: %s. Will proceed to create the app and db objects."
            % (str(app_config))
        )
        root_path = os.path.join(trivial.get_project_root(), "webapp")
        _config_defaults = None
        if app_config == const.ENV_VAR_CONFIG_DEV:
            _config_defaults = profiles.ProfileDev
        elif app_config == const.ENV_VAR_CONFIG_TEST:
            _config_defaults = profiles.ProfileTest
        elif app_config == const.ENV_VAR_CONFIG_PROD:
            _config_defaults = profiles.ProfileProd
        elif app_config == const.ENV_VAR_CONFIG_PREPROD:
            _config_defaults = profiles.ProfilePreProd
        else:
            raise ValueError("Ambiguous app config value: %s." % app_config)
        app = webapp.Webapp(__name__, root_path=root_path, profile=_config_defaults)
        db = app.db

        @app.cli.command()
        @click.argument(
            "task", type=click.Choice([_e.value for _e in const.ScriptJobName])
        )
        @click.option(
            "--input_dir",
            help="An input directory (only makes sense for SQL conversion at present)",
            default=None,
        )
        @click.option(
            "--paper_code",
            help="Specify the paper code for generatePaperAuthorLists ",
            default=None,
        )
        @click.option(
            "--project",
            help="Specify the object for genPeopleListWhoJoinAndLeftProject",
            default=None,
        )
        @click.option(
            "--old-inst-code",
            help="Old inst code for renaming institute",
            default=None,
        )
        @click.option(
            "--new-inst-code",
            help="New inst code for renaming institute",
            default=None,
        )
        def script(task, input_dir, paper_code, project, old_inst_code, new_inst_code):
            if task == const.ScriptJobName.CHECK_APPS.value:
                from scripts import check_authorship_application

                check_authorship_application.main()
            elif task == const.ScriptJobName.CADI_REMINDERS.value:
                from scripts import cadiReminders

                cadiReminders.main(db)
            elif task == const.ScriptJobName.FILL_FOOH.value:
                from scripts import fill_fora_only_on_hypernews

                fill_fora_only_on_hypernews.main(db)
            elif task == const.ScriptJobName.GENERATE_PAPER_AUTHOR_LISTS.value:
                from scripts import generatePaperAuthorLists

                generatePaperAuthorLists.main(code=paper_code)
            elif task == const.ScriptJobName.UPDATE_AL_STATUS.value:
                from scripts import updateALstatus

                updateALstatus.update_al_statuses_in_new_db(app.db)
            elif task == const.ScriptJobName.TEST_UPDATE_AL_STATUS.value:
                from scripts import updateALstatus

                updateALstatus.get_al_status_map_from_old_db()
            elif task == const.ScriptJobName.CHECK_NEW_MEMBER_ACCOUNTS.value:
                from scripts import checkNewCMSmemberAccounts

                checkNewCMSmemberAccounts.getNewUsers()
            elif task == const.ScriptJobName.GENERATE_PLWJALP.value:
                from scripts import genPeopleListWhoJoinAndLeftProject

                genPeopleListWhoJoinAndLeftProject.main(project)
            elif task == const.ScriptJobName.ORCIDS_FROM_IDENTITIES.value:
                from scripts import orcids_from_identities

                orcids_from_identities.main()
            elif task == const.ScriptJobName.PROJECT_MEMBER_STATS.value:
                from scripts import project_member_stats

                project_member_stats.main()
            elif task == const.ScriptJobName.RENAME_INSTITUTE_SQL.value:
                from scripts import rename_institute_sql

                rename_institute_sql.main(old_inst_code, new_inst_code)

        # It looks like Flask-Migrate added the following one automagically (under the 'db' entry):
        # from flask_migrate import cli as flask_migrate_cli
        # app.cli.add_command(flask_migrate_cli.db, 'db_cli')
    except Exception as _:
        traceback.print_exc()

    return app


def __setup_logging(filename, level=logging.DEBUG):
    base_dir = trivial.get_project_root()
    logs_dir = os.path.join(base_dir, const.DIR_NAME_LOGS)

    if not os.path.isdir(logs_dir):
        os.mkdir(logs_dir)

    log_path = os.path.join(logs_dir, filename)
    print(
        "Logging setup in progress! %s file will be written to, as well as the console."
        % log_path
    )
    logger_fmt = (
        "%(asctime)s [%(levelname)s]: %(message)s (%(module)s.%(funcName)s:%(lineno)d)"
    )

    logging.basicConfig(filename=log_path, level=level, format=logger_fmt)

    console = logging.StreamHandler()
    console.setLevel(level)
    formatter = logging.Formatter(logger_fmt)
    console.setFormatter(formatter)
    logging.getLogger("").addHandler(console)
    logging.info("Loggers set up, this is a logging output.")


def __get_app_config():
    app_config = os.environ.get(const.ENV_VAR_NAME_CONFIG)
    app_config_values = (
        const.ENV_VAR_CONFIG_DEV,
        const.ENV_VAR_CONFIG_TEST,
        const.ENV_VAR_CONFIG_PROD,
        const.ENV_VAR_CONFIG_PREPROD,
    )
    if not app_config or app_config not in app_config_values:
        raise ValueError(
            "Application config undefined. Please set %s to one of the following values: %s."
            % (const.ENV_VAR_NAME_CONFIG, ", ".join(app_config_values))
        )
    return app_config


@click.group(cls=flask.cli.FlaskGroup, create_app=factory_proxy)
def cli():
    """This is a command line interface to iCMS-toolkit."""


def main():
    conf = __get_app_config()
    if conf != const.ENV_VAR_CONFIG_PROD:
        __setup_logging("%s_%s.log" % ("toolkit_log_", conf.lower()))
    cli()


if __name__ == "__main__":
    main()
