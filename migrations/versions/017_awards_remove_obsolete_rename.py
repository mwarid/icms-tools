"""Awards: Remove title, description and type description; Rename project column

Revision ID: 017
Revises: 016
Create Date: 2022-12-04 18:51:18.146156

"""

# revision identifiers, used by Alembic.
revision = "017"
down_revision = "016"

import sqlalchemy as sa
from alembic import op


def upgrade():
    op.drop_column("award", "description")
    op.drop_column("award", "title")
    op.drop_column("award_type", "description")
    op.alter_column(
        "award_nomination",
        "nominee_project",
        existing_type=sa.String(length=64),
        existing_nullable=False,
        new_column_name="nomination_projects",
    )


def downgrade():
    op.add_column(
        "award_type",
        sa.Column(
            "description", sa.VARCHAR(length=1000), nullable=False, server_default=""
        ),
    )
    op.add_column(
        "award",
        sa.Column("title", sa.VARCHAR(length=200), nullable=False, server_default=""),
    )
    op.add_column(
        "award",
        sa.Column(
            "description", sa.VARCHAR(length=1000), nullable=False, server_default=""
        ),
    )
    op.alter_column(
        "award_nomination",
        "nomination_projects",
        existing_type=sa.String(length=64),
        existing_nullable=False,
        new_column_name="nominee_project",
    )
