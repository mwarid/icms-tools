"""Awards schema

Revision ID: 014
Revises: 013
Create Date: 2021-12-01 17:41:42.780323

"""

# revision identifiers, used by Alembic.
revision = '014'
down_revision = '013'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from icms_orm.toolkit import person_activity_enum


def upgrade():
    op.create_table('award_type',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('type', sa.String(length=200), nullable=False),
        sa.Column('description', sa.String(length=1000), nullable=False),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_award_type')),
        sa.UniqueConstraint('type', name=op.f('uq_award_type_type')),
        schema='toolkit'
    )
    op.create_table('award',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('award_type_id', sa.Integer(), nullable=False),
        sa.Column('title', sa.String(length=200), nullable=False),
        sa.Column('description', sa.String(length=1000), nullable=False),
        sa.Column('year', sa.Integer(), nullable=False),
        sa.Column('nominations_open_date', sa.Date(), nullable=False),
        sa.Column('nominations_deadline', sa.Date(), nullable=False),
        sa.Column('awardee_publication_date', sa.Date(), nullable=False),
        sa.Column('remarks', sa.String(length=1000), nullable=True),
        sa.Column('is_active', sa.Boolean(), nullable=False),
        sa.Column('last_updated_at', sa.DateTime(), nullable=False),
        sa.Column('last_updated_by', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['award_type_id'], ['toolkit.award_type.id'], name=op.f('fk_award_award_type_id_award_type'), onupdate='CASCADE'),
        sa.ForeignKeyConstraint(['last_updated_by'], ['public.person.cms_id'], name=op.f('fk_award_last_updated_by_person'), onupdate='CASCADE'),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_award')),
        schema='toolkit'
    )
    op.create_table('award_nomination',
        sa.Column('nomination_id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('award_id', sa.Integer(), nullable=False),
        sa.Column('nominator_cms_id', sa.Integer(), nullable=False),
        sa.Column('nominee_cms_id', sa.Integer(), nullable=True),
        sa.Column('nominee_activity', person_activity_enum, nullable=True),
        sa.Column('nominee_project', sa.String(length=64), nullable=False),
        sa.Column('nominee_institute', sa.String(length=32), nullable=True),
        sa.Column('nominee_phd_date', sa.Date(), nullable=True),
        sa.Column('working_relationship', sa.String(length=1000), nullable=False),
        sa.Column('justification', sa.String(length=1000), nullable=False),
        sa.Column('proposed_citation', sa.String(length=1000), nullable=False),
        sa.Column('remarks', sa.String(length=1000), nullable=True),
        sa.Column('is_active', sa.Boolean(), nullable=False),
        sa.Column('last_updated_at', sa.DateTime(), server_default=sa.text("timezone('UTC', now())"), nullable=False),
        sa.Column('last_updated_by', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['award_id'], ['toolkit.award.id'], name=op.f('fk_award_nomination_award_id_award'), onupdate='CASCADE'),
        sa.ForeignKeyConstraint(['last_updated_by'], ['public.person.cms_id'], name=op.f('fk_award_nomination_last_updated_by_person'), onupdate='CASCADE'),
        sa.ForeignKeyConstraint(['nominator_cms_id'], ['public.person.cms_id'], name=op.f('fk_award_nomination_nominator_cms_id_person'), onupdate='CASCADE'),
        sa.ForeignKeyConstraint(['nominee_cms_id'], ['public.person.cms_id'], name=op.f('fk_award_nomination_nominee_cms_id_person'), onupdate='CASCADE'),
        sa.ForeignKeyConstraint(['nominee_institute'], ['public.institute.code'], name=op.f('fk_award_nomination_nominee_institute_institute'), onupdate='CASCADE'),
        sa.PrimaryKeyConstraint('nomination_id', name=op.f('pk_award_nomination')),
        schema='toolkit'
    )
    op.create_table('awardee',
        sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
        sa.Column('award_id', sa.Integer(), nullable=False),
        sa.Column('cms_id', sa.Integer(), nullable=True),
        sa.Column('activity', person_activity_enum, nullable=True),
        sa.Column('project', sa.String(length=64), nullable=False),
        sa.Column('institute', sa.String(length=32), nullable=True),
        sa.Column('citation', sa.String(length=1000), nullable=False),
        sa.Column('remarks', sa.String(length=1000), nullable=True),
        sa.Column('is_active', sa.Boolean(), nullable=False),
        sa.Column('last_updated_at', sa.DateTime(), server_default=sa.text("timezone('UTC', now())"), nullable=False),
        sa.Column('last_updated_by', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['award_id'], ['toolkit.award.id'], name=op.f('fk_awardee_award_id_award'), onupdate='CASCADE'),
        sa.ForeignKeyConstraint(['cms_id'], ['public.person.cms_id'], name=op.f('fk_awardee_cms_id_person'), onupdate='CASCADE'),
        sa.ForeignKeyConstraint(['institute'], ['public.institute.code'], name=op.f('fk_awardee_institute_institute'), onupdate='CASCADE'),
        sa.ForeignKeyConstraint(['last_updated_by'], ['public.person.cms_id'], name=op.f('fk_awardee_last_updated_by_person'), onupdate='CASCADE'),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_awardee')),
        schema='toolkit'
    )


def downgrade():
    op.drop_table('awardee', schema='toolkit')
    op.drop_table('award_nomination', schema='toolkit')
    op.drop_table('award', schema='toolkit')
    op.drop_table('award_type', schema='toolkit')
