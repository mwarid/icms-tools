"""Weekly Meetings

Revision ID: 015
Revises: 014
Create Date: 2022-11-01 09:46:27.546976

"""

# revision identifiers, used by Alembic.
revision = '015'
down_revision = '014'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from icms_orm.toolkit import odd_even_weekly_enum, day_of_week_enum, state_enum

def upgrade():
    odd_even_weekly_enum.create(op.get_bind())
    day_of_week_enum.create(op.get_bind())
    state_enum.create(op.get_bind())

    op.create_table('weekly_meetings',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('title', sa.String(length=128), nullable=False),
    sa.Column('odd_even_weekly', odd_even_weekly_enum, nullable=False),
    sa.Column('day_of_week', day_of_week_enum, nullable=False),
    sa.Column('start_time', sa.Time(), nullable=False),
    sa.Column('end_time', sa.Time(), nullable=False),
    sa.Column('room', sa.String(length=80), nullable=False),
    sa.Column('primary_contact', sa.String(length=128), nullable=True),
    sa.Column('state', state_enum, nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_weekly_meetings')),
    schema='toolkit'
    )
    op.create_table('weekly_meetings_convener',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('cms_id', sa.SmallInteger(), nullable=False),
    sa.Column('weekly_meetings_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['cms_id'], ['public.person.cms_id'], name=op.f('fk_weekly_meetings_convener_cms_id_person'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['weekly_meetings_id'], ['toolkit.weekly_meetings.id'], name=op.f('fk_weekly_meetings_convener_weekly_meetings_id_weekly_meetings'), ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_weekly_meetings_convener')),
    schema='toolkit'
    )


def downgrade():
    op.drop_table('weekly_meetings_convener', schema='toolkit')
    op.drop_table('weekly_meetings', schema='toolkit')
    state_enum.drop(op.get_bind())
    day_of_week_enum.drop(op.get_bind())
    odd_even_weekly_enum.drop(op.get_bind())
