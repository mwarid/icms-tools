"""fresh postgres starting point

Revision ID: 001
Revises: None
Create Date: 2017-04-04 12:49:54.176684

"""

# revision identifiers, used by Alembic.
revision = '001'
down_revision = None

from alembic import op
import sqlalchemy as sa


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('vote_delegation', schema='toolkit')
    op.drop_table('voting_merger_member', schema='toolkit')
    op.drop_table('voting', schema='toolkit')
    op.drop_table('voting_merger', schema='toolkit')
    op.drop_table('room_request_convener', schema='toolkit')
    op.drop_table('room_request', schema='toolkit')
    op.drop_table('room', schema='toolkit')
    op.drop_table('ext_authors_flags_rel', schema='toolkit')
    op.drop_table('ext_author_flag', schema='toolkit')
    op.drop_table('ext_author', schema='toolkit')
    op.drop_table('ext_author_project', schema='toolkit')
    op.drop_table('cms_week', schema='toolkit')
    op.drop_table('cms_project', schema='toolkit')
    op.drop_table('cern_country', schema='toolkit')
    op.drop_table('cern_country_status', schema='toolkit')
    op.drop_table('authorship_application_check', schema='toolkit')
    op.drop_table('activity_check', schema='toolkit')
    op.drop_table('status', schema='toolkit')
    # ### end Alembic commands ###



def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('status',
                    sa.Column('code', sa.String(length=32), nullable=False),
                    sa.PrimaryKeyConstraint('code', name=op.f('pk_status')),
                    schema='toolkit'
                    )
    op.create_table('cms_project',
                    sa.Column('code', sa.String(length=32), nullable=False),
                    sa.Column('name', sa.String(length=128), nullable=False),
                    sa.Column('official', sa.Boolean(), nullable=False),
                    sa.Column('status', sa.String(length=32), nullable=True),
                    sa.ForeignKeyConstraint(['status'], [u'toolkit.status.code'], name=op.f('fk_cms_project_status_status'), onupdate='CASCADE'),
                    sa.PrimaryKeyConstraint('code', name=op.f('pk_cms_project')),
                    sa.UniqueConstraint('name', name=op.f('uq_cms_project_name')),
                    schema='toolkit'
                    )
    op.create_table('activity_check',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('for_cms_id', sa.SmallInteger(), nullable=True),
                    sa.Column('by_cms_id', sa.SmallInteger(), nullable=True),
                    sa.Column('for_activity', sa.SmallInteger(), nullable=True),
                    sa.Column('timestamp', sa.DateTime(), nullable=True),
                    sa.Column('confirmed', sa.Boolean(), nullable=True),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_activity_check')),
                    schema='toolkit'
                    )
    op.create_table('authorship_application_check',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('run_number', sa.Integer(), nullable=False),
                    sa.Column('datetime', sa.DateTime(), server_default=sa.text(u'now()'), nullable=False),
                    sa.Column('cms_id', sa.Integer(), nullable=False),
                    sa.Column('inst_code', sa.String(length=32), nullable=False),
                    sa.Column('activity_id', sa.Integer(), nullable=False),
                    sa.Column('passed', sa.Boolean(), nullable=False),
                    sa.Column('failed', sa.Boolean(), nullable=False),
                    sa.Column('reason', sa.String(length=128), nullable=True),
                    sa.Column('days_count', sa.Integer(), nullable=False),
                    sa.Column('worked_self', sa.Float(), nullable=False),
                    sa.Column('worked_inst', sa.Float(), nullable=False),
                    sa.Column('needed_inst', sa.Float(), nullable=False),
                    sa.Column('possible_slipthrough', sa.Boolean(), nullable=False),
                    sa.Column('mo_status', sa.Boolean(), nullable=False),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_authorship_application_check')),
                    schema='toolkit'
                    )
    op.create_table('cern_country_status',
                    sa.Column('id', sa.SmallInteger(), nullable=False),
                    sa.Column('name', sa.String(length=64), nullable=False),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_cern_country_status')),
                    schema='toolkit'
                    )
    op.create_table('cern_country',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(length=64), nullable=False),
                    sa.Column('status_id', sa.SmallInteger(), nullable=True),
                    sa.ForeignKeyConstraint(['status_id'], [u'toolkit.cern_country_status.id'], name=op.f('fk_cern_country_status_id_cern_country_status'), onupdate='CASCADE', ondelete='SET NULL'),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_cern_country')),
                    schema='toolkit'
                    )
    op.create_table('cms_week',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('title', sa.String(length=80), nullable=False),
                    sa.Column('date', sa.Date(), nullable=False),
                    sa.Column('date_end', sa.Date(), nullable=False),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_cms_week')),
                    schema='toolkit'
                    )
    op.create_table('ext_author_project',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(length=128), nullable=False),
                    sa.Column('active', sa.Boolean(), nullable=False),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_ext_author_project')),
                    schema='toolkit'
                    )
    op.create_table('ext_author',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(length=128), nullable=False),
                    sa.Column('cms_id', sa.SmallInteger(), nullable=True),
                    sa.Column('hr_id', sa.Integer(), nullable=True),
                    sa.Column('inst_code', sa.String(length=40), nullable=False),
                    sa.Column('inst_code_also', sa.String(length=40), nullable=True),
                    sa.Column('inst_code_now', sa.String(length=40), nullable=True),
                    sa.Column('reason', sa.Text(), nullable=True),
                    sa.Column('date_work_start', sa.Date(), nullable=True),
                    sa.Column('date_work_end', sa.Date(), nullable=True),
                    sa.Column('date_sign_end', sa.Date(), nullable=True),
                    sa.Column('project_id', sa.Integer(), nullable=False),
                    sa.Column('spires_id', sa.String(length=40), nullable=True),
                    sa.ForeignKeyConstraint(['project_id'], [u'toolkit.ext_author_project.id'], name=op.f('fk_ext_author_project_id_ext_author_project'), ondelete='CASCADE'),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_ext_author')),
                    schema='toolkit'
                    )
    op.create_table('ext_author_flag',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(length=128), nullable=False),
                    sa.Column('project_id', sa.Integer(), nullable=False),
                    sa.Column('active', sa.Boolean(), nullable=False),
                    sa.ForeignKeyConstraint(['project_id'], [u'toolkit.ext_author_project.id'], name=op.f('fk_ext_author_flag_project_id_ext_author_project'), ondelete='CASCADE'),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_ext_author_flag')),
                    schema='toolkit'
                    )
    op.create_table('ext_authors_flags_rel',
                    sa.Column('author_id', sa.Integer(), nullable=False),
                    sa.Column('flag_id', sa.Integer(), nullable=False),
                    sa.ForeignKeyConstraint(['author_id'], [u'toolkit.ext_author.id'], name=op.f('fk_ext_authors_flags_rel_author_id_ext_author'), ondelete='CASCADE'),
                    sa.ForeignKeyConstraint(['flag_id'], [u'toolkit.ext_author_flag.id'], name=op.f('fk_ext_authors_flags_rel_flag_id_ext_author_flag'), ondelete='CASCADE'),
                    sa.PrimaryKeyConstraint('author_id', 'flag_id', name=op.f('pk_ext_authors_flags_rel')),
                    schema='toolkit'
                    )
    op.create_table('room',
                    sa.Column('id', sa.SmallInteger(), nullable=False),
                    sa.Column('indico_id', sa.SmallInteger(), nullable=True),
                    sa.Column('building', sa.String(length=8), nullable=False),
                    sa.Column('floor', sa.String(length=4), nullable=False),
                    sa.Column('room_nr', sa.String(length=8), nullable=False),
                    sa.Column('custom_name', sa.String(length=128), nullable=True),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_room')),
                    sa.UniqueConstraint('indico_id', name=op.f('uq_room_indico_id')),
                    schema='toolkit'
                    )
    op.create_table('room_request',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('cms_id_by', sa.SmallInteger(), nullable=False),
                    sa.Column('cms_id_for', sa.SmallInteger(), nullable=False),
                    sa.Column('title', sa.String(length=256), nullable=False),
                    sa.Column('project', sa.String(length=32), nullable=False),
                    sa.Column('webcast', sa.Boolean(), nullable=False),
                    sa.Column('time_start', sa.DateTime(), nullable=False),
                    sa.Column('duration', sa.SmallInteger(), nullable=False),
                    sa.Column('cms_week_id', sa.Integer(), nullable=True),
                    sa.Column('room_id_preferred', sa.SmallInteger(), nullable=False),
                    sa.Column('room_id', sa.SmallInteger(), nullable=True),
                    sa.Column('capacity', sa.SmallInteger(), nullable=False),
                    sa.Column('password', sa.String(length=16), nullable=True),
                    sa.Column('official', sa.Boolean(), nullable=False),
                    sa.Column('remarks', sa.Text(), nullable=True),
                    sa.Column('reason', sa.Text(), nullable=True),
                    sa.Column('status', sa.String(length=32), nullable=True),
                    sa.Column('time_created', sa.DateTime(), nullable=True),
                    sa.Column('time_updated', sa.DateTime(), nullable=True),
                    sa.ForeignKeyConstraint(['cms_week_id'], [u'toolkit.cms_week.id'], name=op.f('fk_room_request_cms_week_id_cms_week'), ondelete='CASCADE'),
                    sa.ForeignKeyConstraint(['project'], [u'toolkit.cms_project.code'], name=op.f('fk_room_request_project_cms_project'), onupdate='CASCADE'),
                    sa.ForeignKeyConstraint(['room_id'], [u'toolkit.room.id'], name=op.f('fk_room_request_room_id_room')),
                    sa.ForeignKeyConstraint(['room_id_preferred'], [u'toolkit.room.id'], name=op.f('fk_room_request_room_id_preferred_room')),
                    sa.ForeignKeyConstraint(['status'], [u'toolkit.status.code'], name=op.f('fk_room_request_status_status'), onupdate='CASCADE'),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_room_request')),
                    schema='toolkit'
                    )
    op.create_table('room_request_convener',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('cms_id', sa.SmallInteger(), nullable=False),
                    sa.Column('request_id', sa.Integer(), nullable=False),
                    sa.Column('active', sa.Boolean(), nullable=False),
                    sa.ForeignKeyConstraint(['request_id'], [u'toolkit.room_request.id'], name=op.f('fk_room_request_convener_request_id_room_request'), ondelete='CASCADE'),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_room_request_convener')),
                    schema='toolkit'
                    )
    op.create_table('voting',
                    sa.Column('code', sa.String(length=16), nullable=False),
                    sa.Column('title', sa.String(length=128), nullable=False),
                    sa.Column('start_time', sa.DateTime(), nullable=False),
                    sa.Column('end_time', sa.DateTime(), nullable=True),
                    sa.Column('delegation_deadline', sa.DateTime(), nullable=True),
                    sa.Column('type', sa.String(length=20), nullable=False),
                    sa.PrimaryKeyConstraint('code', name=op.f('pk_voting')),
                    schema='toolkit'
                    )
    op.create_table('vote_delegation',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('voting_code', sa.String(length=16), nullable=True),
                    sa.Column('cms_id_from', sa.Integer(), nullable=False),
                    sa.Column('cms_id_to', sa.Integer(), nullable=False),
                    sa.Column('status', sa.String(length=32), nullable=False),
                    sa.Column('is_long_term', sa.Boolean(), nullable=False),
                    sa.Column('cms_id_creator', sa.Integer(), nullable=False),
                    sa.Column('time_created', sa.DateTime(), nullable=True),
                    sa.Column('cms_id_updater', sa.Integer(), nullable=True),
                    sa.Column('time_updated', sa.DateTime(), nullable=True),
                    sa.ForeignKeyConstraint(['status'], [u'toolkit.status.code'], name=op.f('fk_vote_delegation_status_status'), onupdate='CASCADE'),
                    sa.ForeignKeyConstraint(['voting_code'], [u'toolkit.voting.code'], name=op.f('fk_vote_delegation_voting_code_voting'), onupdate='CASCADE', ondelete='CASCADE'),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_vote_delegation')),
                    schema='toolkit'
                    )
    op.create_table('voting_merger',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('country', sa.String(length=64), nullable=False),
                    sa.Column('valid_from', sa.Date(), nullable=False),
                    sa.Column('valid_till', sa.Date(), nullable=True),
                    sa.Column('representative_cms_id', sa.Integer(), nullable=False),
                    sa.PrimaryKeyConstraint('id', name=op.f('pk_voting_merger')),
                    schema='toolkit'
                    )
    op.create_table('voting_merger_member',
                    sa.Column('merger_id', sa.Integer(), autoincrement=False, nullable=False),
                    sa.Column('inst_code', sa.String(length=64), nullable=False),
                    sa.ForeignKeyConstraint(['merger_id'], [u'toolkit.voting_merger.id'], name=op.f('fk_voting_merger_member_merger_id_voting_merger')),
                    sa.PrimaryKeyConstraint('merger_id', 'inst_code', name=op.f('pk_voting_merger_member')),
                    schema='toolkit'
                    )
    # ### end Alembic commands ###

