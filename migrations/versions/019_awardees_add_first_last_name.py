"""Awards: Add first and last name

Revision ID: 019
Revises: 018
Create Date: 2023-06-01 17:15:00.000000

"""

# revision identifiers, used by Alembic.
revision = "019"
down_revision = "018"

import sqlalchemy as sa
from alembic import op


def upgrade():
    op.add_column(
        "awardee",
        sa.Column("first_name", sa.VARCHAR(length=255), nullable=True),
    )
    op.add_column(
        "awardee",
        sa.Column("last_name", sa.VARCHAR(length=255), nullable=True),
    )


def downgrade():
    op.drop_column("awardee", "first_name")
    op.drop_column("awardee", "last_name")
