"""Adding an ID column to Voting table

Revision ID: 010
Revises: 009
Create Date: 2019-11-18 16:37:16.450524

"""

# revision identifiers, used by Alembic.
from flask import current_app
from sqlalchemy import Sequence
from sqlalchemy.sql.ddl import CreateSequence

revision = '010'
down_revision = '009'

from alembic import op
import sqlalchemy as sa


def upgrade():
    # db = Manager(config=current_app.config, Model=DeclarativeBase)

    conn = op.get_bind()
    id_sequence = Sequence('voting_id_seq')
    op.execute(CreateSequence(id_sequence))
    op.add_column('voting', sa.Column('id', sa.Integer(), server_default=id_sequence.next_value(), nullable=True))
    conn.execute("update voting set id=(select 1+count(*) from voting as sub where sub.start_time < voting.start_time)")
    op.alter_column('voting', 'id', nullable=False)
    op.create_unique_constraint(op.f('uq_voting_code'), 'voting', ['code'], schema='toolkit')
    conn.execute('select setval(\'voting_id_seq\', (select max(id) from voting));')


def downgrade():
    op.drop_constraint(op.f('uq_voting_code'), 'voting', schema='toolkit', type_='unique')
    op.alter_column('voting', 'code',
               existing_type=sa.VARCHAR(length=16),
               nullable=False)
    op.drop_column('voting', 'id')
    op.execute('drop sequence voting_id_seq')