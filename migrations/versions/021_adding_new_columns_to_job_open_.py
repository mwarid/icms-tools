"""adding_status_column_to_job_open_position_table

Revision ID: 021
Revises: 020
Create Date: 2023-01-26 11:35:27.678151

"""

# revision identifiers, used by Alembic.
revision = "021"
down_revision = "020"

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


def upgrade():
    op.add_column(
        "job_open_position", sa.Column("status", sa.String(length=128), nullable=False)
    )
    op.add_column("job_open_position", sa.Column("start_date", sa.Date(), nullable=True))
    op.add_column("job_open_position", sa.Column("end_date", sa.Date(), nullable=True))
    op.add_column("job_open_position", sa.Column("nominations_deadline", sa.Date(), nullable=True))


def downgrade():
    op.drop_column("job_open_position", "status")
    op.drop_column("job_open_position", "start_date")
    op.drop_column("job_open_position", "end_date")
    op.drop_column("job_open_position", "nominations_deadline")
