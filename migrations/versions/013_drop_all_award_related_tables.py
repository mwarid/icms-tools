"""Remove outdated Award tables

Revision ID: 013
Revises: 012
Create Date: 2021-12-01 17:09:30.111777

"""

import sqlalchemy as sa
from alembic import op


# revision identifiers, used by Alembic.
revision = '013'
down_revision = '012'


def upgrade():
    op.execute('DROP TABLE IF EXISTS toolkit.award CASCADE')
    op.execute('DROP TABLE IF EXISTS toolkit.award_nomination CASCADE')
    op.execute('DROP TABLE IF EXISTS toolkit.award_nomination_status CASCADE')
    op.execute('DROP TABLE IF EXISTS toolkit.award_nominee CASCADE')
    op.execute('DROP TABLE IF EXISTS toolkit.award_type CASCADE')
    # op.drop_table('award', schema='toolkit')
    # op.drop_table('award_nomination', schema='toolkit')
    # op.drop_table('award_nomination_status', schema='toolkit')
    # op.drop_table('award_nominee', schema='toolkit')
    # op.drop_table('award_type', schema='toolkit')


def downgrade():
    # This seems strange, but the aim is to remove these tables from any prior point.
    # These tables were previously created using the common alembic and this lead to inconsistencies
    #  and permission issues. From now on, award tables should be managed by toolkit's alembic, same as
    # the other tables in the toolkit schema.
    # TODO: Add this migration to the common alembic or remove the migration that added the award tables!
    pass
