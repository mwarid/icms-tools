import sys
from datetime import date, timedelta, datetime
import flask
from flask_login import login_required, current_user
from icms_orm.common import ApplicationAsset

from util.structs import Table, Link
from .blueprint import cadi_viewer_blueprint
from .forms import CadiLatexCheckForm, CadiLatexCheckAdminForm
from collections import OrderedDict
from icms_orm.cmsanalysis import CadiHistory, CadiAnalysis, AnalysisAnalysts, AWG, AnalysisNoteRel as NoteRel, CDS
from sqlalchemy import func
from .forms import DateRangeForm
from icmsutils import timedate
from icmsutils.businesslogic import cadi
import re
import json
import urllib.request as urllib2
from importlib import reload
from sqlalchemy import distinct
from util import flask_extensions, constants
import logging
import sqlalchemy as sa

from scripts.doLatexBuild import CMSLatexBuildChecker

reload(sys)  # Reload does the trick!


@cadi_viewer_blueprint.route('/dashboard', methods=['POST', 'GET'])
@login_required
def route_dashboard():

    warnings = {}
    form = DateRangeForm()
    (time_range_from, time_range_till) = (not form.is_submitted() or form.validate(
    )) and (form.date_from.data, form.date_till.data) or (None, None)

    if not time_range_till or not time_range_from:
        return flask.render_template('generic_page.html', forms=[form])

    ssn = CadiAnalysis.session

    status_changes_sq = ssn.query(CadiHistory.masterId.label('anId'), sa.func.max(CadiHistory.id).label('hist_id')). \
        filter(CadiHistory.table == CadiAnalysis.__tablename__). \
        filter(CadiHistory.field == CadiAnalysis.status.name). \
        filter(CadiHistory.newValue != CadiHistory.oldValue). \
        filter(func.str_to_date(CadiHistory.updateDate, '%W, %d %M %Y').between(time_range_from, time_range_till)).\
        group_by(CadiHistory.masterId).\
        subquery()

    queried = (CadiAnalysis.code, CadiAnalysis.name, CadiHistory.updateDate,
               CadiHistory.newValue, CadiHistory.updaterName, CadiHistory.updaterId)

    q = ssn.query(*queried). \
        join(status_changes_sq, CadiAnalysis.id == status_changes_sq.c.anId). \
        join(CadiHistory, CadiHistory.id == status_changes_sq.c.hist_id)

    # ignore the hours, minutes and seconds (%H:%i:%s) since we're looking for a day-level granularity
    q = q.filter(func.str_to_date(CadiHistory.updateDate,
                                  '%W, %d %M %Y').between(time_range_from, time_range_till))

    rows = q.all()

    stats = {}
    final_changes = {}

    hidden_stuff = {}

    processors = {
        CadiHistory.updateDate: lambda x: timedate.parse_datetime_verbose(x),
        CadiAnalysis.code: lambda x: Link.link_to_cadi_line(x),
        CadiHistory.updaterId: lambda x: Link.link_to_user_profile(int(x)),
    }

    def process(row, column):
        return processors.get(column, lambda x: x)(getattr(row, column.key))

    mergers = {
        'ReadyForSub': 'ReadyForSub\FR',
        'FinalReading': 'ReadyForSub\FR',
        'ReadyForCWR': '[ReadyFor]CWR',
        'CWR': '[ReadyFor]CWR',
    }

    skip_states = {'free', 'planned', 'started', 'awg', 'superseded', 'waiting', 'thesis-approved',
                   'inactive', 'completed'}

    def make_dom_id(state): return 'details_%s' % re.sub(
        r'[\[\]\\]', '_', state)

    row_headers = {CadiAnalysis.name: 'Topic'}

    for r in rows:
        state = getattr(r, CadiHistory.newValue.key)
        if state.lower() in skip_states:
            continue
        aggregate_state = mergers.get(state, state)
        stats[aggregate_state] = stats.get(aggregate_state, 0) + 1
        # final changes track individual states
        final_changes[state] = final_changes.get(state, 0) + 1

        hidden_key = make_dom_id(aggregate_state)
        if hidden_key not in hidden_stuff:
            hidden_stuff[hidden_key] = Table.get_empty(
                keys=[row_headers.get(x, x.key) for x in queried])
        hidden_stuff[hidden_key].vals.append({
            row_headers.get(x, x.key): process(r, x) for x in queried
        })
        hidden_stuff[hidden_key].html_class = 'cadiLinesTable'

    keys = cadi.CadiState.get_ordered_state_names()

    stats = OrderedDict([
        (Link.link_reveal_hidden(make_dom_id(key), key, title='CADI lines in %s state (change between %s and %s)' %
                                 (key, time_range_from, time_range_till)), stats.get(key))
        for key in OrderedDict([(mergers.get(k, k), None) for k in keys]) if key in stats.keys()
    ])

    all_changes = {r[0]: r[1] for r in ssn.query(CadiHistory.newValue, func.count(distinct(CadiHistory.masterId))).
                   filter(CadiHistory.table == CadiAnalysis.__tablename__).
                   filter(CadiHistory.field == CadiAnalysis.status.name).
                   filter(CadiHistory.newValue != CadiHistory.oldValue).
                   filter(func.str_to_date(CadiHistory.updateDate, '%W, %d %M %Y').between(time_range_from, time_range_till)).
                   group_by(CadiHistory.newValue).all()}

    all_changes = OrderedDict([(key, all_changes.get(key)) for key in keys if key.lower(
    ) not in skip_states and key in all_changes.keys()])
    url = 'http://cms-results.web.cern.ch/cms-results/public-results/publications-vs-time/data/papers.json'
    graph_data = {}
    try:
        graph_data = json.loads(urllib2.urlopen(url).read())
        graph_data = {int(re.search(
            r'([\d]+)', x['url']).groups()[0]): x['title'] for x in graph_data if x}

    except Exception as e:
        warnings['Graph data'] = 'Failed to retrieve!'
    papers_data = Table.get_empty(
        keys=['Year', 'WG', 'Code', 'CDS record', 'In Graph?', 'CADI status'])

    cds_data = {}
    cadi_data = {r[0][0] == 'd' and r[0][1:] or r[0]: r[1] for r in ssn.query(
        CadiAnalysis.code, CadiAnalysis.status).all() if r[0]}
    try:
        cds_data = {int(k): v for k, v in (
            ApplicationAsset.retrieve('cadi_cds_records') or {}).items()}
        for x in [x for x in graph_data.keys() if x not in cds_data.keys()]:
            warnings['CDS record %d' %
                     x] = 'Included in graph but not in CDS papers.'

        for record_id, an_code in cds_data.items():
            if an_code:
                papers_data.vals.append({
                    papers_data.keys[0]: 2000 + int(an_code.split('-')[1]),
                    papers_data.keys[1]: an_code.split('-')[0],
                    papers_data.keys[2]: an_code,
                    papers_data.keys[3]: record_id,
                    papers_data.keys[4]: record_id in graph_data,
                    papers_data.keys[5]: cadi_data.get(an_code, False)
                })
            else:
                warnings['CDS record %d' % record_id] = 'Found no AN code.'
            if papers_data.vals[-1][papers_data.keys[0]] > date.today().year:
                warnings['CDS record %d' %
                         record_id] = 'Appears to be from year %d.' % papers_data.vals[-1][papers_data.keys[0]]
    except Exception as e:
        warnings['CDS data'] = 'Failed to retrieve!'

    def remap_awg(x):
        for merger in ['B2G/EXO', 'BPH/TOP', 'FSQ/PRF']:
            if x in merger.split('/'):
                return merger
        if x in {'DIF', 'EWK', 'FWD', 'GEN', 'PPS', 'QCD', 'SBM'}:
            return None
        return x

    SUBKEY_OTHER = 'Other'
    SUBKEY_ALL = 'All'

    awgs = sorted({remap_awg(r[0]) for r in ssn.query(
        AWG.name, AWG.type).all() if r[1] == 'PAG' and remap_awg(r[0])})
    awgs.append(SUBKEY_OTHER)
    awgs.append(SUBKEY_ALL)
    states = [cadi.CadiState.ACCEPT, cadi.CadiState.SUB,
              cadi.CadiState.CWR, cadi.CadiState.PHYS_APP, cadi.CadiState.PRE_APP]

    digest = dict()

    q_digest = ssn.query(sa.func.count(CadiAnalysis.id), AWG.name, CadiHistory.newValue) \
        .join(AWG, CadiAnalysis.awg == AWG.id) \
        .join(status_changes_sq, CadiAnalysis.id == status_changes_sq.c.anId) \
        .join(CadiHistory, CadiHistory.id == status_changes_sq.c.hist_id) \
        .group_by(AWG.name, CadiHistory.newValue).filter(CadiHistory.newValue.in_([x.code for x in states]))

    for count, awg, status in q_digest.all():
        awg_subkey = remap_awg(awg) in awgs and remap_awg(awg) or SUBKEY_OTHER
        for subkey in (awg_subkey, SUBKEY_ALL):
            digest[(status, subkey)] = digest.get((status, subkey), 0)
            digest[(status, subkey)] += count

    snapshot_data = {}

    q_snapshot = ssn.query(sa.func.count(CadiAnalysis.id), AWG.name, CadiAnalysis.status). \
        join(AWG, CadiAnalysis.awg == AWG.id).filter(CadiAnalysis.status.in_([x.code for x in states])). \
        group_by(AWG.name, CadiAnalysis.status)

    for count, awg, status in q_snapshot.all():
        awg_subkey = remap_awg(awg) in awgs and remap_awg(awg) or SUBKEY_OTHER
        for subkey in (awg_subkey, SUBKEY_ALL):
            snapshot_data[(status, subkey)] = snapshot_data.get(
                (status, subkey), 0)
            snapshot_data[(status, subkey)] += count

    from blueprints.api_restplus.endpoints.cadi_endpoints import xeb_report_period

    return flask.render_template('pub_comm_dashboard.html', title='PubComm Dashboard', forms=[form], hidden_data=hidden_stuff,
                                 data_maps=OrderedDict([
                                     (('Most recent status changes between %s and %s (only the last change per each CADI line counts)' % (
                                         time_range_from, time_range_till), 'all_stats'), stats),
                                     (('Status changes between %s and %s (any change counts except repeating analysis-state pairs)' % (
                                         time_range_from, time_range_till), 'all_changes'), all_changes),
                                     (('Digest', 'digest'), digest),
                                     # (('%d not PUB entries with CDS records' % len(db_info), 'cds_data'), db_info), (('Missing CDS records', 'missing_records'), missing_records)
                                 ]), table_data=papers_data, warnings=warnings, date_today=date.today(),
                                 matrix_data={'states': states, 'awgs': awgs,
                                              'cell_data': {(state, awg): Link(href=flask_extensions.quick_url(route_dialog_analysis_details, date_from=time_range_from, date_till=time_range_till, awg=awg, state=state),
                                                                               type=Link.Type.MODAL_PASSIVE, label=str(digest.get((state, awg), 0))) for (state, awg) in digest.keys()},
                                              'state_totals': {state.code: sum([digest.get((state.code, awg), 0) for awg in awgs]) for state in states}},
                                 snapshot={'data': {(state, awg):
                                                    Link(label=snapshot_data.get((state, awg), 0), type=Link.Type.MODAL_PASSIVE,
                                                         href=flask_extensions.quick_url(route_dialog_analysis_details, awg=awg, state=state, here_and_now=True))
                                                    for state, awg in snapshot_data}, 'states': states, 'awgs': awgs,
                                           'state_totals': {state.code: sum([snapshot_data.get((state.code, awg), 0) for awg in awgs]) for state in states}},

                                 xeb_refresh_endpoint_short=flask.url_for('%s.%s' % (
                                     constants.BP_NAME_API_RESTPLUS, 'cadi_xeb_report')),
                                 xeb_refresh_endpoint_long=flask.url_for('%s.%s' % (
                                     constants.BP_NAME_API_RESTPLUS, 'cadi_xeb_report'), **{xeb_report_period: 14})

                                 )


@cadi_viewer_blueprint.route('/listCadiLines')
@login_required
def route_dialog_analysis_details():
    (date_from, date_till, awg, state, here_and_now) = [flask.request.args.get(
        x, None) for x in ['date_from', 'date_till', 'awg', 'state', 'here_and_now']]

    title = '{awg} analyses {qualifier} {state} state{window}'.format(
        qualifier='in' if here_and_now else 'that entered',
        awg=awg,
        state=state,
        window=(date_from and date_till and ' between %s and %s' % (
            date_from, date_till) or ' as of %s' % (date_till or date.today(), ))
    )

    date_from = date_from or date(1900, 1, 1)
    date_till = date_till or date.today()

    ssn = CadiHistory.session

    last_status_change_sq = ssn.query(CadiHistory.masterId.label('anId'), sa.func.max(CadiHistory.id).label('hist_id')). \
        filter(CadiHistory.table == CadiAnalysis.__tablename__). \
        filter(CadiHistory.field == CadiAnalysis.status.name). \
        filter(CadiHistory.newValue != CadiHistory.oldValue). \
        filter(func.str_to_date(CadiHistory.updateDate,
                                '%W, %d %M %Y').between(date_from, date_till))

    last_status_change_sq = state and last_status_change_sq.filter(
        CadiHistory.newValue.ilike(state)) or last_status_change_sq
    last_status_change_sq = last_status_change_sq.group_by(
        CadiHistory.masterId).subquery()

    queried = (CadiAnalysis.code, func.coalesce(CDS.title, CadiAnalysis.name), func.str_to_date(CadiHistory.updateDate,
                                                                                                '%W, %d %M %Y'), CadiHistory.newValue, CadiAnalysis.status, CadiAnalysis.publi)

    sq_cds = ssn.query(sa.func.max(CDS.id).label('cds_max_id'), CDS.code.label('cds_code')).group_by(
        CDS.code).subquery()

    q = ssn.query(*queried). \
        join(last_status_change_sq, CadiAnalysis.id == last_status_change_sq.c.anId). \
        join(CadiHistory, CadiHistory.id == last_status_change_sq.c.hist_id).\
        join(AWG, AWG.id == CadiAnalysis.awg). \
        outerjoin(sq_cds, CadiAnalysis.code.in_([sq_cds.c.cds_code, sa.func.concat('d', sq_cds.c.cds_code)])). \
        outerjoin(CDS, sq_cds.c.cds_max_id == CDS.id)

    if awg.upper() == 'ALL':
        pass
    elif '/' in awg:
        q = q.filter(AWG.name.in_(awg.split('/')))
    elif awg.upper() in ['OTHER', 'NON-PAG', 'NONPAG']:
        non_pag_awgs = {r[0] for r in ssn.query(
            AWG.name).filter(AWG.type != 'PAG').all()}
        q = q.filter(
            sa.or_(*[CadiAnalysis.code.ilike('%%%s%%' % token) for token in non_pag_awgs]))
    else:
        q = awg and q.filter(CadiAnalysis.code.ilike('%%%s%%' % awg)) or q

    if here_and_now and state:
        q = q.filter(CadiAnalysis.status.ilike(state))

    q = q.filter(func.str_to_date(CadiHistory.updateDate,
                                  '%W, %d %M %Y').between(date_from, date_till))

    table = Table.get_empty(
        keys=['Code', 'Title', 'State change date', 'Target state', 'Present state'])
    for row in q.all():
        row = [x for x in row]
        if row[0].startswith('d'):
            row[0] = row[0][1:]
        row[0] = Link.link_to_cadi_line(code=row[0], label=row[0])
        table.append_values_from_db_row(row)
        if state in {cadi.CadiState.CWR.code, cadi.CadiState.ACCEPT.code, cadi.CadiState.SUB.code}:
            if table.keys[-1] != 'Journal':
                table.keys.append('Journal')
            try:
                table.vals[-1]['Journal'] = CadiAnalysis.from_ia_dict(
                    {CadiAnalysis.publi: row[-1]}).get_publication_info().get('journals', 'None')
            except ValueError as ve:
                logging.warning(
                    'Failed to extract target journals for {code}'.format(code=row[0]))
                table.vals[-1]['Journal'] = 'error fetching journals'

    return flask_extensions.render_generic_dialog_details_template(title=title, details=table)


@cadi_viewer_blueprint.route('/checkLatexBuild', methods=['GET', 'POST'])
@login_required
def checkLatexBuild():

    latexCheckForm = None
    if current_user.is_admin():
        latexCheckForm = CadiLatexCheckAdminForm()
    else:
        latexCheckForm = CadiLatexCheckForm()

    # set some defaults:
    (runTime, buildTime) = (-1., -1.)
    verbose = False
    keepFiles = False

    if latexCheckForm.validate_on_submit():
        cadiType = latexCheckForm.data['cadiType']
        cadiLine = latexCheckForm.data['cadiLine']
        dataMC = latexCheckForm.data['dataMC']
        preview = latexCheckForm.data['preview']
        if current_user.is_admin():
            verbose = latexCheckForm.data['verbose']
            keepFiles = latexCheckForm.data['keepFiles']

        args = []
        if 'pas' in cadiType.lower():
            args += ['--wrap']
        if 'paper' in cadiType.lower():
            args += ['--export']
        if preview:
            args = ['--preview', '--nodraft']
        if dataMC != 'd':
            args += ['--nodata']

        lbc = CMSLatexBuildChecker()
        if verbose:
            lbc.verbose = True
        if keepFiles:
            lbc.keepFiles = True

        log, (runTime, buildTime) = lbc.processOne(args + [cadiType, cadiLine])

        return flask.render_template('cadiCheckLatexBuild.html',
                                     cadiLine=cadiLine,
                                     dataMC=dataMC,
                                     cadiType=cadiType,
                                     preview=preview,
                                     verbose=verbose,
                                     keepFiles=keepFiles,
                                     log=log,
                                     form=latexCheckForm)

    return flask.render_template('cadiCheckLatexBuild.html',
                                 cadiLine='',
                                 dataMC='',
                                 cadiType='',
                                 preview=False,
                                 verbose=False,
                                 keepFiles=False,
                                 log=None,
                                 form=latexCheckForm)
