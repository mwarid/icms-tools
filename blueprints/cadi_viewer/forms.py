from flask_wtf import Form
from wtforms.fields import StringField, RadioField, SubmitField, BooleanField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired
from datetime import date, timedelta as td


class CadiLatexCheckForm(Form):
    cadiLine = StringField(label="Enter CADI line")
    cadiType = RadioField(label="Type", choices=[
                          ('pas', 'PAS'), ('paper', 'Paper')], default='pas')
    preview = BooleanField(label="Preview (MathJax check)", default=False)
    dataMC = RadioField(
        label="Data/MC", choices=[('d', 'data'), ('m', 'MC')], default='d')
    submit = SubmitField()


class CadiLatexCheckAdminForm(CadiLatexCheckForm):
    verbose = BooleanField(label="verbose", default=False)
    keepFiles = BooleanField(label="keep files and dirs", default=False)


class DateRangeForm(Form):
    date_from = DateField(default=date.today() -
                          td(days=13), validators=[DataRequired()])
    date_till = DateField(default=date.today(), validators=[DataRequired()])
    last_week = SubmitField('Last Week')
    last_two_weeks = SubmitField('Last 2 Weeks')
    this_month = SubmitField('This Month')
    this_year = SubmitField('This Year')
    submit = SubmitField('Range Indicated Above')

    def validate(self):
        today = date.today()
        override = None
        if self.last_week.data:
            override = (today - td(days=6), today)
        elif self.last_two_weeks.data:
            override = (today - td(days=13), today)
        elif self.this_month.data:
            override = (date(today.year, today.month, 1), today)
        elif self.this_year.data:
            override = (date(today.year, 1, 1), today)
        else:
            pass

        if override:
            (self.date_from.data, self.date_till.data) = override

        result = Form.validate(self)
        if not result:
            return result
        elif self.date_from.data > self.date_till.data:
            self.date_from.errors.append('Starting date after the end date.')
            self.date_till.errors.append('End date preceeds the start date.')
            return False
        return True
