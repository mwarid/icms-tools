from flask import Blueprint
from util import constants as const

cadi_viewer_blueprint = Blueprint(const.BP_NAME_CADI_VIEWER, __name__, template_folder='templates',
                                  static_folder='static')
