from collections import defaultdict
from datetime import date, timedelta
from json import loads
from logging import error
from typing import List, Optional, Dict, Set

from flask import current_app
from ldap import SERVER_DOWN
from pydantic import BaseModel
from sqlalchemy import func, or_, and_

from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory
from icms_orm.cmspeople import Person
from icms_orm.cmsanalysis import PaperAuthor, PaperAuthorHistory
from icmsutils.ldaputils import LdapProxy

t_0 = date(2022, 10, 10)


class PersonData(BaseModel):
    cms_id: int
    hr_id: int
    last_name: str
    first_name: str
    date_end_sign: Optional[date] = None
    date_creation: Optional[date] = None
    remarks: Optional[str] = None
    remarks_secr: Optional[str] = None
    is_author: Optional[bool] = None
    exit_date: Optional[date] = None
    last_sign_date: Optional[date] = None
    days_before: Optional[int] = None
    computed_end: Optional[date] = None
    extra_months: Optional[int] = None
    discrepancy: Optional[int] = None
    egroups: Optional[Set] = None

    class Config:
        json_encoders = {
            date: lambda d: d.strftime("%Y-%m-%d")
        }

    @classmethod
    def from_lists(cls, row: List, cols: List):
        entry = {col: value for col, value in zip(cols, row)}
        person = cls(**entry)
        exit_date, end_date_sign = entry["exit_date"], entry["date_end_sign"]
        person.exit_date = None if exit_date == "0000-00-00" else exit_date
        person.date_end_sign = None if end_date_sign == "0000-00-00" else end_date_sign
        person.days_before = (t_0 - person.date_creation).days
        person.extra_months = max(min(4 * person.days_before // 365, 48), 0)
        person.computed_end = person.exit_date + timedelta(round((12 + person.extra_months) * 365 // 12))
        person.discrepancy = person.computed_end - (person.date_end_sign or person.exit_date + timedelta(days=365)).days / (365 / 12.0)
        return person

    def categorize(self) -> str:
        if not self.last_sign_date or (self.exit_date - self.last_sign_date).days > 21:
            return "not_signing"
        elif self.date_end_sign and abs((self.date_end_sign - self.computed_end).days) < 30:
            return "correct"
        elif self.computed_end >= date.today():
            return "fixable"
        else:
            return "outdated"


class DateEndSignCall(BaseApiUnit):
    person_data = ModelFactory.Nested(ModelFactory.make_model("Date end sign person data", {
        "cms_id": ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        }),
        "hr_id": ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: False
        }),
        "last_name": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True
        }),
        "first_name": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True
        }),
        "date_end_sign": ModelFactory.field_proxy(ModelFactory.Date, {
            ModelFactory.REQUIRED: False
        }),
        "date_creation": ModelFactory.field_proxy(ModelFactory.Date, {
            ModelFactory.REQUIRED: False
        }),
        "remarks": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False
        }),
        "remarks_secr": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False
        }),
        "is_author": ModelFactory.field_proxy(ModelFactory.Boolean, {
            ModelFactory.REQUIRED: False
        }),
        "exit_date": ModelFactory.field_proxy(ModelFactory.Date, {
            ModelFactory.REQUIRED: False
        }),
        "last_sign_date": ModelFactory.field_proxy(ModelFactory.Date, {
            ModelFactory.REQUIRED: False
        }),
        "days_before": ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: False
        }),
        "computed_end": ModelFactory.field_proxy(ModelFactory.Date, {
            ModelFactory.REQUIRED: False
        }),
        "extra_months": ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: False
        }),
        "discrepancy": ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: False
        }),
        "egroups": ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.REQUIRED: False,
            ModelFactory.ELEMENT_TYPE: ModelFactory.Integer
        }),
    }))

    _model = ModelFactory.make_model("Date end sign", {
        "correct": ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.REQUIRED: True,
            ModelFactory.READONLY: True,
            ModelFactory.ELEMENT_TYPE: person_data
        }),
        "not_signing": ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.REQUIRED: True,
            ModelFactory.READONLY: True,
            ModelFactory.ELEMENT_TYPE: person_data
        }),
        "fixable": ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.REQUIRED: True,
            ModelFactory.READONLY: True,
            ModelFactory.ELEMENT_TYPE: person_data
        }),
        "outdated": ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.REQUIRED: True,
            ModelFactory.READONLY: True,
            ModelFactory.ELEMENT_TYPE: person_data
        }),
    })

    @staticmethod
    def get_data() -> List:
        statuses = ["EXMEMBER", "CMSEXTENDED"]
        ssn = PaperAuthor.session()

        sq = (ssn.query(PaperAuthor.cmsid.label("cmsId"),
                       func.max(func.str_to_date(PaperAuthorHistory.date, "%d/%m/%Y")).label("last_sign_date"))
            .join(PaperAuthorHistory, PaperAuthor.code == PaperAuthorHistory.code)
            .join(Person, Person.cmsId == PaperAuthor.cmsid).filter(Person.status.in_(statuses))
            .filter(PaperAuthorHistory.action == "ListGen").filter(PaperAuthor.status.ilike("%in%"))
            .group_by(PaperAuthor.cmsid)
            .subquery())

        return (ssn.query(Person.cmsId, Person.hrId, Person.lastName, Person.firstName, Person.status,
                         Person.dateEndSign, Person.dateCreation, Person.remarks, Person.remarksSecr,
                         Person.isAuthor, Person.exDate, sq.c.last_sign_date)
            .filter(Person.status.in_(statuses))
            .filter(or_(and_(Person.isAuthor2009 == 1, Person.dateCreation < t_0), Person.status == "CMSEXTENDED"))
            .filter(Person.exDate)
            .filter(Person.dateEndSign)
            .filter(func.str_to_date(Person.exDate, "%Y-%m-%d") > date(year=date.today().year - 5, month=date.today().month, day=date.today().day))
            .outerjoin(sq, Person.cmsId == sq.c.cmsId)
            .all())

    @staticmethod
    def find_egroups(people: List[PersonData]) -> Dict:
        hrids = [person.hr_id for person in people]
        egroup_info = defaultdict(set)
        try:
            ldap_info = LdapProxy.get_instance(ldap_uri_override=current_app.config.get("LDAP_URI", None)) \
                .find_people_by_hrids(hrids)
            for person in ldap_info:
                for egroup in person.cernEgroups + person.cmsEgroups:
                    if egroup in {"zp", "z2", "z5"}:
                        egroup_info[person.hr_id].add(egroup)
        except SERVER_DOWN:
            error("Cannot connect to LDAP")
        finally:
            return egroup_info

    @staticmethod
    def get_date_end_sign():
        tables = {"fixable": [], "outdated": [], "not_signing": [], "correct": []}
        cols = ["cms_id", "hr_id", "last_name", "first_name", "status", "date_end_sign", "date_creation", "remarks",
                "remarks_secr", "is_author", "exit_date", "last_sign_date"]

        data = DateEndSignCall.get_data()
        people = []
        for row in data:
            people.append(PersonData.from_lists(row, cols))
        egroups = DateEndSignCall.find_egroups(people)
        for person in people:
            person.egroups = egroups[person.hr_id]
            object = loads(person.json())
            tables[person.categorize()].append(object)

        return tables
