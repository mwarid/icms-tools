from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from typing import List
from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit, ModelFactory, ModelFieldDefinition as MDF
from icms_orm.toolkit import RestrictedResource as RR


class RestrictedResourcesApiCall(AbstractCRUDUnit):
    
    _model_fields = [
        MDF.builder(ModelFactory.Integer).column(RR.id).id().readonly(True).build(),
        MDF.builder(ModelFactory.String).column(RR.type).build(),
        MDF.builder(ModelFactory.String).column(RR.key).build(),
        MDF.builder(ModelFactory.Raw).column(RR.filters).build()
    ]
    
    @classmethod
    def get_model_name(cls) -> str:
        return 'Restricted Resource'

    @classmethod
    def get_model_fields_list(cls) -> List[MDF]:
        return cls._model_fields

    @classmethod
    def create_filter(cls, id: int):
        return ResourceFilter(column=RR.id, value=id)
