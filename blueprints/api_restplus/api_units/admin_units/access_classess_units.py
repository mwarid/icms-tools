from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from typing import List
from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit
from blueprints.api_restplus.api_units.basics import ModelFactory, ModelFieldDefinition as MDF
from icms_orm.toolkit import AccessClass


class AccessClassesApiCall(AbstractCRUDUnit):
      
    _model_fields = [
        MDF.builder(ModelFactory.Integer).column(AccessClass.id).id().readonly(True).build(),
        MDF.builder(ModelFactory.String).column(AccessClass.name).build(),
        MDF.builder(ModelFactory.Raw).column(AccessClass.rules).build()
    ]

    @classmethod
    def get_model_fields_list(cls) -> List[MDF]:
        return cls._model_fields

    @classmethod
    def get_model_name(cls) -> str:
        return 'Access Class'

    @classmethod
    def create_filter(cls, id: int):
        return ResourceFilter(column=AccessClass.id, value=id)

