from icms_orm.common import EmailMessage as Msg, EmailLog as Log, EmailMessage
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from datetime import datetime
from sqlalchemy import func, desc

class EmailsApiCall(BaseApiUnit):
    _model = ModelFactory.make_model('Email List Item', {
        Msg.id.key : ModelFactory.Integer,
        Msg.subject.key : ModelFactory.String,
        Msg.to.key : ModelFactory.String,
        Msg.bcc.key : ModelFactory.String,
        Msg.last_modified.key : ModelFactory.DateTime,
        Msg.sender.key : ModelFactory.String,
        Msg.reply_to.key : ModelFactory.String,
        Msg.body.key: ModelFactory.String,
        Log.status.key : ModelFactory.String,
        Log.action.key : ModelFactory.String,
        Log.timestamp.key : ModelFactory.DateTime,
        Msg.source_application.key : ModelFactory.String,
    })

    @classmethod
    def get_emails(cls):
        ssn = EmailMessage.session()

        s = ssn.query(func.max(Log.id).label('log_id'), Log.email_id).group_by(Log.email_id).subquery()

        cols = [
            Msg.id, 
            Msg.subject, 
            Msg.to, 
            Msg.bcc, 
            Msg.last_modified, 
            Msg.sender,
            Msg.reply_to, 
            Log.status, 
            Log.action,
            Log.timestamp,
            Msg.source_application
        ]

        data = ssn.query(*cols).join(s, Msg.id == s.c.email_id).join(Log, s.c.log_id == Log.id).order_by(
            desc(Msg.id)).limit(500).all()
        return cls.rows_to_list_of_dicts(data, cols_list=cols)
        
class EmailApiCall(BaseApiUnit):

    _model = ModelFactory.make_model('Email Info', {
        Msg.id.key : ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: False,
        }),
        Msg.sender.key : ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
        }),
        Msg.reply_to.key : ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False,
        }),
        Msg.to.key : ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
        }),
        Msg.subject.key : ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
        }),
        Msg.cc.key : ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False,
        }),
        Msg.bcc.key : ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False,
        }),
        Msg.body.key : ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
        }),
        Msg.source_application.key : ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False,
        }),
        Msg.hash.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False,
        }),
        Msg.remarks.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False,
        }),
        Msg.last_modified.key : ModelFactory.field_proxy(ModelFactory.DateTime, {
            ModelFactory.REQUIRED: False,
        }),

        'logs' : ModelFactory.List(ModelFactory.Nested(ModelFactory.make_model('Logs Info', {
            Log.id.key: ModelFactory.Integer,
            Log.email_id.key: ModelFactory.Integer,
            Log.timestamp.key: ModelFactory.DateTime,
            Log.action.key: ModelFactory.String,
            Log.status.key: ModelFactory.String,
            Log.details.key: ModelFactory.String,
        })))
    })

    _args = ParserBuilder()\
    .add_argument(Msg.id.key, ParserBuilder.INTEGER, True)\
    .parser

    @classmethod
    def _do_get_email(cls, args):
        msg_cols = [
            Msg.id,
            Msg.sender,
            Msg.reply_to,
            Msg.to, 
            Msg.cc,
            Msg.bcc, 
            Msg.subject,
            Msg.body,
            Msg.hash,
            Msg.source_application,
            Msg.remarks,
            Msg.last_modified
        ]
        log_cols = [
            Log.id,
            Log.email_id,
            Log.timestamp,
            Log.action,
            Log.status,
            Log.details,
        ]

        email = Msg.session().query(*msg_cols).filter(Msg.id == args['id']).one()
        logs = Msg.session().query(*log_cols).filter(Log.email_id == args['id']).all()
        
        response = cls.row_to_dict(email, cols_list=msg_cols)
        response['logs'] = cls.rows_to_list_of_dicts(logs, cols_list=log_cols)
        return response

    @classmethod
    def get_email(cls):
        args = cls.parse_args()
        return cls._do_get_email(args)

    @classmethod
    def post_email(cls):
        payload = cls.get_payload()
        email, logs = EmailMessage.compose_message(sender=payload['sender'], to=payload['to'], subject=payload['subject'],
                body=payload['body'], cc=payload['cc'], bcc=payload['bcc'], source_app='toolkit', reply_to=None,
                remarks='Submitted manually through the admin interface.', db_session=EmailMessage.session())
        return cls._do_get_email({EmailMessage.id.key: email.get(EmailMessage.id)})