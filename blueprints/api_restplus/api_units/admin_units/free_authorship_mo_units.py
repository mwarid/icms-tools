from datetime import date, datetime

from sqlalchemy import and_, func

from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory
from icms_orm.cmspeople import MoData, MemberActivity, Person, Institute
from icms_orm.epr import TimeLineUser as TLU, Category, TimeLineInst as TLI


class FreeAuthorshipMOCall(BaseApiUnit):
    INNER_ELEMENT_TYPE = ModelFactory.Nested(ModelFactory.make_model("Inner free MO", {
        "cms_id": ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        }),
        "surname": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True
        }),
        "name": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True
        }),
        "institute": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True
        }),
        "activity_now": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False
        }),
        "free_mo": ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False
        }),
        "institute_join_date": ModelFactory.field_proxy(ModelFactory.Date, {
            ModelFactory.REQUIRED: False
        }),
    }))

    _model = ModelFactory.make_model("Free MO", {
        "post_closure_arrivals": ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.REQUIRED: True,
            ModelFactory.READONLY: True,
            ModelFactory.ELEMENT_TYPE: INNER_ELEMENT_TYPE
        }),
        "members": ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.REQUIRED: True,
            ModelFactory.READONLY: True,
            ModelFactory.ELEMENT_TYPE: INNER_ELEMENT_TYPE
        }),
    })

    @staticmethod
    def get_free_auhorship_mo():
        ssn = MoData.session()
        cutoff_date = date(datetime.now().year - 1, 8, 15)
        mo_year = cutoff_date.year + 1

        sq = (ssn.query(TLU.cmsId.label("cms_id"), TLU.instCode, func.min(TLU.timestamp).label("day"))
            .filter(TLU.year == cutoff_date.year)
            .group_by(TLU.instCode, TLU.cmsId).subquery())
        rows = (ssn.query(sq, TLU, Category.name).join(TLU, and_(TLU.cmsId == sq.c.cms_id, TLU.timestamp == sq.c.day))
            .join(Category, Category.id == TLU.category).filter(sq.c.day > cutoff_date).filter(
            Category.name.ilike("physicist")).all())

        mo_column = getattr(MoData, MoData.freeMo2020.key.replace("2020", str(mo_year)))
        xcheck = {x[0]: x for x in
                  ssn.query(Person.cmsId, Person.lastName, Person.firstName, mo_column, Person.instCode)
                      .join(MoData, Person.cmsId == MoData.cmsId).filter(Person.cmsId.in_([x[0] for x in rows])).all()}

        post_closure_arrivals = []

        for row in rows:
            cms_id = row[0]
            xcheck_data = xcheck.get(cms_id)
            if not xcheck_data:
                continue
            entry = {
                "cms_id": row[0],
                "surname": xcheck_data[1],
                "name": xcheck_data[2],
                "institute": (row[1] == xcheck_data[4] and row[1]) or f"{row[1]} is {xcheck_data[4]}",
                "free_mo_year": xcheck_data[3],
                "transfer_date": row[2].strftime("%Y-%m-%d"),
                "activity_during_transfer": row[-1]
            }
            post_closure_arrivals.append(entry)

        insts = {code: timestamp for code, timestamp in ssn.query(TLI.code, func.min(TLI.timestamp))
            .filter(TLI.cmsStatus.ilike("yes"))
            .group_by(TLI.code).all() if timestamp.year == cutoff_date.year}

        rows = (ssn.query(Person.cmsId, Person.lastName, Person.firstName, Person.instCode, MemberActivity.name,
                         mo_column) \
            .join(MoData, MoData.cmsId == Person.cmsId).join(Institute, Institute.code == Person.instCode)
            .join(MemberActivity, MemberActivity.id == Person.activityId)
            .filter(Institute.cmsStatus.ilike("yes"))
            .filter(MemberActivity.name.ilike("physicist"))
            .filter(Person.instCode.in_(insts.keys()))
            .all())

        members = []
        for row in rows:
            entry = {
                "cms_id": row[0],
                "surname": row[1],
                "name": row[2],
                "institute": row[3],
                "activity_now": row[4],
                "free_mo": row[5],
                "institute_join_date": insts.get(row[3]).strftime("%Y-%m-%d")
            }
            members.append(entry)

        return {
            "post_closure_arrivals": post_closure_arrivals,
            "members": members
        }
