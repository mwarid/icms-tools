from blueprints.api_restplus.api_units.basics import BaseApiUnit, ParserBuilder, ModelFactory
import flask
from flask_login import current_user
from blueprints.api_restplus.exceptions import AccessViolationException
from util.accountCheck import AccountChecker
import logging, traceback
from util import constants


class AccountStatusCall(BaseApiUnit):

    _model = ModelFactory.make_model('Placeholder', {
        'hr_id': ModelFactory.Integer,
        'operation': ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.ENUM: ['check', 'add_to_zh', 'send_email'],
            ModelFactory.REQUIRED: False,
            ModelFactory.DEFAULT: 'check'
        }),
        'result': ModelFactory.String,
        'details': ModelFactory.Raw
    })

    _args = ParserBuilder().\
        add_argument('hr_id', ParserBuilder.INTEGER, required=True).\
        add_argument('action', ParserBuilder.STRING, default='check', required=False,
                     choices=['check', 'add_to_zh', 'send_email']). \
        parser

    @classmethod
    def _get_hr_id(cls):
        args = cls.parse_args()
        return args.get('hr_id', current_user.cms_id)

    @classmethod
    def _get_account_check_result(cls, hr_id):
        try:
            ac = AccountChecker(verbose=False)
            ac.check(hrId=hr_id)
            issues_count = len(ac.issues['error'])
            account_check_result = '{0} issue{1} found'.format(issues_count or 'No', issues_count != 1 and 's' or '')
        except Exception as e:
            logging.warning(traceback.format_exc())
            account_check_result = 'Exception encountered while checking account status.'
        return account_check_result

    @classmethod
    def get(cls):
        hr_id = cls._get_hr_id()
        return {
            'hr_id': hr_id,
            'operation': 'check',
            'result': cls._get_account_check_result(hr_id),
            'details': {'url': flask.url_for('%s.route_account_status' % constants.BP_NAME_USERS, hr_id=hr_id)}
        }

    @classmethod
    def post(cls):
        hr_id = cls._get_hr_id()
        if not current_user.is_admin():
            raise AccessViolationException('Only admins can perform this operation (fixing account status).')
        else:
            pass
        return {'hr_id': hr_id}
