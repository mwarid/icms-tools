from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from typing import List
from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit, ModelFactory, ParserBuilder, ModelFieldDefinition
from icms_orm.toolkit import Permission


class AccessPermissionsApiCall(AbstractCRUDUnit):
      
    _model_fields = [
        ModelFieldDefinition.builder(ModelFactory.Integer).column(Permission.id).required().readonly().id().build(),
        ModelFieldDefinition.builder(ModelFactory.Integer).column(Permission.resource_id).build(),
        ModelFieldDefinition.builder(ModelFactory.Integer).column(Permission.access_class_id).build(),
        ModelFieldDefinition.builder(ModelFactory.String).column(Permission.action).build()
    ]

    @classmethod
    def get_model_fields_list(cls) -> List[ModelFieldDefinition]:
        return cls._model_fields

    @classmethod
    def get_model_name(cls) -> str:
        return 'Access Permission'

    @classmethod
    def create_filter(cls, id: int):
        return ResourceFilter(column=Permission.id, value=id)
