from blueprints.api_restplus.api_units.basics import BaseApiUnit, ParserBuilder, ModelFactory
from icmsutils.businesslogic import paperstats


class AuthorshipStatsCall(BaseApiUnit):
    _args = ParserBuilder().add_argument('cms_id', ParserBuilder.INTEGER, required=False).parser
    _model = ModelFactory.make_model('Authorship stats', {
        'cms_id': ModelFactory.field_proxy(ModelFactory.Integer, {ModelFactory.REQUIRED: False}),
        'counts': ModelFactory.field_proxy(ModelFactory.Raw, {ModelFactory.EXAMPLE: {2018: 42, 2019: 151}})
    })

    @classmethod
    def get(cls):
        cms_id = cls.parse_args().get('cms_id', None)
        result = {'cms_id': cms_id, 'counts': {}}
        if cms_id is not None:
            result['counts'] = paperstats.count_signed_papers_by_year(cms_id=cms_id)
        else:
            result['counts'] = paperstats.count_generated_al_per_year()
        return result



