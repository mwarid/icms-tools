import secrets
import flask
from builtins import classmethod
from time import strftime
from datetime import datetime, time
from collections import defaultdict

from flask_login import current_user
from sqlalchemy.orm import contains_eager

from icms_orm.cmspeople import User
from icms_orm.common import Person
from icms_orm.toolkit import WeeklyMeetings, WeeklyMeetingsConvener, WeeklyMeetingState, EmailMessage

from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit, ModelFactory, ParserBuilder
)
from blueprints.api_restplus.api_units.basics.model_field_types import List
from blueprints.api_restplus.exceptions import BadRequestException, NotFoundException, AccessViolationException


class WeeklyMeetingsApiCall(BaseApiUnit):

    _model = ModelFactory.make_model('Weekly Meetings', {
        WeeklyMeetings.id.key: ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: False,
            ModelFactory.READONLY: True,
        }),
        WeeklyMeetings.title.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
        }),
        WeeklyMeetings.odd_even_weekly.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
            ModelFactory.EXAMPLE: "odd"
        }),
        WeeklyMeetings.day_of_week.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
            ModelFactory.EXAMPLE: "monday"
        }),
        WeeklyMeetings.start_time.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
            ModelFactory.EXAMPLE: "11:00"
        }),
        WeeklyMeetings.end_time.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
            ModelFactory.EXAMPLE: "15:00"
        }),
        WeeklyMeetings.room.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False,
            ModelFactory.EXAMPLE: "28/S-029"
        }),
        WeeklyMeetings.primary_contact.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: False,
        }),
        WeeklyMeetings.state.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
            ModelFactory.EXAMPLE: "proposed"
        }),
        'weekly_meeting_conveners' : ModelFactory.List(ModelFactory.Nested(ModelFactory.make_model('Weekly Meeting Conveners', {
            WeeklyMeetingsConvener.cms_id.key: ModelFactory.field_proxy(ModelFactory.Integer, {
                ModelFactory.REQUIRED: True,
                ModelFactory.EXAMPLE: 1
            }),
            Person.first_name.key: ModelFactory.field_proxy(ModelFactory.String, {
                ModelFactory.READONLY: True,
            }),
            Person.last_name.key: ModelFactory.field_proxy(ModelFactory.String, {
                ModelFactory.READONLY: True,
            }),
        }))),
        '_allowed_actions': ModelFactory.field_proxy(ModelFactory.List, {
            ModelFactory.ELEMENT_TYPE: ModelFactory.String,
            ModelFactory.EXAMPLE: ['approve', 'edit', 'cancel'],
            ModelFactory.REQUIRED: False,
            ModelFactory.READONLY: True,
            ModelFactory.DEFAULT: ['approve', 'edit', 'cancel'],
        }),
    })
    _args = (
        ParserBuilder()
        .add_argument(WeeklyMeetings.title.key, type=ParserBuilder.STRING, required=False)
        .add_argument(WeeklyMeetings.odd_even_weekly.key, type=ParserBuilder.STRING, required=False)
        .add_argument(WeeklyMeetings.day_of_week.key, type=ParserBuilder.STRING, required=False)
        .add_argument(WeeklyMeetings.start_time.key, type=ParserBuilder.DATE, required=False)
        .add_argument(WeeklyMeetings.end_time.key, type=ParserBuilder.DATE, required=False)
        .add_argument(WeeklyMeetings.room.key, type=ParserBuilder.STRING, required=False)
        .add_argument(WeeklyMeetings.primary_contact.key, type=ParserBuilder.STRING, required=False)
        .add_argument(WeeklyMeetings.state.key, type=ParserBuilder.STRING, required=False)
        .add_argument('include_deleted', type=ParserBuilder.BOOLEAN, required=False, default=False)
        .parser
    )
    _args_mapping = [
        WeeklyMeetings.id,
        WeeklyMeetings.title,
        WeeklyMeetings.odd_even_weekly,
        WeeklyMeetings.day_of_week,
        WeeklyMeetings.start_time,
        WeeklyMeetings.end_time,
        WeeklyMeetings.room,
        WeeklyMeetings.primary_contact,
        WeeklyMeetings.state,
    ]
    _cols_out = [
        WeeklyMeetings.id,
        WeeklyMeetings.title,
        WeeklyMeetings.odd_even_weekly,
        WeeklyMeetings.day_of_week,
        WeeklyMeetings.start_time,
        WeeklyMeetings.end_time,
        WeeklyMeetings.room,
        WeeklyMeetings.primary_contact,
        WeeklyMeetings.state,
        WeeklyMeetingsConvener.cms_id,
        Person.first_name,
        Person.last_name,
    ]
    _cols_in = [
        WeeklyMeetings.id,
        WeeklyMeetings.title,
        WeeklyMeetings.odd_even_weekly,
        WeeklyMeetings.day_of_week,
        WeeklyMeetings.start_time,
        WeeklyMeetings.end_time,
        WeeklyMeetings.room,
        WeeklyMeetings.primary_contact,
        WeeklyMeetings.state,
    ]

    @classmethod
    def is_convener_of(cls, meeting) -> bool:
        for convener in meeting.weekly_meeting_conveners:
            if current_user.cms_id == convener.cms_id:
                return True
        return False

    @classmethod
    def can_edit(cls, meeting) -> bool:
        if (current_user.is_admin() or current_user.is_secretariat_member()):
            return meeting.state != WeeklyMeetingState.CANCELLED
        if cls.is_convener_of(meeting):
            return meeting.state == WeeklyMeetingState.PROPOSED
        return False

    @classmethod
    def can_cancel(cls, meeting) -> bool:
        if (
            current_user.is_admin()
            or current_user.is_secretariat_member()
            or cls.is_convener_of(meeting)
        ):
            return meeting.state != WeeklyMeetingState.CANCELLED
        return False

    @classmethod
    def can_approve(cls, meeting) -> bool:
        if (current_user.is_admin() or current_user.is_secretariat_member()):
            return meeting.state != WeeklyMeetingState.APPROVED
        return False

    @classmethod
    def get_permissions(cls, meeting) -> list:
        item_permissions = []
        if cls.can_edit(meeting):
            item_permissions.append('edit')
        if cls.can_approve(meeting):
            item_permissions.append('approve')
        if cls.can_cancel(meeting):
            item_permissions.append('cancel')
        return item_permissions

    @classmethod
    def build_get_query(cls):
        return (
            WeeklyMeetings.session()
            .query(WeeklyMeetings, Person.first_name, Person.last_name)
            .join(
                WeeklyMeetingsConvener,
                WeeklyMeetingsConvener.weekly_meetings_id == WeeklyMeetings.id,
                isouter=True,  # also fetch meetings without conveners
            )
            .options(contains_eager(WeeklyMeetings.weekly_meeting_conveners))
            .join(
                Person,
                Person.cms_id == WeeklyMeetingsConvener.cms_id,
                isouter=True
            )
        )

    @classmethod
    def build_meeting_from_rows(cls, rows):
        weekly_meeting = rows[0][0]
        if not weekly_meeting.weekly_meeting_conveners:
            return weekly_meeting
        for i, row in enumerate(rows):
            _, first_name, last_name = row
            convener = weekly_meeting.weekly_meeting_conveners[i]
            convener.first_name = first_name
            convener.last_name = last_name
        return weekly_meeting

    @classmethod
    def dispatch_notification(cls, meeting, method):
        state = meeting.state
        title = meeting.title
        start_time = meeting.start_time
        end_time = meeting.end_time
        day_of_week = meeting.day_of_week
        odd_even_weekly = meeting.odd_even_weekly
        room = meeting.room
        primary_contact = meeting.primary_contact
        conveners = meeting.weekly_meeting_conveners
        convener_cms_ids = [convener.cms_id for convener in conveners]
        if current_user.cms_id not in convener_cms_ids:
            convener_cms_ids.append(current_user.cms_id)
        convener_users = User.session().query(User).filter(User.cmsId.in_(convener_cms_ids)).all()
        convener_emails = [user.get_email() for user in convener_users]
        support = 'icms-support@cern.ch'
        secr = 'Cms.Secretariat@cern.ch'
        args = {
            'meeting_title': title,
            'meeting_state': state,
            'meeting_odd_even_weekly': odd_even_weekly,
            'meeting_day_of_week': day_of_week,
            'meeting_start_time': start_time,
            'meeting_end_time': end_time,
            'meeting_room': room,
            'meeting_primary_contact': primary_contact,
            'conveners': [f'{convener.first_name} {convener.last_name}' for convener in conveners]
        }
        subject_message = ""
        if method == "post":
            subject_message = "New meeting proposed"
        elif method == "put":
            if state == "approved":
                subject_message = "Meeting approved"
            elif state == "proposed":
                subject_message = "Proposed meeting was updated"
            elif state == "cancelled":
                subject_message = "Meeting cancelled!"
        mail_body = flask.render_template('emails/weekly_meeting_request.txt', **args)
        EmailMessage.compose_message(
            sender=secr,
            bcc=f'{secr},{support}',
            reply_to=secr,
            subject=f'[CMS Weekly Meetings] {subject_message}',
            source_app='toolkit',
            to=','.join(convener_emails),
            body=mail_body,
            db_session=flask.current_app.db.session
        )

    @classmethod
    def get_weekly_meetings(cls):
        args = cls.parse_args()
        q = cls.build_get_query()
        q = cls.attach_query_filters(q, args, cls._args_mapping)
        res = q.all()

        weekly_meeting_dict = defaultdict(list)
        for _, row in enumerate(res):
            weekly_meeting, _, _ = row
            weekly_meeting_dict[weekly_meeting.id].append(row)

        meetings = []
        for _, meeting_rows in weekly_meeting_dict.items():
            meeting = cls.build_meeting_from_rows(meeting_rows)
            meeting._allowed_actions = cls.get_permissions(meeting)
            meeting.start_time = meeting.start_time.strftime('%H:%M')
            meeting.end_time = meeting.end_time.strftime('%H:%M')
            meetings.append(meeting)
        return meetings

    @classmethod
    def get_weekly_meeting(cls, meeting_id):
        q = (
            cls.build_get_query()
            .filter(WeeklyMeetings.id == meeting_id)
        )
        res = q.all()
        if not res:
            raise NotFoundException(f"No cms weekly meeting with id {meeting_id}")
        meeting = cls.build_meeting_from_rows(res)
        WeeklyMeetings.session().expunge_all()
        meeting._allowed_actions = cls.get_permissions(meeting)
        meeting.start_time = meeting.start_time.strftime('%H:%M')
        meeting.end_time = meeting.end_time.strftime('%H:%M')
        return meeting

    @classmethod
    def validate_time(cls, time_str: str) -> time:
        try:
            response = datetime.strptime(time_str, '%H:%M').time()
        except ValueError as e:
            raise BadRequestException(f'Invalid time: {time_str}') from e
        return response

    @classmethod
    def create_weekly_meeting(cls):
        payload = cls.get_payload()
        if payload.get('state') != WeeklyMeetingState.PROPOSED:
            raise BadRequestException('Cannot create meeting in a state other than proposed.')

        weekly_meeting_data = {
            k: v
            for k, v in payload.items()
            if k != 'weekly_meeting_conveners'
        }
        for attr in ['start_time', 'end_time']:
            weekly_meeting_data[attr] = cls.validate_time(weekly_meeting_data[attr])
        new_weekly_meeting: WeeklyMeetings = WeeklyMeetings(**weekly_meeting_data)
        ssn = WeeklyMeetings.session()
        weekly_meeting_conveners = payload.get('weekly_meeting_conveners', [])
        for convener_dict in weekly_meeting_conveners:
            cms_id = convener_dict['cms_id']
            if cms_id <= 0:
                raise BadRequestException('Invalid convener CMS ID.')
            new_weekly_meeting.weekly_meeting_conveners.append(WeeklyMeetingsConvener(cms_id=cms_id))
        ssn.add(new_weekly_meeting)
        ssn.commit()
        meeting = cls.get_weekly_meeting(new_weekly_meeting.id)
        cls.dispatch_notification(meeting, method='post')
        return meeting

    @classmethod
    def edit_weekly_meeting(cls, weekly_meeting_id):
        payload = cls.get_payload()
        for attr in ['start_time', 'end_time']:
            payload[attr] = cls.validate_time(payload[attr])
        session = WeeklyMeetings.session()
        weekly_meeting_to_update = (
            session
            .query(WeeklyMeetings)
            .join(
                WeeklyMeetingsConvener,
                WeeklyMeetingsConvener.weekly_meetings_id == WeeklyMeetings.id,
                isouter=True
            )
            .options(contains_eager(WeeklyMeetings.weekly_meeting_conveners))
            .filter(WeeklyMeetings.id == weekly_meeting_id)
            .one()
        )
        if not cls.can_edit(weekly_meeting_to_update):
            raise AccessViolationException('You are not allowed to edit this meeting.')
        if (
            payload.get('state') == WeeklyMeetingState.APPROVED
            and not cls.can_approve(weekly_meeting_to_update)
        ):
            raise AccessViolationException('You cannot approve this meeting.')
        if (
            payload.get('state') == WeeklyMeetingState.CANCELLED
            and not cls.can_cancel(weekly_meeting_to_update)
        ):
            raise AccessViolationException('You cannot cancel this meeting.')
        existing_convener_cms_ids = set(
            convener.cms_id
            for convener in weekly_meeting_to_update.weekly_meeting_conveners
        )
        updated_convener_cms_ids = set(
            convener_dict['cms_id']
            for convener_dict in payload.get('weekly_meeting_conveners', [])
        )
        if any(cms_id <= 0 for cms_id in updated_convener_cms_ids):
            raise BadRequestException('Invalid convener CMS ID.')
        conveners_to_remove = existing_convener_cms_ids - updated_convener_cms_ids
        for convener in weekly_meeting_to_update.weekly_meeting_conveners:
            if convener.cms_id in conveners_to_remove:
                session.delete(convener)
        for cms_id in updated_convener_cms_ids - existing_convener_cms_ids:
            weekly_meeting_to_update.weekly_meeting_conveners.append(WeeklyMeetingsConvener(cms_id=cms_id))
        cls.update_db_object(
            weekly_meeting_to_update,
            {**payload},
            cls._cols_in
        )
        session.commit()
        meeting = cls.get_weekly_meeting(weekly_meeting_to_update.id)
        cls.dispatch_notification(meeting, method="put")
        return meeting
