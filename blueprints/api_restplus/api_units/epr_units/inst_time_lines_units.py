from blueprints.api_restplus.api_units.basics import BaseApiUnit, ParserBuilder, ModelFactory
from icms_orm.epr import TimeLineInst as TLI


class EprInstTimeLinesCall(BaseApiUnit):
    _args = ParserBuilder().\
        add_argument(TLI.code.key, ParserBuilder.STRING, False).\
        add_argument(TLI.year.key, ParserBuilder.STRING, False).\
        parser

    _model = ModelFactory.make_model('EPR Inst TimeLine', {
        TLI.code: ModelFactory.String,
        TLI.year: ModelFactory.Integer,
        TLI.timestamp: ModelFactory.DateTime,
        'status': ModelFactory.String,
    })

    @classmethod
    def _get_query_with_cols(cls):
        args = cls.parse_args()
        _cols = [TLI.id, TLI.code, TLI.year, TLI.timestamp, TLI.cmsStatus.label('status')]
        q = TLI.session().query(*_cols)
        q = cls.attach_query_filters(q, args, [TLI.code, TLI.year]).order_by(TLI.year, TLI.timestamp)
        return q, _cols

    @classmethod
    def get_timeline(cls):
        _q, _cols = cls._get_query_with_cols()
        return cls.row_to_dict(_q.one(), _cols)

    @classmethod
    def get_timelines(cls):
        _q, _cols = cls._get_query_with_cols()
        return cls.rows_to_list_of_dicts(_q.all(), _cols)
