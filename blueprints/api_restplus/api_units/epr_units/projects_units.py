from blueprints.api_restplus.api_units.basics import BaseApiUnit, ParserBuilder, ModelFactory
from icms_orm.common import Project


class ProjectsCall(BaseApiUnit):
    _args = ParserBuilder().\
        add_appendable_argument(Project.code.key, ParserBuilder.STRING, False).\
        parser

    _model_cols_map = {
        Project.id: ModelFactory.String,
        Project.code: ModelFactory.String,
        Project.name: ModelFactory.String
    }

    _model = ModelFactory.make_model('ProjectModel', _model_cols_map)

    @classmethod
    def _get_query_with_cols(cls):
        args = cls.parse_args()
        _cols = list(cls._model_cols_map.keys())
        q = Project.session().query(*_cols)
        q = cls.attach_query_filters(
            q, args, [Project.code]).order_by(Project.code)
        return q, _cols
 
    @classmethod
    def get_one(cls):
        _q, _cols = cls._get_query_with_cols()
        return cls.row_to_dict(_q.one(), _cols)

    @classmethod
    def get_many(cls):
        _q, _cols = cls._get_query_with_cols()
        return cls.rows_to_list_of_dicts(_q.all(), _cols)
