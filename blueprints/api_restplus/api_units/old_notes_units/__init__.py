import re
from util import constants as const
from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ParserBuilder,
    ModelFactory,
)
from icms_orm.old_notes import (
    Note,
    WorkflowRecord as WfRecord,
    WorkflowData as WfData,
    WorkflowProcess as WfProcess,
)
import sqlalchemy as sa
import logging


class OldNoteProcessesCall(BaseApiUnit):
    _args = (
        ParserBuilder()
        .add_appendable_argument(
            WfProcess.status.key,
            ParserBuilder.STRING,
            False,
            choices=[
                "Sec_Completed",
                "Submitted",
                "SubEditor_Accepted",
                "Editor_Accepted",
                "SubEditor_Completed",
                "CoRefEditor_Accepted",
                "RefEditor_Accepted",
            ],
        )
        .add_appendable_argument(WfData.subproject.key, ParserBuilder.STRING, False)
        .add_appendable_argument(WfRecord.acquiredBy.key, ParserBuilder.INTEGER, False)
        .add_appendable_argument(WfRecord.process.key, ParserBuilder.INTEGER, False)
        .parser
    )
    _model_fields_map = {
        WfProcess.id: ModelFactory.Integer,
        WfProcess.status: ModelFactory.String,
        WfProcess.type: ModelFactory.String,
        WfProcess.submitter: ModelFactory.String,
        WfProcess.submitterName: ModelFactory.String,
        WfRecord.dateString: ModelFactory.String,
        WfRecord.status.label("recordStatus"): ModelFactory.String,
        WfRecord.acquiredBy: ModelFactory.String,
        WfRecord.operationReceived: ModelFactory.String,
        WfRecord.operationAwaited: ModelFactory.String,
        WfRecord.state.label("recordState"): ModelFactory.String,
        WfData.cmsNoteId: ModelFactory.String,
        WfData.title: ModelFactory.String,
        WfData.subeditor: ModelFactory.Integer,
        WfData.subeditorName: ModelFactory.String,
        WfData.editor: ModelFactory.Integer,
        WfData.editorName: ModelFactory.String,
        WfData.referee: ModelFactory.String,
        WfData.refCMSid: ModelFactory.Integer,
        WfData.subproject: ModelFactory.String,
        WfData.status.label("dataStatus"): ModelFactory.String,
        WfData.accepted: ModelFactory.String,
        WfData.acceptedBySubeditor: ModelFactory.String,
        WfData.acceptedByEditor: ModelFactory.String,
        WfData.acceptedBySecr: ModelFactory.String,
    }
    _model = ModelFactory.make_model("Note Workflow Info", _model_fields_map)

    @classmethod
    def get_latest(cls):
        ssn = WfProcess.session()
        _cols = list(cls._model_fields_map.keys())

        sq = (
            ssn.query(
                WfRecord.process.label("process_id"),
                sa.func.max(WfRecord.id).label("max_id"),
            )
            .group_by(WfRecord.process)
            .subquery()
        )

        q = (
            ssn.query(*_cols)
            .join(sq, sq.c.process_id == WfProcess.id)
            .join(WfRecord, sq.c.max_id == WfRecord.id)
            .join(WfData, sq.c.max_id == WfData.id)
        )
        q = cls.attach_query_filters(
            q,
            cls.parse_args(),
            [
                WfProcess.status,
                WfData.subproject,
                WfRecord.acquiredBy,
                WfRecord.process,
            ],
        )
        return cls.rows_to_list_of_dicts(q.all(), _cols)


class OldNotesStepsCall(BaseApiUnit):
    _args = (
        ParserBuilder()
        .add_argument(WfRecord.process.key, ParserBuilder.INTEGER, True)
        .parser
    )

    _model_fields_tuples = [
        (WfRecord.process, ModelFactory.Integer),
        (WfRecord.dateString, ModelFactory.String),
        (WfRecord.status.label("recordStatus"), ModelFactory.String),
        (WfRecord.acquiredBy, ModelFactory.String),
        (WfRecord.operationReceived, ModelFactory.String),
        (WfRecord.operationAwaited, ModelFactory.String),
        (WfRecord.state.label("recordState"), ModelFactory.String),
        (WfData.cmsNoteId, ModelFactory.String),
        (WfData.title, ModelFactory.String),
        (WfData.subeditor, ModelFactory.Integer),
        (WfData.subeditorName, ModelFactory.String),
        (WfData.editor, ModelFactory.Integer),
        (WfData.editorName, ModelFactory.String),
        (WfData.referee, ModelFactory.String),
        (WfData.refCMSid, ModelFactory.Integer),
        (WfData.subproject, ModelFactory.String),
        (WfData.status.label("dataStatus"), ModelFactory.String),
        (WfData.accepted, ModelFactory.String),
        (WfData.acceptedBySubeditor, ModelFactory.String),
        (WfData.acceptedByEditor, ModelFactory.String),
        (WfData.acceptedBySecr, ModelFactory.String),
    ]

    _model = ModelFactory.make_model(
        "Note Process Step", {k: v for k, v in _model_fields_tuples}
    )

    @classmethod
    def get(cls):
        _cols = [t[0] for t in cls._model_fields_tuples]
        q = WfData.session().query(*_cols).join(WfData, WfData.id == WfRecord.id)
        q = cls.attach_query_filters(q, cls.parse_args(), [WfRecord.process])
        return cls.rows_to_list_of_dicts(q.all(), _cols)

    @classmethod
    def undo_last_step(cls):
        """
        EXPERIMENTAL! For old notes DB, where the workflow is involved, removes last Data / Workflow entries and
        toggles the necessary statuses - involving some guesswork for the Process.status.
        """
        args = cls.parse_args()
        proc_id = args.get(WfRecord.process.key)
        ssn = WfProcess.session()
        # This part will require:
        # - Dropping the last data / wfrecord pair per process
        # - Marking the now-last data / wfrecord pair as the current one (rather than 'done')
        # - Setting the Process.status
        # The last step is a mess though - we need to find a matching process status for all the other parameters
        # describing the workflow's progress
        # A sample GuesSQL could look like:
        # select concat(state,'-',opReceived,'-', opWaitFor, '-', Process.type) as 'premise', Process.status as 'procStatus', count(*) as N from WorkflowRecord left join Process on (Process.id=process) where WorkflowRecord.status='current' group by premise, procStatus order by premise, N desc

        # query the most recent marked as done - crash if not found is intended and desired
        penultimate_record, penultimate_data, process = (
            ssn.query(WfRecord, WfData, WfProcess)
            .join(WfData, WfRecord.id == WfData.id)
            .join(WfProcess, WfRecord.process == WfProcess.id)
            .filter(sa.and_(WfRecord.process == proc_id, WfRecord.status.ilike("done")))
            .order_by(sa.desc(WfRecord.id))
            .limit(1)
            .one()
        )

        # delete the now-current step
        last_record, last_data = (
            ssn.query(WfRecord, WfData)
            .join(WfData, WfRecord.id == WfData.id)
            .filter(
                sa.and_(WfRecord.process == proc_id, WfRecord.status.ilike("current"))
            )
            .one()
        )
        last_record.delete()
        last_data.delete()
        # set prev current as current
        penultimate_record.set(WfRecord.status, "current")
        # guess the value of process.status
        process.set(
            WfProcess.status,
            cls._guess_matching_process_status(
                {
                    WfProcess.type: process.type,
                    WfRecord.operationAwaited: penultimate_record.operationAwaited,
                    WfRecord.operationReceived: penultimate_record.operationReceived,
                    WfRecord.state: penultimate_record.state,
                    WfData.acceptedBySubeditor: penultimate_data.acceptedBySubeditor,
                    WfData.acceptedByEditor: penultimate_data.acceptedByEditor,
                    WfData.acceptedBySecr: penultimate_data.acceptedBySecr,
                }
            ),
        )
        ssn.add(process)
        ssn.add(penultimate_record)
        ssn.commit()
        return {WfProcess.id.key: process.id, WfProcess.status.key: process.status}

    @classmethod
    def _guess_matching_process_status(cls, filters_dict):
        ssn = WfData.session()
        q = (
            ssn.query(WfProcess.status, sa.func.count(WfProcess.id))
            .join(WfRecord, WfRecord.process == WfProcess.id)
            .join(WfData, WfData.id == WfRecord.id)
            .filter(WfRecord.status.ilike("current"))
        )
        for _k, _v in filters_dict.items():
            q = q.filter(_k == _v)
        q = q.group_by(WfProcess.status)
        odds = {_r[0]: _r[1] for _r in q.all()}
        logging.debug(
            "Found the following potions for setting note's process status: {0}".format(
                odds
            )
        )
        highest_support = max(odds.values())
        for _k, _v in odds.items():
            if _v == highest_support:
                return _k
        raise Exception(
            "No suitable status has been found to assign to this process. Aborting!"
        )


class AutocompleteNoteIdsApiCall(BaseApiUnit):
    _model = ModelFactory.make_model(
        "Cms Note Ids",
        {
            Note.cmsNoteId.key: ModelFactory.String,
            Note.title.key: ModelFactory.String,
        },
    )
    _args = (
        ParserBuilder()
        .add_argument("search_term", type=ParserBuilder.STRING, required=True)
        .add_argument(
            "exclude_frozen_deleted",
            type=ParserBuilder.BOOLEAN,
            required=False,
            default=True,
        )
        .add_argument(Note.cmsNoteId.key, type=ParserBuilder.STRING, required=False)
        .add_argument(Note.title.key, type=ParserBuilder.STRING, required=False)
        .parser
    )
    _args_mapping = [
        Note.cmsNoteId,
        Note.title,
    ]

    @classmethod
    def get_cms_note_ids_suggestions(cls):
        args = cls.parse_args()
        search_term = args["search_term"]
        results = []
        output_cols = [Note.cmsNoteId, Note.title]
        if search_term:
            q = (
                Note.session()
                .query(*output_cols)
                .filter(Note.cmsNoteId.ilike(f"%{search_term}%"))
            )
            q = cls.attach_query_filters(q, args, cls._args_mapping)
            if args.get("exclude_frozen_deleted"):
                q = q.filter(Note.status.notin_(["FROZEN", "DELETED"]))
            results = q.limit(const.NUMBER_OF_SUGGESTED_RESULTS).all()
        return cls.rows_to_list_of_dicts(results, output_cols)
