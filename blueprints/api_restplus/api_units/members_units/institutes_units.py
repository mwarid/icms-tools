from typing import Any, List

import sqlalchemy as sa
from icms_orm.cmspeople.institutes import Institute as OldInstitute
from icms_orm.common import (
    Institute,
    InstituteLeader,
    InstituteStatus,
    OrgUnit,
    OrgUnitType,
    OrgUnitTypeName,
    Person,
    Position,
    PositionName,
    Tenure,
)
from icms_orm.common.common_schema_people_tables import Country
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.sql.expression import literal_column

from blueprints.api_restplus.api_units.basics import BaseApiUnit
from blueprints.api_restplus.api_units.basics import ModelFactory as MF
from blueprints.api_restplus.api_units.basics import ParserBuilder as PB


class InstitutesCall(BaseApiUnit):

    _model = MF.make_model(
        "Institute Info",
        {
            Institute.code.key: MF.field_proxy(MF.String, {MF.REQUIRED: True}),
            Institute.name.key: MF.field_proxy(MF.String, {MF.REQUIRED: True}),
            Institute.country_code.key: MF.field_proxy(MF.String, {MF.REQUIRED: True}),
            OldInstitute.address.key: MF.field_proxy(
                MF.String, {MF.REQUIRED: False, MF.READONLY: True}
            ),
            OldInstitute.town.key: MF.field_proxy(
                MF.String, {MF.REQUIRED: False, MF.READONLY: True}
            ),
            OldInstitute.usState.key: MF.field_proxy(
                MF.String, {MF.REQUIRED: False, MF.READONLY: True}
            ),
            "country_name": MF.field_proxy(
                MF.String, {MF.REQUIRED: False, MF.READONLY: True}
            ),
            InstituteStatus.status.key: MF.field_proxy(MF.String, {MF.REQUIRED: True}),
            "management": MF.field_proxy(MF.Raw, {MF.READONLY: True}),
        },
    )

    _args = (
        PB()
        .add_argument(Institute.code.key, PB.STRING, required=False, action="append")
        .add_argument(
            Institute.country_code.key, PB.STRING, required=False, action="append"
        )
        .add_argument(
            InstituteStatus.status.key, PB.STRING, required=False, action="append"
        )
        .parser
    )

    @classmethod
    def get(cls):
        return cls._do_get(cls.parse_args())

    @classmethod
    def get_one(cls):
        return cls._do_get(cls.parse_args(), one_row_only=True)

    @classmethod
    def _add_institute_address(cls, institute, institute_address_by_code):
        (
            institute[OldInstitute.address.key],
            institute[OldInstitute.town.key],
            institute[OldInstitute.usState.key],
        ) = institute_address_by_code[institute[Institute.code.key]]

    @classmethod
    def _do_get(cls, args, one_row_only=False):
        _grouping_cols = [
            Institute.code,
            Institute.name,
            Institute.country_code,
            InstituteStatus.status,
            Country.name.label("country_name"),
        ]

        q_tenures = (
            Tenure.session()
            .query(
                Tenure.cms_id.label(Tenure.cms_id.key),
                Person.first_name.label(Person.first_name.key),
                Person.last_name.label(Person.last_name.key),
                OrgUnit.domain.label(InstituteLeader.inst_code.key),
                Position.name.label("role"),
                Tenure.start_date.label(Tenure.start_date.key),
            )
            .join(Person, Tenure.cms_id == Person.cms_id)
            .join(OrgUnit, Tenure.unit_id == OrgUnit.id)
            .join(Position, Position.id == Tenure.position_id)
            .join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id)
            .filter(Tenure.end_date.is_(None))
            .filter(Position.name == PositionName.COMMUNICATIONS_PERSON)
            .filter(OrgUnitType.name == OrgUnitTypeName.INSTITUTE)
        )

        # maydo: use rank to detrmine for sure the latest status of each institute
        # (should not have 2 null end date)
        q_leaders = (
            InstituteLeader.session()
            .query(
                InstituteLeader.cms_id.label(Person.cms_id.key),
                Person.first_name.label(Person.first_name.key),
                Person.last_name.label(Person.last_name.key),
                InstituteLeader.inst_code.label(InstituteLeader.inst_code.key),
                sa.case(
                    [(InstituteLeader.is_primary.is_(True), "leader")], else_="deputy"
                ).label("role"),
                InstituteLeader.start_date.label(Tenure.start_date.key),
            )
            .join(Person, InstituteLeader.cms_id == Person.cms_id)
            .filter(InstituteLeader.end_date.is_(None))
        )

        sq = q_leaders.union(q_tenures).subquery().alias("mgmt_sq")

        _cols = _grouping_cols + [
            sa.func.json_agg(sa.func.row_to_json(literal_column("mgmt_sq"))).label(
                "management"
            )
        ]

        q = (
            Institute.session()
            .query(*_cols)
            .join(
                InstituteStatus,
                sa.and_(
                    InstituteStatus.code == Institute.code,
                    InstituteStatus.end_date.is_(None),
                ),
            )
            .join(sq, sa.and_(sa.and_(sq.c.inst_code == Institute.code)), isouter=True)
            .join(Country, Institute.country_code == Country.code, isouter=True)
            .group_by(*_grouping_cols)
        )

        q = cls.attach_query_filters(
            q,
            params_map=args,
            rules=[
                (Institute.code, None, None),
                (Institute.country_code, None, None),
                (InstituteStatus.status, None, None),
            ],
        )

        institute_addresses = (
            OldInstitute.session()
            .query(
                OldInstitute.code,
                OldInstitute.address,
                OldInstitute.town,
                OldInstitute.usState,
            )
            .all()
        )
        institute_address_by_code = {
            code: (address, town, state)
            for code, address, town, state in institute_addresses
        }
        if one_row_only:
            enriched_institute = cls.row_to_dict(q.one(), cols_list=_cols)
            cls._add_institute_address(enriched_institute, institute_address_by_code)
            return enriched_institute
        enriched_institutes = cls.rows_to_list_of_dicts(rows=q.all(), cols_list=_cols)
        for inst in enriched_institutes:
            cls._add_institute_address(inst, institute_address_by_code)
        return enriched_institutes

    @classmethod
    def row_to_dict(cls, row: List[Any], cols_list: List[InstrumentedAttribute]):
        """
        Ugly fix to get rid of [None] result for institutes without management
        """
        retval = super().row_to_dict(row, cols_list)
        if retval["management"] == [None]:
            retval["management"] = []
        return retval
