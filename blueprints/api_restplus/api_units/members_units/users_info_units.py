import logging
from datetime import date
import math
from typing import Dict, List, MutableSet

import sqlalchemy as sa
from sqlalchemy.orm import Session
from flask_login import current_user

from icms_orm.common import Affiliation, Assignment, Project
from icms_orm.common import PersonStatusValues as StatusValues
from icms_orm.common.common_schema_views import PersonView
from icms_orm.cmspeople import Person as OldPerson

from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit, ModelFactory, ParserBuilder
)
from blueprints.api_restplus.exceptions import (
    AccessViolationException, BadRequestException
)


class UsersInfoCall(BaseApiUnit):
    _args = ParserBuilder().\
        add_appendable_argument(PersonView.cms_id.key, ParserBuilder.INTEGER, required=False).\
        add_appendable_argument(Affiliation.inst_code.key, ParserBuilder.STRING, required=False).\
        add_appendable_argument(PersonView.status.key, ParserBuilder.STRING, required=False, default=[StatusValues.CMS], choices=StatusValues.values()).\
        parser

    _fields_map = {
        PersonView.cms_id: ModelFactory.field_proxy(ModelFactory.Integer, {ModelFactory.REQUIRED: True}),
        PersonView.hr_id: ModelFactory.field_proxy(ModelFactory.Integer, {ModelFactory.REQUIRED: False, ModelFactory.DEFAULT: None}),
        PersonView.first_name: ModelFactory.field_proxy(ModelFactory.String, {ModelFactory.READONLY: True}),
        PersonView.last_name: ModelFactory.field_proxy(ModelFactory.String, {ModelFactory.READONLY: True}),
        PersonView.status: ModelFactory.field_proxy(ModelFactory.String, {ModelFactory.READONLY: True}),
        PersonView.activity: ModelFactory.field_proxy(ModelFactory.String, {ModelFactory.READONLY: True}),
        PersonView.is_author: ModelFactory.field_proxy(ModelFactory.Boolean, {ModelFactory.READONLY: True}),
        PersonView.author_block: ModelFactory.field_proxy(ModelFactory.Boolean, {ModelFactory.READONLY: True}),
        PersonView.epr_suspension: ModelFactory.field_proxy(ModelFactory.Boolean, {ModelFactory.READONLY: True}),
        PersonView.author_block: ModelFactory.field_proxy(ModelFactory.Boolean, {ModelFactory.READONLY: True}),
        PersonView.affiliations: ModelFactory.field_proxy(ModelFactory.Raw, {ModelFactory.READONLY: True}),
        PersonView.projects: ModelFactory.field_proxy(ModelFactory.Raw, {ModelFactory.READONLY: False}),
        PersonView.flags: ModelFactory.field_proxy(ModelFactory.Raw, {ModelFactory.READONLY: True}),
        PersonView.team_duties: ModelFactory.field_proxy(ModelFactory.Raw, {ModelFactory.READONLY: True}),
        PersonView.managed_unit_ids: ModelFactory.field_proxy(
            ModelFactory.Raw, {ModelFactory.READONLY: True})
    }

    _model = ModelFactory.make_hypermedia_model('UserSnapshotInfo', fields_map=_fields_map)


    @classmethod
    def get_many(cls):
        ssn = PersonView.session()
        _today = date.today()
        _cols = list(UsersInfoCall._fields_map.keys())
        q = ssn.query(*_cols).join(Affiliation, sa.and_(
            PersonView.cms_id == Affiliation.cms_id,
            Affiliation.is_primary == True,
            sa.between(_today, Affiliation.start_date,
                       sa.func.coalesce(Affiliation.end_date, _today))
        ))
        q = cls.attach_query_filters(q, cls.parse_args(
        ), [PersonView.cms_id, Affiliation.inst_code, PersonView.status])
        return cls.rows_to_list_of_dicts(q.all(), cols_list=_cols)



class AssignmentsCalls(BaseApiUnit):
    _MAX_NUM_PROJECTS = 5

    _args = (
        ParserBuilder()
        .add_appendable_argument(
            Assignment.id.key, ParserBuilder.INTEGER, required=False
        )
        .add_appendable_argument(
            Assignment.cms_id.key, ParserBuilder.INTEGER, required=False
        )
        .add_appendable_argument(
            Assignment.project_code.key, ParserBuilder.STRING, required=False
        )
        .add_argument('active_only', ParserBuilder.BOOLEAN, required=False, default=True)
        .parser
    )

    _assignment_fields_map = {
        Assignment.id: ModelFactory.Integer,
        Assignment.cms_id: ModelFactory.Integer,
        Assignment.project_code: ModelFactory.String,
        Assignment.fraction: ModelFactory.Float,
        Assignment.start_date: ModelFactory.field_proxy(
            ModelFactory.Date, {ModelFactory.READONLY: True}
        ),
        Assignment.end_date: ModelFactory.field_proxy(
            ModelFactory.Date, {ModelFactory.READONLY: True}
        ),
    }

    _model = ModelFactory.make_hypermedia_model(
        'AssignmentInfo',
        _assignment_fields_map
    )

    new_assignment_model = ModelFactory.make_model(
        'NewAssignments', {
            Assignment.cms_id: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {ModelFactory.REQUIRED: True}
            ),
            'assignments': ModelFactory.field_proxy(
                ModelFactory.List, {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.Nested(
                        ModelFactory.make_model('ProjectFractions', {
                            Assignment.project_code: ModelFactory.field_proxy(
                                ModelFactory.String,
                                {ModelFactory.REQUIRED: True}
                            ),
                            Assignment.fraction: ModelFactory.field_proxy(
                                ModelFactory.Float,
                                {ModelFactory.REQUIRED: True}
                            )
                        })
                    ),
                    ModelFactory.REQUIRED: True
                }
            ),
        }
    )

    @classmethod
    def _q_cols(cls):
        return list(AssignmentsCalls._assignment_fields_map.keys())

    @classmethod
    def attach_query_filters(cls, query, params_map=None, rules=None):
        params_map = params_map or cls.parse_args()
        rules = rules or cls._q_cols()
        if params_map.get('active_only') is True:
            query = query.filter(
                sa.and_(
                    Assignment.start_date <= date.today(),
                    sa.or_(
                        Assignment.end_date.is_(None),
                        Assignment.end_date > date.today()
                    )
                )
            )
        return super().attach_query_filters(query, params_map, rules)

    @classmethod
    def get(cls):
        q = Assignment.session().query(*cls._q_cols())
        q = cls.attach_query_filters(q)
        return cls.rows_to_list_of_dicts(q.all(), cls._q_cols())

    @classmethod
    def validate_assignments(cls, payload: Dict):
        possible_project_codes = set(_[0] for _ in (
            Project.session()
            .query(Project.code)
            .all()
        ))
        project_codes = []
        fraction_sum = 0
        cms_id = payload[Assignment.cms_id.key]
        if cms_id < 0:
            raise BadRequestException('Invalid CMS ID')
        assignments = payload['assignments']
        if not assignments:
            # empty assignments: person is no longer working for any project
            return cms_id, []
        if len(assignments) > cls._MAX_NUM_PROJECTS:
            raise BadRequestException(
                f'Up to {cls._MAX_NUM_PROJECTS} concurrent projects are allowed'
            )
        for assignment in assignments:
            project_code = assignment[Assignment.project_code.key]
            if project_code not in possible_project_codes:
                raise BadRequestException(f'Invalid project code: {project_code}')
            project_codes.append(project_code)
            fraction = assignment[Assignment.fraction.key]
            if fraction <= 0 or fraction > 1:
                raise BadRequestException('The fraction should lie in (0, 1]')
            fraction_sum += fraction
        if len(set(project_codes)) != len(project_codes):
            raise BadRequestException('Duplicate project code detected')
        if not math.isclose(fraction_sum, 1):
            raise BadRequestException('The sum of all fractions must equal 1')
        return cms_id, assignments

    @classmethod
    def check_edit_permission_for(cls, member_cms_id: int):
        if current_user.is_admin() or current_user.is_leader_of_member(member_cms_id):
            return
        raise AccessViolationException(
            'Only available to the respective (deputy) team leaders!'
        )

    @classmethod
    def update_assignments(cls, cms_id: int, new_project_fractions: MutableSet):
        today = date.today()

        assignment_session: Session = Assignment.session()
        current_assignment_query = (
            assignment_session.query(Assignment)
            .filter(Assignment.cms_id == cms_id)
            .filter(Assignment.start_date <= today)
            .filter(sa.or_(
                Assignment.end_date.is_(None),
                Assignment.end_date > today
            ))
        )
        current_assignments = (
            current_assignment_query
            .populate_existing()
            .with_for_update()
            .all()
        )
        for cur_assignment in current_assignments:
            project_fraction = (cur_assignment.project_code, cur_assignment.fraction)
            if project_fraction in new_project_fractions:
                # pre-existing assignment; still valid
                new_project_fractions.remove(project_fraction)
            else:
                # obsolete current assignment; end it
                cur_assignment.set(Assignment.end_date, today)
                assignment_session.add(cur_assignment)
        for new_project_fraction in new_project_fractions:
            # new assignment; create it
            new_assignment = Assignment.from_ia_dict({
                Assignment.cms_id: cms_id,
                Assignment.project_code: new_project_fraction[0],
                Assignment.fraction: new_project_fraction[1],
                Assignment.start_date: today,
            })
            assignment_session.add(new_assignment)
        assignment_session.commit()

    @classmethod
    def update_main_project(cls, cms_id: int, new_assignments: List[Dict]):
        new_main_project = (
            max(new_assignments, key=lambda x: x[Assignment.fraction.key], default={})
            .get(Assignment.project_code.key, "")
        )
        old_person_session: Session = OldPerson.session()
        old_person = (
            old_person_session.query(OldPerson)
            .filter(OldPerson.cmsId == cms_id)
            .one()
        )
        if new_main_project != old_person.project:
            old_person.project = new_main_project
            old_person_session.add(old_person)
            old_person_session.commit()

    @classmethod
    def edit_or_create(cls) -> None:
        payload = cls.get_payload()
        cms_id, new_assignments = cls.validate_assignments(payload)
        cls.check_edit_permission_for(member_cms_id=cms_id)
        new_project_fractions = {
            (new_assignment['project_code'], new_assignment['fraction'])
            for new_assignment in new_assignments
        }
        cls.update_assignments(cms_id, new_project_fractions)
        cls.update_main_project(cms_id, new_assignments)
        return None
