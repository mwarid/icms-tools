from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)
from icms_orm.cmspeople import (
    Person,
    User,
    MoData,
    MemberActivity,
    Institute,
    PersonCernData,
)
from blueprints.api_restplus.exceptions import AccessViolationException
from datetime import date
import sqlalchemy as sa
from flask_login import current_user
from sqlalchemy.sql.expression import case


class OldDbPersonalInfoBase(BaseApiUnit):
    _args = (
        ParserBuilder()
        .add_argument("cms_id", ParserBuilder.INTEGER, required=True)
        .parser
    )


class OldDbMoCall(OldDbPersonalInfoBase):
    _model = ModelFactory.make_model(
        "Person MO Info",
        {
            "year": ModelFactory.Integer,
            "cms_id": ModelFactory.Integer,
            "mo": ModelFactory.Boolean,
            "mo_type": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.ENUM: ["free", "phd", "requested"],
                    ModelFactory.REQUIRED: False,
                },
            ),
            "mo_fa": ModelFactory.field_proxy(
                ModelFactory.String, {ModelFactory.REQUIRED: False}
            ),
            "mo_ic": ModelFactory.field_proxy(
                ModelFactory.String, {ModelFactory.REQUIRED: False}
            ),
        },
    )

    @classmethod
    def get(cls):
        data = (
            MoData.session()
            .query(MoData)
            .filter(MoData.cmsId == cls.parse_args().get("cms_id"))
            .one()
        )
        assert isinstance(data, MoData)
        results = []
        from_year = (
            date.today().year + 1 if date.today().month >= 10 else date.today().year
        )
        for _y in range(from_year, from_year - 7, -1):
            (
                mo_col,
                phd_mo_col,
                free_mo_col,
                phd_ic_col,
                phd_fa_col,
            ) = MoData.get_columns_for_year(_y)
            results.append(
                {
                    "cms_id": data.cmsId,
                    "year": _y,
                    "mo": data.get(phd_mo_col) == "YES"
                    or data.get(free_mo_col) == "YES",
                    "mo_type": data.get(phd_mo_col) == "YES"
                    and "phd"
                    or data.get(free_mo_col) == "YES"
                    and "free"
                    or data.get(mo_col) == "YES"
                    and "requested"
                    or None,
                    "mo_fa": data.get(phd_fa_col) or None,
                    "mo_ic": data.get(phd_ic_col) or None,
                }
            )
        return results


class OldDbPersonInfoCall(OldDbPersonalInfoBase):
    _args = (
        ParserBuilder()
        .add_appendable_argument("cms_id", ParserBuilder.INTEGER, required=False)
        .add_appendable_argument("inst_code", ParserBuilder.STRING, required=False)
        .add_appendable_argument("project", ParserBuilder.STRING, required=False)
        .add_appendable_argument("inst_status", ParserBuilder.STRING, required=False)
        .add_appendable_argument("ind_status", ParserBuilder.STRING, required=False)
        .add_appendable_argument("inst_country", ParserBuilder.STRING, required=False)
        .add_appendable_argument("is_author", ParserBuilder.BOOLEAN, required=False)
        .parser
    )

    _model = ModelFactory.make_model(
        "Person information",
        {
            Person.cmsId: ModelFactory.field_proxy(
                ModelFactory.Integer, {ModelFactory.REQUIRED: True}
            ),
            Person.hrId: ModelFactory.field_proxy(
                ModelFactory.Integer, {ModelFactory.REQUIRED: True}
            ),
            Person.lastName: ModelFactory.String,
            Person.firstName: ModelFactory.String,
            User.title: ModelFactory.String,
            Person.loginId: ModelFactory.String,
            Person.niceLogin: ModelFactory.String,
            "email": ModelFactory.String,
            Person.instCode: ModelFactory.String,
            Institute.country: ModelFactory.String,
            Person.instCodeOther: ModelFactory.String,
            Person.isAuthor: ModelFactory.Boolean,
            Person.isAuthorSuspended: ModelFactory.Boolean,
            Person.isAuthorBlock: ModelFactory.Boolean(),
            Person.status: ModelFactory.String,
            "activity": ModelFactory.field_proxy(
                ModelFactory.String, {ModelFactory.ATTRIBUTE: MemberActivity.name.key}
            ),
            Person.project: ModelFactory.String,
            User.localPercent: ModelFactory.Integer,
            Person.dateCreation: ModelFactory.Date,
            Person.dateRegistration: ModelFactory.Date,
            Person.zhFlag: ModelFactory.Boolean,
            Person.physicsAccess: ModelFactory.Boolean,
            "mo%d" % (date.today().year - 0): ModelFactory.String,
            "mo%d" % (date.today().year - 1): ModelFactory.String,
            "mo%d" % (date.today().year - 2): ModelFactory.String,
        },
    )

    @classmethod
    def _get_query_and_cols(cls, q_cols=None):
        q_cols = q_cols or [
            Person.cmsId,
            Person.firstName,
            Person.hrId,
            Person.lastName,
            Person.isAuthor,
            Person.isAuthorBlock,
            Person.niceLogin,
            Person.loginId,
            Person.isAuthorSuspended,
            Person.instCode,
            Institute.country,
            Person.status,
            MemberActivity.name,
            Person.project,
            Person.dateRegistration,
            Person.dateCreation,
            Person.instCodeOther,
            Person.zhFlag,
            Person.physicsAccess,
            User.title,
            User.localPercent,
            User.sex.label("gender"),
            case(
                value=User.mailWhere,
                whens={"institute": sa.func.coalesce(User.email1, User.email2)},
                else_=sa.func.coalesce(User.emailCern, User.email1, User.email2),
            ).label("email"),
        ]

        for _year in range(date.today().year, date.today().year - 4, -1):
            _free_col = MoData.get_free_mo_column_for_year(_year)
            _phd_col = MoData.get_phd_mo_column_for_year(_year)
            q_cols.append(
                sa.case(
                    [(_phd_col.like("YES"), "PhD"), (_free_col.like("YES"), "free")],
                    else_="no",
                ).label("mo%d" % _year)
            )

        q = (
            Person.session()
            .query(*q_cols)
            .join(MemberActivity, Person.activityId == MemberActivity.id, isouter=True)
            .join(Institute, Institute.code == Person.instCode)
            .join(MoData, MoData.cmsId == Person.cmsId)
            .join(User, User.cmsId == Person.cmsId)
            .join(PersonCernData, PersonCernData.cmsId == Person.cmsId)
        )

        arg_params = cls.parse_args()

        q = cls.attach_query_filters(
            q,
            arg_params,
            rules=[
                (Person.cmsId, "cms_id", None),
                (Person.instCode, "inst_code", None),
                (Person.project, "project", None),
                (Institute.cmsStatus, "inst_status", None),
                (Person.status, "ind_status", None),
                (Institute.country, "inst_country", None),
                (Person.isAuthor, "is_author", None),
            ],
        )
        return q, q_cols

    @classmethod
    def get(cls):
        q, q_cols = cls._get_query_and_cols()
        return cls.row_to_dict(q.one(), q_cols)


class OldDbPeopleInfoCall(OldDbPersonInfoCall):
    @classmethod
    def get(cls):
        q, q_cols = cls._get_query_and_cols()
        return cls.rows_to_list_of_dicts(q.all(), q_cols)


class OldDbPersonRestrictedInfoCall(OldDbPersonInfoCall):
    # paste makes waste so we define the dictionary once and use it to initialize 2 different variables
    _model_members, _model = (
        lambda x: (x, ModelFactory.make_model("Person Restricted Info", x))
    )(
        {
            Person.cmsId: ModelFactory.Integer,
            PersonCernData.cernId: ModelFactory.String,
            Person.cmsTime: ModelFactory.String,
            Person.datetimeModified: ModelFactory.DateTime,
            Person.dateEndSign: ModelFactory.Date,
            Person.dateAuthorUnblock: ModelFactory.Date,
            Person.dateAuthorBlock: ModelFactory.Date,
            Person.dateAuthorSuspension: ModelFactory.Date,
            Person.dateAuthorUnsuspension: ModelFactory.Date,
            Person.nationality: ModelFactory.String,
            Person.nationality2: ModelFactory.String,
            Person.birthDate: ModelFactory.Date,
            User.sex: ModelFactory.String,
            Person.remarks: ModelFactory.String,
            Person.remarksSecr: ModelFactory.String,
            Person.photoPublic: ModelFactory.String,
            PersonCernData.status: ModelFactory.String,
            Person.exDate: ModelFactory.String,
            Person.priEnabled: ModelFactory.Boolean,
        }
    )

    @classmethod
    def get(cls):
        if (
            current_user.is_admin()
            or current_user.is_diversity_office_chair
            or current_user.cms_id == cls.parse_args().get("cms_id", None)
        ):
            q, q_cols = cls._get_query_and_cols(list(cls._model_members.keys()))
            return cls.row_to_dict(q.one(), q_cols)
        else:
            raise AccessViolationException(
                "Restricted information can only be viewed by its owner and by the admins."
            )
