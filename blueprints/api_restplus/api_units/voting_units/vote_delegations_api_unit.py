from blueprints.voting.voting_lists.persisted_voting_list_model import PersistedVotingListModel
from blueprints.voting.voting_lists.abstract_voting_list_model import AbstractVotingListModel
from blueprints.voting.voting_lists import VotingListModel
import flask
from icms_orm.common.common_schema_misc_tables import EmailMessage
import flask_login
from flask_login import current_user
from blueprints.api_restplus.exceptions import AccessViolationException, ArbitraryErrorCodeException
from sqlalchemy.orm.query import Query
from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from datetime import datetime
import logging
from typing import Any, Dict, List, Optional, Type

from blueprints.api_restplus.api_units.basics.model_field_types import Boolean, DateTime, Integer, String
from blueprints.api_restplus.api_units.basics.builders import ModelFieldDefinition
from blueprints.api_restplus.api_units.basics.crud import AbstractCRUDUnit

from icms_orm.toolkit import Status, VoteDelegation, Person, Voting
import sqlalchemy as sa
from blueprints.api_restplus.exceptions import IcmsException

Delegate = sa.orm.aliased(Person)

log: logging.Logger = logging.getLogger(__name__)


class VoteDelegationsApiUnit(AbstractCRUDUnit):
    FIELD_ID = ModelFieldDefinition.builder(Integer).column(
        VoteDelegation.id).id().readonly().name('id').build()
    FIELD_VOTING_CODE = ModelFieldDefinition.builder(String).column(
        VoteDelegation.voting_code).name('voting_code').build()
    FIELD_PRINCIPAL_CMS_ID = ModelFieldDefinition.builder(Integer).column(
        VoteDelegation.cms_id_from).name('principal_cms_id').required().build()
    FIELD_DELEGATE_CMS_ID = ModelFieldDefinition.builder(Integer).column(
        VoteDelegation.cms_id_to).name('delegate_cms_id').required().build()
    FIELD_IS_PERENNIAL = ModelFieldDefinition.builder(Boolean).column(
        VoteDelegation.is_long_term).name('is_perennial').default(False).build()
    FIELD_INST_CODE = ModelFieldDefinition.builder(String).column(
        VoteDelegation.specific_inst_code).name('inst_code').required().build()
    FIELD_STATUS = ModelFieldDefinition.builder(String).column(VoteDelegation.status).name(
        'status').enum([Status.ACTIVE, Status.CANCELLED]).default(Status.ACTIVE).build()
    # The following fields will be read-only:
    FIELD_CREATION = ModelFieldDefinition.builder(DateTime).column(
        VoteDelegation.time_created).name('creation').readonly().build()
    FIELD_LAST_UPDATE = ModelFieldDefinition.builder(DateTime).column(
        VoteDelegation.time_updated).name('last_update').readonly().build()
    FIELD_PRINCIPAL_FORENAME = ModelFieldDefinition.builder(String).column(
        Person.first_name).name('principal_forename').readonly().build()
    FIELD_PRINCIPAL_SURNAME = ModelFieldDefinition.builder(String).column(
        Person.last_name).name('principal_surname').readonly().build()
    FIELD_PRINCIPAL_EMAIL = ModelFieldDefinition.builder(String).column(
        Person.email).name('principal_email').readonly().build()
    FIELD_DELEGATE_FORENAME = ModelFieldDefinition.builder(String).column(
        Delegate.first_name).name('delegate_forename').readonly().build()
    FIELD_DELEGATE_SURNAME = ModelFieldDefinition.builder(String).column(
        Delegate.last_name).name('delegate_surname').readonly().build()
    FIELD_DELEGATE_EMAIL = ModelFieldDefinition.builder(String).column(
        Delegate.email).name('delegate_email').readonly().build()

    @classmethod
    def get_query(cls: Type['VoteDelegationsApiUnit'], resource_filter: Optional[ResourceFilter]) -> Query:
        query: Query = super().get_query(resource_filter)
        query = query.join(Person, VoteDelegation.cms_id_from == Person.cms_id).join(
            Delegate, VoteDelegation.cms_id_to == Delegate.cms_id)
        return query

    @classmethod
    def is_model_field_eligible_for_parser(cls: Type['VoteDelegationsApiUnit'], field: ModelFieldDefinition) -> bool:
        return field in [cls.FIELD_VOTING_CODE, cls.FIELD_PRINCIPAL_CMS_ID, cls.FIELD_DELEGATE_CMS_ID, cls.FIELD_IS_PERENNIAL]

    @classmethod
    def get_model_fields_list(cls: Type['VoteDelegationsApiUnit']) -> List[ModelFieldDefinition]:
        return [cls.FIELD_ID, cls.FIELD_PRINCIPAL_CMS_ID, cls.FIELD_DELEGATE_CMS_ID, cls.FIELD_VOTING_CODE,
                cls.FIELD_IS_PERENNIAL, cls.FIELD_INST_CODE, cls.FIELD_STATUS, cls.FIELD_LAST_UPDATE,
                cls.FIELD_CREATION, cls.FIELD_DELEGATE_FORENAME, cls.FIELD_DELEGATE_SURNAME,
                cls.FIELD_DELEGATE_EMAIL, cls.FIELD_PRINCIPAL_FORENAME, cls.FIELD_PRINCIPAL_SURNAME,
                cls.FIELD_PRINCIPAL_EMAIL]

    @classmethod
    def get_model_name(cls: Type['VoteDelegationsApiUnit']) -> str:
        return 'Vote Delegation'

    @classmethod
    def customize_mapper_before_persisting(cls: Type['VoteDelegationsApiUnit'], target: VoteDelegation):
        utc_now: datetime = datetime.utcnow()
        user_id = flask_login.current_user.cms_id
        if target.id is None:
            target.time_created = utc_now
            target.cms_id_creator = user_id
        target.time_updated = utc_now
        target.cms_id_updater = user_id

    @classmethod
    def validate_mapper_before_persisting(cls: Type['VoteDelegationsApiUnit'], mapper: VoteDelegation):
        '''
        Permissions setup can allow editing the resource depending on submitted parameters 
        (i.e. inst_code being within the set of codes of insts managed by the current user).
        However, the code originally stored in the DB could have been different and that's what we need to rule out here.
        '''
        payload = cls.get_payload()
        if mapper.specific_inst_code != payload.get(cls.FIELD_INST_CODE.name):
            raise AccessViolationException(
                f'Illegal attempt to change institute from {mapper.specific_inst_code} to {payload.get(cls.FIELD_INST_CODE.name)}.')
        if mapper.is_long_term is not True and mapper.voting_code is None:
            raise ArbitraryErrorCodeException(
                400, 'The delegation is neither event-specific (null voting code) not perennial.')
        if mapper.status == Status.ACTIVE and mapper.is_long_term is False:
            log.debug('Now checking against the relevant voting model')
            model: AbstractVotingListModel = VotingListModel.get_instance(
                voting_code=mapper.voting_code)
            if isinstance(model, PersistedVotingListModel):
                raise IcmsException(
                    f'Voting list for {mapper.voting_code} has already been saved and cannot be modified.')
            if mapper.cms_id_from not in model.get_voting_cms_ids():
                raise IcmsException(
                    f'Specified principal does not have a vote to delegate for {mapper.voting_code}')
            if mapper.cms_id_to in model.get_voting_cms_ids():
                raise IcmsException(
                    f'Specified delegate is already voting at {mapper.voting_code}')
        return super().validate_mapper_before_persisting(mapper)

    @classmethod
    def db_object_to_model_compliant_dict(cls: Type['VoteDelegationsApiUnit'], db_object: VoteDelegation) -> Dict[str, Any]:
        '''At the expense of one more DB query we get all the joined-in fields populated'''
        return cls.get_by_identity(db_object.id)

    @classmethod
    def post(cls: Type['VoteDelegationsApiUnit']) -> Dict[str, object]:
        retval: Dict[str, Any] = super().post()
        cls._dispatch_notification_email(retval)
        return retval

    @classmethod
    def put_by_identity(cls: Type['VoteDelegationsApiUnit'], *params) -> Dict[str, Any]:
        retval: Dict[str, Any] = super().put_by_identity(*params)
        cls._dispatch_notification_email(retval)
        return retval

    @classmethod
    def _dispatch_notification_email(cls: Type['VoteDelegationsApiUnit'], data: Dict[str, Any]):

        template: str = 'emails/{event_type}_proxy_{action}.txt'.format(
            event_type=data[cls.FIELD_IS_PERENNIAL.name] is True and 'lt' or 'voting',
            action=data[cls.FIELD_STATUS.name] == 'active' and 'set' or 'cancelled'
        )

        subject: str = '{code} vote delegation {action}'.format(
            code=data[cls.FIELD_VOTING_CODE.name] or 'Perennial',
            action=data[cls.FIELD_STATUS.name] == 'active' and 'established' or 'cancelled'
        )

        log.debug(f'Need to fetch the details of voting {data[cls.FIELD_VOTING_CODE.name]}')
        voting: Optional[Voting] = None if data[cls.FIELD_IS_PERENNIAL.name] else Voting.session(
        ).query(Voting).filter(Voting.code == data[cls.FIELD_VOTING_CODE.name]).one()

        mail_message = flask.render_template(
            template,
            title=current_user.person.user.title,
            recipient=f'{data[cls.FIELD_PRINCIPAL_FORENAME.name]} {data[cls.FIELD_PRINCIPAL_SURNAME.name]}',
            delegate=f'{data[cls.FIELD_DELEGATE_FORENAME.name]} {data[cls.FIELD_DELEGATE_SURNAME.name]}',
            delegate_inst=data[cls.FIELD_INST_CODE.name],
            voting_code=data[cls.FIELD_VOTING_CODE.name],
            voting_title=voting and voting.title or None,
            voting_date=voting and voting.start_time or None
        )

        EmailMessage.compose_message(
            sender=flask.current_app.config['EMAIL_FROM'],
            to=data[cls.FIELD_PRINCIPAL_EMAIL.name],
            cc=data[cls.FIELD_DELEGATE_EMAIL.name],
            bcc=flask.current_app.config['EMAIL_BCC'],
            subject=subject,
            body=mail_message,
            reply_to=flask.current_app.config['EMAIL_REPLY_TO'],
            db_session=EmailMessage.session()
        )
