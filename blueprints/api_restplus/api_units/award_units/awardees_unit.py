"""Awardees API unit"""

from datetime import date
from typing import Dict

from flask_login import current_user
from icms_orm.common import Person
from icms_orm.toolkit import Award, Awardee, AwardType
from sqlalchemy import func

from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)
from blueprints.api_restplus.exceptions import (
    AccessViolationException,
    BadRequestException,
)

from .project_unit import AwardProjectsApiCall


class AwardeesApiCall(BaseApiUnit):
    """Awardees API unit"""

    _model = ModelFactory.make_model(
        "Awardee",
        {
            Awardee.id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            Awardee.award_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            # Extra info for award
            "award_title": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "award_year": ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            Awardee.cms_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            Awardee.first_name: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            Awardee.last_name: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            Awardee.activity.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            Awardee.project.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            Awardee.institute.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            Awardee.citation.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            Awardee.remarks.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            Awardee.is_active.key: ModelFactory.field_proxy(
                ModelFactory.Boolean,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "is_public": ModelFactory.field_proxy(
                ModelFactory.Boolean,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            Awardee.last_updated_at.key: ModelFactory.field_proxy(
                ModelFactory.DateTime,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            Awardee.last_updated_by.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "_permissions": ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.String,
                    ModelFactory.EXAMPLE: ["select"],
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
        },
    )
    _args = (
        ParserBuilder()
        .add_argument(Awardee.award_id.key, type=ParserBuilder.INTEGER, required=False)
        .add_argument(Awardee.cms_id.key, type=ParserBuilder.INTEGER, required=False)
        .add_argument("include_deleted", type=ParserBuilder.BOOLEAN, required=False)
        .parser
    )
    _args_mapping = [Awardee.award_id, Awardee.cms_id]
    _cols_out = [
        Awardee.id,
        Awardee.award_id,
        AwardType.type.label("award_type"),
        Award.year.label("award_year"),
        Award.awardee_publication_date.label("awardee_publication_date"),
        Award.nominations_deadline.label("nominations_deadline"),
        Awardee.cms_id,
        Awardee.first_name,
        Awardee.last_name,
        Awardee.activity,
        Awardee.project,
        Awardee.institute,
        Awardee.citation,
        Awardee.remarks,
        Awardee.is_active,
        Awardee.last_updated_at,
        Awardee.last_updated_by,
    ]
    _cols_in = [
        Awardee.award_id,
        Awardee.cms_id,
        Awardee.first_name,
        Awardee.last_name,
        Awardee.activity,
        Awardee.project,
        Awardee.institute,
        Awardee.citation,
        Awardee.remarks,
        Awardee.is_active,
        Awardee.last_updated_by,
    ]
    _project_choices = AwardProjectsApiCall.get_projects()

    @staticmethod
    def _get_award_title(award_dict):
        return f"{award_dict['award_type']} {award_dict['award_year']}"

    @classmethod
    def _base_get_query(cls):
        query = (
            Awardee.session()
            .query(*cls._cols_out)
            .join(Award, Awardee.award_id == Award.id)
            .join(AwardType, AwardType.id == Award.award_type_id)
        )
        if not current_user.is_award_admin:
            query = query.filter(Award.awardee_publication_date.isnot(None)).filter(
                Award.awardee_publication_date <= func.current_date()
            )
        args = cls.parse_args()
        query = cls.attach_query_filters(query, args, cls._args_mapping)
        if not (args.get("include_deleted") is True and current_user.is_award_admin):
            query = query.filter(Awardee.is_active.is_(True))
        return query

    @staticmethod
    def _can_update(awardee: Dict):
        if not current_user.is_award_admin:
            return False
        nominations_deadline = awardee["nominations_deadline"]
        awardee_publication_date = awardee["awardee_publication_date"]
        return (
            nominations_deadline
            and nominations_deadline < date.today()
            and (
                not awardee_publication_date or date.today() < awardee_publication_date
            )
        )

    @classmethod
    def _enrich_awardee(cls, awardee: Dict):
        awardee["award_title"] = cls._get_award_title(awardee)
        awardee_publication_date = awardee["awardee_publication_date"]
        awardee["is_public"] = (
            awardee_publication_date and awardee_publication_date <= date.today()
        )
        awardee["_permissions"] = ["edit", "delete"] if cls._can_update(awardee) else []

    @classmethod
    def list_awardees(cls):
        query = cls._base_get_query()
        awardees = cls.rows_to_list_of_dicts(query.all(), cls._cols_out)
        for awardee in awardees:
            cls._enrich_awardee(awardee)
        return awardees

    @classmethod
    def get_awardee(cls, awardee_id):
        query = cls._base_get_query().filter(Awardee.id == awardee_id)
        awardee = cls.row_to_dict(query.one(), cls._cols_out)
        cls._enrich_awardee(awardee)
        return awardee

    @classmethod
    def _validate_payload(cls, payload):
        for project in payload["project"].split(";"):
            if project not in cls._project_choices:
                raise BadRequestException(f"Invalid CMS project choice: {project}!")

    @staticmethod
    def _awardee_update_helper(awardee_id: int):
        if not current_user.is_award_admin:
            raise AccessViolationException("Only CMS Award admins can manage awardees!")
        awardee_to_update = Awardee.session().query(Awardee).get(awardee_id)
        if awardee_to_update is None or not awardee_to_update.is_active:
            raise BadRequestException(
                "Specified awardee not found (could have been deleted)!"
            )
        return awardee_to_update

    @classmethod
    def create_awardee(cls):
        if not current_user.is_award_admin:
            raise AccessViolationException("Only CMS Award admins can select awardees!")
        payload = cls.get_payload()
        cls._validate_payload(payload)
        award_id = payload["award_id"]
        cms_id = payload.get("cms_id")
        if cms_id is not None:
            existing_awardee = (
                Awardee.session()
                .query(Awardee)
                .filter(Awardee.cms_id == cms_id)
                .filter(Awardee.award_id == award_id)
                .filter(Awardee.is_active.is_(True))
                .scalar()
            )
            if existing_awardee is not None:
                raise BadRequestException(
                    f"Member {cms_id} has already been selected for award {award_id}!"
                )
        new_awardee = Awardee.from_ia_dict({})
        cls.update_db_object(
            new_awardee,
            {**payload, "last_updated_by": current_user.cms_id},
            cls._cols_in,
        )
        return cls.get_awardee(new_awardee.id)

    @classmethod
    def edit_awardee(cls, awardee_id):
        awardee_to_update = cls._awardee_update_helper(awardee_id)
        payload = cls.get_payload()
        cls._validate_payload(payload)
        cls.update_db_object(
            awardee_to_update,
            {**payload, "last_updated_by": current_user.cms_id},
            cls._cols_in,
        )
        return cls.get_awardee(awardee_to_update.id)

    @classmethod
    def delete_awardee(cls, awardee_id):
        awardee_to_delete = cls._awardee_update_helper(awardee_id)
        cls.update_db_object(
            awardee_to_delete, {"is_active": False}, [Awardee.is_active]
        )
        return {}
