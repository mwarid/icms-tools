from icms_orm.toolkit import AwardType

from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)


class AwardTypesApiCall(BaseApiUnit):
    _model_cols_dict = {
        AwardType.id.key: ModelFactory.field_proxy(
            ModelFactory.Integer,
            {
                ModelFactory.REQUIRED: False,
                ModelFactory.READONLY: True,
            },
        ),
        AwardType.type.key: ModelFactory.field_proxy(
            ModelFactory.String,
            {
                ModelFactory.REQUIRED: True,
            },
        ),
    }
    _model = ModelFactory.make_model("Award Type", _model_cols_dict)
    _args = (
        ParserBuilder()
        .add_argument(AwardType.type.key, type=ParserBuilder.STRING, required=False)
        .parser
    )
    _args_mapping = [AwardType.id, AwardType.type]
    _cols_out = [AwardType.id, AwardType.type]
    _cols_in = [AwardType.type]

    @classmethod
    def list_award_types(cls):
        filter_args = cls.parse_args()
        ssn = AwardType.session()
        q = ssn.query(*cls._cols_out)
        q = cls.attach_query_filters(q, filter_args, cls._args_mapping)
        res = q.all()
        return cls.rows_to_list_of_dicts(res, cls._cols_out)

    @classmethod
    def get_award_type(cls, award_type_id):
        ssn = AwardType.session()
        q = ssn.query(*cls._cols_out).filter(AwardType.id == award_type_id)
        res = q.one()
        return cls.row_to_dict(res, cls._cols_out)

    @classmethod
    def create_award_type(cls):
        payload = cls.get_payload()
        new_award_type = AwardType.from_ia_dict({})
        cls.update_db_object(new_award_type, payload, cls._cols_in)
        return new_award_type

    @classmethod
    def update_award_type(cls, award_type_id):
        payload = cls.get_payload()
        award_type_to_update = AwardType.session().query(AwardType).get(award_type_id)
        cls.update_db_object(award_type_to_update, payload, cls._cols_in)
        return award_type_to_update
