from datetime import date

from flask_login import current_user
from icms_orm.toolkit import Award, AwardType

from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)
from blueprints.api_restplus.exceptions import (
    AccessViolationException,
    BadRequestException,
)


class AwardsApiCall(BaseApiUnit):

    _model = ModelFactory.make_model(
        "Award",
        {
            Award.id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            Award.award_type_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            AwardType.type.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "title": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            Award.year.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            Award.nominations_open_date.key: ModelFactory.field_proxy(
                ModelFactory.Date,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            Award.nominations_deadline.key: ModelFactory.field_proxy(
                ModelFactory.Date,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            Award.awardee_publication_date.key: ModelFactory.field_proxy(
                ModelFactory.NullableDate,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            Award.remarks.key: ModelFactory.field_proxy(
                ModelFactory.NullableString,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            Award.is_active.key: ModelFactory.field_proxy(
                ModelFactory.Boolean,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            Award.last_updated_at.key: ModelFactory.field_proxy(
                ModelFactory.DateTime,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            Award.last_updated_by.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "_permissions": ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.String,
                    ModelFactory.EXAMPLE: ["edit-award"],
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
        },
    )

    award_list_model = ModelFactory.make_model(
        "AwardList",
        {
            "awards": ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.Nested(_model),
                    ModelFactory.READONLY: True,
                },
            ),
            "_collection_permissions": ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.String,
                    ModelFactory.READONLY: True,
                    ModelFactory.EXAMPLE: ["create-award"],
                },
            ),
        },
    )

    _args = (
        ParserBuilder()
        .add_argument("include_deleted", type=ParserBuilder.BOOLEAN, required=False)
        .add_argument(
            "only_accepting_nominations", type=ParserBuilder.BOOLEAN, required=False
        )
        .add_argument("award_type", type=ParserBuilder.STRING, required=False)
        .add_argument(Award.year.key, type=ParserBuilder.INTEGER, required=False)
        .parser
    )
    _args_mapping = [
        (Award.year, None, None),
        (AwardType.type, "award_type", None),
        (Award.is_active, "include_deleted", lambda x: [True, False] if x else True),
    ]
    _cols_out = [
        Award.id,
        AwardType.type,
        Award.year,
        Award.nominations_open_date,
        Award.nominations_deadline,
        Award.awardee_publication_date,
        Award.remarks,
        Award.is_active,
        Award.last_updated_at,
        Award.last_updated_by,
        Award.award_type_id,
    ]
    _cols_in = [
        Award.award_type_id,
        Award.year,
        Award.nominations_open_date,
        Award.nominations_deadline,
        Award.awardee_publication_date,
        Award.remarks,
        Award.last_updated_by,
        Award.is_active,
    ]

    @staticmethod
    def get_generic_permissions() -> list:
        if current_user.is_award_admin:
            return ["create-award"]
        return []

    @staticmethod
    def nominations_are_open(item: dict) -> bool:
        return (
            item.get("is_active", False) or item.get("award_is_active", False)
        ) and item["nominations_open_date"] <= date.today() <= item[
            "nominations_deadline"
        ]

    @classmethod
    def get_item_permissions(cls, item: dict) -> list:
        item_permissions = (
            ["edit-award", "delete-award"] if current_user.is_award_admin else []
        )
        if cls.nominations_are_open(item):
            item_permissions.append("create-nomination")
        return item_permissions

    @staticmethod
    def validate_date_order(params):
        nominations_open_date = params["nominations_open_date"]
        nominations_deadline = params["nominations_deadline"]
        awardee_publication_date = params.get("awardee_publication_date")
        if nominations_open_date > nominations_deadline:
            raise BadRequestException("The nominations cannot close before they open!")
        if awardee_publication_date:
            if nominations_deadline >= awardee_publication_date:
                raise BadRequestException(
                    "The awardees cannot be published before the nominations close!"
                )

    @staticmethod
    def ensure_unique_type_year_combo(payload, existing_id=None):
        type_id = payload["award_type_id"]
        year = payload["year"]
        query = (
            Award.session()
            .query(Award.id)
            .filter(
                Award.year == year,
                Award.award_type_id == type_id,
                Award.is_active.is_(True),
            )
        )
        if existing_id is not None:
            query = query.filter(Award.id != existing_id)
        if query.first() is not None:
            raise BadRequestException("This award already exists!")

    @staticmethod
    def get_title(award_dict):
        return f"{award_dict['type']} {award_dict['year']}"

    @classmethod
    def list_awards(cls):
        args = cls.parse_args()
        ssn = Award.session()
        q = ssn.query(*cls._cols_out).join(
            AwardType, AwardType.id == Award.award_type_id
        )
        q = cls.attach_query_filters(q, args, cls._args_mapping)
        if args.get("only_accepting_nominations") is True:
            q = q.filter(
                Award.is_active.is_(True),
                Award.nominations_open_date <= date.today(),
                Award.nominations_deadline >= date.today(),
            )
        res = q.all()
        item_list = cls.rows_to_list_of_dicts(res, cls._cols_out)
        for item in item_list:
            item["title"] = cls.get_title(item)
            item["_permissions"] = cls.get_item_permissions(item)
        return {
            "awards": item_list,
            "_collection_permissions": cls.get_generic_permissions(),
        }

    @classmethod
    def get_award(cls, award_id):
        ssn = Award.session()
        q = (
            ssn.query(*cls._cols_out)
            .join(AwardType, AwardType.id == Award.award_type_id)
            .filter(Award.id == award_id)
        )
        res = q.one()
        item = cls.row_to_dict(res, cls._cols_out)
        item["title"] = cls.get_title(item)
        item["_permissions"] = cls.get_item_permissions(item)
        return item

    @classmethod
    def create_award(cls):
        if not current_user.is_award_admin:
            raise AccessViolationException("Only award admins can create awards")
        payload = cls.get_payload()
        cls.validate_date_order(payload)
        cls.ensure_unique_type_year_combo(payload)
        new_award = Award.from_ia_dict({})
        cls.update_db_object(
            new_award, {**payload, "last_updated_by": current_user.cms_id}, cls._cols_in
        )
        # get award before returning to add generated fields and hypermedia
        return cls.get_award(new_award.id)

    @classmethod
    def edit_award(cls, award_id):
        if not current_user.is_award_admin:
            raise AccessViolationException("Only award admins can edit awards")
        payload = cls.get_payload()
        cls.validate_date_order(payload)
        cls.ensure_unique_type_year_combo(payload, existing_id=award_id)
        award_to_update = Award.session().query(Award).get(award_id)
        cls.update_db_object(
            award_to_update,
            {**payload, "last_updated_by": current_user.cms_id},
            cls._cols_in,
        )
        # get award before returning to add generated fields and hypermedia
        return cls.get_award(award_to_update.id)

    @classmethod
    def delete_award(cls, award_id):
        if not current_user.is_award_admin:
            raise AccessViolationException("Only award admins can delete awards")
        award_to_delete = Award.session().query(Award).get(award_id)
        cls.update_db_object(award_to_delete, {"is_active": False}, [Award.is_active])
        return {}
