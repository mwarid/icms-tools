"""Award nominations API unit"""

from datetime import date
from typing import List, Optional

import logging

from flask_login import current_user
from icms_orm.common import (
    Affiliation,
    CareerEvent,
    Country,
    Institute,
    Person,
    PersonStatus,
    Region,
)
from icms_orm.toolkit import Award, Awardee, AwardNomination, AwardType
from sqlalchemy import and_, extract, func, or_
from sqlalchemy.orm import Query, aliased

from blueprints.api_restplus.api_units.award_units.award_unit import AwardsApiCall
from blueprints.api_restplus.api_units.award_units.project_unit import (
    AwardProjectsApiCall,
)
from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)
from blueprints.api_restplus.exceptions import (
    AccessViolationException,
    BadRequestException,
)

log = logging.getLogger(__name__)
log.setLevel('INFO')

class AwardNominationsApiCall(BaseApiUnit):

    _model = ModelFactory.make_model(
        "Award_Nomination",
        {
            AwardNomination.nomination_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.award_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: True,
                    ModelFactory.EXAMPLE: 1,
                },
            ),
            # Extra info for award
            "award_title": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "award_year": ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "award_type": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nominator_cms_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            # Extra info for nominator
            "nominator_first_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominator_last_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nominee_cms_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: True,
                    ModelFactory.EXAMPLE: 1,
                },
            ),
            # Extra info for nominee
            "nominee_first_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominee_last_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominee_cms_status": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nominee_activity.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nomination_projects.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                    ModelFactory.EXAMPLE: "BRIL;Other",
                },
            ),
            AwardNomination.nominee_institute.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            # Extra info for nominee_institute
            "nominee_institute_country_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominee_institute_region_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nominee_phd_date.key: ModelFactory.field_proxy(
                ModelFactory.Date,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.working_relationship.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            AwardNomination.justification.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            AwardNomination.proposed_citation.key: ModelFactory.field_proxy(
                ModelFactory.NullableString,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            AwardNomination.remarks.key: ModelFactory.field_proxy(
                ModelFactory.NullableString,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            # some more info on the nominee:
            "nominee_nationality": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominee_gender": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominee_birth_year": ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.is_active.key: ModelFactory.field_proxy(
                ModelFactory.Boolean,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            AwardNomination.last_updated_at.key: ModelFactory.field_proxy(
                ModelFactory.DateTime,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.last_updated_by.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "_permissions": ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.String,
                    ModelFactory.EXAMPLE: ["edit-nomination"],
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
        },
    )
    _project_choices = AwardProjectsApiCall.get_projects()

    _args = (
        ParserBuilder()
        .add_argument("include_deleted", type=ParserBuilder.BOOLEAN, required=False)
        .add_argument(
            "own_nominations_only", type=ParserBuilder.BOOLEAN, required=False
        )
        .add_argument(
            AwardNomination.award_id.key, type=ParserBuilder.INTEGER, required=False
        )
        .add_argument(
            AwardNomination.nominator_cms_id.key,
            type=ParserBuilder.INTEGER,
            required=False,
        )
        .add_argument(
            AwardNomination.nominee_cms_id.key,
            type=ParserBuilder.INTEGER,
            required=False,
        )
        .add_argument(
            AwardNomination.nomination_projects.key,
            type=ParserBuilder.STRING,
            choices=_project_choices,
            required=False,
        )
        .parser
    )
    _args_mapping = [
        (AwardNomination.award_id, None, None),
        (AwardNomination.nominator_cms_id, None, None),
        (AwardNomination.nominee_cms_id, None, None),
        (AwardNomination.nomination_projects, None, None),
    ]
    Nominator = aliased(Person, name="nominator")
    Nominee = aliased(Person, name="nominee")
    NomineeStatus: PersonStatus = aliased(PersonStatus, name="nominee_status")
    NomineeCountry = aliased(Country, name="nominee_country")
    NomineeRegion = aliased(Region, name="nominee_region")
    _cols_out = [
        AwardNomination.nomination_id,
        AwardNomination.award_id,
        Award.year.label("award_year"),
        AwardType.type.label("award_type"),
        AwardNomination.nominator_cms_id,
        Nominator.first_name.label("nominator_first_name"),
        Nominator.last_name.label("nominator_last_name"),
        AwardNomination.nominee_cms_id,
        Nominee.first_name.label("nominee_first_name"),
        Nominee.last_name.label("nominee_last_name"),
        NomineeStatus.status.label("nominee_cms_status"),
        AwardNomination.nominee_activity,
        AwardNomination.nomination_projects,
        AwardNomination.nominee_institute,
        NomineeCountry.name.label("nominee_institute_country_name"),
        NomineeRegion.name.label("nominee_institute_region_name"),
        AwardNomination.nominee_phd_date,
        AwardNomination.working_relationship,
        AwardNomination.justification,
        AwardNomination.proposed_citation,
        AwardNomination.remarks,
        AwardNomination.is_active,
        AwardNomination.last_updated_at,
        AwardNomination.last_updated_by,
        Award.nominations_open_date.label("nominations_open_date"),
        Award.nominations_deadline.label("nominations_deadline"),
        Award.awardee_publication_date.label("awardee_publication_date"),
        Award.is_active.label("award_is_active"),
    ]
    _cols_in = [
        AwardNomination.award_id,
        AwardNomination.nominator_cms_id,
        AwardNomination.nominee_cms_id,
        AwardNomination.nomination_projects,
        AwardNomination.working_relationship,
        AwardNomination.justification,
        AwardNomination.proposed_citation,
        AwardNomination.remarks,
        AwardNomination.last_updated_by,
        AwardNomination.is_active,
        AwardNomination.nominee_activity,
        AwardNomination.nominee_institute,
        AwardNomination.nominee_phd_date,
    ]

    @staticmethod
    def get_generic_permissions() -> list:
        if current_user.is_award_admin:
            return ["view-all-nominations"]
        return ["view-own-nominations"]

    @staticmethod
    def current_user_is_nominator(nominator_cms_id: int) -> bool:
        return nominator_cms_id is not None and nominator_cms_id == current_user.cms_id

    @classmethod
    def get_item_permissions(cls, item: dict) -> list:
        nominations_are_open = AwardsApiCall.nominations_are_open(item)
        current_user_is_nominator = cls.current_user_is_nominator(
            item["nominator_cms_id"]
        )
        item_permissions = (
            ["edit-nomination", "cancel-nomination"]
            if nominations_are_open and current_user_is_nominator
            else []
        )
        return item_permissions + cls.get_generic_permissions()

    @classmethod
    def check_nomination_constraints(
        cls,
        payload: Optional[dict] = None,
        nomination_id_to_update: Optional[int] = None,
        intend_to_delete: bool = False,
    ):
        if payload and "nomination_projects" in payload:
            for project in payload["nomination_projects"].split(";"):
                if project not in cls._project_choices:
                    raise BadRequestException(f"Invalid CMS project choice: {project}!")
        affected_awards: List[Award] = []
        if not intend_to_delete:
            award_id = payload.get("award_id") if payload else None
            award = Award.session().query(Award).with_for_update().get(award_id)
            if award is None:
                raise BadRequestException("Non-existent award id provided!")
            affected_awards.append(award)
        if nomination_id_to_update:
            affected_awards.append(
                Award.session()
                .query(Award)
                .join(AwardNomination, AwardNomination.award_id == Award.id)
                .filter(AwardNomination.nomination_id == nomination_id_to_update)
                .with_for_update()
                .one()
            )
        for award in affected_awards:
            if not AwardsApiCall.nominations_are_open(
                {
                    "nominations_open_date": award.nominations_open_date,
                    "nominations_deadline": award.nominations_deadline,
                    "award_is_active": award.is_active,
                }
            ):
                raise AccessViolationException(
                    "Creating and updating nominations is not allowed if"
                    " nominations for the award are not open!"
                )
        if nomination_id_to_update:
            nomination_to_update = (
                AwardNomination.session()
                .query(AwardNomination)
                .with_for_update()
                .get(nomination_id_to_update)
            )
            if not cls.current_user_is_nominator(nomination_to_update.nominator_cms_id):
                raise AccessViolationException(
                    "You cannot update someone else's nomination!"
                )
        if not intend_to_delete:
            assert payload is not None
            query = (
                AwardNomination.session()
                .query(AwardNomination)
                .filter(
                    AwardNomination.award_id == payload.get("award_id"),
                    AwardNomination.nominator_cms_id == current_user.cms_id,
                    AwardNomination.nominee_cms_id == payload.get("nominee_cms_id"),
                    AwardNomination.nomination_projects
                    == payload.get("nomination_projects"),
                    AwardNomination.is_active.is_(True),
                )
            )
            if nomination_id_to_update is not None:
                query = query.filter(
                    AwardNomination.nomination_id != nomination_id_to_update
                )
            duplicate_nomination_exists = query.first() is not None
            if duplicate_nomination_exists:
                raise BadRequestException(
                    "You have already nominated this person for the"
                    " same award and project combination!"
                )

    @classmethod
    def base_get_query(cls):
        return (
            AwardNomination.session()
            .query(*cls._cols_out)
            .join(Award, Award.id == AwardNomination.award_id)
            .join(AwardType, Award.award_type_id == AwardType.id)
            .join(
                cls.Nominator, cls.Nominator.cms_id == AwardNomination.nominator_cms_id
            )
            .join(cls.Nominee, cls.Nominee.cms_id == AwardNomination.nominee_cms_id)
            .join(
                cls.NomineeStatus,
                and_(
                    cls.NomineeStatus.cms_id == AwardNomination.nominee_cms_id,
                    cls.NomineeStatus.start_date <= date.today(),
                    or_(
                        cls.NomineeStatus.end_date.is_(None),
                        cls.NomineeStatus.end_date >= date.today(),
                    ),
                ),
            )
            .join(Institute, Institute.code == AwardNomination.nominee_institute)
            .join(cls.NomineeCountry, cls.NomineeCountry.code == Institute.country_code)
            .join(cls.NomineeRegion, cls.NomineeRegion.code == cls.NomineeCountry.region_code )
        )

    @staticmethod
    def get_title(award_dict):
        return f"{award_dict['award_type']} {award_dict['award_year']}"

    @classmethod
    def list_nominations(cls):
        args = cls.parse_args()
        q = cls.base_get_query()
        q = cls.attach_query_filters(q, args, cls._args_mapping)
        if (
            args.get("own_nominations_only")
            or "view-all-nominations" not in cls.get_generic_permissions()
        ):
            q = q.filter(AwardNomination.nominator_cms_id == current_user.cms_id)
        if args.get("include_deleted") is not True:
            q = q.filter(AwardNomination.is_active.is_(True))
        res = q.all()
        item_list = cls.rows_to_list_of_dicts(res, cls._cols_out)
        for item in item_list:
            item["award_title"] = cls.get_title(item)
            item["_permissions"] = cls.get_item_permissions(item)
            item.update( cls.nomination_enriching_dict(item) )
        return item_list

    @classmethod
    def get_nomination(cls, nomination_id):
        q = cls.base_get_query().filter(AwardNomination.nomination_id == nomination_id)
        res = q.one()
        item = cls.row_to_dict(res, cls._cols_out)
        item["award_title"] = cls.get_title(item)
        item["_permissions"] = cls.get_item_permissions(item)
        item.update( cls.nomination_enriching_dict(item) )
        return item

    @classmethod
    def nomination_enriching_dict(cls, params: dict):
        today = date.today()
        q: Query = (
            Person.session()
            .query(
                PersonStatus.activity.label("activity"),
                Affiliation.inst_code.label("institute"),
                CareerEvent.date.label("phd_date"),
                Person.nationality.label("nationality"),
                Person.gender.label("gender"),
                Person.date_of_birth.label("birth_year"),
            )
            .select_from(Person)
            .join(
                PersonStatus,
                and_(
                    PersonStatus.cms_id == Person.cms_id,
                    PersonStatus.start_date <= today,
                    or_(
                        PersonStatus.end_date.is_(None), PersonStatus.end_date >= today
                    ),
                ),
            )
            .join(
                Affiliation,
                and_(
                    Affiliation.cms_id == Person.cms_id,
                    Affiliation.is_primary.is_(True),
                    Affiliation.start_date <= today,
                    or_(
                        Affiliation.end_date.is_(None), 
                        Affiliation.end_date >= today, 
                        and_( 
                             PersonStatus.status.in_(['EXMEMBER','DECEASED']), 
                             PersonStatus.start_date==Affiliation.end_date
                        ) 
                    ),
                ),
            )
            .join(
                CareerEvent,
                and_(
                    CareerEvent.cms_id == Person.cms_id,
                    CareerEvent.event_type == "phd completion",
                ),
                isouter=True,
            )
            .filter(Person.cms_id == params["nominee_cms_id"])
        )
        try:
            activity, institute, phd_date, nationality, gender, birth_date = q.distinct().one()
        except Exception as e:
            log.error( f'nomination_enriching_dict> got error: {str(e)} --- from DB: { q.distinct().all() }' )
            print ( f'nothing found for cmsId {params["nominee_cms_id"]} ?!?!' )
            raise e
        
        return {
            "nominee_activity": activity,
            "nominee_institute": institute,
            "nominee_phd_date": phd_date,
            "nominee_nationality" : nationality,
            "nominee_gender" : gender,
            "nominee_birth_year" : birth_date.year,
            "nominator_cms_id": current_user.cms_id,
            "last_updated_by": current_user.cms_id,
        }

    @classmethod
    def create_nomination(cls):
        payload = cls.get_payload()
        cls.check_nomination_constraints(payload=payload)
        new_nomination = AwardNomination.from_ia_dict({})
        cls.update_db_object(
            new_nomination,
            {**payload, **cls.nomination_enriching_dict(payload)},
            cls._cols_in,
        )
        return cls.get_nomination(new_nomination.nomination_id)

    @classmethod
    def edit_nomination(cls, nomination_id):
        payload = cls.get_payload()
        cls.check_nomination_constraints(
            payload=payload, nomination_id_to_update=nomination_id
        )
        nomination_to_update = (
            AwardNomination.session().query(AwardNomination).get(nomination_id)
        )
        cls.update_db_object(
            nomination_to_update,
            {**payload, **cls.nomination_enriching_dict(payload)},
            cls._cols_in,
        )
        return cls.get_nomination(nomination_to_update.nomination_id)

    @classmethod
    def delete_nomination(cls, nomination_id):
        cls.check_nomination_constraints(
            nomination_id_to_update=nomination_id, intend_to_delete=True
        )
        nomination_to_delete = (
            AwardNomination.session().query(AwardNomination).get(nomination_id)
        )
        cls.update_db_object(
            nomination_to_delete, {"is_active": False}, [AwardNomination.is_active]
        )
        return {}

    review_model = ModelFactory.make_model(
        # read-only model
        "Award_Nomination_For_Review",
        {
            AwardNomination.nomination_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nominator_cms_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            # Basic info for nominator
            "nominator_first_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominator_last_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nominee_cms_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            # Basic info for nominee
            "nominee_first_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominee_last_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            # Rest of nomination info
            AwardNomination.nomination_projects.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                    ModelFactory.EXAMPLE: "BRIL;Other",
                },
            ),
            AwardNomination.working_relationship.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            AwardNomination.justification.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                },
            ),
            AwardNomination.proposed_citation.key: ModelFactory.field_proxy(
                ModelFactory.NullableString,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            AwardNomination.remarks.key: ModelFactory.field_proxy(
                ModelFactory.NullableString,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            # Extra nominee info
            "nominee_cms_status": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nominee_activity.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nominee_institute.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominee_institute_country_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominee_institute_region_name": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            AwardNomination.nominee_phd_date.key: ModelFactory.field_proxy(
                ModelFactory.Date,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominee_nationality": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominee_gender": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "nominee_birth_year": ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            # corrsponding awardee, if the nominee has already been selected
            "awardee_id": ModelFactory.field_proxy(
                ModelFactory.NullableInteger,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
            "_permissions": ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.String,
                    ModelFactory.EXAMPLE: ["select"],
                    ModelFactory.REQUIRED: False,
                    ModelFactory.READONLY: True,
                },
            ),
        },
    )

    _cols_out_for_review = [
        AwardNomination.nomination_id,
        AwardNomination.nominator_cms_id,
        Nominator.first_name.label("nominator_first_name"),
        Nominator.last_name.label("nominator_last_name"),
        AwardNomination.nominee_cms_id,
        Nominee.first_name.label("nominee_first_name"),
        Nominee.last_name.label("nominee_last_name"),
        AwardNomination.nomination_projects,
        AwardNomination.working_relationship,
        AwardNomination.justification,
        AwardNomination.proposed_citation,
        AwardNomination.remarks,
        NomineeStatus.status.label("nominee_cms_status"),
        AwardNomination.nominee_activity,
        AwardNomination.nominee_institute,
        NomineeCountry.name.label("nominee_institute_country_name"),
        NomineeRegion.name.label("nominee_institute_region_name"),
        AwardNomination.nominee_phd_date,
        Nominee.nationality.label("nominee_nationality"),
        Nominee.gender.label("nominee_gender"),
        extract("year", Nominee.date_of_birth).label("nominee_birth_year"),
        Awardee.id.label("awardee_id"),
        Award.nominations_deadline.label("nominations_deadline"),
        Award.awardee_publication_date.label("awardee_publication_date"),
        Award.is_active.label("award_is_active"),
    ]

    @classmethod
    def get_selection_permissions(cls, nomination):
        if not nomination["award_is_active"]:
            print(">Inactive award!")
            return []
        awardee_pub_date = nomination["awardee_publication_date"]
        today = date.today()
        award_in_selection_window = nomination["nominations_deadline"] < today and (
            awardee_pub_date is None or today < awardee_pub_date
        )
        if not award_in_selection_window:
            return []
        nominee_is_selected = nomination["awardee_id"] is not None
        return ["de-select" if nominee_is_selected else "select"]

    @classmethod
    def list_nominations_for_review(cls, award_id: int):
        """Lists all nominations for the specified award, alongside demographic data"""

        if not current_user.is_award_admin:
            raise AccessViolationException(
                "Only award admins can fully review nominations!"
            )

        query = (
            AwardNomination.session()
            .query(*cls._cols_out_for_review)
            .join(Award, Award.id == AwardNomination.award_id)
            .join(
                cls.Nominator, cls.Nominator.cms_id == AwardNomination.nominator_cms_id
            )
            .join(cls.Nominee, cls.Nominee.cms_id == AwardNomination.nominee_cms_id)
            .join(
                cls.NomineeStatus,
                and_(
                    cls.NomineeStatus.cms_id == AwardNomination.nominee_cms_id,
                    cls.NomineeStatus.start_date <= func.current_date(),
                    or_(
                        cls.NomineeStatus.end_date.is_(None),
                        cls.NomineeStatus.end_date >= func.current_date(),
                    ),
                ),
            )
            .join(Institute, Institute.code == AwardNomination.nominee_institute)
            .join(cls.NomineeCountry, cls.NomineeCountry.code == Institute.country_code)
            .join(
                cls.NomineeRegion,
                cls.NomineeRegion.code == cls.NomineeCountry.region_code,
                isouter=True,
            )
            .join(Awardee, Awardee.cms_id == cls.Nominee.cms_id, isouter=True)
            .filter(Award.id == award_id)
            .filter(AwardNomination.is_active.is_(True))
            .order_by(cls.Nominee.cms_id, AwardNomination.nomination_id)
        )
        nomination_list = cls.rows_to_list_of_dicts(
            query.all(), cls._cols_out_for_review
        )
        for nomination in nomination_list:
            nomination["_permissions"] = cls.get_selection_permissions(nomination)
        return nomination_list
