import dataclasses
from typing import Optional, Union
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.sql.schema import Column


@dataclasses.dataclass
class ResourceFilter():
    column: Union[Column, InstrumentedAttribute, None] = dataclasses.field()
    value: object = dataclasses.field()
    # for scenarios involving composite resource IDs/PKs as well as custom searches on multiple parameters
    next: Optional['ResourceFilter'] = dataclasses.field(default=None)
    # workaround for cases where API model's fields and/or query parameters do not map to DB columns
    fallback_key: Optional[str] = dataclasses.field(default=None)

    def __iter__(self):
        self._iter_at = self
        return self

    def __next__(self) -> 'ResourceFilter':
        value: ResourceFilter = self._iter_at
        if value:
            self._iter_at = self._iter_at.next
            return value
        else:
            raise StopIteration
