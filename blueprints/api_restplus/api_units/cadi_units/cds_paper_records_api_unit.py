import json
import re
from webapp.webapp import Webapp
from icms_orm.common.common_schema_misc_tables import ApplicationAsset
from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from typing import Any, Dict, List, Optional, Type
from blueprints.api_restplus.api_units.basics.builders import \
    ModelFieldDefinition
from blueprints.api_restplus.api_units.basics.crud import (AbstractCRUDUnit,
                                                           Listonly)
from blueprints.api_restplus.api_units.basics.model_field_types import (
    Boolean, Integer, String)
from icms_orm import PseudoEnum
import urllib.request as urllib2
import logging

log: logging.Logger = logging.getLogger(__name__)


class CadiLineExternalMetaDataUnit(AbstractCRUDUnit, Listonly):
    '''
    This class provides some external metadata on CADI analyses.
    It combines the info fetched from CDS with the records used to generate the CMS results page, 
    then lists the records along with an indication on whether the record was found in each
    of the consulted sources.
    '''
    class Field(PseudoEnum):
        RECORD_NUMBER = ModelFieldDefinition.builder(
            Integer).name('cds_record').id().build()
        CADI_CODE = ModelFieldDefinition.builder(
            String).name('cadi_code').build()
        IN_RESULTS_PAPERDATA = ModelFieldDefinition.builder(
            Boolean).name('found_in_cms_results_paperdata').build()
        IN_CDS_PAPERDATA = ModelFieldDefinition.builder(
            Boolean).name('found_in_cds_paperdata').build()
        RESULTS_TITLE = ModelFieldDefinition.builder(
            String).name('results_title').build()

    @classmethod
    def get_model_fields_list(cls: Type['CadiLineExternalMetaDataUnit']) -> List[ModelFieldDefinition]:
        return cls.Field.values()

    @classmethod
    def get_model_name(cls):
        return 'CadiLineExternalMeta'

    @classmethod
    def find(cls: Type['CadiLineExternalMetaDataUnit'], needle: Optional[ResourceFilter]) -> List[Dict[str, Any]]:
        '''Returns, along with their corresponding CADI codes, a list of CDS record numbers fetched periodically from CDS'''

        url: str = Webapp.current_app().config.get('CMSRESULTS_CDSPAPERDATA_JSON', '')
        graph_data = {}
        cds_data = {int(k): v for k, v in (
            ApplicationAsset.retrieve('cadi_cds_records') or {}).items()}
        try:
            graph_data = json.loads(urllib2.urlopen(url).read())
            graph_data = {int(re.search(
                r'([\d]+)', str(x['url'])).groups()[0]): x['title'] for x in graph_data if x}
        except Exception as e:
            log.warning(
                f'Could not fetch the CMS results paperdata from {url}. Error: {e}')
        records = set(list(graph_data.keys()) + list(cds_data.keys()))

        retval = []
        for record in records:
            retval.append({
                cls.Field.RESULTS_TITLE.name: graph_data.get(record, None),
                cls.Field.RECORD_NUMBER.name: record,
                cls.Field.IN_RESULTS_PAPERDATA.name: record in graph_data,
                cls.Field.CADI_CODE.name: cds_data.get(record, None),
                cls.Field.IN_CDS_PAPERDATA.name: record in cds_data,
            })
        return retval
