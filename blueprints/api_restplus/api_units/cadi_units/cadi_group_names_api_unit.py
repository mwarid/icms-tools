from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)
from icms_orm.cmsanalysis import AWG


class CadiGroupNamesApiCall(BaseApiUnit):
    _model = ModelFactory.make_model(
        "Cadi Group Names",
        {
            AWG.name: ModelFactory.String,
            AWG.fullName: ModelFactory.String,
        },
    )
    _args = (
        ParserBuilder()
        .add_argument(AWG.name.key, type=ParserBuilder.STRING, required=False)
        .add_argument(AWG.fullName.key, type=ParserBuilder.STRING, required=False)
        .add_argument(
            "active_only", type=ParserBuilder.BOOLEAN, required=False, default=True
        )
        .parser
    )
    _args_mapping = [
        AWG.name,
        AWG.fullName,
    ]

    @classmethod
    def get_cadi_group_names(cls):
        args = cls.parse_args()
        q_cols = AWG.ia_list()
        q = AWG.session().query(*q_cols).order_by(AWG.name)
        q = cls.attach_query_filters(q, args, cls._args_mapping)
        if args.get("active_only"):
            q = q.filter(AWG.status.in_(["Active", "Active,D"]))
        return cls.rows_to_list_of_dicts(q.all(), q_cols)
