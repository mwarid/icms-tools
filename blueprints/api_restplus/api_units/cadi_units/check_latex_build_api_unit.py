import json
from typing import List
from blueprints.api_restplus.api_units.basics import ModelFactory
from blueprints.api_restplus.api_units.basics.crud import AbstractCRUDUnit
from blueprints.api_restplus.api_units.basics.crud import ModelFieldDefinition as MFD
from flask_login import current_user
from scripts.doLatexBuild import CMSLatexBuildChecker

class CheckLatexBuildCall(AbstractCRUDUnit):

    FIELD_CADI_LINE = MFD.builder(ModelFactory.String).name('cadiLine').default('').build()
    FIELD_CADI_TYPE = MFD.builder(ModelFactory.String).name('cadiType').default('pas').build()
    FIELD_DATA_MC = MFD.builder(ModelFactory.String).name('dataMC').default('d').build()
    FIELD_PREVIEW = MFD.builder(ModelFactory.Boolean).name('preview').default(False).build()
    FIELD_VERBOSE = MFD.builder(ModelFactory.Boolean).name('verbose').default(False).build()
    FIELD_KEEP_FILES = MFD.builder(ModelFactory.Boolean).name('keepFiles').default(False).build()
    FIELD_LOGS = MFD.builder(ModelFactory.Raw).name('logs').readonly(True).build()
    
    @classmethod
    def get_model_fields_list(cls) -> List[MFD]:
        return [
            cls.FIELD_CADI_LINE,
            cls.FIELD_CADI_TYPE,
            cls.FIELD_DATA_MC,
            cls.FIELD_PREVIEW,
            cls.FIELD_VERBOSE,
            cls.FIELD_KEEP_FILES,
            cls.FIELD_LOGS
        ]

    @classmethod
    def get_model_name(cls):
        return 'Check Latex Build Params'

    @classmethod
    def check(cls):
        payload = cls.get_payload()

        cadiType = payload['cadiType']
        cadiLine = payload['cadiLine']
        dataMC   = payload['dataMC']
        preview  = payload['preview']
        verbose  = payload['verbose']
        keepFiles = payload['keepFiles'] if current_user.is_admin() else False

        args = []
        if 'pas'   in cadiType.lower(): args += [ '--wrap' ]
        if 'paper' in cadiType.lower(): args += [ '--export' ]
        if preview : args = [ '--preview', '--nodraft' ]
        if dataMC != 'd': args += [ '--nodata' ]

        lbc = CMSLatexBuildChecker()
        lbc.verbose = verbose
        lbc.keepFiles = keepFiles

        log, (runTime, buildTime) = lbc.processOne( args + [cadiType, cadiLine] )
        return {
            cls.FIELD_LOGS.name: [{'title': x[0], 'content': x[1] } for x in log.items()],
            cls.FIELD_CADI_LINE.name: cadiLine,
            cls.FIELD_CADI_TYPE.name: cadiType,
            cls.FIELD_DATA_MC.name: dataMC,
            cls.FIELD_PREVIEW.name: preview,
            cls.FIELD_VERBOSE.name: verbose,
            cls.FIELD_KEEP_FILES.name: keepFiles,
        }
