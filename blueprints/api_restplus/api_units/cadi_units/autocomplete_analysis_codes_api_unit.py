import re
import sqlalchemy as sa
from util import constants as const

from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)
from icms_orm.cmsanalysis import CadiAnalysis


class AutocompleteAnalysisCodesApiCall(BaseApiUnit):
    _model = ModelFactory.make_model(
        "Analysis Codes",
        {
            CadiAnalysis.code.key: ModelFactory.String,
            CadiAnalysis.name.key: ModelFactory.String,
        },
    )
    _args = (
        ParserBuilder()
        .add_argument("search_term", type=ParserBuilder.STRING, required=True)
        .add_argument(
            "include_inactive",
            type=ParserBuilder.BOOLEAN,
            required=False,
            default=False,
        )
        .add_argument(CadiAnalysis.code.key, type=ParserBuilder.STRING, required=False)
        .add_argument(CadiAnalysis.name.key, type=ParserBuilder.STRING, required=False)
        .parser
    )
    _args_mapping = [
        CadiAnalysis.code,
        CadiAnalysis.name,
    ]

    @classmethod
    def get_analysis_codes_suggestions(cls):
        args = cls.parse_args()
        search_term = args.get("search_term")
        results = []
        output_cols = [CadiAnalysis.code, CadiAnalysis.name]
        if search_term:
            q = (
                CadiAnalysis.session()
                .query(CadiAnalysis)
                .filter(CadiAnalysis.code.ilike(f"%{search_term}%"))
            )
            q = cls.attach_query_filters(q, args, cls._args_mapping)
            if not args.get("include_inactive"):
                q = q.filter(CadiAnalysis.status != "Inactive")
            for r in q.limit(const.NUMBER_OF_SUGGESTED_RESULTS).all():
                if r.code.startswith("d"):
                    results.append((r.code[1:], r.name))
                else:
                    results.append((r.code, r.name))
        return cls.rows_to_list_of_dicts(results, output_cols)
