import flask
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory
from models.icms_people import Person, Flag, PeopleFlagsAssociation


class FlagsApiCall(BaseApiUnit):

    _model = ModelFactory.make_model('Flag Info', {
        Person.cmsId.key: ModelFactory.Integer,
        Person.lastName.key: ModelFactory.String,
        Person.firstName.key: ModelFactory.String,
        Person.instCode.key: ModelFactory.String,
        Flag.id.key: ModelFactory.String,
        Flag.desc.key: ModelFactory.String
    })

    @classmethod
    def get_flags(cls):
        PFA = PeopleFlagsAssociation
        ssn = PFA.session

        cols = [Person.cmsId, Person.lastName, Person.firstName,
                Person.instCode, Flag.id, Flag.desc]
        q = ssn.query(*cols).join(PFA, Person.cmsId ==
                                  PFA.cmsId).join(Flag, Flag.id == PFA.flagId)

        return cls.rows_to_list_of_dicts(q.all(), cols)
