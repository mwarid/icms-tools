from typing import List
from icms_orm.toolkit import CmsWeek
from blueprints.api_restplus.api_units.basics import ModelFieldDefinition
from blueprints.api_restplus.api_units.basics import ModelFieldDefinition
from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit
from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from blueprints.api_restplus.api_units.basics import ModelFactory



class CmsWeekApiCall(AbstractCRUDUnit):
    
    _model_fields = [
        ModelFieldDefinition.builder(ModelFactory.Integer).column(CmsWeek.id).id().readonly(True).build(),
        ModelFieldDefinition.builder(ModelFactory.String).column(CmsWeek.title).build(),
        ModelFieldDefinition.builder(ModelFactory.Date).column(CmsWeek.date).build(),
        ModelFieldDefinition.builder(ModelFactory.Date).column(CmsWeek.date_end).build(),
        ModelFieldDefinition.builder(ModelFactory.Boolean).column(CmsWeek.is_external).build()
    ]

    @classmethod
    def get_model_name(cls) -> str:
        return 'CMS Week'

    @classmethod
    def get_model_fields_list(cls) -> List[ModelFieldDefinition]:
        return cls._model_fields

    @classmethod
    def make_filter(cls, id: int):
        return ResourceFilter(CmsWeek.id, id)
