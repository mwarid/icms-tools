from typing import List
from icms_orm.toolkit import Room
from blueprints.api_restplus.api_units.basics import ModelFieldDefinition
from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit
from blueprints.api_restplus.api_units.basics import ModelFactory


class RoomApiCall(AbstractCRUDUnit):
    @classmethod
    def get_model_fields_list(cls) -> List[ModelFieldDefinition]:
        return [
            ModelFieldDefinition.builder(ModelFactory.Integer).column(
                Room.id).id().readonly(True).build(),
            ModelFieldDefinition.builder(
                ModelFactory.Integer).column(Room.indico_id).build(),
            ModelFieldDefinition.builder(
                ModelFactory.String).column(Room.building).build(),
            ModelFieldDefinition.builder(
                ModelFactory.String).column(Room.floor).build(),
            ModelFieldDefinition.builder(
                ModelFactory.String).column(Room.room_nr).build(),
            ModelFieldDefinition.builder(ModelFactory.String).column(
                Room.custom_name).build(),
            ModelFieldDefinition.builder(
                ModelFactory.Boolean).column(Room.at_cern).build(),
            ModelFieldDefinition.builder(
                ModelFactory.Integer).column(Room.capacity).build()
        ]

    @classmethod
    def get_model_name(cls) -> str:
        return 'Room'
