from typing import Type, Union
from flask_restx import fields


Integer = fields.Integer
Boolean = fields.Boolean
String = fields.String
Raw = fields.Raw
Date = fields.Date
DateTime = fields.DateTime
Nested = fields.Nested
List = fields.List
Float = fields.Float


class AnalysisCode(fields.String):
    def __init__(self, *args, **kwargs):
        kwargs['pattern'] = kwargs.get(
            'pattern', r'^\s*[A-Z0-9]{3}-\d{2}-\d{3}\s*$')
        super().__init__(*args, **kwargs)


class NullableInteger(fields.Integer):
    __schema_type__ = ['integer', 'null']
    __schema_example__ = 42

    def format(self, value):
        if value is not None:
            return fields.Integer.format(self, value)
        return None


class NullableString(fields.String):
    __schema_type__ = ['string', 'null']
    __schema_example__ = 'hello'

    def format(self, value):
        if value is not None:
            return fields.String.format(self, value)
        return None


class NullableDate(fields.Date):
    __schema_type__ = ['string', 'null']
    __schema_example__ = '2019-06-24'

    def format(self, value):
        if value is not None:
            return fields.Date.format(self, value)
        return None


AnyFieldType = Union[Type[Integer], Type[Boolean], Type[String],
                     Type[Raw], Type[Date], Type[DateTime], Type[List], Type[Float],
                     Type[AnalysisCode], Type[NullableDate], Type[NullableInteger], Type[NullableString]
                     ]
