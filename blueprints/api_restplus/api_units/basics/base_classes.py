import logging
import re
from webapp.webapp import Webapp
import flask
from flask.globals import current_app
import sqlalchemy as sa
from typing import Any, Dict, List, Optional, Type

from werkzeug.routing import Rule
from blueprints.api_restplus import SingletonApiFactory
from blueprints.api_restplus.api_units.basics.wrappers import HyperlinkWrapper
from blueprints.api_restplus.decorators import AuthorizationContextFactory
from blueprints.api_restplus.param_stores import ParamStoreProvider
from icms_orm.toolkit import RestrictedActionTypeValues as ActionType
from sqlalchemy.orm.attributes import InstrumentedAttribute
from webapp.permissions import PermissionsManager

log = logging.getLogger(__name__)
log.setLevel('INFO')


class BaseApiUnit(object):
    _model = None
    _args = None
    _hyperlink_wrappers = None
    # list of triplets of (ia_attr, payload_key (default: ia_attr.key), payload value processor (default: pass-through)
    # used for updating the object from received payload data
    _payload_translation_rules = None
    # list of triplets used to create a select query
    _query_translation_rules = None

    class ProcessorFunctions(object):
        @staticmethod
        def wildcard_match(value):
            return value is not None and '%{value}%'.format(value=value) or value

        @staticmethod
        def tokenized_match(val):
            return re.findall(r'\w+', val or '')

    @classmethod
    def _add_hypermedia_to_row_dict(cls: Type['BaseApiUnit'], input_dict: Dict[str, Any], path_variables=None) -> Dict[str, Any]:
        # Emulating hypermedia capabilities: 1) determining available param for _links
        _key = '_links'
        while _key in input_dict.keys():
            _key = '_' + _key
        # 2) checking registered links providers, verifying access rights and populating the map accordingly
        hyperlink_dict = dict()
        log.debug('Adding, maybe, some hypermedia to the output. Registered hyperlinks: {0} for class {1}'.format(
            [x.label for x in (cls._hyperlink_wrappers or [])], cls.__name__))
        for entry in (cls._hyperlink_wrappers or []):
            entry: HyperlinkWrapper
            context = AuthorizationContextFactory.create_context()
            context.register_parameter_store('r', input_dict)
            ps = ParamStoreProvider.get_for_view_args(lambda: path_variables)
            context.register_parameter_store(ps.prefix, ps.getter())
            action = ActionType.get_for_http_method_name(entry.method.lower())

            # Getting rid of the script SCRIPT_NAME
            resource_url: str = entry.get_url(path_variables)

            matching_rule: Optional[Rule] = Webapp.get_routing_rule_matching_path(
                current_app, resource_url)

            if matching_rule is not None:
                if PermissionsManager.can_perform(action=action, resource_key=matching_rule.rule, context=context):
                    hyperlink_dict[entry.label] = {
                        'url': resource_url,
                        'method': entry.method
                    }
                    log.debug(
                        'Verification passed, adding hyperlink labelled {0}'.format(entry.label))
                else:
                    log.debug(
                        f'Verification failed for resource URI {resource_url}')
                input_dict[_key] = hyperlink_dict
        return input_dict

    @classmethod
    def row_to_dict(cls: Type['BaseApiUnit'], row: List[Any], cols_list: List[InstrumentedAttribute]) -> Dict[str, Any]:
        """
        Called to transform a result of a query, a list of column values, into a dictionary
        (the latter subsequently being converted into a model by restplus).
        Override this method to add some custom processing, synthetic columns etc.
        """
        result = {}
        assert len(cols_list) == len(row)
        for _value, _label in zip(row, cols_list):
            _key = None
            if isinstance(_label, str):
                _key = _label
            elif hasattr(_label, 'key'):
                _key = _label.key
            elif hasattr(_label, 'label'):
                _key = _label.label
            result[_key] = _value

        return cls._add_hypermedia_to_row_dict(result)

    @classmethod
    def rows_to_list_of_dicts(cls, rows: List[List[Any]], cols_list: List[InstrumentedAttribute]) -> List[Dict[str, Any]]:
        result = [cls.row_to_dict(row, cols_list) for row in rows]
        return result

    @classmethod
    def register_hyperlink_wrapper(cls, link_data: HyperlinkWrapper):
        cls._hyperlink_wrappers = cls._hyperlink_wrappers or []
        cls._hyperlink_wrappers.append(link_data)
        log.debug('Registered hyperlinks with class {0}: {1}'.format(
            cls.__name__, [x.label for x in cls._hyperlink_wrappers]))

    @classmethod
    def get_model(cls):
        return cls._model

    @classmethod
    def get_args(cls):
        if cls._args is None:
            raise NotImplementedError(
                'Class {0} does not have any argument parser defined!'.format(cls.__name__))
        return cls._args

    @classmethod
    def parse_args(cls, request=None):
        request = request or flask.request
        return cls.get_args().parse_args(request)

    @classmethod
    def get_payload(cls):
        return SingletonApiFactory.get_api().payload

    @classmethod
    def update_db_object(cls, target, payload, rules, defer_commit=False):
        """
        Updates provided DB object using the information from payload. Rule tuples are used to describe relationship
        between object fields, payload keys and, should need be, translation methods
        :param target: the DB object
        :param payload: the payload received in the API
        :param rules: tuples of (column_ia, payload_key (or column_ia.key), translation_method (or lambda x: x))
        :param defer_commit: enable if calling code wants to perform some further inspections before persisting the data
        :return:
        """
        ssn = target.session() if callable(target.session) else target.session
        _changed = False
        for _field, _key, _processor in [_r if isinstance(_r, tuple) else (_r, None, None) for _r in rules]:
            _key = _key or _field.key
            _processor = _processor or (lambda x: x)
            if _key in payload:
                _new_val = _processor(payload.get(_key))
                if _new_val != target.get(_field):
                    target.set(_field, _new_val)
                    _changed = True
        if _changed and not defer_commit:
            ssn.add(target)
            ssn.commit()
        return _changed

    @classmethod
    def attach_query_filters(cls, query, params_map, rules):
        """
        :param query: query object
        :param params_map: a dictionary with input params
        :param rules: single col_ia objects or tuples: (col_ia, map_key [or col_ia.key], ref_processor [or lambda x: x])
        :return:
        """
        for _col, _key, _processor in [_r if isinstance(_r, tuple) else (_r, None, None) for _r in rules]:
            _key = _key or _col.key
            _processor = _processor or (lambda x: x)
            _ref_val = params_map.get(_key)
            # for appendable arguments, we receive the values as array. nulls come as Nones, not eg. empty array
            if isinstance(_ref_val, list):
                _ref_val = [_processor(_e) for _e in _ref_val]
            else:
                _ref_val = _processor(_ref_val)
            if _ref_val is None:
                continue

            # pack the reference value into a list if it's not one already
            if not isinstance(_ref_val, list):
                _refs = [_ref_val]
            else:
                _refs = _ref_val

            _criteria = []

            for _ref in _refs:
                _like = False
                # postgres enums don't like to be "like"d so we determine which we can use
                try:
                    if _col.type.python_type == str:
                        _like = True
                        # would be good but enums translate to str in python SADLY
                        if _col.type.native_enum:
                            # DELIBERATELY NESTED IF - non-enum _col_type does not have the .native_enum - exceptions fly!
                            _like = False
                except Exception as e:
                    log.debug(
                        'Caught an exception but expected one. Whatever is in _like should remain.')
                if _like:
                    _criteria.append(_col.ilike(_ref))
                elif isinstance(_ref, list):
                    _criteria.append(_col.in_(_ref))
                else:
                    _criteria.append(_col == _ref)

            query = query.filter(sa.or_(*_criteria))
        return query
