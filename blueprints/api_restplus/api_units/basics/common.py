from blueprints.api_restplus.exceptions import AccessViolationException


def can_manage_job_nominations(current_user):
    return (
        current_user.is_admin
        or current_user.is_job_nomination_committee
        or current_user.is_project_manager
        or current_user.is_spokesperson_team_member
        or current_user.is_cms_jobop_admins
    )
