import logging
from typing import Any, Iterable, List, Optional, Union
from dataclasses import dataclass, field
import dataclasses
from flask_restx import reqparse
from flask_restx import fields
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.sql.elements import ColumnElement
from sqlalchemy.sql.schema import Column
from werkzeug.datastructures import FileStorage
from blueprints.api_restplus.api_units.basics.model_field_types import AnyFieldType


log: logging.Logger = logging.getLogger(__name__)


class ParserBuilder(object):
    INTEGER = int
    STRING = str
    BOOLEAN = fields.boolean
    DATE = fields.date_from_iso8601
    FILE = FileStorage

    def __init__(self):
        self._parser = reqparse.RequestParser(argument_class=reqparse.Argument)

    def add_argument(self, name, type, required, default=None, choices=None, location=None, action=None, help=None):
        """
        :param name:
        :param type:
        :param required:
        :param default:
        :param choices:
        :param location: 'form', 'data' etc.
        :param action: use 'append' for chaining the params
        :param help:
        :return:
        """
        # File type must be sent with files
        if type == ParserBuilder.FILE:
            location = 'files'

        _params = dict(type=type, required=required, default=default, choices=choices, location=location,
                       action=action, help=help)
        # not passing the None values
        self._parser.add_argument(reqparse.Argument(
            name, **{_k: _v for _k, _v in _params.items() if _v is not None}))
        return self

    def add_appendable_argument(self, name, type, required, default=None, choices=None, location=None, help=None):
        return self.add_argument(name, type, required, default, choices=choices, location=location, action='append', help=help)

    @property
    def parser(self):
        return self._parser


class ModelFieldDefinitionBuilder():
    def __init__(self, type: AnyFieldType):
        self._data = dict()
        self._data['type'] = type

    def column(self, column: Union[InstrumentedAttribute, ColumnElement]) -> 'ModelFieldDefinitionBuilder':
        self._data['column'] = column
        if 'name' not in self._data.keys():
            if isinstance(column, Column) or isinstance(column, InstrumentedAttribute):
                self.name(column.key)
            else:
                log.debug(
                    'No candidate name for model\'s field name. It better be set by a later call...')
        return self

    def required(self, is_required: bool = True):
        self._data['required'] = is_required
        return self

    def readonly(self, is_readonly: bool = True):
        self._data['readonly'] = is_readonly
        return self

    def name(self, name: str):
        self._data['name'] = name
        return self

    def default(self, value: object):
        self._data['default'] = value
        return self

    def enum(self, pool: Iterable[Any]):
        self._data['enum'] = list(pool)
        return self

    def id(self, value: bool = True):
        self._data['id'] = value
        return self

    def build(self):
        return ModelFieldDefinition(**self._data)


@dataclass
class ModelFieldDefinition():

    type: AnyFieldType = field(metadata=dict(export_as=None))
    name: str
    required: bool = field(default=False)
    readonly: bool = field(default=False)
    default: object = field(default=None)
    description: Optional[str] = field(default=None)
    example: object = field(default=None)
    # can be used to create a field mirroring alchemy's column (types not translated!)
    column: Optional[Union[InstrumentedAttribute, Column]
                     ] = field(default=None)
    # list of possible values
    enum: Optional[List[object]] = field(default=None)
    # defines the name of source object's attribute to obtain the value from
    attribute: Optional[str] = field(default=None)
    # defines the nested type, eg. for a list
    nested_type: object = field(
        default=None, metadata=dict(export_as='cls_or_instance'))
    # marks attrubute as (part of) the resource's ID
    id: bool = field(default=False)

    @classmethod
    def builder(cls, type: AnyFieldType) -> ModelFieldDefinitionBuilder:
        return ModelFieldDefinitionBuilder(type)

    def as_pruned_dict(self):
        """
        dataclasses.asdict would hit the recursion limit for some reason so using it as a starting point (before filtering some fields out) was not feasible
        """
        result = dict()
        field: dataclasses.Field
        for field in dataclasses.fields(self):
            value = getattr(self, field.name)
            export_as = (field.metadata or dict()).get('export_as', field.name)
            if value is not None and export_as is not None:
                result[export_as] = value
        return result
