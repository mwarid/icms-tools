import logging
from datetime import date
from logging import Logger
from typing import Any, Dict, List, Optional, Tuple, Type

import flask
import flask_login
import sqlalchemy as sa
from icms_orm.cmsanalysis import (
    PaperAuthor,
    PaperAuthorHistory,
    PaperAuthorHistoryActions,
    PaperAuthorStatusValues,
)
from icms_orm.cmspeople import Institute as OldInstitute
from icms_orm.cmspeople import Person as OldPerson
from icms_orm.common import Affiliation, Person, PersonStatus
from icmsutils.businesslogic.cadi.authorlists import (
    AuthorListModel,
    generate_db_author_list,
    mark_in_db_as_closed,
)
from icmsutils.exceptions import IcmsException
from sqlalchemy import and_, or_
from sqlalchemy.orm.query import Query

from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from blueprints.api_restplus.api_units.basics import (
    AbstractCRUDUnit,
    BaseApiUnit,
    ParserBuilder,
)
from blueprints.api_restplus.api_units.basics.builders import (
    ModelFieldDefinition as FieldDef,
)
from blueprints.api_restplus.api_units.basics.crud import Deleteless, Postless, Putless
from blueprints.api_restplus.api_units.basics.factories import ModelFactory
from blueprints.api_restplus.api_units.basics.model_field_types import (
    AnalysisCode,
    Boolean,
    Date,
    Integer,
    NullableDate,
    NullableInteger,
    String,
)
from blueprints.api_restplus.api_units.icms_public_api_units import ParsedHistoryCall
from blueprints.api_restplus.exceptions import (
    ArbitraryErrorCodeException,
    BadRequestException,
)
from scripts.generatePaperAuthorLists import PaperAuthorListGenerator

log: Logger = logging.getLogger(__name__)


class AuthorListEntryUnit(AbstractCRUDUnit, Postless, Deleteless, Putless):
    """
    Exposes the operations that can be performed on individual AL entry.
    In particular it allows fetching all entries by paper code or author cms id but also the modifications where possible.
    """

    FIELD_ID = (
        FieldDef.builder(type=Integer)
        .column(PaperAuthor.authorID)
        .name("id")
        .id()
        .readonly(True)
        .build()
    )
    FIELD_CODE = (
        FieldDef.builder(type=AnalysisCode)
        .column(PaperAuthor.code)
        .readonly(True)
        .build()
    )
    FIELD_CMS_ID = (
        FieldDef.builder(type=Integer).column(PaperAuthor.cmsid).readonly(True).build()
    )
    FIELD_STATUS = (
        FieldDef.builder(type=String)
        .column(PaperAuthor.status)
        .enum(PaperAuthorStatusValues.values())
        .build()
    )
    FIELD_INST_CODE = FieldDef.builder(type=String).column(PaperAuthor.instCode).build()
    FIELD_NAME_SIGN = (
        FieldDef.builder(type=String).column(PaperAuthor.nameSignature).build()
    )
    FIELD_CMS_STATUS = (
        FieldDef.builder(type=String).column(PaperAuthor.statusCms).build()
    )
    FIELD_CMS_ACTIVITY = (
        FieldDef.builder(type=String).column(PaperAuthor.activity).build()
    )

    @classmethod
    def get_model_name(cls) -> str:
        return "AuthorListEntry"

    @classmethod
    def get_model_fields_list(cls) -> List[FieldDef]:
        return [
            cls.FIELD_ID,
            cls.FIELD_CODE,
            cls.FIELD_CMS_ID,
            cls.FIELD_STATUS,
            cls.FIELD_NAME_SIGN,
            cls.FIELD_INST_CODE,
            cls.FIELD_CMS_ACTIVITY,
            cls.FIELD_CMS_STATUS,
        ]

    @classmethod
    def find(
        cls: Type["AbstractCRUDUnit"], needle: Optional[ResourceFilter]
    ) -> List[Dict[str, Any]]:
        """Returns author list entries (paper-person combinations) matching specified criteria"""
        return super().find(needle=needle)

    @classmethod
    def get_by_identity(cls: Type["AbstractCRUDUnit"], *params) -> Dict[str, Any]:
        """Returns information on a specific author list entry (paper-person combination)"""
        return super().get_by_identity(*params)


class AuthorListUnit(AbstractCRUDUnit):
    """
    Exposes operations that can be performed on an author list.
    Basically: generating (inDB) the AL, closing it (usually that happens
    implicitly upon creating the AL files but conceptually they are separate notions),
    deleting altogether or fetching the information on AL object: current state, paper code, creation date and such.

    should only allow GET, POST (creating new), PUT (updating the status only - meaning closing) and, perhaps, DELETE - dropping the thing
    """

    SUBQUERY_LIST_CLOSE_ENTRIES = (
        Query(
            [
                sa.func.max(PaperAuthorHistory.historyID).label("al_close_id"),
                PaperAuthorHistory.code,
            ]
        )
        .filter(PaperAuthorHistory.action == PaperAuthorHistoryActions.LIST_CLOSE)
        .group_by(PaperAuthorHistory.code)
        .subquery()
    )

    SUBQUERY_LIST_GEN_ENTRIES = (
        Query(
            [
                sa.func.max(PaperAuthorHistory.historyID).label("al_gen_id"),
                PaperAuthorHistory.code.label("code"),
            ]
        )
        .filter(PaperAuthorHistory.action == PaperAuthorHistoryActions.LIST_GEN)
        .group_by(PaperAuthorHistory.code)
        .subquery()
    )

    FIELD_PAPER_CODE = (
        FieldDef.builder(type=AnalysisCode)
        .id()
        .required()
        .column(PaperAuthorHistory.code)
        .name("paper_code")
        .build()
    )
    FIELD_GENERATION_DATE = (
        FieldDef.builder(type=Date)
        .column(PaperAuthorHistory.date)
        .name("generation_date")
        .readonly()
        .build()
    )
    FIELD_CREATOR_CMS_ID = (
        FieldDef.builder(type=Integer)
        .column(PaperAuthorHistory.cmsid)
        .name("generated_by")
        .readonly()
        .build()
    )
    FIELD_IS_CLOSED = (
        FieldDef.builder(type=Boolean)
        .name("is_closed")
        .readonly(False)
        .column(
            SUBQUERY_LIST_CLOSE_ENTRIES.c.al_close_id is not None
            and SUBQUERY_LIST_CLOSE_ENTRIES.c.al_close_id
            > SUBQUERY_LIST_GEN_ENTRIES.c.al_gen_id
        )
        .build()
    )

    @classmethod
    def get_query(cls, resource_filter: Optional[ResourceFilter]) -> Query:
        return (
            super()
            .get_query(resource_filter=resource_filter)
            .join(
                cls.SUBQUERY_LIST_GEN_ENTRIES,
                cls.SUBQUERY_LIST_GEN_ENTRIES.c.al_gen_id
                == PaperAuthorHistory.historyID,
                isouter=False,
            )
            .join(
                cls.SUBQUERY_LIST_CLOSE_ENTRIES,
                cls.SUBQUERY_LIST_CLOSE_ENTRIES.c.code == PaperAuthorHistory.code,
                isouter=True,
            )
        )

    @classmethod
    def get_model_name(cls) -> str:
        return "AuthorList"

    @classmethod
    def get_model_fields_list(cls) -> List[FieldDef]:
        return [
            cls.FIELD_PAPER_CODE,
            cls.FIELD_GENERATION_DATE,
            cls.FIELD_CREATOR_CMS_ID,
            cls.FIELD_IS_CLOSED,
        ]

    @classmethod
    def _write_or_rewrite_the_al(
        cls, code: Optional[str] = None, rewrite: bool = False
    ):
        """
        :param str code:
        """
        payload = cls.get_payload()
        cms_id = flask_login.current_user.cms_id
        code = code or payload.get(cls.FIELD_PAPER_CODE.name)

        palg = PaperAuthorListGenerator(db=flask.current_app.db)
        if not palg.isInitialised:
            raise IcmsException(palg.errMessages)

        returned_values = palg.createAuthorListInDB(code, cms_id, rewrite)
        (status, msg) = returned_values
        if not status:
            raise IcmsException(msg)

        # The PALG generates the necessary DB entries, so we can just fetch them and carry on happily
        return cls.get_by_identity(code)

    @classmethod
    def post(cls) -> Dict[str, object]:
        """Creates an author list for given paper code along with the corresponding AL entries"""
        return cls._write_or_rewrite_the_al(rewrite=False)

    @classmethod
    def find(
        cls: Type["AbstractCRUDUnit"], needle: Optional[ResourceFilter]
    ) -> List[Dict[str, Any]]:
        """Returns information on matching author lists"""
        return super().find(needle=needle)

    @classmethod
    def put_by_identity(cls, *params):
        """Updates author list's information.

        The only allowed changes at this point are
        - closing an open AL
        - reopening a closed AL
        """
        code = params[0]
        payload = cls.get_payload()
        json_code = payload.get(cls.FIELD_PAPER_CODE.name)
        if code != json_code:
            raise ArbitraryErrorCodeException(
                400,
                f"Updating paper code is not supported (current: {code}, received: {json_code})",
            )
        current_user_person = flask_login.current_user.person
        is_closed = payload.get(cls.FIELD_IS_CLOSED.name)
        if is_closed is True:
            al_model = AuthorListModel(code=code, actor=current_user_person)
            al_model.close_author_list()
        elif is_closed is False:
            al_model = AuthorListModel(code=code, actor=current_user_person)
            al_model.reopen()
        return cls.get_by_identity(code)

    @classmethod
    def delete_by_identity(cls, *params):
        """Deletes the author list from DB (completely, including the corresponding entries)"""
        code = params[0]
        session = PaperAuthor.session()
        if (
            session.query(PaperAuthorHistory)
            .filter(PaperAuthorHistory.code == code)
            .delete()
            == session.query(PaperAuthor).filter(PaperAuthor.code == code).delete()
            == 0
        ):
            raise ArbitraryErrorCodeException(404, f"Nothing to delete for {code}")

        PaperAuthor.session().commit()


class OpenDataAuthorListApiUnit(BaseApiUnit):
    """
    Compile aggregate author lists over multiple years for OpenData records.

    The compiled list includes any member who has been an author at any time
     in the year range [year_from, year_until] inclusive.
    The affiliation shown is the primary affiliation at the time of the request.
    """

    _model = ModelFactory.make_model(
        "OpenDataAuthorList",
        {
            "compiled_on": ModelFactory.field_proxy(
                ModelFactory.Date, {ModelFactory.READONLY: True}
            ),
            "year_from": ModelFactory.field_proxy(
                ModelFactory.Integer, {ModelFactory.READONLY: True}
            ),
            "year_until": ModelFactory.field_proxy(
                ModelFactory.Integer, {ModelFactory.READONLY: True}
            ),
            "authors": ModelFactory.field_proxy(
                ModelFactory.List,
                {
                    ModelFactory.ELEMENT_TYPE: ModelFactory.Nested(
                        ModelFactory.make_model(
                            "OpenDataAuthor",
                            {
                                "affiliation": ModelFactory.String,
                                "ccid": ModelFactory.String,
                                "inspireid": ModelFactory.String,
                                "name": ModelFactory.String,
                            },
                        )
                    ),
                    ModelFactory.READONLY: True,
                },
            ),
        },
    )
    _args = (
        ParserBuilder()
        .add_argument("year_from", type=ParserBuilder.INTEGER, required=True)
        .add_argument("year_until", type=ParserBuilder.INTEGER, required=True)
        .parser
    )

    new_query_cols = (
        Person.first_name,
        Person.last_name,
        Person.hr_id,
        Affiliation.inst_code,
    )
    old_inst_query_cols = (
        OldInstitute.code,
        OldInstitute.spiresIcn,
    )
    old_person_query_cols = (
        OldPerson.hrId,
        OldPerson.authorId,
    )

    @classmethod
    def get(cls):
        query_params = cls.parse_args()
        year_from = query_params["year_from"]
        year_until = query_params["year_until"]
        day_from = date(year=year_from, month=1, day=1)
        day_until = date(year=year_until, month=1, day=1)
        today = date.today()
        author_query: Query = (
            Person.session()
            .query(*cls.new_query_cols)
            .select_from(Person)
            .join(
                PersonStatus,
                and_(
                    PersonStatus.cms_id == Person.cms_id,
                    PersonStatus.is_author.is_(True),
                    PersonStatus.start_date <= day_from,
                    or_(
                        PersonStatus.end_date.is_(None),
                        PersonStatus.end_date >= day_until,
                    ),
                ),
            )
            .join(
                Affiliation,
                and_(
                    Affiliation.cms_id == Person.cms_id,
                    Affiliation.is_primary.is_(True),
                    Affiliation.start_date <= today,
                    or_(Affiliation.end_date.is_(None), Affiliation.end_date >= today),
                ),
            )
        )
        authors = cls.rows_to_list_of_dicts(author_query.all(), cls.new_query_cols)

        spires_query: Query = OldInstitute.session().query(*cls.old_inst_query_cols)
        spires_data = cls.rows_to_list_of_dicts(
            spires_query.all(), cls.old_inst_query_cols
        )
        spires_dict = {
            spires_code[OldInstitute.code.key]: spires_code[OldInstitute.spiresIcn.key]
            for spires_code in spires_data
        }

        inspire_query: Query = OldPerson.session().query(*cls.old_person_query_cols)
        inspire_data = cls.rows_to_list_of_dicts(
            inspire_query.all(), cls.old_person_query_cols
        )
        inspire_dict = {
            entry[OldPerson.hrId.key]: entry[OldPerson.authorId.key]
            for entry in inspire_data
        }

        combined_author_info = [
            {
                "affiliation": spires_dict[author[Affiliation.inst_code.key]],
                "ccid": f"CCID-{author[Person.hr_id.key]}",
                "inspireid": inspire_dict[author[Person.hr_id.key]],
                "name": f"{author[Person.last_name.key]}, {author[Person.first_name.key]}",
            }
            for author in authors
        ]
        return {
            "compiled_on": today,
            "year_from": year_from,
            "year_until": year_until,
            "authors": combined_author_info,
        }
