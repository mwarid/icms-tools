from blueprints.api_restplus.exceptions import ArbitraryErrorCodeException
from datetime import datetime
import logging

from icmsutils.exceptions import AuthorListException, IcmsException
from scripts.generatePaperAuthorLists import PaperAuthorListGenerator

import flask_login
from blueprints.author_lists.gen_al_views import AuthorListFileType
import flask
from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from blueprints.api_restplus.api_units.basics.crud import Deleteless, Getless, Listless, Listonly, Postless, Putless
from blueprints.api_restplus.api_units.basics.model_field_types import AnalysisCode, Boolean, Date, Integer, Raw, String
from typing import Any, Dict, List, Optional, Type
from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit
from blueprints.api_restplus.api_units.basics import \
    ModelFieldDefinition as FieldDef
from icms_orm.toolkit import ALFileset
from icmsutils.businesslogic.cadi.authorlists import mark_in_db_as_having_al_files_generated


log: logging.Logger = logging.getLogger(__name__)


class ALFileApiUnit(AbstractCRUDUnit, Listonly):
    """
    Until individual files can be generated, this unit will only offer the possibility of listing the entries matching provided criteria
    The latter will be narrowed down to columns overlapping with the DB to keep things simple.
    """

    FIELD_URI = FieldDef.builder(String).name('uri').readonly().id().build()
    FIELD_PAPER_CODE = FieldDef.builder(AnalysisCode).column(
        ALFileset.paper_code).readonly().build()
    FIELD_FILE_TYPE = FieldDef.builder(String).name('type').readonly().build()

    @classmethod
    def get_model_name(cls) -> str:
        return 'AuthorListFile'

    @classmethod
    def get_model_fields_list(cls) -> List[FieldDef]:
        return [cls.FIELD_URI, cls.FIELD_PAPER_CODE, cls.FIELD_FILE_TYPE]

    @classmethod
    def get_db_columns(cls):
        return super().get_db_columns()

    @classmethod
    def find(cls, needle: Optional[ResourceFilter] = None) -> List[Dict[str, Any]]:
        '''Returns information about author list files matching specified criteria'''
        db_results: List[Dict[str, Any]] = super().find(needle=needle)
        expanded_results = list()
        for db_result in db_results:
            for file_type in AuthorListFileType:
                url = flask.url_for(
                    'author_lists.route_get_al_file', code=db_result[cls.FIELD_PAPER_CODE.name], type=file_type.value)
                result = {
                    cls.FIELD_URI.name: url,
                    cls.FIELD_FILE_TYPE.name: file_type.value
                }
                result.update(db_result)
                expanded_results.append(result)
        return expanded_results


class ALFileSetApiCall(AbstractCRUDUnit, Deleteless):
    
    FIELD_CODE = FieldDef.builder(AnalysisCode).column(
        ALFileset.paper_code).id().build()
    FIELD_LAST_UPDATE = FieldDef.builder(Date).column(
        ALFileset.last_update).readonly().build()
    FIELD_LOCATION = FieldDef.builder(String).column(
        ALFileset.location).default('eos').readonly().build()

    @classmethod
    def get_model_name(cls: Type['ALFileSetApiCall']) -> str:
        return 'AuthorListFileSet'

    @classmethod
    def get_model_fields_list(cls: Type['ALFileSetApiCall']) -> List[FieldDef]:
        return [cls.FIELD_CODE, cls.FIELD_LAST_UPDATE, cls.FIELD_LOCATION]

    @classmethod
    def delete_by_identity(cls: Type['ALFileSetApiCall'], *params):
        '''Deletes designated author list fileset.'''
        return super().delete_by_identity(*params)

    @classmethod
    def put_by_identity(cls: Type['ALFileSetApiCall'], *params):
        '''Re-generates designated author list fileset.'''
        code = params[0]
        json_code = cls.get_payload()[cls.FIELD_CODE.name]
        if code != json_code:
            raise ArbitraryErrorCodeException(
                400, f'Illegal paper codes mismatch: URL contains {code}, payload contains {json_code}')
        log.debug(f'About to attempt files re-generation for «{code}»')
        return cls._write_or_rewrite_al_files(True, code or None)

    @classmethod
    def post(cls: Type['ALFileSetApiCall']) -> Dict[str, Any]:
        '''Creates an authorlist fileset for specified parameters'''
        return cls._write_or_rewrite_al_files(False, None)

    @classmethod
    def _write_or_rewrite_al_files(cls: Type['ALFileSetApiCall'], force_rewrite: bool = False, paper_code: Optional[str] = None) -> Dict[str, Any]:
        flask.current_app.remount_eos_if_needed()

        payload = cls.get_payload()
        al_code = payload.get(cls.FIELD_CODE.name)
        cms_id = flask_login.current_user.person.cmsId
        palg = PaperAuthorListGenerator(db=flask.current_app.db)
        if not palg.isInitialised:
            raise IcmsException(palg.errMessages)

        returned_values = palg.generatePaperAuthorListFiles(
            al_code, force_rewrite)
        (status, msg) = returned_values

        if not status:
            raise IcmsException(msg)

        log.debug(
            f'Authorlist files for {al_code} successfully {force_rewrite and "re-" or ""} created.')

        """
        -toDo: to be discussed with George (et al?) if this should be an explict action in the system.
               we should also close the AL before generating the files, and think about how to roll-back
               the closing if there is an issue in the workflow.
               for now, ignore the exception raised when the AL is already closed:
        -myTake: 
            - the actions should be separate: closing AL in DB and generating files). 
            - the tool should inform the user / offer the choice between
              - generating the files AND closing the list
              - generating mock files and leaving list open. Mock files should never be used for submission
        """
        try:
            mark_in_db_as_having_al_files_generated(al_code, cms_id)
        except AuthorListException as e:
            if f'Author list for {al_code} is NOT open!' in str(e):
                log.warning(f'Post-close AL files generation for {al_code}.')
            else:
                raise e
        '''
        Getting this far should indicate there were no duplicates and such - so we can now write to the ALFileset table.
        The superclass should be able to populate the DB as necessary, we just need to nudge it in the right direction:
        - PUT if we have an ID passed as URL path argument (updating resource)
        - POST otherwise
        '''
        if paper_code:
            return super().put_by_identity(paper_code)
        return super().post()

    @classmethod
    def customize_mapper_before_persisting(cls: Type[AbstractCRUDUnit], target: ALFileset):
        target.last_update = datetime.utcnow().date()
