
import flask_login
from icms_orm.common.common_schema_views import PersonStatusView, PersonInstCodeView
from icms_orm.common import PersonStatus
from icmsutils.exceptions import IcmsException
from blueprints.api_restplus.exceptions import AccessViolationException
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory
from icms_orm.cmspeople.people import MoData, Person, PeopleFlagsAssociation as FlagsAssoc
from icms_orm.common import EmailMessage
from icms_orm.toolkit import AuthorshipApplicationCheck as AAC
from sqlalchemy import or_, and_
from flask_login import current_user
from icms_orm.cmspeople.institutes import Institute
from datetime import date, timedelta
from icmsutils.businesslogic import authorship
import sqlalchemy as sa


class PendingAuthorsApiUnit(BaseApiUnit):

    _model = ModelFactory.make_model("Pending Authors Info", {
        Person.cmsId.key: ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        }),
        Person.lastName.key: ModelFactory.String,
        Person.firstName.key: ModelFactory.String,
        Person.instCode.key: ModelFactory.String,
        'activityName': ModelFactory.String,
        'moFullfilled': ModelFactory.Boolean,
        Person.isAuthorBlock.key: ModelFactory.Boolean
    })

    @classmethod
    def get_ssn(cls):
        return AAC.session()

    @classmethod
    def get_pending_authors(cls):
        ssn = cls.get_ssn()

        q = ssn.query(
            AAC.id.label('aac_id'),
            sa.func.rank().over(order_by=[AAC.cmsId, sa.desc(
                AAC.runNumber)], partition_by=[AAC.cmsId]).label('rnk')
        ).join(PersonStatusView, AAC.cmsId == PersonStatusView.cms_id).\
            filter(AAC.passed == True).\
            filter(PersonStatusView.author_block == True).\
            filter(AAC.datetime > (date.today() - timedelta(days=28)))

        if not current_user.is_admin():
            inst_codes = flask_login.current_user.get_represented_institutes_codes()
            q = q.join(PersonInstCodeView, AAC.cmsId ==
                       PersonInstCodeView.cms_id)
            q = q.filter(PersonInstCodeView.inst_code.in_(inst_codes))
        q = q.subquery()

        q = ssn.query(AAC).join(q, AAC.id == q.c.aac_id).filter(q.c.rnk == 1)

        pending = q.all()
        cms_ids = {p.cmsId for p in pending}

        year_str = str(date.today().year)
        people_data = {d[0].cmsId: d for d in ssn.query(Person, MoData).join(
            MoData, Person.cmsId == MoData.cmsId).filter(Person.cmsId.in_(cms_ids)).all()}

        results = []
        for aac in pending:
            p = people_data[aac.cmsId][0]
            mo = people_data[aac.cmsId][1]

            mo_fulfilled = (p.activity is not None and p.activity.name.lower() == 'doctoral student') \
                or getattr(mo, MoData.mo2017.key.replace('2017', year_str)) == 'YES'

            results.append({
                Person.cmsId.key: aac.cmsId,
                Person.lastName.key: p.lastName,
                Person.firstName.key: p.firstName,
                Person.instCode.key: aac.instCode,
                'activityName': p.activity.name,
                'moFullfilled': mo_fulfilled,
                Person.isAuthorBlock.key: p.isAuthorBlock
            })

        return results


class EditPendingAuthorApiCall(PendingAuthorsApiUnit):
    _model = ModelFactory.make_model("Authorship status edit", {
        Person.cmsId.key: ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        }),
        'action': ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
            ModelFactory.ENUM: ['admit', 'freeze']
        }),
    })

    @classmethod
    def edit_pending_author(cls):
        ssn = cls.get_ssn()
        payload = cls.get_payload()
        person: Person
        person = ssn.query(Person).filter(
            Person.cmsId == payload['cmsId']).one()
        if not (current_user.is_admin() or person.instCode in flask_login.current_user.get_represented_institutes_codes()):
            raise AccessViolationException('You lack the privileges necessary to manage members of {wrong_inst_code} institute. '
                                           'User {user} is a member of {wrong_inst_code}.'.format(
                                               wrong_inst_code=person.instCode, user='%s, %s' % (person.lastName, person.firstName)))

        aac = ssn.query(AAC).filter(AAC.cmsId == person.cmsId).order_by(
            sa.desc(AAC.runNumber)).first()

        if aac is None or aac.passed is not True:
            raise IcmsException(
                f'{person.lastName} {person.firstName} does not seem to be a successful applicant!')
        if not person.isAuthorBlock:
            raise IcmsException(
                f'{person.firstName} {person.lastName} is already unblocked (therefore no longer a pending author)')

        if payload['action'] == 'admit':
            authorship.cbi_set_author_yes(db_session=ssn, author_to_be=person,
                                          cbi=current_user.person)
        else:
            authorship.cbi_set_author_no(db_session=ssn, author_to_be_not=person,
                                         cbi=current_user.person)
        # Either way (admit/delay) the author block should be lifted now
        present_status: PersonStatus
        present_status = ssn.query(PersonStatus).filter(PersonStatus.cms_id ==
                                                        person.cmsId).filter(PersonStatus.end_date == None).one()
        new_status: PersonStatus
        new_status = PersonStatus.from_ia_dict(present_status.to_ia_dict())
        new_status.id = None
        new_status.start_date = present_status.end_date = date.today()
        new_status.author_block = False
        ssn.add(present_status)
        ssn.add(new_status)
        ssn.commit()

        return payload
