import datetime
from typing import Optional

import flask
from flask_login import current_user
from sqlalchemy import and_, or_

from icmsutils.businesslogic import flags
from icms_orm.cmspeople.people import MemberActivity, MoData
from icms_orm.cmspeople.people import PeopleFlagsAssociation as FlagsAssoc
from icms_orm.cmspeople.people import Person

from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory
from blueprints.api_restplus.exceptions import AccessViolationException
from util import constants


class MemberAuthorApiUnit(BaseApiUnit):

    _model = ModelFactory.make_model("Member Authors Info", {
        Person.cmsId.key: ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True
        }),
        Person.lastName.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.READONLY: True
        }),
        Person.firstName.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.READONLY: True
        }),
        Person.instCode.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.READONLY: True
        }),
        'phdMoCurrentYear': ModelFactory.field_proxy(ModelFactory.Boolean, {
            ModelFactory.READONLY: True
        }),
        'moCurrentYear': ModelFactory.field_proxy(ModelFactory.Boolean, {
            ModelFactory.READONLY: True
        }),
        'freeMoCurrentYear': ModelFactory.field_proxy(ModelFactory.Boolean, {
            ModelFactory.READONLY: True
        }),

        MemberActivity.name.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.READONLY: True
        }),
        Person.status.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.READONLY: True
        }),
        Person.isAuthor.key: ModelFactory.field_proxy(ModelFactory.Boolean, {
            ModelFactory.READONLY: True
        }),
        FlagsAssoc.flagId.key: ModelFactory.NullableString
    })

    @classmethod
    def current_year(cls):
        return str(datetime.datetime.now().year)

    @classmethod
    def mo_col(cls):
        return getattr(MoData, 'mo' + cls.current_year())

    @classmethod
    def phd_mo_col(cls):
        return getattr(MoData, 'phdMo' + cls.current_year())

    @classmethod
    def free_mo_col(cls):
        return getattr(MoData, 'freeMo' + cls.current_year())

    @classmethod
    def do_get_member_activity(cls, cms_id=None):
        ssn = flask.current_app.db.session

        cols = [
            Person.cmsId,
            Person.lastName,
            Person.firstName,
            Person.instCode,
            cls.mo_col(),
            cls.phd_mo_col(),
            cls.free_mo_col(),
            MemberActivity.name,
            Person.status,
            Person.isAuthor,
            FlagsAssoc.flagId
        ]

        q = ssn.query(*cols).outerjoin(FlagsAssoc, and_(FlagsAssoc.cmsId == Person.cmsId, FlagsAssoc.flagId == flags.Flag.MISC_AUTHORNO)) \
            .join(MoData, Person.cmsId == MoData.cmsId).join(MemberActivity, MemberActivity.id == Person.activityId)

        if cms_id is not None:
            q = q.filter(Person.cmsId == cms_id)
        else:
            q = (
                q.filter(Person.isAuthorBlock == False)
                .filter(Person.status.ilike('CMS%'))
                .filter(or_(
                    MemberActivity.name.ilike('Doctoral%'),
                    cls.phd_mo_col().ilike('yes'),
                    cls.free_mo_col().ilike('yes'),
                ))
            )

        # Admins can see users of every institute
        if not current_user.is_admin():
            inst_codes = current_user.get_represented_institutes_codes()
            q = q.filter(Person.instCode.in_(inst_codes))

        return cls.rows_to_list_of_dicts(q.all(), cols)

    @classmethod
    def row_to_dict(cls, row, cols_list):
        def true_if_yes(string: Optional[str]):
            return (string and string.lower() == 'yes')

        dict_row = super().row_to_dict(row, cols_list)
        dict_row['moCurrentYear'] = true_if_yes(dict_row[cls.mo_col().key])
        dict_row['phdMoCurrentYear'] = true_if_yes(dict_row[cls.phd_mo_col().key])
        dict_row['freeMoCurrentYear'] = true_if_yes(dict_row[cls.free_mo_col().key])
        return dict_row

    @classmethod
    def get_member_authors(cls):
        return cls.do_get_member_activity()

class EditMemberAuthorApiCall(MemberAuthorApiUnit):

    @classmethod
    def edit_member_author(cls):
        payload = cls.get_payload()

        person: Person
        person = Person.session().query(Person).filter(
            Person.cmsId == payload['cmsId']).one()

        if not (current_user.is_admin() or person.instCode in current_user.get_represented_institutes_codes()):
            raise AccessViolationException('You lack privileges necessary to modify the status of %s %s (CMS id %d). Operation aborted.' % (
                person.firstName, person.lastName, person.cmsId), constants.FLASK_FLASH_DANGER)

        if payload["flagId"] is None:
            flags.unset_flag(
                person=person, flag_code=flags.Flag.AUTHOR_NO, actor=current_user.person)
        else:
            flags.set_flag(
                person=person, flag_code=flags.Flag.AUTHOR_NO, actor=current_user.person)

        return cls.do_get_member_activity(payload['cmsId'])
