import dataclasses
from flask.globals import current_app
from icms_orm.common.common_schema_tenure_tables import (
    OrgUnit,
    OrgUnitType,
    Tenure,
    Position,
)
from blueprints.api_restplus.api_units.basics.model_field_types import Integer
from blueprints.api_restplus.api_units.basics.builders import ModelFieldDefinition
from blueprints.api_restplus.exceptions import BadRequestException
from datetime import date, timedelta, datetime
from typing import Any, Dict, List, Union
from sqlalchemy.orm.attributes import InstrumentedAttribute
from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ParserBuilder,
    ModelFactory,
)
from icms_orm.common import PersonStatusValues as PSValues
from icms_orm.common import (
    Institute,
    Person,
    Country,
    PersonStatus,
    Region,
    Affiliation,
)
import sqlalchemy as sa
from sqlalchemy.orm.query import Query
from sqlalchemy import extract, or_, and_


@dataclasses.dataclass
class StatsDTO:
    stats_groupings: list
    labels: dict
    year: int
    count: int


class MemberCountApiCall(BaseApiUnit):
    FIELD_COUNT = (
        ModelFieldDefinition.builder(Integer)
        .column(sa.func.count(sa.distinct(Person.cms_id)).label("count"))
        .name("count")
        .readonly()
        .build()
    )

    # mapping every cms_id to a management level (simplistic query as proof of concept)
    mgmt_sq = (
        Query(
            [
                Tenure.cms_id.label("cms_id"),
                sa.func.min(
                    sa.case(
                        [
                            (OrgUnitType.level <= 1, 1),
                            (OrgUnitType.level == 2, 2),
                            (OrgUnitType.level == 3, 3),
                        ],
                        else_=None,
                    )
                ).label("mgmt_level"),
            ]
        )
        .join(Position, Tenure.position_id == Position.id)
        .join(OrgUnit, OrgUnit.id == Tenure.unit_id)
        .join(OrgUnitType, OrgUnitType.id == Position.unit_type_id)
        .filter(
            sa.between(
                date.today(),
                Tenure.start_date,
                sa.func.coalesce(Tenure.end_date, date.today()),
            )
        )
        .filter(Position.level <= 1)
        .group_by(Tenure.cms_id)
        .subquery()
    )

    FIELD_MGMT_LEVEL = (
        ModelFieldDefinition.builder(Integer)
        .column(mgmt_sq.c.mgmt_level)
        .name("mgmt_level")
        .build()
    )

    inst_country_sq = (
        Query(
            [
                Country.code.label("code"),
                Country.name.label("name"),
                Country.region_code.label("region_code"),
            ]
        )
        .union(
            Query(
                [
                    sa.sql.expression.literal_column("'CERN'"),
                    sa.sql.expression.literal_column("'CERN'"),
                    sa.sql.expression.literal_column("'CERN'"),
                ]
            )
        )
        .subquery()
    )

    _age_bucket_width = 7
    _age_cap = 99
    _privacy_threshold = -1

    # the age buckets below need the zip workaround for how python3 limits the access to enclosing scope access within list comprehensions
    _grouping_cols = {
        Person.gender.key: Person.gender,
        Person.nationality.key: Country.name,
        "inst_country": inst_country_sq.c.name,
        "inst_code": Institute.code,
        "age": sa.case(
            [
                (Person.date_of_birth == None, f"no information"),
            ]
            + [
                (
                    Person.date_of_birth
                    >= date.today()-_c*timedelta(365),
                    f"{_c-_w} - {_c}",
                )
                for _c, _w in zip(
                    range(_age_bucket_width, _age_cap + 1, _age_bucket_width),
                    [_age_bucket_width] * (_age_cap // _age_bucket_width),
                )
            ],
            else_=f"{( _age_cap // _age_bucket_width) * _age_bucket_width } or more ",
        ).label("age"),
        FIELD_MGMT_LEVEL.name: FIELD_MGMT_LEVEL.column,
        "activity": PersonStatus.activity,
        "region": Region.name,
    }

    _args = (
        ParserBuilder()
        .add_appendable_argument(
            name="grouping_column",
            required=False,
            type=ParserBuilder.STRING,
            choices=list(_grouping_cols.keys()),
        )
        .add_argument(
            name=PersonStatus.is_author.key, type=ParserBuilder.BOOLEAN, required=False
        )
        .add_appendable_argument(
            name=PersonStatus.status.key,
            type=ParserBuilder.STRING,
            required=False,
            choices=PSValues.values(),
        )
        .add_argument(name="from_year", type=ParserBuilder.INTEGER, required=False)
        .add_argument(name="to_year", type=ParserBuilder.INTEGER, required=False)
        .parser
    )

    args = None

    @classmethod
    def set_params(cls):
        """
        Needs a working application context to set the variables. Can be modified to grab the values from incoming request
        """
        cls._age_bucket_width = current_app.config.get("STATS_AGE_BUCKET_WIDTH", 7)
        cls._age_cap = current_app.config.get("STATS_AGE_BUCKETING_CAP", 99)
        cls._privacy_threshold = current_app.config.get("STATS_PRIVACY_THRESHOLD", -1)

    @classmethod
    def get_stats(cls):
        cls.set_params()
        ssn = PersonStatus.session()
        cls.args = cls.parse_args()
        grouping_cols = [
            cls._grouping_cols.get(_label).label(_label)
            for _label in cls.args.get("grouping_column") or []
        ]
        current_year = int(date.today().year)
        from_year = cls.args.get("from_year")
        if not from_year:
            from_year = 2015
        to_year = cls.args.get("to_year")
        if not to_year:
            to_year = current_year
        cls.years_validity_check(from_year, to_year)

        _cols = [cls.FIELD_COUNT.column] + grouping_cols
        result = []
        for year in range(from_year, to_year + 1):
            if year == current_year:
                end_affiliation_threshold = date.today()
            else:
                end_affiliation_threshold = date(year, 12, 31)
            q = (
                ssn.query(*_cols)
                .join(Affiliation, Person.cms_id == Affiliation.cms_id)
                .join(PersonStatus, Person.cms_id == PersonStatus.cms_id)
                .join(Country, Person.nationality == Country.code, isouter=True)
                .join(Institute, Institute.code == Affiliation.inst_code)
                .join(
                    cls.inst_country_sq,
                    sa.case(
                        [(Institute.code != "CERN", Institute.country_code)],
                        else_=Institute.code,
                    )
                    == cls.inst_country_sq.c.code,
                    isouter=True,
                )
                .join(
                    cls.mgmt_sq,
                    cls.mgmt_sq.c.cms_id == PersonStatus.cms_id,
                    isouter=True,
                )
                .join(
                    Region,
                    Region.code == cls.inst_country_sq.c.region_code,
                    isouter=True,
                )
                .filter( PersonStatus.start_date <= end_affiliation_threshold,
                         or_(end_affiliation_threshold <= PersonStatus.end_date, PersonStatus.end_date.is_(None))
                )
                .filter( Affiliation.start_date <= end_affiliation_threshold,
                         or_( end_affiliation_threshold <= Affiliation.end_date, Affiliation.end_date.is_(None)),
                )
                .group_by(*grouping_cols)
            )
            q = cls.attach_query_filters(
                q,
                params_map=cls.parse_args(),
                rules=[PersonStatus.is_author, PersonStatus.status],
            )
            result.extend(cls.extract_data(q.all(), cols_list=_cols, year=year))
        return cls.reduce_data(from_year, to_year, result)

    @classmethod
    def years_validity_check(cls, from_year, to_year):
        current_year = int(date.today().year)
        if from_year < 2015 or from_year > current_year:
            raise BadRequestException("Year should be in range 2015 till current year.")
        if to_year < 2015 or to_year > current_year:
            raise BadRequestException("Year should be in range 2015 till current year.")
        if from_year > to_year:
            raise BadRequestException(
                "from_year should be less than or equal to to_year."
            )

    @classmethod
    def extract_data(
        cls, rows: List[List[Any]], cols_list: List[InstrumentedAttribute], year: int
    ) -> List[StatsDTO]:
        stats_groupings = cls.args.get("grouping_column") or []
        result = []
        for row in rows:
            row = cls.row_to_dict(row, cols_list)
            count = row.pop("count")
            stats = StatsDTO(
                stats_groupings=stats_groupings, labels=row, year=year, count=count
            )
            result.append(stats)
        return result

    @classmethod
    def reduce_data(cls, from_year, to_year, stats: List[StatsDTO]) -> Dict:
        extraction_date = datetime.now()
        extraction_date = extraction_date.strftime("%Y-%m-%d %H:%M:%S")
        dict_for_stats = {}
        dict_for_stats["extracted_at"] = extraction_date
        dict_for_stats["stats_groupings"] = cls.args.get("grouping_column") or []
        dict_for_stats["stats_entries_by_year"] = []

        for year in range(from_year, to_year + 1):
            dict_for_stats["stats_entries_by_year"].append(
                {"year": year, "entries": []}
            )
        for entry in stats:
            for index in range(len(dict_for_stats["stats_entries_by_year"])):
                entries = dict_for_stats["stats_entries_by_year"][index]["entries"]
                if entry.year == dict_for_stats["stats_entries_by_year"][index]["year"]:
                    entries.append({"labels": entry.labels, "count": entry.count})
                else:
                    entries.append({"labels": entry.labels, "count": 0})

        for index in range(len(dict_for_stats["stats_entries_by_year"])):
            entries = dict_for_stats["stats_entries_by_year"][index]["entries"]
            labels_counts = {}
            cleaned_data = []
            for entry in entries:
                labels = tuple(sorted(entry["labels"].items()))
                count = entry["count"]
                if count != 0:
                    labels_counts[labels] = count
                    cleaned_data.append(entry)
            for entry in entries:
                labels = tuple(sorted(entry["labels"].items()))
                count = entry["count"]
                if labels not in labels_counts:
                    labels_counts[labels] = count
                    cleaned_data.append(entry)
            dict_for_stats["stats_entries_by_year"][index]["entries"] = cleaned_data
        return dict_for_stats
