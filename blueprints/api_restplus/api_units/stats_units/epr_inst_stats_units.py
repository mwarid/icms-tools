from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder

from icms_orm.epr import TimeLineUser as TLU, TimeLineInst as TLI, EprInstitute, AllInstView
from icms_orm.cmspeople import MemberStatusValues as MSV
import sqlalchemy as sa
import logging
from datetime import datetime


class EprInstStatsCall(BaseApiUnit):
    _args = ParserBuilder().\
        add_argument(TLU.year.key, ParserBuilder.INTEGER, required=False).\
        add_argument(EprInstitute.code.key, ParserBuilder.STRING, required=False).\
        parser

    _model = ModelFactory.make_model('Epr Inst Stats Info', {
        EprInstitute.code: ModelFactory.String,
        TLU.year: ModelFactory.Integer,
        'authors': ModelFactory.Float,
        'due_authors': ModelFactory.Float,
        'due_applicants': ModelFactory.Float,
    })

    @classmethod
    def get_stats(cls):
        _cols = [EprInstitute.code, TLU.year,
                 sa.func.sum(TLU.yearFraction * sa.case([(sa.and_(TLU.isAuthor == True, TLU.status.notin_([MSV.PERSON_STATUS_EXMEMBER, MSV.PERSON_STATUS_CMSEXTENDED])), 1)], else_=0)).label('authors'),
                 sa.func.sum(TLU.dueApplicant).label('due_applicants'),
                 sa.func.sum(TLU.dueAuthor).label('due_authors'),
                 ]
        q = TLU.session().query(*_cols).join(TLU, TLU.instCode == EprInstitute.code).\
            group_by(EprInstitute.code, TLU.year)
        q = cls.attach_query_filters(q, cls.parse_args(), rules=[TLU.year, EprInstitute.code])
        q = q.order_by(EprInstitute.code, TLU.year)
        return cls.rows_to_list_of_dicts(q.all(), _cols)



class EprInstsOverviewCall(BaseApiUnit):
    _args = ParserBuilder().\
        add_argument('start_year', ParserBuilder.INTEGER, required=False).\
        add_argument('end_year', ParserBuilder.INTEGER, required=False).\
        add_argument(EprInstitute.code.key, ParserBuilder.STRING, required=False).\
        parser

    _model = ModelFactory.make_model('Epr Insts Summary Info', {
        # 'check': ModelFactory.String ,
        'code': ModelFactory.String ,
        # 'name': ModelFactory.String ,
        # 'Expected': ModelFactory.Float ,
        # 'Pledged': ModelFactory.Float ,
        # 'PledgedFract': ModelFactory.Float ,
        # 'Accepted': ModelFactory.Float ,
        # 'Done': ModelFactory.Float ,
        # 'DoneFract': ModelFactory.Float ,
        # 'Tasks': ModelFactory.List  ,
        'Authors': ModelFactory.Float ,
        'actAuthors': ModelFactory.Float ,
        # 'ShiftsPld': ModelFactory.Float ,
        # 'EPRpledged': ModelFactory.Float ,
        # 'EPRpldFrac': ModelFactory.Float ,
        # 'ShiftsDone': ModelFactory.Float ,
        'EPRaccounted': ModelFactory.Float ,
        'EPRaccFrac': ModelFactory.Float ,
        # 'sumEprDue': ModelFactory.Float ,
        # 'lastUpdate': ModelFactory.String ,
        'cmsStatus': ModelFactory.String ,
        'country': ModelFactory.String ,
        'year': ModelFactory.Integer ,
    })

    @classmethod
    def get_stats(cls):
        args = cls.parse_args()

        start_year = datetime.today().year
        end_year = datetime.today().year

        if 'start_year' in args:
            start_year = args.get('start_year', datetime.today().year)
        if start_year is None:
            start_year = datetime.today().year

        if 'end_year' in args:
            end_year = args.get('end_year', datetime.today().year)
        if end_year is None:
            end_year = datetime.today().year

        if start_year > end_year:
            start_year, end_year = end_year, start_year

        code = None
        if 'code' in args:
            code = args.get('code', None)

        logging.debug('EprInstsOverviewCall:get_stats> start/end_year is {start_year}/{end_year}; code is {code}' )

        q = (AllInstView.session.query(AllInstView.code,
                                        AllInstView.authors,
                                        AllInstView.actAuthors,
                                        AllInstView.eprAccounted,
                                        AllInstView.eprAccFract,
                                        EprInstitute.cmsStatus,
                                        EprInstitute.country,
                                        AllInstView.year)
                            .join(EprInstitute, AllInstView.code == EprInstitute.code)
                            .filter( EprInstitute.cmsStatus.in_( ['Yes', 'Cooperating', 'Associated']))
            )

        if start_year == end_year:
            q = q.filter(AllInstView.year == start_year)
        else:
            q = q.filter(AllInstView.year >= start_year).filter(AllInstView.year <= end_year)

        if code:
            q = q.filter( AllInstView.code == code )

        result = q.all()

        logging.debug( "len of data %s " % len(result) )
        logging.debug( f"==> data: {result[:3]}" )

        # 'id', 'check', 'code', 'name', 'Expected', 'Pledged', 'PledgedFract', 'Accepted', 'Done', 'DoneFract', 'Tasks', 'Authors', 'actAuthors', 'ShiftsPld', 'EPRpledged', 'EPRpldFrac', 'ShiftsDone', 'EPRaccounted', 'EPRaccFrac', 'sumEprDue', 'lastUpdate', 'cmsStatus', 'country']
        _cols = ['code', 'Authors', 'actAuthors', 'EPRaccounted', 'EPRaccFrac', 'cmsStatus', 'country', 'year']

        return cls.rows_to_list_of_dicts(rows=result, cols_list=_cols)

