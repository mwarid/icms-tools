from icms_orm.toolkit import JobQuestionnaire
from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)
from blueprints.api_restplus.exceptions import AccessViolationException
from flask_login import current_user
from blueprints.api_restplus.api_units.basics.common import can_manage_job_nominations


class JobQuestionnaireApiCall(BaseApiUnit):

    _model = ModelFactory.make_model(
        "JobQuestionnaire Info",
        {
            JobQuestionnaire.id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            JobQuestionnaire.title.key: ModelFactory.field_proxy(
                ModelFactory.String, {ModelFactory.REQUIRED: True}
            ),
            JobQuestionnaire.questions.key: ModelFactory.field_proxy(
                ModelFactory.String, {ModelFactory.REQUIRED: True}
            ),
            JobQuestionnaire.date_created.key: ModelFactory.field_proxy(
                ModelFactory.DateTime, {ModelFactory.READONLY: True}
            ),
            JobQuestionnaire.date_updated.key: ModelFactory.field_proxy(
                ModelFactory.DateTime, {ModelFactory.READONLY: True}
            ),
        },
    )

    _args = ParserBuilder().parser

    @classmethod
    def get_all_questionnaires(cls):
        cols = [
            JobQuestionnaire.id,
            JobQuestionnaire.title,
            JobQuestionnaire.questions,
            JobQuestionnaire.date_created,
            JobQuestionnaire.date_updated,
        ]

        data = JobQuestionnaire.session().query(*cols).all()
        return cls.rows_to_list_of_dicts(data, cols)

    @classmethod
    def get_questionnaire(cls, id):
        return (
            JobQuestionnaire.session()
            .query(JobQuestionnaire)
            .filter(JobQuestionnaire.id == id)
            .one()
        )

    _payload_translation_rules = [
        (JobQuestionnaire.id, None, None),
        (JobQuestionnaire.title, None, None),
        (JobQuestionnaire.questions, None, None),
        (JobQuestionnaire.date_created, None, None),
        (JobQuestionnaire.date_updated, None, None),
    ]

    @classmethod
    def create_questionnaire(cls):
        if can_manage_job_nominations(current_user):
            payload = cls.get_payload()
            questionnaire = JobQuestionnaire.from_ia_dict({})
            cls.update_db_object(
                questionnaire, {**payload}, rules=cls._payload_translation_rules
            )
            return questionnaire
        else:
            raise AccessViolationException(
                "You lack the privilege necessary to add Questionnaires"
            )

    @classmethod
    def edit_questionnaire(cls, id):
        if can_manage_job_nominations(current_user):
            payload = cls.get_payload()
            questionnaire = (
                JobQuestionnaire.session()
                .query(JobQuestionnaire)
                .filter(JobQuestionnaire.id == id)
                .one()
            )
            cls.update_db_object(
                questionnaire, payload, rules=cls._payload_translation_rules
            )
            return questionnaire
        else:
            raise AccessViolationException(
                "You lack the privilege necessary to edit Questionnaires"
            )
