from icms_orm.common import Tenure, Position, PositionName, OrgUnit, OrgUnitType, OrgUnitTypeName, Person, Affiliation
from icms_orm.common import InstituteLeader
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from datetime import date, datetime
from blueprints.api_restplus.api_units.tenures_units.tenure_api_unit import TenureApiCall
from flask_login import current_user
import sqlalchemy as sa
from blueprints.api_restplus.exceptions import AccessViolationException


class DesignationApiCall(TenureApiCall):

    # these args will only be used for deletions. for searching those from superclass will do just fine
    _args = ParserBuilder().add_argument(Tenure.id.key, type=ParserBuilder.INTEGER, required=True).parser

    @classmethod
    def _assert_user_can_manage(cls, tenure):
        assert isinstance(tenure, Tenure)
        ssn = Tenure.session()
        _ok = ssn.query(Tenure).filter(sa.and_(
                Tenure.cms_id == current_user.cms_id,
                Tenure.end_date == None,
                sa.or_(Tenure.unit_id == tenure.unit_id, OrgUnit.superior_unit_id == tenure.unit_id),
                Position.level == 1
        )).join(Position, Tenure.position_id == Position.id).join(OrgUnit, Tenure.unit_id == OrgUnit.id).count() > 0

        # todo: once the inst-leaders table is absorbed by tenures, the following query will (break and) be obsolete:
        if not _ok:
            _ok = ssn.query(InstituteLeader).filter(sa.and_(
                InstituteLeader.cms_id == current_user.cms_id,
                InstituteLeader.end_date == None,
                InstituteLeader.is_primary == True,
                OrgUnitType.name == OrgUnitTypeName.INSTITUTE,
                OrgUnit.id == tenure.unit_id
            )).\
            join(OrgUnit, OrgUnit.domain == InstituteLeader.inst_code).\
            join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id).count() > 0

        if not _ok:
            raise AccessViolationException()
        return _ok

    @classmethod
    def post_tenure(cls):
        """
        Designations are not expected to be passed here with IDs. Why would they?
          - maybe: for terminations (implement DELETE instead)
          - maybe: for re-assignments (cancel existing designation first)
          - no: we don't want to deal with all the logic for designations' edition
        """

        tenure = Tenure.from_ia_dict({})
        cls.update_db_object(tenure, cls.get_payload(), rules=cls._payload_translation_rules, defer_commit=True)
        # Allow people from the very top of a given unit to designate for lower positions.

        # For the requesting user get all tenures for the same unit (CHECK FOR TOP-LEVEL FUNCTIONS)
        ssn = Tenure.session()
        assert isinstance(tenure, Tenure)

        # check if requesting user is the boss of corresponding org unit (or the boss of that unit's superior unit)
        cls._assert_user_can_manage(tenure)

        # all checks cleared it seems, passing it back to the superclass to perform the insertion (more work, less code)
        return TenureApiCall.post_tenure()

    @classmethod
    def terminate_tenure(cls):
        args = cls.parse_args()
        ssn = Tenure.session()
        tenure = ssn.query(Tenure).filter(Tenure.id == args.get(Tenure.id.key)).one()
        cls._assert_user_can_manage(tenure)
        tenure.set(Tenure.end_date, date.today())
        ssn.add(tenure)
        ssn.commit()
        return
