from icms_orm.toolkit import JobQuestionnaire, JobOpening, JobOpenPosition
from sqlalchemy.orm import joinedload
from icms_orm.common import Position, OrgUnit, OrgUnitType
from icms_orm.common import ExOfficioMandate as EOM
from blueprints.api_restplus.api_units.basics import (
    BaseApiUnit,
    ModelFactory,
    ParserBuilder,
)
from datetime import date, datetime
import sqlalchemy as sa
from flask_login import current_user
from blueprints.api_restplus.exceptions import (
    AccessViolationException,
    NotFoundException,
    BadRequestException,
)
from blueprints.api_restplus.api_units.basics.common import can_manage_job_nominations
from util import constants as const


class JobOpeningsApiCall(BaseApiUnit):
    _ARG_NAME_START_DATE = "start_date"
    _ARG_NAME_END_DATE = "end_date"
    _ARG_NAME_MIN_DEADLINE = "min_deadline"
    _ARG_NAME_MAX_DEADLINE = "max_deadline"

    _model = ModelFactory.make_model(
        "JobOpening Info",
        {
            JobOpening.id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            "jop_id": ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            JobOpening.title.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {ModelFactory.REQUIRED: True, ModelFactory.EXAMPLE: "Spokesperson"},
            ),
            JobOpening.description.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                    ModelFactory.EXAMPLE: "Job as Spokesperson - not as easy as it looks ...",
                },
            ),
            JobOpening.requirements.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: True,
                    ModelFactory.EXAMPLE: "Excellent management and communication skills",
                },
            ),
            JobOpenPosition.start_date.key: ModelFactory.field_proxy(
                ModelFactory.Date,
                {ModelFactory.EXAMPLE: date(2021, 9, 1).isoformat()},
            ),
            JobOpenPosition.end_date.key: ModelFactory.field_proxy(
                ModelFactory.Date, {ModelFactory.EXAMPLE: date(2023, 8, 31).isoformat()}
            ),
            JobOpenPosition.nominations_deadline.key: ModelFactory.field_proxy(
                ModelFactory.Date, {ModelFactory.EXAMPLE: date(2021, 7, 9).isoformat()}
            ),
            JobOpening.questionnaire_id.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                },
            ),
            "questionnaire_title": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            Position.name.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.EXAMPLE: 42,
                },
            ),
            "position": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            "position_id": ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.EXAMPLE: 42,
                },
            ),
            "job_unit_id": ModelFactory.field_proxy(
                ModelFactory.Integer,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.EXAMPLE: 42,
                },
            ),
            "job_open_position_status": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            "position_level": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            "unit_domain": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            "unit_type": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            "unit_level": ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.READONLY: True,
                },
            ),
            JobOpening.status.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {ModelFactory.REQUIRED: True, ModelFactory.EXAMPLE: "created"},
            ),
            JobOpening.comment.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.EXAMPLE: "Spokesperson response",
                },
            ),
            JobOpening.days_for_nominee_response.key: ModelFactory.field_proxy(
                ModelFactory.Integer,
                {ModelFactory.REQUIRED: False, ModelFactory.EXAMPLE: "2 weeks"},
            ),
            JobOpening.nominee_no_reply.key: ModelFactory.field_proxy(
                ModelFactory.Boolean,
                {ModelFactory.REQUIRED: False, ModelFactory.EXAMPLE: "Rejected or Ok"},
            ),
            JobOpening.send_mail_to_nominee.key: ModelFactory.field_proxy(
                ModelFactory.Boolean,
                {ModelFactory.REQUIRED: False, ModelFactory.EXAMPLE: "Mail Sent"},
            ),
            JobOpening.questionnaire_uri.key: ModelFactory.field_proxy(
                ModelFactory.String,
                {
                    ModelFactory.REQUIRED: False,
                    ModelFactory.EXAMPLE: "link to questionnaire",
                },
            ),
            JobOpening.last_modified.key: ModelFactory.field_proxy(
                ModelFactory.Date, {ModelFactory.EXAMPLE: date(2017, 1, 1).isoformat()}
            ),
        },
    )

    _args = (
        ParserBuilder()
        .add_appendable_argument("id", type=ParserBuilder.INTEGER, required=False)
        .add_appendable_argument(
            "title", type=ParserBuilder.STRING, required=False, default=None
        )
        .add_appendable_argument(
            "description", type=ParserBuilder.STRING, required=False, default=None
        )
        .add_appendable_argument(
            "requirements", type=ParserBuilder.STRING, required=False, default=None
        )
        .add_argument(
            _ARG_NAME_START_DATE,
            type=ParserBuilder.INTEGER,
            required=False,
            default=None,
        )
        .add_argument(
            _ARG_NAME_END_DATE, type=ParserBuilder.INTEGER, required=False, default=None
        )
        .add_argument(
            _ARG_NAME_MIN_DEADLINE,
            type=ParserBuilder.DATE,
            required=False,
            default=None,
        )
        .add_argument(
            _ARG_NAME_MAX_DEADLINE,
            type=ParserBuilder.DATE,
            required=False,
            default=None,
        )
        .add_appendable_argument(
            "status", type=ParserBuilder.STRING, required=False, default=None
        )
        .parser
    )

    _payload_translation_rules = [
        (JobOpening.title, None, None),
        (JobOpening.description, None, None),
        (JobOpening.requirements, None, None),
        (JobOpenPosition.start_date, None, None),
        (JobOpenPosition.end_date, None, None),
        (JobOpenPosition.nominations_deadline, None, None),
        (JobOpening.status, None, None),
        (JobOpening.comment, None, None),
        (JobOpening.days_for_nominee_response, None, None),
        (JobOpening.nominee_no_reply, None, None),
        (JobOpening.send_mail_to_nominee, None, None),
        (JobOpening.questionnaire_id, None, None),
        (JobOpening.questionnaire_uri, None, None),
    ]

    @classmethod
    def _do_get_job_openings(cls, args_dict):
        """ """
        ssn = JobOpening.session()

        # These are the columns we will want to select
        _cols = [
            JobOpening.id,
            JobOpenPosition.id.label("jop_id"),
            JobOpening.title,
            JobOpening.description,
            JobOpening.requirements,
            JobOpenPosition.position_id,
            JobOpenPosition.job_unit_id,
            JobOpening.questionnaire_id,
            Position.name.label("position"),
            Position.level.label("position_level"),
            JobOpenPosition.status.label("job_open_position_status"),
            OrgUnit.domain.label("unit_domain"),
            OrgUnitType.name.label("unit_type"),
            OrgUnitType.level.label("unit_level"),
            JobOpenPosition.start_date,
            JobOpenPosition.end_date,
            JobOpenPosition.nominations_deadline,
            JobOpening.comment,
            JobOpening.days_for_nominee_response,
            JobOpening.nominee_no_reply,
            JobOpening.send_mail_to_nominee,
            JobOpening.status,
            JobOpening.questionnaire_uri,
            JobOpening.last_modified,
        ]

        q = (
            ssn.query(*_cols)
            .join(JobOpenPosition, JobOpening.id == JobOpenPosition.job_opening_id)
            .join(Position, JobOpenPosition.position_id == Position.id)
            .join(OrgUnit, JobOpenPosition.job_unit_id == OrgUnit.id)
            .join(OrgUnitType, OrgUnit.type_id == OrgUnitType.id)
            .group_by(
                JobOpening.id,
                JobOpenPosition.id,
                JobOpening.title,
                OrgUnit.id,
                OrgUnitType.id,
                OrgUnitType.name,
                JobOpenPosition.position_id,
                JobOpenPosition.status,
                JobOpenPosition.start_date,
                JobOpenPosition.end_date,
                JobOpenPosition.nominations_deadline,
                JobOpenPosition.job_unit_id,
                Position.name,
                Position.level,
                OrgUnit.domain,
                OrgUnitType.name,
                OrgUnitType.level,
            )
            .order_by(JobOpening.id)
        )

        min_deadline = args_dict.get("min_deadline")
        if min_deadline is not None:
            q = q.filter(JobOpenPosition.nominations_deadline >= min_deadline)
        max_deadline = args_dict.get("max_deadline")
        if max_deadline is not None:
            q = q.filter(JobOpening.nominations_deadline <= max_deadline)

        start_date = args_dict.get("start_date")
        if start_date is not None:
            q = q.filter(JobOpenPosition.nominations_deadline > start_date)
        end_date = args_dict.get("end_date")
        if end_date is not None:
            q = q.filter(JobOpenPosition.nominations_deadline <= end_date)

        selected_status = args_dict.get("status")
        if selected_status is not None:
            q = q.filter(JobOpening.status.in_(selected_status))

        _query_filtering_rules = [
            (JobOpenPosition.nominations_deadline, None, None),
            (JobOpening.id, None, None),
            (JobOpening.title, None, cls.ProcessorFunctions.wildcard_match),
            (JobOpening.description, None, cls.ProcessorFunctions.wildcard_match),
            (JobOpening.requirements, None, cls.ProcessorFunctions.wildcard_match),
            (JobOpenPosition.position_id, None, None),
            (JobOpenPosition.job_unit_id, None, None),
        ]

        q = cls.attach_query_filters(
            query=q, params_map=args_dict, rules=_query_filtering_rules
        )
        return cls.rows_to_list_of_dicts(q.all(), _cols)

    @classmethod
    def get_job_openings(cls):
        return cls._do_get_job_openings(cls.parse_args())


class JobOpeningApiCall(JobOpeningsApiCall):
    _args = (
        ParserBuilder()
        .add_argument(JobOpening.id.key, ParserBuilder.INTEGER, True)
        .parser
    )

    @classmethod
    def get_job_opening(cls):
        return cls._do_get_job_openings(cls.parse_args())

    @classmethod
    def create_job_opening(cls):
        # if not can_manage_job_nominations(current_user):
        #     raise AccessViolationException("You lack the privilege necessary to access")
        payload = cls.get_payload()
        ssn = JobOpening.session()
        _sent_id = payload.get(JobOpening.id.key)

        if (
            datetime.strptime(payload["start_date"], "%Y-%m-%d").date() < date.today()
            or datetime.strptime(payload["end_date"], "%Y-%m-%d").date() < date.today()
            or datetime.strptime(payload["nominations_deadline"], "%Y-%m-%d").date()
            < date.today()
        ):
            raise BadRequestException(
                "Entered date cannot be in the past for creation of Job Opening"
            )

        if (
            datetime.strptime(payload["start_date"], "%Y-%m-%d").date()
            == datetime.strptime(payload["end_date"], "%Y-%m-%d").date()
        ):
            raise BadRequestException(
                "Entered startdate and enddate cannot be same for the creation of Job Opening"
            )

        if (
            datetime.strptime(payload["start_date"], "%Y-%m-%d").date()
            > datetime.strptime(payload["end_date"], "%Y-%m-%d").date()
        ):
            raise BadRequestException(
                "Entered startdate should be less than enddate for the creation of Job Opening"
            )

        if (
            datetime.strptime(payload["nominations_deadline"], "%Y-%m-%d").date()
            > datetime.strptime(payload["end_date"], "%Y-%m-%d").date()
        ):
            raise BadRequestException(
                "Entered nominationdeadline should be less than or equalto enddate for the creation of Job Opening"
            )

        job_opening = (
            _sent_id
            and ssn.query(JobOpening).filter(JobOpening.id == _sent_id).one()
            or JobOpening.from_ia_dict({})
        )
        job_open_position = JobOpenPosition.from_ia_dict({})
        cls.update_db_object(job_opening, payload, rules=cls._payload_translation_rules)
        ssn = job_open_position.session()
        for open_position in payload["positions"]:
            job_open_position_record = JobOpenPosition.from_ia_dict({})
            job_open_position_record.position_id = open_position["position_id"]
            job_open_position_record.job_unit_id = open_position["job_unit_id"]
            job_open_position_record.job_opening_id = job_opening.id
            job_open_position_record.status = const.JOB_STATUS_ACTIVE
            job_open_position_record.start_date = payload["start_date"]
            job_open_position_record.end_date = payload["end_date"]
            job_open_position_record.nominations_deadline = payload[
                "nominations_deadline"
            ]
            ssn.add(job_open_position_record)
        ssn.commit()
        return cls._do_get_job_openings(
            {JobOpening.id.key: job_opening.get(JobOpening.id)}
        )

    @classmethod
    def edit_job_opening(cls, job_opening_id):
        payload = cls.get_payload()

        if not can_manage_job_nominations(current_user):
            raise AccessViolationException("You lack the privilege necessary to access")
        ssn = JobOpening.session()
        job_opening = (
            ssn.query(JobOpening).filter(JobOpening.id == job_opening_id).first()
        )
        if job_opening is None:
            raise NotFoundException("Job Opening not found in DB")
        if job_opening.status == const.JOB_STATUS_CLOSED:
            raise BadRequestException("The requested Job Opening is closed")
        cls.update_db_object(job_opening, payload, rules=cls._payload_translation_rules)

        jop_query = ssn.query(JobOpenPosition).filter(
            JobOpenPosition.job_opening_id == job_opening.get(JobOpening.id)
        )

        for open_position in payload["positions"]:
            jop = jop_query.filter(
                JobOpenPosition.position_id == (open_position["position_id"]),
                JobOpenPosition.job_unit_id == (open_position["job_unit_id"]),
            ).first()
            for key, value in open_position.items():
                if hasattr(jop, key):
                    setattr(jop, key, value)

        ssn.commit()
        return cls._do_get_job_openings(
            {JobOpening.id.key: job_opening.get(JobOpening.id)}
        )

    @classmethod
    def delete_job_opening(cls, id):
        payload = cls.get_payload()
        positions = []
        job_unit_ids = []

        if payload.get("positions") is not None:
            positions = [item.get("position") for item in payload.get("positions")]
            job_unit_ids = [
                item.get("job_unit_id") for item in payload.get("positions")
            ]

        if not can_manage_job_nominations(current_user):
            raise AccessViolationException("You lack the privilege necessary to access")

        ssn = JobOpenPosition.session()
        jop_query = ssn.query(JobOpenPosition).filter(
            JobOpenPosition.job_opening_id == id
        )
        jops = jop_query.all()
        message = ""
        if len(positions) > 0:
            message = ",".join(str(i) for i in positions)
            jops = jop_query.filter(
                JobOpenPosition.position_id.in_(positions),
                JobOpenPosition.job_unit_id.in_(job_unit_ids),
            ).all()

        for jop in jops:
            jop.status = const.JOB_STATUS_CLOSED

        ssn.commit()
        total_jop = jop_query.count()
        total_closed_jop = jop_query.filter(
            JobOpenPosition.status == const.JOB_STATUS_CLOSED
        ).count()
        job_opening = ssn.query(JobOpening).filter(JobOpening.id == id).one()

        if total_jop == total_closed_jop:
            ssn = JobOpening.session()

            if job_opening:
                job_opening.set(JobOpening.end_date, date.today())
                job_opening.set(JobOpening.status, const.JOB_STATUS_CLOSED)
                ssn.add(job_opening)
                ssn.commit()
                message = job_opening.title

        return message + "is deleted successfully"
