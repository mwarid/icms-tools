from icms_orm.common import Tenure, Position, PositionName, OrgUnit, OrgUnitType, OrgUnitTypeName, Person, Affiliation
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder


class PositionsApiCall(BaseApiUnit):
    _args = ParserBuilder(). \
        add_appendable_argument(Position.id.key, ParserBuilder.INTEGER, required=False). \
        add_appendable_argument(Position.name.key, ParserBuilder.STRING, required=False). \
        add_appendable_argument('unit_type', ParserBuilder.STRING, required=False,
                     choices=[_v.lower() for _v in OrgUnitTypeName.values()]). \
        add_appendable_argument('unit_id', ParserBuilder.INTEGER, required=False,
                     help='Used to filter matching positions based on specified unit\'s type.'). \
        parser
    _model = ModelFactory.make_model('Positions Info', {
        Position.id.key: ModelFactory.Integer,
        Position.name.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
            ModelFactory.ENUM: PositionName.values()
        }),
        'unit_type': ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.READONLY: True,
            ModelFactory.ENUM: OrgUnitTypeName.values()
        }),
        'unit_type_id': ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True,
        }),
        Position.level.key: ModelFactory.field_proxy(ModelFactory.Integer, {
            ModelFactory.REQUIRED: True,
            ModelFactory.EXAMPLE: 3
        }),
        Position.is_active.key: ModelFactory.field_proxy(ModelFactory.Boolean, {
            ModelFactory.REQUIRED: False,
            ModelFactory.DEFAULT: True
        })
    })

    @classmethod
    def _do_get_positions(cls, args_dict):
        ssn = Position.session()
        _cols = [Position.id, Position.name, Position.level, OrgUnitType.name.label('unit_type'), OrgUnitType.id.label('unit_type_id'), Position.is_active]
        q = ssn.query(*_cols).join(OrgUnitType, OrgUnitType.id == Position.unit_type_id)

        # don't just check for presence (blabla in args_dict) - it'll be there even when mapped to None
        if args_dict.get('unit_id', None) is not None:
            q = q.join(OrgUnit, OrgUnit.type_id == OrgUnitType.id)

        q = cls.attach_query_filters(q, args_dict, [
            (Position.id, None, None),
            (Position.name, None, cls.ProcessorFunctions.wildcard_match),
            # only including the unit type if the unit ID is NOT specified
            (OrgUnit.id, 'unit_id', None),
            (OrgUnitType.name, 'unit_type', cls.ProcessorFunctions.wildcard_match),
        ])
        return cls.rows_to_list_of_dicts(q.all(), cols_list=_cols)

    @classmethod
    def get_positions(cls):
        return cls._do_get_positions(cls.parse_args())

    @classmethod
    def post_position(cls):
        payload = cls.get_payload()
        ssn = Position.session()
        _sent_id = payload.get(Position.id.key, None)
        position = _sent_id and ssn.query(Position).filter(Position.id == _sent_id).one() or Position.from_ia_dict({})

        cls.update_db_object(position, payload,
                             rules=[Position.name, Position.level, Position.unit_type_id, Position.is_active])

        return cls._do_get_positions({Position.id.key: position.get(Position.id)})[0]
