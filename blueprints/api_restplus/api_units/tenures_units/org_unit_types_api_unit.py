from icms_orm.common import Tenure, Position, PositionName, OrgUnit, OrgUnitType, OrgUnitTypeName, Person, Affiliation
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
import sqlalchemy as sa


class OrgUnitTypesApiCall(BaseApiUnit):

    _args = ParserBuilder(). \
        add_argument(OrgUnitType.id.key, type=ParserBuilder.INTEGER, required=False, default=None). \
        add_argument(OrgUnitType.name.key, type=ParserBuilder.STRING, required=False, default=None). \
        add_argument(OrgUnitType.level.key, type=ParserBuilder.INTEGER, required=False, default=None) . \
        parser

    _model = ModelFactory.make_model('Org Unit Type Info', {
        OrgUnitType.id.key: ModelFactory.Integer,
        OrgUnitType.name.key: ModelFactory.field_proxy(ModelFactory.String, {
            ModelFactory.REQUIRED: True,
            ModelFactory.ENUM: [_v.lower() for _v in OrgUnitTypeName.values()]
        }),
        OrgUnitType.level.key: ModelFactory.Integer
    })

    @classmethod
    def get_types(cls):
        kwargs = cls.parse_args()
        ssn = OrgUnitType.session()
        _cols = [OrgUnitType.id, OrgUnitType.name, OrgUnitType.level]
        q = ssn.query(*_cols)
        q = cls.attach_query_filters(q, kwargs, [(_c, None, None) for _c in OrgUnitType.ia_list()])
        return cls.rows_to_list_of_dicts(q.all(), cols_list=_cols)

    @classmethod
    def post_type(cls):
        """
        A placeholder, probably we're not going to implement unconstrained addition of unit types
        (we keep a tight list in the code - for the greater good)
        """
        pass
