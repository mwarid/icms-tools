from blueprints.api_restplus.api_units.requests_api_units.requests_api_unit import RequestsApiUnit
from blueprints.api_restplus.api_units.requests_api_units.person_status_requests_unit import PersonStatusRequestsApiUnit
from blueprints.api_restplus.api_units.requests_api_units.request_steps_api_unit import RequestStepsApiUnit

"""
This API unit package builds on top of the functionality provided in icms-common.
It handles the requests processing through the RequestsService class and conceptually works with requests and steps.
Steps are created upfront, whenever some previous step is executed - they can all be created upfront or only the directly following one.
As such they can be modified (put), which in turn triggers the corresponding request processor to:
- change the request status, if needed
- generate subsequent steps, if needed
- launch the request executor, if the request becomes fully approved (without delayed execution)

The following conventions have been adpoted:
- only admins can write the generic request resource
- by contrast, specific requests shall have their own resources (eg. PersonStatusRequestApiUnit) for posting to (allows better defined models)
- request steps only have the generic API unit, exposing the methods to act upon them (accept / reject)
"""
