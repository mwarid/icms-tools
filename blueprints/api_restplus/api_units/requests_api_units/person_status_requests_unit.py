from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from typing import Any, Dict, Iterable, List, Optional, Type

from icmsutils.businesslogic.requests import RequestsService
from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit
from blueprints.api_restplus.api_units.basics import \
    ModelFieldDefinition as FieldDef
from blueprints.api_restplus.api_units.basics.crud import Deleteless, Getless, Putless
from blueprints.api_restplus.api_units.basics.model_field_types import (
    Integer, String)
from icms_orm.cmspeople.people import MemberStatusValues
from icms_orm.common import Request
from icmsutils import ActorData
from icmsutils.businesslogic.requests import PersonStatusChangeRequestDefinition
from flask_login import current_user


class PersonStatusRequestsApiUnit(AbstractCRUDUnit, Deleteless, Putless):
    """
    Doesn't expose delete or put (if need be entries can be admin-manupulated through the "root" requests unit).
    Approvals / rejections shall be handled through the RequestSteps API unit.

    Permissions need to be very relaxed so as to allow the RequestsService to control them.
    Hypermedia could be customised later on to use RequestsService.
    """

    # Fields from the generic request object, included as read-only
    FIELD_ID = FieldDef.builder(Integer).column(
        Request.id).id().readonly().build()
    FIELD_TYPE = FieldDef.builder(String).column(Request.type).enum(
        pool=RequestsService.get_registered_request_types()).readonly().build()
    FIELD_STATUS = FieldDef.builder(String).column(
        Request.status).readonly().build()
    # Fields from the request's processing data to be pulled into the model representation
    # like: {id: x, status: y, data:{foo: 12, bar: null}} -> {id: x, status: y, foo: 12, bar: null}
    # posting those creates a new request
    FIELD_CMS_ID = FieldDef.builder(Integer).name('cms_id').build()
    FIELD_CREATOR_CMS_ID = FieldDef.builder(
        Integer).name('creator_cms_id').readonly().build()
    FIELD_NEW_STATUS = FieldDef.builder(String).name(
        'new_status').enum(MemberStatusValues.values()).build()

    @classmethod
    def get_model_name(cls) -> str:
        return 'PersonStatusChangeRequest'

    @classmethod
    def get_model_fields_list(cls) -> List[FieldDef]:
        return [cls.FIELD_ID, cls.FIELD_TYPE, cls.FIELD_STATUS, cls.FIELD_CMS_ID, cls.FIELD_CREATOR_CMS_ID, cls.FIELD_NEW_STATUS]

    @classmethod
    def is_model_field_eligible_for_parser(cls, field: FieldDef) -> bool:
        """
        Disables all-but-status query string parameters
        """
        return field.name == cls.FIELD_STATUS.name

    @classmethod
    def find(cls: Type['PersonStatusRequestsApiUnit'], needle: Optional[ResourceFilter] = None) -> List[Dict[str, Any]]:
        """
        Fetches matching requests from RequestsService. Type is imposed, status can be provided.
        """
        types_pool = [PersonStatusChangeRequestDefinition.get_name()]
        statuses_pool = []
        while needle:
            if needle.column == cls.FIELD_STATUS.column:
                statuses_pool.append(needle.value)
            needle = needle.next

        requests: Iterable[Request] = RequestsService.get_matching_requests(
            types_pool=types_pool, statuses_pool=statuses_pool)
        return [cls.db_object_to_model_compliant_dict(r) for r in requests]

    @classmethod
    def get_by_identity(cls, *params):
        """
        Fetches the resource by id, from RequestsService
        """
        return cls.db_object_to_model_compliant_dict(RequestsService.get_request_data(params[0]))

    @classmethod
    def post(cls) -> Dict[str, object]:
        """
        Invokes RequestsService's create_request logic
        """
        actor: ActorData = ActorData(current_user.cms_id)
        payload = cls.get_payload()
        request: Request = RequestsService.create_request(PersonStatusChangeRequestDefinition.get_name(), actor_data=actor, **{
            cls.FIELD_CMS_ID.name: payload[cls.FIELD_CMS_ID.name],
            cls.FIELD_NEW_STATUS.name: payload[cls.FIELD_NEW_STATUS.name],
            cls.FIELD_CREATOR_CMS_ID.name: actor.cms_id
        })
        # Need to translate the mapper's instance into a dict can restx can work with
        return cls.db_object_to_model_compliant_dict(request)

    @classmethod
    def db_object_to_model_compliant_dict(cls, db_object: Request) -> Dict[str, Any]:
        """
        Translates db objects into dicts that restx can handle.
        """
        first_dict = super().db_object_to_model_compliant_dict(db_object)
        if isinstance(db_object.processing_data, dict):
            first_dict.update(db_object.processing_data)
        return first_dict
