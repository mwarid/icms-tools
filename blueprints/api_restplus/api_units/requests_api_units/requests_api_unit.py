from dataclasses import Field
from typing import List
from blueprints.api_restplus.api_units.basics.model_field_types import Date, DateTime, Integer, Raw, String
from blueprints.api_restplus.api_units.basics import AbstractCRUDUnit, ModelFieldDefinition as FieldDef
from icms_orm.common import Request, RequestStep, RequestStatusValues, RequestStepStatusValues
from icmsutils.businesslogic.requests import RequestsService


class RequestsApiUnit(AbstractCRUDUnit):
    """
    The generic CRUD interface to requests table. Only admins are meant to write here (default permissions setting).
    Can be used to retrieve some accompanying information on specific requests, if needed.
    """
    FIELD_ID = FieldDef.builder(Integer).column(Request.id).id().build()
    FIELD_TYPE = FieldDef.builder(String).column(Request.type).enum(
        pool=RequestsService.get_registered_request_types()).build()
    FIELD_STATUS = FieldDef.builder(String).column(Request.status).build()
    FIELD_CREATOR_CMS_ID = FieldDef.builder(
        Integer).column(Request.creator_cms_id).build()
    FIELD_CREATION = FieldDef.builder(
        DateTime).column(Request.creation_date).build()
    FIELD_MODIFICATION = FieldDef.builder(
        DateTime).column(Request.last_modified).build()
    FIELD_DATA = FieldDef.builder(Raw).column(Request.processing_data).build()
    FIELD_EXECUTION = FieldDef.builder(DateTime).column(
        Request.scheduled_execution_date).build()
    FIELD_REMARKS = FieldDef.builder(String).column(Request.remarks).build()

    @classmethod
    def get_model_name(cls) -> str:
        return 'RequestInfo'

    @classmethod
    def get_model_fields_list(cls) -> List[FieldDef]:
        return [cls.FIELD_ID,
                cls.FIELD_TYPE,
                cls.FIELD_STATUS,
                cls.FIELD_CREATOR_CMS_ID,
                cls.FIELD_CREATION,
                cls.FIELD_MODIFICATION,
                cls.FIELD_DATA,
                cls.FIELD_EXECUTION,
                cls.FIELD_REMARKS]
