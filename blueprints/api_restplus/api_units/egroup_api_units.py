
from asyncio.log import logger
from werkzeug.exceptions import Conflict, NotFound, InternalServerError
from flask_login import current_user
import json
import os

from blueprints.api_restplus.api_factory import handle_errors, SingletonApiFactory
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
from blueprints.api_restplus.exceptions import BadRequestException
from util.EgroupHandler import EgroupHandler
from util import constants as const

import logging

# do not logger.info(very long) replies from the SOAP service
ztl = logging.getLogger("zeep.transports")
ztl.setLevel(logging.INFO)

def readOnlyOnStaging( eGroupName ):

    # allow any access on PROD
    if os.environ.get(const.ENV_VAR_NAME_CONFIG) == 'PROD': return False

    # if we're running on staging/-dev, only read-only accesses for cms* eGroups (except "cms-apf*" ones) are allowed    
    if ( (eGroupName.startswith( 'cms' ) and not eGroupName.startswith('cms-apf')) 
        or (eGroupName.startswith('icms-')) ): return True # read-only !

    # allow updates/writes for any other eGroup
    return False

def checkInBlockListDelete( eGroupName ):
    #-toDo: decide if we should go and protect all 'cms-*' (or even "cms*" ?) eGroups ??
    blockListDelete = [ 'apf-test-nodelete',
        'cms-physics', 'cms-web-access', 'cms-physics-access', 'cms-authors', 
        'cms-members', 'cms-users', 'cms-under-35', 'cms-under-40', 
        'cms-icms-secr-ab', 'cms-icms-secr-all', 'cms-icms-secr-cb', 'cms-icms-secr-cbins', 
        'cms-icms-secr-cbvoting', 'cms-icms-secr-eb', 'cms-icms-secr-ebext', 'cms-icms-secr-ecif', 
        'cms-icms-secr-ecsign', 'cms-icms-secr-fb', 'cms-icms-secr-fbeu', 'cms-icms-secr-fbext', 
        'cms-icms-secr-ibe', 'cms-icms-secr-ibt', 'cms-icms-secr-ibtexof', 'cms-icms-secr-ibtinst', 
        'cms-icms-secr-ibtinv', 'cms-icms-secr-ibtvoting', 'cms-icms-secr-irepr', 'cms-icms-secr-lpfa', 
        'cms-icms-secr-mb', 'cms-icms-secr-members', 'cms-icms-secr-misc', 'cms-icms-secr-pm', 
        'cms-icms-secr-referees', 'cms-icms-secr-regrepr', 'cms-icms-secr-sc' 'cms-icms-secr-test', 
        'cms-icms-secr-tl', 
        'cms-icms-int-cae', 'cms-icms-int-cah', 'cms-icms-int-comp', 'cms-icms-int-da', 
        'cms-icms-int-mu', 'cms-icms-int-ph', 'cms-icms-int-sw', 'cms-icms-int-tk', 
        'cms-icms-prj-core', 'cms-icms-prj-daq', 'cms-icms-prj-ec', 'cms-icms-prj-hc', 'cms-icms-prj-mucsc', 
        'cms-icms-prj-mudt', 'cms-icms-prj-murpc', 'cms-icms-prj-off', 'cms-icms-prj-run', 'cms-icms-prj-tech', 
        'cms-icms-prj-tk', 'cms-icms-prj-trg', 'pubcomm-chairs',
    ]

    if eGroupName.replace('@cern.ch', '') in blockListDelete:
        return True
    
    return False

class EGroup(object):
    group_name = 'group_name'
    hr_ids = 'hr_ids'
    egroups = 'egroups'
    accounts = 'accounts'
    emails = 'emails'
    admin_egroup = 'admin_egroup'
    privacy_info = 'privacy_info'
    email_properties = 'email_properties'


def _getGroupName(cls, args_dict):
    if not args_dict['group_name']:
        msg = f'ERROR> missing parameters: "group_name" has to be specified.'
        logger.error(msg)
        raise BadRequestException(msg)
    return args_dict['group_name']

def _getGroupMembers(cls, args_dict):
    args_dict = cls.parse_args()
    logger.info( f'_getGroupMembers> {args_dict}' )
    egName = args_dict['group_name']
    try:
        memList, memberNoAccount, egMemberList, accountList, emailList = EgroupHandler().getGroupMembers( egName )
    except NotFound as e:
        logger.info( { 'error': str(e), 'code': 404 } )
        cls._api.abort( 404, str(e) )
    except InternalServerError as e:
        logger.info( { 'error': e.msg, 'code': e.code } )
        cls._api.abort( e.code, e.msg )
    except Exception as e:
        raise e
    
    hrIds = []
    for x in memList:
        hrId = x.split(';')[1]
        if hrId is None or hrId == 'None': continue
        hrIds.append( int(hrId) )

    logger.info( f'got {len(hrIds)} hrIds' )

    result = { 
        'group_name': egName, 
        'hr_ids': hrIds,
        'egroups': [ x for x in egMemberList ],
        'accounts': [ x for x in accountList ],
        'emails': [ x for x in emailList ],
    }
    # careful, the following can be _very_ long ...
    logging.debug( f'\n returning: {result}' )
    return result
    
def _getMemberListFromPayload(cls, payload):

    if not payload:
        msg = f'ERROR> missing parameters: no payload found in request.'
        logger.error(msg)
        raise BadRequestException(msg)

    egName   = payload.get('group_name', None)
    hrIds    = payload.get('hr_ids', None)
    eGroups  = payload.get('egroups', None)
    accounts = payload.get('accounts', None)
    emails   = payload.get('emails', None)

    if not hrIds and not eGroups and not accounts and not emails:
        msg = f'ERROR> missing parameters: at least one of "hr_ids", "accounts", "emails", or "egroups" has to be specified.'
        logger.error(msg)
        raise BadRequestException(msg)

    # check if we need to convert some values from string
    if hrIds is None: hrIds = []
    elif type(hrIds)==type("") :
        hrIds = [ int(x.strip()) for x in hrIds.split(',')]

    if eGroups is None: eGroups = []
    elif type(eGroups)==type(""):
        eGroups = [ x.strip() for x in eGroups.split(',')]

    if accounts is None: accounts = []
    elif type(accounts)==type(""):
        accounts = [ x.strip() for x in accounts.split(',')]

    if emails is None: emails = []
    elif type(emails)==type(""):
        emails = [ x.strip() for x in emails.split(',')]

    return egName, set(hrIds), set(eGroups), set(accounts), set(emails)


def _convert( result ):
    if not result[0]: 
        return { 'msg': 'invalid result, ignoring', 'code': 500 }
    
    info = result[1]
    emProps = str(info['EmailProperties']).replace('\n','').replace("'", '"').replace('False', '"False"').replace('None', '"None"')
    logger.info( f'\n emailProps: type: {type(emProps)} -- replace: "{emProps}" \n')
    return {
        'group_name': info['Name'],
        'hr_ids': [ x['ID'] for x in info['Members'] if x['Type'] == 'Person' ],
        'egroups': [ x['Name'] for x in info['Members'] if x['Type'] == 'StaticEgroup' ] + [ x['Name'] for x in info['Members'] if x['Type'] == 'DynamicEgroup' ],
        'accounts': [ x['Name'] for x in info['Members'] if x['Type'] == 'Account' ],
        'emails': [ x['Email'] for x in info['Members'] if x['Type'] == 'External' ],
        'admin_egroup': info['AdministratorEgroup'],
        'privacy_info': { 
            'Selfsubscription': info['Selfsubscription'], 
            'Privacy': info['Privacy'],
        },
        'email_properties': json.loads( emProps ),
    }

defaultProperties = {
            'mailProps': { 'MailPostingRestrictions' : { 'PostingRestrictions' : 'OwnerAdminsAndOthers'},
                           'SenderAuthenticationEnabled' : False,
                           'WhoReceivesDeliveryErrors'   :  'GroupOwner',
                           'MaxMailSize'                 : 10,
                           'ArchiveProperties'           : 'DoesNotExist',
                         }, 
            'privacyProps': { 'Privacy' : 'Members',
                            'Selfsubscription' : 'Closed',
                            },
            'admin_egroup': 'cms-web-access-admins',
        }
class EgroupApiCall(BaseApiUnit):
    _model = ModelFactory.make_model( 'EGroup Info', {
        EGroup.group_name: ModelFactory.String(),
        EGroup.hr_ids: ModelFactory.String(),
        EGroup.egroups: ModelFactory.String(),
        EGroup.accounts: ModelFactory.String(),
        EGroup.emails: ModelFactory.String(),
        EGroup.admin_egroup: ModelFactory.String(),
        EGroup.privacy_info: ModelFactory.String(),
        EGroup.email_properties: ModelFactory.String(),
    } )

    # for the start, we try to simplify the work on the Java/JSP/CADI side by simply passing 
    # strings for the arguments and do the parsing here. In the longer term, there should also 
    # be to communicate properly via JSON (e.g. for other services and/or scripts)
    _args = (ParserBuilder()
                .add_argument('group_name', type=ParserBuilder.STRING, required=True, default=None)
                .add_argument('hr_ids', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('egroups', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('accounts', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('emails', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('admin_egroup', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('privacy_info', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('email_properties', type=ParserBuilder.STRING, required=False, default=None)
                .parser)

    _api = SingletonApiFactory.get_api()

    @classmethod
    def getGroupName(cls, args_dict):
        return _getGroupName(cls, args_dict)

    @classmethod
    def getNewMemberList(cls, args_dict):
        return _getGroupMembers(cls, args_dict)

    @classmethod
    def _do_get_egMembers(cls, args_dict):
        return _getGroupMembers(cls, args_dict)


    @classmethod
    def _createEgroupWithMembers(cls, payload):
        
        egName = payload.get('group_name', None)
        if readOnlyOnStaging(egName): 
            msg = f'ERROR: request to create eGroup {egName} DENIED on non-PROD instance.'
            logger.error( msg )
            raise BadRequestException(msg)
        
        logger.info( f'request to create eGroup {egName}:' )
        
        adminEgroupInfo = payload.get( "admin_egroup" )

        mailProps = defaultProperties['mailProps']
        privacyProps = defaultProperties['privacyProps']
        adminEgroup = defaultProperties['admin_egroup']

        if adminEgroupInfo: 
            adminEgroup = adminEgroupInfo

        logger.info( f'\n mailProps: {mailProps}\n')

        try:
            egName_foo, hrIds, eGroups, accounts, emails = _getMemberListFromPayload(cls, payload)
        except BadRequestException as e:
            # ignore case that eGroup was created w/o members:
            if 'missing parameters: at least one of "hr_ids",' not in str(e):
                raise e
            egName_foo = egName # set default in case ...
        finally:
            hrIds, eGroups, accounts, emails = ([], [], [], [])
            if egName != egName_foo:
                msg = f'ERROR: found different eGroup name in payload: found {egName_foo} for {egName} '
                logger.error( f'post_egroup> {msg}' )
                raise InternalServerError(msg)

        try:
            status, msg = EgroupHandler().newGroup(egName, description='testing', mailProps=mailProps, privacy=privacyProps, adminEgroup=adminEgroup)
            logger.info( f'\n - result: {status} - {msg}')
        except Conflict as e:
            raise e
        except Exception as e:
            # logger.info( f'Ignoring the Exception I just caught: str(e)')
            raise e

        if not status:
            logger.error( f'ERROR: {msg}' )
            raise InternalServerError(msg)

        # check if there is any type of member requested in the call, if so, add them:
        if hrIds or eGroups or accounts or emails:
            status, msg = EgroupHandler().addMembersBulk(egName, { 'hr_ids'  : hrIds,
                                                               'egroups' : eGroups,
                                                               'accounts': accounts,
                                                               'emails'  : emails,
                                                            }                                                     )
            logger.info( f'\n++> bulk addition of members returned: {status} - {msg}')
        return egName

    @classmethod
    def get_egroup_info(cls):
        args_dict = cls.parse_args()
        egName = _getGroupName(cls, args_dict )
        try:
            result = EgroupHandler().findGroup( egName )
        except Exception as e:
            logger.info( f'Ignoring the Exception I just caught: {str(e)}')
            raise NotFound( str(e) )
        
        # logger.info( f'\nget_egroup_info> result: {result}')
        return _convert(result)

    @classmethod
    def delete_egroup(cls):
        payload = cls.get_payload()
        
        if not payload:
            msg = f'missing parameters: no payload found in request.'
            logger.error( f'delete_egroup> {msg}' )
            raise BadRequestException(msg)

        egName   = payload.get('group_name', None)
        if readOnlyOnStaging(egName): 
            msg = f'ERROR: request to DELETE eGroup {egName} DENIED on non-PROD instance.'
            logger.error( msg )
            raise BadRequestException(msg)
        
        logger.info( f'\nrequest to DELETE eGroup {egName} \n' )
        
        if checkInBlockListDelete( egName ):
            msg = f'eGroup {egName} found in blocklist for delete, NOT deleting it.'
            logger.error( f'delete_egroup> {msg}' )
            raise BadRequestException(msg)
        
        result = EgroupHandler().deleteGroup( egName )
        logger.info( f'\nDELETING eGroup {egName} returned: {result} \n' )
        
        return { 'status': 'OK', 'message': f'eGroup {egName} successfully deleted.' }

    @classmethod
    def post_egroup(cls):
        payload = cls.get_payload()
        
        if not payload:
            msg = f'ERROR> missing parameters: no payload found in request.'
            logger.error(msg)
            raise BadRequestException(msg)

        # read-only check done in class-method
        egName = cls._createEgroupWithMembers(payload)

        return cls._do_get_egMembers(egName)

class EgroupMembersApiCall(BaseApiUnit):
    _model = ModelFactory.make_model( 'EGroupMember Info', {
        EGroup.group_name: ModelFactory.String(),
        EGroup.hr_ids: ModelFactory.String(),
        EGroup.egroups: ModelFactory.String(),
        EGroup.accounts: ModelFactory.String(),
        EGroup.emails: ModelFactory.String(),
    } )

    # for the start, we try to simplify the work on the Java/JSP/CADI side by simply passing 
    # strings for the arguments and do the parsing here. In the longer term, there should also 
    # be to communicate properly via JSON (e.g. for other services and/or scripts)
    _args = (ParserBuilder()
                .add_argument('group_name', type=ParserBuilder.STRING, required=True, default=None)
                .add_argument('hr_ids', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('egroups', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('accounts', type=ParserBuilder.STRING, required=False, default=None)
                .add_argument('emails', type=ParserBuilder.STRING, required=False, default=None)
                .parser)

    _api = SingletonApiFactory.get_api()

    @classmethod
    def _do_get_egMembers(cls, args_dict):
        return _getGroupMembers(cls, args_dict)

    @classmethod
    def get_egroup_members(cls):
        return cls._do_get_egMembers(cls.parse_args())

    @classmethod
    def post_egroup_members(cls):
        payload = cls.get_payload()
        logger.info( f'\ngot: {payload}')

        egName, hrIds, eGroups, accounts, emails = _getMemberListFromPayload(cls, payload)
        if readOnlyOnStaging(egName): 
            msg = f'ERROR: request to modify members for eGroup {egName} DENIED on non-PROD instance.'
            logger.error( msg )
            raise BadRequestException(msg)

        logger.info( f'request to add members to {egName}:' )
        logger.info( f'\thrIDs   : {hrIds}')
        logger.info( f'\teGroups : {eGroups}')
        logger.info( f'\taccounts: {accounts}')
        logger.info( f'\temails  : {emails}')

        status, msg = EgroupHandler().addMembersBulk(egName, { 'hr_ids'  : hrIds,
                                                               'egroups' : eGroups,
                                                               'accounts': accounts,
                                                               'emails'  : emails,
                                                            }                                                     )

        logger.info( f'\n++> bulk addition of members returned: {status} - {msg}')

        return cls._do_get_egMembers(egName)

    @classmethod
    def delete_mem_from_egroup(cls):
        args = cls.parse_args()
        logger.info( f'\nargs: {args}')

        payload = cls.get_payload()
        logger.info( f'\npayload: {payload}')

        egName, hrIds, eGroups, accounts, emails = _getMemberListFromPayload(cls, payload)
        if readOnlyOnStaging(egName): 
            msg = f'ERROR: request to modify members for eGroup {egName} DENIED on non-PROD instance.'
            logger.error( msg )
            raise BadRequestException(msg)

        logger.info( f'request to DELETE members from {egName}:' )
        logger.info( f'\thrIDs   : {hrIds}')
        logger.info( f'\teGroups : {eGroups}')
        logger.info( f'\taccounts: {accounts}')
        logger.info( f'\temails  : {emails}')

        for item in hrIds:
            logger.info( f'going to remove hrId "{item}" from {egName}')
            status, msg = EgroupHandler().removeMemberPerson(egName, item)
            logger.info( f'\n - result: {status} - {msg}')

        for item in eGroups:
            logger.info( f'going to remove eGroup "{item}" from {egName}')
            status, msg = EgroupHandler().removeMemberEGroup(egName, item)
            logger.info( f'\n - result: {status} - {msg}')

        for item in accounts:
            logger.info( f'going to remove account "{item}" from {egName}')
            status, msg = EgroupHandler().removeMemberAccount(egName, item)
            logger.info( f'\n - result: {status} - {msg}')

        for item in emails:
            logger.info( f'going to remove email "{item}" from {egName}')
            status, msg = EgroupHandler().removeMemberAccount(egName, item)
            logger.info( f'\n - result: {status} - {msg}')

        return cls._do_get_egMembers(egName)
