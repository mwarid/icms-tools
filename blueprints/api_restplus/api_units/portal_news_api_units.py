from icms_orm.portal_news import NewsItem, Channel
from blueprints.api_restplus.api_units.basics import BaseApiUnit, ModelFactory, ParserBuilder
import sqlalchemy as sa
import re
from urllib import request
import logging
import traceback


class PortalNewsItemApiCall(BaseApiUnit):
    _model = ModelFactory.make_model('Portal News Item Info', {
        NewsItem.id.key: ModelFactory.Integer,
        NewsItem.title.key: ModelFactory.String,
        NewsItem.description.key: ModelFactory.String,
        NewsItem.channel_id.key: ModelFactory.Integer,
        NewsItem.creator.key: ModelFactory.String,
        NewsItem.date.key: ModelFactory.DateTime,
        NewsItem.link.key: ModelFactory.String
    })

    _args = ParserBuilder().add_argument('channel_id', type=ParserBuilder.INTEGER, required=False, default=None). \
        add_argument('limit', type=ParserBuilder.INTEGER, required=False, default=6).parser

    @classmethod
    def get_news(cls):
        kwargs = cls.parse_args()
        ssn = NewsItem.session()

        _cols = [NewsItem.id, NewsItem.channel_id, NewsItem.description, NewsItem.title, NewsItem.creator,
                 NewsItem.date, NewsItem.link]
        q = ssn.query(*_cols).order_by(sa.desc(NewsItem.date))
        _channel_id = kwargs.get('channel_id', None)
        if _channel_id:
            q = q.filter(NewsItem.channel_id == _channel_id)
        q = q.limit(int(kwargs.get('limit')))

        _rows = []
        for _row in q.all():
            _map = cls.row_to_dict(_row, cols_list=_cols)
            _link_matches = re.findall(r'/iCMS.*_\d{8}_\d{6}', _map.get(NewsItem.link.key))
            _map[NewsItem.link.key] = _link_matches[0] if _link_matches else None
            _rows.append(_map)
        return _rows


class PortalNewsContentApiCall(BaseApiUnit):
    _model = ModelFactory.make_model('Portal News Item Text', {
        NewsItem.id.key: ModelFactory.Integer,
        'file_content': ModelFactory.String
    })

    _args = ParserBuilder().add_argument(NewsItem.id.key, type=ParserBuilder.INTEGER, required=True).parser

    @classmethod
    def get_content(cls):
        kwargs = cls.parse_args()
        _id = kwargs.get(NewsItem.id.key)
        link = NewsItem.session().query(NewsItem.link).filter(NewsItem.id == _id).one()[0]
        link = NewsItem.parse_link(link)

        content = None
        try:
            content = request.urlopen('http://cms.cern.ch{0}'.format(link)).read()
            if isinstance(content, bytes) or isinstance(content, bytearray):
                content = content.decode()
        except Exception as whatever_it_is_that_this_thing_might_throw_even_if_it_is_a_potato:
            logging.warning(traceback.format_exc())

        return {NewsItem.id.key: _id, 'file_content': str(content)}


class PortalNewsChannelApiCall(BaseApiUnit):
    _model = ModelFactory.make_model('Portal News Channel Info', {
        Channel.id.key: ModelFactory.Integer,
        Channel.title.key: ModelFactory.String,
        Channel.description.key: ModelFactory.String
    })

    @classmethod
    def get_channels(cls):
        ssn = Channel.session()
        _cols = [Channel.id, Channel.title, Channel.description]
        q = ssn.query(*_cols)
        return cls.rows_to_list_of_dicts(q.all(), cols_list=_cols)
