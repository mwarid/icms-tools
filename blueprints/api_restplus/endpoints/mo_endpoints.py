import sqlalchemy as sa_
from flask_restx import Resource
from blueprints.api_restplus.blueprint import api
from icmsutils.businesslogic import mo_new
import logging
from blueprints.api_restplus.models import mo_models
from flask_restx import reqparse, inputs
from datetime import date
from blueprints.api_restplus.api_units import OldDbMoCall
from blueprints.api_restplus.api_units import MoListCall, MoStepsCall, MoProjectListCall
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace


ns = IcmsApiNamespace('mo', description='Operations related to M&O', ordered=True)


mo_model_args = reqparse.RequestParser()
mo_model_args.add_argument('year', type=int, required=False, default=date.today().year)
mo_model_args.add_argument('mode', type=str, choices=mo_new.MoModel.Mode.values())
mo_model_args.add_argument('inst', type=str)


@ns.route('/people_list')
class MoStatuses(Resource):
    @api.expect(mo_model_args)
    @api.marshal_list_with(mo_models.phd_list_entry)
    def get(self):
        """
        Uses the old database - hopefully it will go away one day
        """
        _year, _mode = [mo_model_args.parse_args().get(k) for k in ('year', 'mode')]
        logging.debug('About to fetch the MO list for year %d and mode %s' % (_year, _mode))
        model = mo_new.MoModel.get_instance(year=_year, mode=_mode)
        _records = model.get_records()
        return _records


@ns.route('/person_mo')
class PersonMoInfo(Resource):
    @api.expect(OldDbMoCall.get_args(), validate=True)
    @api.marshal_list_with(OldDbMoCall.get_model())
    def get(self):
        """
        Also uses the old database - the sooner it becomes obsolete, the better
        """
        return OldDbMoCall.get()


@ns.route('/list')
class PeopleMoList(Resource):
    @api.expect(MoListCall.get_args(), validate=True)
    @api.marshal_list_with(MoListCall.get_model())
    def get(self):
        """
        Returns a list of people info with their MO status for the year requested. Uses the new DB.
        """
        return MoListCall.get()


@ns.route('/steps')
class MoStepsList(Resource):
    @api.expect(MoStepsCall.get_args(), validate=True)
    @api.marshal_list_with(MoStepsCall.get_model())
    def get(self):
        """
        Returns all the (possibly intermediate) states for given person and year
        """
        return MoStepsCall.get()

    # This time round we expect the model (not args) as input - to store it in the DB
    @api.expect(MoStepsCall.get_model(), validate=True)
    @api.marshal_with(MoStepsCall.get_model())
    @api.response(403, 'You are not authorized to create that step now')
    def post(self):
        """
        Always creates a new step - we never erase the existing ones. Returns the created step
        """
        return MoStepsCall.post()


@ns.route('/phd_project_list')
class MoProjectList(Resource):
    @api.expect(MoProjectListCall.get_args(), validate=True)
    @api.marshal_list_with(MoProjectListCall.get_model())
    def get(self):
        """
        Uses the old DB to fetch the PhDs for a given year alongside their project.
        """
        return MoProjectListCall.get()
