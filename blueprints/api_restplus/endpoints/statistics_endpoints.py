import flask
from blueprints.api_restplus.models import statistics_models
from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.logic import statistics_logic
from flask_restx import Resource, reqparse
from util.trivial import current_db_ssn
from icms_orm.cmspeople import Pledge as OldPledge, Institute as OldInst, Person as OldPerson, Project as OldProject
from icms_orm.epr import Pledge as NewPledge, EprInstitute as NewInst, EprUser as NewUser, Task as NewTask
from icms_orm.epr import Project as NewProject, ShiftTaskMap as STM, TimeLineUser as TLU, Shift
from icmsutils.businesslogic import servicework as sw
from flask_restx import inputs
from blueprints.api_restplus.api_units import AuthorshipFractionsCall, EprAggregatesCall, EprInstsOverviewCall, MemberCountApiCall, MemberOverviewApiCall, RegionsApiCall
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace


stats_ns = IcmsApiNamespace('statistics', description='Statistics endpoints for diversity information')
stats_epr_ns = IcmsApiNamespace('statistics/epr', description='Statistics endpoints for EPR information')


class Arg(object):
    year = 'year'
    inst = 'inst_code'
    cms_id = 'cms_id'
    project = 'project'
    date = 'date'


stats_args = reqparse.RequestParser()
stats_args.add_argument(Arg.year, type=int, required=True, help='Year', location='args')

people_status_args = reqparse.RequestParser()
people_status_args.add_argument(Arg.date, type=inputs.date, help='Date in a %Y-%m-%d format', required=True)


@stats_ns.route('/work')
class WorkBreakdown(Resource):
    @api.expect(stats_args, validate=True)
    def get(self):
        """
        :return: JSON representing the breakdown of total work delivered across the projects, grouped by institutes and individuals
        """
        ssn = current_db_ssn()
        args = stats_args.parse_args(flask.request)
        year = args.get(Arg.year)
        grouping_cols = (inst_col, cms_id_col, task_col) = year >= 2015 and (NewInst.code, NewUser.cmsId, NewProject.name) or (OldPledge.instCode, OldPledge.cmsId, OldPledge.projectId)
        return sw._get_worked(db_session=ssn, year=year, grouping_column=grouping_cols)


@stats_ns.route('/shifts')
class ShiftsBreakdown(Resource):
    @api.expect(stats_args, validate=True)
    def get(self):
        """
        :return: JSON representing the breakdown of total shift work delivered across the projects, grouped by institutes and individuals
        """
        ssn = current_db_ssn()
        args = stats_args.parse_args(flask.request)
        year = args.get(Arg.year)
        grouping_columns = \
            year >= 2017 and (TLU.instCode, NewUser.cmsId, NewProject.name) \
            or year >= 2015 and (TLU.instCode, NewUser.cmsId, Shift.subSystem) \
            or (OldPledge.instCode, OldPledge.cmsId, OldPledge.projectId)
        return sw._get_shifts_done(db_session=ssn, year=year, grouping_column=grouping_columns)


@stats_ns.route('/people')
class PeopleInfo(Resource):
    @api.marshal_list_with(statistics_models.person_info)
    def get(self):
        """
        :return: JSON of static people information
        """
        return statistics_logic.get_people_info()


@stats_ns.route('/people_status')
class PeopleStatusInfo(Resource):
    @api.marshal_with(statistics_models.person_status_info)
    @api.expect(people_status_args, validate=True)
    def get(self):
        """
        :return: JSON of people status information at the indicated of time
        """
        args = people_status_args.parse_args(flask.request)
        _date = args.get(Arg.date)
        return statistics_logic.get_people_status_info(_date)


@stats_ns.route('/institutes_status')
class InstitutesStatusInfo(Resource):
    @api.expect(people_status_args, validate=True)
    @api.marshal_list_with(statistics_models.institute_status_info)
    def get(self):
        """
        :return: JSON of institutes' status information at the indicated time
        """
        args = people_status_args.parse_args(flask.request)
        _date = args.get(Arg.date)
        return statistics_logic.get_institutes_status_info(_date)


@stats_ns.route('/institutes')
class InstitutesInfo(Resource):
    @api.marshal_list_with(statistics_models.institute_info)
    def get(self):
        """
        :return: JSON of static institute information
        """
        return statistics_logic.get_institutes_info()


@stats_ns.route('/authorship')
class AuthorshipInfo(Resource):
    @api.marshal_with(statistics_models.authorship_stats_info)
    @api.expect(stats_args)
    def get(self):
        """
        :return: JSON indicating how many Author Lists each person (CMS ID) made it onto in the selected year
        """
        args = stats_args.parse_args(flask.request)
        year = args.get(Arg.year)
        return statistics_logic.get_authorship_stats(year)


@stats_ns.route('/authorship_fractions')
class AuthorshipFractionsInfo(Resource):
    @api.marshal_list_with(AuthorshipFractionsCall.get_model())
    @api.expect(AuthorshipFractionsCall.get_args(), validate=True)
    def get(self):
        """
        Returns a number denoting a fraction of a given year when the given CMS ID holder was an author
        """
        return AuthorshipFractionsCall.get()


@stats_epr_ns.route('/aggregates')
class EprAggregatesInfo(Resource):
    @api.marshal_list_with(EprAggregatesCall.get_model())
    @api.expect(EprAggregatesCall.get_args(), validate=True)
    def get(self):
        return EprAggregatesCall.get()

@stats_epr_ns.route('/inst_overview')
class EprInstsOverviewInfo(Resource):
    @api.marshal_list_with(EprInstsOverviewCall.get_model())
    @api.expect(EprInstsOverviewCall.get_args(), validate=True)
    def get(self):
        return EprInstsOverviewCall.get_stats()


@stats_ns.route('/members_count')
class MembersCountInfo(Resource):
    @api.expect(MemberCountApiCall.get_args(), validate=True)
    def get(self):
        return MemberCountApiCall.get_stats()

@stats_ns.route('/members_overview')
class MembersOverview(Resource):
    @api.expect(MemberOverviewApiCall.get_args(), validate=True)
    def get(self):
        return MemberOverviewApiCall.get_stats()

@stats_ns.route('/regions')
class Regions(Resource):
    def get(self):
        return RegionsApiCall.get()