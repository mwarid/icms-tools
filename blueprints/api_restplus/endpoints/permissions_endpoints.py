from blueprints.api_restplus.blueprint import api
from flask_restx import Resource
from blueprints.api_restplus.api_units import AccessClassesApiCall
from blueprints.api_restplus.api_units import AccessPermissionsApiCall, RestrictedResourcesApiCall
from blueprints.api_restplus.decorators import ParamStoreProvider
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace
import inspect


ns = IcmsApiNamespace(
    'admin/permissions', description='Access permissions management', ordered=False)

ns.register_crud_handler_class(api, AccessClassesApiCall, '/classes')
ns.register_crud_handler_class(api, RestrictedResourcesApiCall, '/resources')
ns.register_crud_handler_class(api, AccessPermissionsApiCall, '/rights')


@ns.route('/stores')
class ParamStoresCollection(Resource):
    def get(self):
        """
        A quick reminder and an always-in-sync reference on the prefixes that can be used.
        """
        return [dict(prefix=g.prefix, source=inspect.getsource(g.getter)) for g in ParamStoreProvider.get_all()]
