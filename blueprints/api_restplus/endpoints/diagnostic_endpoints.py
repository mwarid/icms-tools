import flask
from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.decorators import admin_required
from flask_restx import Resource, reqparse
from util.trivial import current_db_ssn
from icms_orm.common import ApplicationAsset
from datetime import datetime
from blueprints.api_restplus.models import diagnostics_models
from blueprints.api_restplus.logic import diagnostics_logic
from util.trivial import get_deployment_info
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace


diagnostics_ns = IcmsApiNamespace('diagnostics', description='Endpoints providing diagnostic info for iCMS')


class Attr(object):
    asset_name = 'asset_name'


asset_params = reqparse.RequestParser()
asset_params.add_argument(Attr.asset_name, type=str, required=True, help='Application asset name')

sync_agents_params = reqparse.RequestParser()
sync_agents_params.add_argument(Attr.asset_name, type=str, required=False, help='Sync agents info asset name',
        default='fwd_sync_last_run_info')


@diagnostics_ns.route('/app_assets')
class AppAssetNamesList(Resource):
    @admin_required
    def get(self):
        """
        :return: a list of application asset names found in the database
        """
        return [r[0] for r in ApplicationAsset.session().query(ApplicationAsset.name).all()]


@diagnostics_ns.route('/app_asset')
class AppAssetInfo(Resource):
    @api.marshal_list_with(diagnostics_models.application_asset_info)
    @api.expect(asset_params)
    @admin_required
    def get(self):
        args = asset_params.parse_args(flask.request)
        name = args.get(Attr.asset_name)
        return diagnostics_logic.get_application_assets_by_name(name)


@diagnostics_ns.route('/sync_agents')
class SyncAgentsInfo(Resource):
    @api.expect(sync_agents_params)
    @admin_required
    def get(self):
        """
        :return: info about the last run times of sync agents
        """
        ssn = current_db_ssn()
        asset_name = sync_agents_params.parse_args(flask.request).get(Attr.asset_name)
        name, data = ssn.query(ApplicationAsset.name, ApplicationAsset.data). \
            filter(ApplicationAsset.name.like('%{key}%'.format(key=asset_name))).one()
        timestamps_key = 'timestamps'
        for key, val in (data.get(timestamps_key) or {}).items():
            data[timestamps_key][key] = str(datetime.utcfromtimestamp(val))
        return {name: data}

@diagnostics_ns.route('/deployment_info')
class DeploymentInfo(Resource):
    @admin_required
    def get(self):
        return get_deployment_info()
