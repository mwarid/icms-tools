"""
This module has been partially rewritten to follow the CRUD-imposed conventions.
"""
from blueprints.api_restplus.api_units import VotingListsApiUnit
import flask
from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.decorators import admin_required
from flask_restx import Resource
from util.trivial import current_db_ssn
from blueprints.api_restplus.models.voting_api_models import voting_merger_model
from blueprints.api_restplus.parsers import pagination_arguments
from blueprints.api_restplus.logic import voting_logic as voting_business
from flask_restx.reqparse import RequestParser
from flask_restx import inputs
from blueprints.api_restplus.api_units import VotingParticipantsApiUnit
from blueprints.api_restplus.api_units import VotingEventsApiUnit
from blueprints.api_restplus.api_units import VoteDelegationsApiUnit
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace

ns = IcmsApiNamespace(
    'voting', description='Operations related to voting, vote delegations etc.')
ns.register_crud_handler_class(api, VoteDelegationsApiUnit, '/delegations')
ns.register_crud_handler_class(api, VotingListsApiUnit, '/lists')
ns.register_crud_handler_class(api, VotingEventsApiUnit, '/events')

argn_voting_code = 'voting'
voting_code_parser = RequestParser()
voting_code_parser.add_argument(
    argn_voting_code, type=str, required=True, help='Voting code')
single_date_parser = RequestParser().add_argument('date', type=inputs.date,
                                                  required=False, help='show only the mergers active a of this date')


@ns.route('/mergers/<int:id>')
@ns.param('id', 'ID of the merger being subject of current query')
class VotingMergerItem(Resource):
    @api.marshal_with(voting_merger_model)
    def get(self, id):
        """Retrieve a specific voting merger"""
        return voting_business.get_merger(current_db_ssn(), id)

    @admin_required
    def delete(self,  id):
        """Remove a specific voting merger"""
        return voting_business.remove_merger(current_db_ssn(), id)

    @admin_required
    def put(self, id):
        """Edit a specific voting merger"""
        raise NotImplementedError('Editing mergers not supported yet!')


@ns.route('/mergers/')
class VotingMergersCollection(Resource):
    @api.marshal_list_with(voting_merger_model)
    @api.expect(single_date_parser, pagination_arguments, validate=True)
    def get(self):
        """List voting mergers"""
        args = single_date_parser.parse_args()
        return voting_business.get_mergers(current_db_ssn(), args.get('date', None), **pagination_arguments.parse_args(flask.request))

    @api.expect(voting_merger_model, validate=True)
    @api.marshal_with(voting_merger_model)
    @admin_required
    def post(self):
        """Create a new voting merger"""
        return voting_business.create_voting_merger(current_db_ssn(), api.payload)


@ns.route('/participants')
class VotingEntities(Resource):
    @api.expect(VotingParticipantsApiUnit.get_args(), validate=True)
    @api.marshal_list_with(VotingParticipantsApiUnit.get_model())
    def get(self):
        return VotingParticipantsApiUnit.get_voting_participants()
