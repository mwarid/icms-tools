from flask_restx import Resource, reqparse
from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.logic import members_logic as logic
from blueprints.api_restplus.api_units import InstitutesCall, LdapPersonInfoCall, LdapPeopleLiteInfoCall
from blueprints.api_restplus.api_units import OldDbPersonRestrictedInfoCall, OldDbPersonInfoCall, OldDbPeopleInfoCall
from blueprints.api_restplus.api_units import CarrerEventApiCall, ExternalOutreachContactsCall, UsersInfoCall, AssignmentsCalls
from blueprints.api_restplus.api_units import FlagsApiCall
from blueprints.api_restplus.api_units import EgroupPersonInfoCall
from blueprints.api_restplus.api_units.basics import HyperlinkWrapper
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace


ns = IcmsApiNamespace(
    'members', description='Operations related to CMS members: institutes and people')

filter_insts_params = reqparse.RequestParser()
filter_insts_params.add_argument(logic.Arg.inst_code, type=str, required=False)
filter_insts_params.add_argument(logic.Arg.country, type=str, required=False)
filter_insts_params.add_argument(
    logic.Arg.inst_status, type=str, required=False)


@ns.route('/institutes')
class InstitutesInfo(Resource):
    @api.expect(InstitutesCall.get_args(), validate=True)
    @api.marshal_list_with(InstitutesCall.get_model())
    def get(self):
        """Returns the list of CMS institutes"""
        return InstitutesCall.get()


@ns.route('/institute')
class InstituteInfo(Resource):
    @api.expect(InstitutesCall.get_args(), validate=True)
    @api.marshal_with(InstitutesCall.get_model())
    @api.response(404, 'Either none or more than one institutes found for query')
    def get(self):
        """Returns a single institute"""
        return InstitutesCall.get_one()


@ns.route('/people')
class PeopleResource(Resource):
    @api.expect(OldDbPeopleInfoCall.get_args(), validate=True)
    @api.marshal_list_with(OldDbPeopleInfoCall.get_model())
    def get(self):
        """
        Returns the most relevant info on requested set of people
        """
        return OldDbPeopleInfoCall.get()


@ns.route('/person')
class PersonResource(Resource):
    @api.expect(OldDbPersonInfoCall.get_args(), validate=True)
    @api.marshal_with(OldDbPersonInfoCall.get_model())
    @api.response(404, 'Not found or found many instead of one')
    def get(self):
        """
        Info on a person, fetched from the old iCMS DB
        """
        return OldDbPersonInfoCall.get()


@ns.route('/ldap_person_info')
class LdapPersonInfo(Resource):
    @api.expect(LdapPersonInfoCall.get_args(), validate=True)
    @api.marshal_with(LdapPersonInfoCall.get_model())
    @api.response(404, 'LDAP entry not found for search terms provided')
    def get(self):
        return LdapPersonInfoCall.get_info()


@ns.route('/ldap_people_lite_info')
class LdapPeopleLiteInfo(Resource):
    @api.expect(LdapPeopleLiteInfoCall.get_args(), validate=True)
    @api.marshal_list_with(LdapPeopleLiteInfoCall.get_model())
    def get(self):
        return LdapPeopleLiteInfoCall.get_lite_info()

@ns.route('/egroup-info')
class EgroupPersonInfo(Resource):
    @api.expect(EgroupPersonInfoCall.get_args(), validate=True)
    @api.marshal_list_with(EgroupPersonInfoCall.get_model())
    def get(self):
        return EgroupPersonInfoCall.get_info()



@ns.route('/person_restricted_info')
class PersonRestrictedInfo(Resource):
    @api.expect(OldDbPersonRestrictedInfoCall.get_args(), validate=True)
    @api.marshal_list_with(OldDbPersonRestrictedInfoCall.get_model())
    @api.response(403, 'Insufficient privileges')
    @api.response(404, 'Not found or found many instead of one')
    def get(self):
        return OldDbPersonRestrictedInfoCall.get()


@ns.route('/career_event')
class CareerEventInfo(Resource):
    @api.expect(CarrerEventApiCall.get_args(), validate=True)
    @api.marshal_list_with(CarrerEventApiCall.get_model())
    @api.response(403, 'Insufficient privileges')
    @api.response(404, 'Failed to find exactly one entry matching the criteria provided.')
    def get(self):
        return CarrerEventApiCall.get_one()

    @api.expect(CarrerEventApiCall.get_model(), validate=True)
    @api.marshal_list_with(CarrerEventApiCall.get_model())
    @api.response(403, 'Insufficient privileges')
    @api.response(404, 'Failed to find exactly one entry matching the criteria provided.')
    def post(self):
        return CarrerEventApiCall.post()

    @api.expect(CarrerEventApiCall.get_args(), validate=True)
    # @api.marshal_list_with(CarrerEventApiCall.get_model())
    @api.response(403, 'Insufficient privileges')
    @api.response(404, 'Failed to find exactly one entry matching the criteria provided.')
    def delete(self):
        return CarrerEventApiCall.delete()


@ns.route('/career_events')
class CareerEventsList(Resource):
    @api.expect(CarrerEventApiCall.get_args(), validate=True)
    @api.marshal_list_with(CarrerEventApiCall.get_model())
    @api.response(403, 'Insufficient privileges')
    @api.response(404, 'Failed to find exactly one entry matching the criteria provided.')
    def get(self):
        return CarrerEventApiCall.get_many()


@ns.route('/external_outreach_contacts')
class ExternalOutReachContacts(Resource):
    @api.marshal_list_with(ExternalOutreachContactsCall.get_model())
    def get(self):
        return ExternalOutreachContactsCall.list()

    @api.expect(ExternalOutreachContactsCall.get_model(), validate=True)
    @api.marshal_with(ExternalOutreachContactsCall.get_model())
    def post(self):
        return ExternalOutreachContactsCall.save()

    def patch(self):
        """
        Admins only: converts existing entries to the new format, returns the "before" and "after" versions.
        """
        return ExternalOutreachContactsCall.convert_old_entries()


@ns.route('/external_outreach_contact')
class ExternalOutreachContact(Resource):
    @api.expect(ExternalOutreachContactsCall.get_args(), validate=True)
    @api.marshal_with(ExternalOutreachContactsCall.get_model())
    def get(self):
        return ExternalOutreachContactsCall.get()

    @api.expect(ExternalOutreachContactsCall.get_model(), validate=True)
    @api.marshal_with(ExternalOutreachContactsCall.get_model())
    def put(self):
        return ExternalOutreachContactsCall.save()

    @api.expect(ExternalOutreachContactsCall.get_args(), validate=True)
    def delete(self):
        return ExternalOutreachContactsCall.delete()


@ns.route('/users_info')
class UsersInfoResource(Resource):
    @api.expect(UsersInfoCall.get_args(), validate=True)
    @api.marshal_list_with(UsersInfoCall.get_model())
    def get(self):
        return UsersInfoCall.get_many()


@ns.route('/assignments')
class AssignmentsCollection(Resource):
    @api.expect(AssignmentsCalls.get_args(), validate=True)
    @api.marshal_list_with(AssignmentsCalls.get_model())
    def get(self):
        return AssignmentsCalls.get()

    @api.expect(AssignmentsCalls.new_assignment_model, validate=True)
    def post(self):
        return AssignmentsCalls.edit_or_create()


@ns.route('/flags')
class FLagsInfo(Resource):
    @api.marshal_list_with(FlagsApiCall.get_model())
    def get(self):
        return FlagsApiCall.get_flags()
