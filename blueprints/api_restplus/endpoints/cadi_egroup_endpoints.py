"""
Endpoints attaching to the eGroup handling
"""

from wsgiref import validate
from blueprints.api_restplus.blueprint import api
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace
from blueprints.api_restplus.decorators import admin_required
from flask_restx import Resource
from blueprints.api_restplus.api_units import EgroupCADIApiCall


ns = IcmsApiNamespace('egroups', description='Endpoints attaching to the eGroup handling')


@ns.route('/cadi_egroup')
class EgroupCadiInfo(Resource):
    skip_auto_permission_check = True

    @api.expect(EgroupCADIApiCall._model)
    @api.marshal_with(EgroupCADIApiCall._response_model)
    def post(self):
        """
        Create the specified eGroup and update the params and members
        """
        return EgroupCADIApiCall.post_cadi_egroup()
