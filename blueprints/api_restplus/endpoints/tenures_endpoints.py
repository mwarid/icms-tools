from blueprints.api_restplus.blueprint import api
from flask_restx import Resource
from blueprints.api_restplus.api_units import TenuresApiCall, TenureApiCall
from blueprints.api_restplus.api_units import (
    OrgUnitsTreeApiCall,
    OrgUnitsApiCall,
    OrgUnitTypesApiCall,
)
from blueprints.api_restplus.api_units import PositionsApiCall, DesignationApiCall
from blueprints.api_restplus.api_units import ExOfficioMandatesApiCall
from blueprints.api_restplus.api_units import JobOpeningsApiCall, JobOpeningApiCall
from blueprints.api_restplus.api_units import (
    JobNominationApiCall,
    JobNominationStatusApiCall,
)
from blueprints.api_restplus.api_units import JobQuestionnaireApiCall
from blueprints.api_restplus.decorators import admin_required
from icmsutils.exceptions import IcmsInsufficientRightsException
from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace
from blueprints.api_restplus.exceptions import (
    AccessViolationException,
    NotFoundException,
)


ns = IcmsApiNamespace(
    "org_chart",
    description="Endpoints for management and retrieval of tenures information",
)


@ns.route("/designations")
class DesignationInfo(Resource):
    """
    To keep the TenuresInfo clean as admin-only editor, designation will go through a separate endpoint.
    The two might also diverge further over time if we decide to track more info on designations that would
    make no sense for admin-entered data going through the other endpoint.
    """

    @api.expect(DesignationApiCall.get_model(), validate=True)
    @api.marshal_list_with(DesignationApiCall.get_model())
    def post(self):
        return DesignationApiCall.post_tenure()

    @ns.response(403, "No rights to manage specified designation")
    @ns.response(404, "Designation not found in DB")
    @api.expect(DesignationApiCall.get_args(), validate=True)
    def delete(self):
        DesignationApiCall.terminate_tenure()
        return "", 204


@ns.route("/tenure")
class TenureInfo(Resource):
    @api.expect(TenureApiCall.get_args(), validate=True)
    @api.marshal_with(TenureApiCall.get_model())
    def get(self):
        return TenureApiCall.get_tenure()


@ns.route("/tenures")
class TenuresInfo(Resource):
    @api.expect(TenuresApiCall.get_args(), validate=True)
    @api.marshal_list_with(TenuresApiCall.get_model())
    def get(self):
        """
        Returns information about tenures matching given criteria
        """
        return TenuresApiCall.get_tenures()

    @api.expect(TenuresApiCall.get_model(), validate=True)
    @api.marshal_with(TenuresApiCall.get_model())
    def post(self):
        """
        Creates or edits existing tenure information
        """
        return TenureApiCall.post_tenure()

    @api.expect(TenureApiCall.get_args(), validate=True)
    def delete(self):
        """
        Removes the tenure matching input parameters. Returns an error if more than one match is found.
        """
        TenureApiCall.delete_tenure()
        return "", 204


@ns.route("/positions")
class PositionsInfo(Resource):
    @api.expect(PositionsApiCall.get_args(), validate=True)
    @api.marshal_list_with(PositionsApiCall.get_model())
    def get(self):
        """
        Lists registered positions matching the search criteria (if provided).
        """
        return PositionsApiCall.get_positions()

    @api.expect(PositionsApiCall.get_model())
    @api.marshal_with(PositionsApiCall.get_model())
    @admin_required
    def post(self):
        """
        Creates a new position entry from the input data.
        """
        return PositionsApiCall.post_position()


@ns.route("/unit")
class OrgUnitInfo(Resource):
    @api.expect(OrgUnitsApiCall.get_args(), validate=True)
    @api.marshal_with(OrgUnitsApiCall.get_model())
    def get(self):
        """
        Returns information on the single OrgUnit matching provided criteria (or an error should no single unit match)
        """
        return OrgUnitsApiCall.get_unit()


@ns.route("/units")
class OrgUnitsInfo(Resource):
    @api.expect(OrgUnitsApiCall.get_args(), validate=True)
    @api.marshal_with(OrgUnitsApiCall.get_model())
    def get(self):
        """
        Returns a flat list of information on OrgUnits matching given criteria
        """
        return OrgUnitsApiCall.get_units()

    @api.expect(OrgUnitsApiCall.get_model(), validate=True)
    @api.marshal_with(OrgUnitsApiCall.get_model())
    def post(self):
        """
        Allows creating a new OrgUnit or editing existing ones
        """
        return OrgUnitsApiCall.post_unit()


@ns.route("/units_tree")
class OrgUnitsTreeInfo(Resource):
    @api.marshal_with(OrgUnitsTreeApiCall.get_model())
    def get(self):
        """
        Returns a JSON tree of OrgUnits
        """
        return OrgUnitsTreeApiCall.get_units_tree()


@ns.route("/unit_types")
class OrgUnitTypesInfo(Resource):
    @api.expect(OrgUnitTypesApiCall.get_args(), validate=True)
    @api.marshal_with(OrgUnitTypesApiCall.get_model())
    def get(self):
        """
        Lists registered org unit types matching the search criteria (if provided).
        """
        return OrgUnitTypesApiCall.get_types()

    # That's how a post method would be defined, but given we want to keep a dictionary of type names in the ORM,
    # a free addition should probably not be made possible.
    #
    # @api.expect(OrgUnitTypesApiCall.get_model())
    # @api.marshal_with(OrgUnitTypesApiCall.get_model())
    # def post(self):
    #     """
    #     :return:
    #     """
    #     return OrgUnitTypesApiCall.post_type()


@ns.route("/ex_officio_mandates")
class ExOfficioMandatesInfo(Resource):
    @api.expect(ExOfficioMandatesApiCall.get_args(), validate=True)
    @api.marshal_list_with(ExOfficioMandatesApiCall.get_model())
    def get(self):
        return ExOfficioMandatesApiCall.get_mandates()

    @api.expect(ExOfficioMandatesApiCall.get_model(), validate=True)
    @api.marshal_with(ExOfficioMandatesApiCall.get_model())
    def post(self):
        return ExOfficioMandatesApiCall.post_mandate()


@ns.route("/ex_officio_mandate")
class ExOfficioMandateInfo(Resource):
    @api.expect(ExOfficioMandatesApiCall.get_args(), validate=True)
    @api.marshal_with(ExOfficioMandatesApiCall.get_model())
    def get(self):
        return ExOfficioMandatesApiCall.get_mandate()


@ns.route("/job_opening")
class JobOpeningInfo(Resource):
    @api.expect(JobOpeningApiCall.get_args(), validate=True)
    @api.marshal_with(JobOpeningApiCall.get_model())
    def get(self):
        return JobOpeningApiCall.get_job_opening()


@ns.route("/job_openings")
class JobOpeningsInfo(Resource):
    skip_auto_permission_check = True

    @api.expect(JobOpeningsApiCall.get_args(), validate=True)
    @api.marshal_list_with(JobOpeningsApiCall.get_model())
    def get(self):
        """
        Returns information about tenures matching given criteria
        """
        return JobOpeningsApiCall.get_job_openings()

    @api.expect(JobOpeningsApiCall.get_model(), validate=True)
    @api.marshal_with(JobOpeningsApiCall.get_model())
    def post(self):
        """
        Creates or edits existing tenure information
        """
        return JobOpeningApiCall.create_job_opening()


@ns.route("/job_openings/<int:id>")
class JobOpeningsInfo(Resource):
    skip_auto_permission_check = True

    @api.expect(JobOpeningApiCall.get_model(), validate=True)
    @api.marshal_with(JobOpeningApiCall.get_model())
    def put(self, id):
        """
        Returns information about tenures matching given criteria
        """
        return JobOpeningApiCall.edit_job_opening(id)

    @ns.response(403, "No rights to manage specified Job Opening")
    @ns.response(404, "Job Opening not found in DB")
    def delete(self, id):
        return JobOpeningApiCall.delete_job_opening(id)


@ns.route("/job_nomination")
class JobNominationInfo(Resource):
    skip_auto_permission_check = True

    @api.expect(JobNominationApiCall.get_args(), validate=True)
    @api.marshal_list_with(JobNominationApiCall.get_model())
    def get(self):
        """
        Returns information about Nomination matching given criteria
        """
        return JobNominationApiCall.get_all_nominations()

    @api.expect(JobNominationApiCall.get_model(), validate=True)
    @api.marshal_with(JobNominationApiCall.get_model())
    def post(self):
        """
        Creates  Nomination
        """
        return JobNominationApiCall.create_nomination()


@ns.route("/job_nomination/<int:id>")
class EditNomination(Resource):
    skip_auto_permission_check = True

    @api.marshal_with(JobNominationApiCall.get_model())
    def get(self, id):
        """
        Returns information about Nomination matching given criteria
        """
        return JobNominationApiCall.get_nomination_obj(id)

    @api.expect(JobNominationApiCall.get_model(), validate=True)
    @api.marshal_with(JobNominationApiCall.get_model())
    def put(self, id):
        """
        Edits existing nomination
        """
        return JobNominationApiCall.edit_nomination(id)


@ns.route("/job_questionnaire")
class GetJobQuestionnaire(Resource):
    skip_auto_permission_check = True

    @api.expect(JobQuestionnaireApiCall.get_args(), validate=True)
    @api.marshal_list_with(JobQuestionnaireApiCall.get_model())
    def get(self):
        """
        Returns all questionaires
        """
        return JobQuestionnaireApiCall.get_all_questionnaires()

    @api.expect(JobQuestionnaireApiCall.get_model(), validate=True)
    @api.marshal_with(JobQuestionnaireApiCall.get_model())
    def post(self):
        """
        Creates  Questionnaire
        """
        return JobQuestionnaireApiCall.create_questionnaire()


@ns.route("/job_questionnaire/<int:id>")
class EditJobQuestionnaire(Resource):
    skip_auto_permission_check = True

    @api.marshal_with(JobQuestionnaireApiCall.get_model())
    def get(self, id):
        """
        Get  Questionnaire
        """
        return JobQuestionnaireApiCall.get_questionnaire(id)

    @api.expect(JobQuestionnaireApiCall.get_model(), validate=True)
    @api.marshal_list_with(JobQuestionnaireApiCall.get_model())
    def put(self, id):
        """
        Edits existing Questionnaire
        """
        return JobQuestionnaireApiCall.edit_questionnaire(id)
