from flask_restx import fields
from blueprints.api_restplus import api
from icms_orm.epr import TimeLineUser as TLU, Category, EprInstitute as Inst
from icms_orm.cmspeople import MemberStatusValues
from sqlalchemy.orm.attributes import InstrumentedAttribute


tlu_info = api.model('EPR User TimeLine', {isinstance(k, InstrumentedAttribute) and k.key or k: v for k, v in {
    TLU.id: fields.Integer(required=False, example=42),
    TLU.cmsId: fields.Integer(required=True, example=9981),
    TLU.year: fields.Integer(required=True, example=2016),
    TLU.instCode: fields.String(required=True),
    TLU.status: fields.String(requried=True, enum=MemberStatusValues.values()),
    TLU.category: fields.String(required=True, attribute=Category.name.key),
    TLU.mainProj: fields.String(required=True),
    TLU.timestamp: fields.DateTime(),
    TLU.yearFraction: fields.Float(),
    TLU.dueAuthor: fields.Float(required=False, example=3.14),
    TLU.dueApplicant: fields.Float(required=False, example=4.13),
    TLU.isAuthor: fields.Boolean(),
    TLU.isSuspended: fields.Boolean(),
    TLU.comment: fields.String(),
}.items()})
