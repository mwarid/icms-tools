from icms_orm.common import ApplicationAsset
from flask_restx import fields
from blueprints.api_restplus import api


application_asset_info = api.model('Application Asset Info', {
    ApplicationAsset.name.key: fields.String(description='A string', example='some_info'),
    ApplicationAsset.application.key: fields.String(description='Source application', example='CADI'),
    ApplicationAsset.data.key: fields.Raw(description='Some JSON', example='{}')
})
