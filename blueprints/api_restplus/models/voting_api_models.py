from flask_restx import fields
from blueprints.api_restplus import api
from icms_orm.toolkit import Voting, VotingMerger, VotingMergerMember
from datetime import datetime as dt

voting_merger_member_model = api.model('Voting Merger Member Model', {
    VotingMergerMember.inst_code.key: fields.String(required=True, example='CERN')
})

voting_merger_model = api.model('Voting Merger', {
    VotingMerger.id.key: fields.Integer(readonly=True, description='The ID', example='42'),
    VotingMerger.representative_cms_id.key: fields.Integer(required=True,
                                                           description='CMS ID of the person meant to vote on merger\'s behalf',
                                                           example=3020),
    VotingMerger.country.key: fields.String(readonly=True, description='Country', example='Canada'),
    VotingMerger.valid_from.key: fields.Date(required=False, default=dt.today),
    VotingMerger.valid_till.key: fields.Date(required=False, default=None),
    'members': fields.List(fields.Nested(voting_merger_member_model)),
})