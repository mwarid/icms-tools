from flask_restx import fields
from blueprints.api_restplus import api
from icms_orm.cmsanalysis import ARC, ARCMember, AnalysisStateChanges
from icms_orm.cmspeople import Person
import datetime
from icmsutils.timedate import parse_date_concise

arc = api.model('Analysis Review Committee', {
    ARC.id.key: fields.Integer(required=False, description='Internal ARC\'s ID'),
    ARC.name.key: fields.String(required=True, description='Name', max_length=255),
    ARC.creatorId.key: fields.Integer(required=True, description='CMS ID of ARC\'s creator'),
    ARC.createDate.key: fields.Date(required=True, description='Date of ARC creation',
                                    attribute=lambda x: parse_date_concise(getattr(x, ARC.createDate.key))),
    ARC.status.key: fields.String(required=True, description='Status of ARC', max_length=255),
    ARC.updaterId.key: fields.Integer(required=False, description='CMS ID of ARC\'s updater'),
    ARC.updateDate.key: fields.Date(required=False, description='Date of ARC\'s last update',
                                    attribute=lambda x: parse_date_concise(getattr(x, ARC.updateDate.key)))
})

arc_xl = api.inherit('Analysis Review Committee - full info', arc, {
    ARC.creatorName.key: fields.String(required=False, description='Name of ARC\'s creator', max_length=255),
    ARC.url.key: fields.String(required=False, description='Somewhat enigmatic', max_length=255),
    ARC.updaterName.key: fields.String(required=False, description='', max_length=255),
    ARC.remarks.key: fields.String(required=False, description='', max_length=255),
})


arc_member = api.model('ARC Member', {
    ARCMember.cmsId.key: fields.Integer(),
    ARCMember.name.key: fields.String(),
    ARCMember.status.key: fields.String(),
    'role': fields.String(attribute=ARCMember.propstatus.key),
    ARCMember.startDate.key: fields.String(),
    Person.lastName.key: fields.String(),
    Person.firstName.key: fields.String()
})

# arc_xl = api.inherit()

# __tablename__ = 'Committee'
# id = Column('id', Integer, primary_key=True, autoincrement=True)
# name = Column('name', String(255))
# creatorId = Column('creator', Integer)
# creatorName = Column('creatorName', String(255))
# createDate = Column('creatorDate', String(255))
# url = Column('URL', String(255))
# status = Column('status', String(255))
# updaterId = Column('updater', Integer)
# updaterName = Column('updaterName', String(255))
# updateDate = Column('updaterDate', String(255))
# remarks = Column('remarks', String(255))

analysis_status_change = api.model('CADI Line Status Change', {
    AnalysisStateChanges.id.key: fields.Integer(),
    AnalysisStateChanges.lineId.key: fields.Integer(),
    AnalysisStateChanges.code.key: fields.String(),
    AnalysisStateChanges.oldValue.key: fields.String(),
    AnalysisStateChanges.newValue.key: fields.String(),
    AnalysisStateChanges.changeDate.key: fields.Date(),
})
