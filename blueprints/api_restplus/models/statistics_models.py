from flask_restx import fields
from blueprints.api_restplus import api
from icms_orm.cmspeople import Person, Institute, User
from icms_orm.common import PrehistoryTimeline, PersonStatus
from icms_orm.epr import TimeLineInst as TLI
from util.regions import regions

class PersonBirthYear(fields.Integer):
    def format(self, value):
        return value and hasattr(value, 'year') and value.year or None


class InstituteRegionField(fields.Raw):
    def format(self, value):
        region = 'Unknown'
        for r in regions:
            if value in r['countries']:
                region = r['name']
        return region


person_info = api.model('Person Info', {
    Person.cmsId.key: fields.Integer(),
    Person.hrId.key: fields.Integer(),
    'gender': fields.String(attribute=User.sex.key),
    'birthYear': PersonBirthYear(attribute=Person.birthDate.key),
    'phdDate': fields.Date(skip_none=True)
})

institute_info = api.model('Institute Info', {
    Institute.code.key: fields.String(),
    Institute.country.key: fields.String(),
    'region': InstituteRegionField(attribute=Institute.country.key, default='Unknown')
})

institute_status_info = api.model('Institute Status Info', {
    'instCode': fields.String(attribute=TLI.code.key),
    'activeMembersCount': fields.Integer(default=-1),
    'status': fields.String(attribute=TLI.cmsStatus.key)
})

person_status_info = api.model('Person Status Info', {
    PrehistoryTimeline.cms_id.key: fields.Integer(),
    PrehistoryTimeline.status_cms.key: fields.String(),
    PrehistoryTimeline.activity_cms.key: fields.String(),
    PrehistoryTimeline.inst_code.key: fields.String(),
    PersonStatus.is_author.key: fields.Boolean()
})

authorship_stats_info=api.model('Authorship Stats Info', {
    'cmsId': fields.Integer(),
    'authorLists': fields.Integer(),
})
