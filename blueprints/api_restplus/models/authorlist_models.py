from flask_restx import fields
from blueprints.api_restplus import api
from icms_orm.cmsanalysis import PaperAuthorHistory as PAH, PaperAuthor as PA, CadiAnalysis as A, CDS


class JournalFromPubliStringField(fields.String):
    def format(self, value):
        pairs = value.split('+')
        for pair in pairs:
            tokens = pair.split('=')
            if tokens[0] == 'journals':
                return tokens[1]
        return None


authorlist_info = api.model('Authorlist info', {
    'code': fields.String(attribute=PAH.code.key),
    'opened_on': fields.Date(),
    'cadi_status': fields.String(attribute=A.status.key),
    'al_status': fields.String()
})


author_mentions_info = api.model('Author Mentions Info', {
    'code': fields.String(),
    'title': fields.String(),
    'journal': JournalFromPubliStringField(attribute=A.publi.key),
    'cds_upload_date': fields.Date(),
    'cadi_status': fields.String(attribute=A.status.key),
})