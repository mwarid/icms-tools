from flask_restx import reqparse
from flask_restx.inputs import boolean, date

argn_pag_page = "page"
argn_pag_per_page = "per_page"
argn_pag_order = "desc"

pagination_arguments = reqparse.RequestParser()
pagination_arguments.add_argument(
    argn_pag_page, type=int, required=False, default=1, help="Page number"
)
pagination_arguments.add_argument(
    argn_pag_per_page,
    type=int,
    required=False,
    choices=[1, 10, 20, 50, 100, 1000],
    default=10,
    help="Results per page {error_msg}",
)
pagination_arguments.add_argument(
    argn_pag_order,
    type=boolean,
    required=False,
    default=False,
    help="Allows reversing the default, ascending ordering",
)


argn_cadi_code = "ancode"
argn_cadi_awg = "anawg"
argn_cadi_year = "anyear"

cadi_arguments = reqparse.RequestParser()
# todo: enforce the regex
cadi_arguments.add_argument(
    argn_cadi_code, type=str, required=False, default="", help="Analysis code"
)
cadi_arguments.add_argument(
    argn_cadi_awg, type=str, required=False, default="", help="AWG"
)
cadi_arguments.add_argument(
    argn_cadi_year, type=int, required=False, default=None, help="Analysis year"
)


updated_analyses_args = reqparse.RequestParser()
updated_analyses_args.add_argument(
    "awg", type=str, required=False, default="", help="AWG"
)
updated_analyses_args.add_argument(
    "year", type=int, required=False, default=None, help="Analysis year"
)
updated_analyses_args.add_argument(
    "to_status",
    type=str,
    required=False,
    action="split",
    help="Comma-separated list of analysis statuses after the updates",
)
updated_analyses_args.add_argument(
    "start_date",
    type=date,
    required=False,
    help="Start date (from when changes are tracked)",
)
updated_analyses_args.add_argument(
    "end_date",
    type=date,
    required=False,
    help="End date (until when changes are tracked)",
)
updated_analyses_args.add_argument(
    "current_status",
    type=str,
    required=False,
    action="split",
    help="Comma-separated list of current analysis statuses",
)
