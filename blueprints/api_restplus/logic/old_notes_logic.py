from icms_orm.cmspeople import Person
from icms_orm.old_notes import Note, WorkflowProcess as WfProcess, WorkflowRecord as WfRecord, WorkflowData as WfData
from icms_orm.old_notes import NoteFile
import sqlalchemy as sa, re, logging, os
from util.trivial import current_db_ssn
import app_profiles as config
from subprocess import Popen, PIPE
from flask import send_file
import tempfile


class Attr(object):
    cms_id = 'cms_id'
    note_id = 'cms_note_id'
    note_title = 'title'
    note_type = 'type'
    subproject = 'subproject'
    file = 'file'
    keep_backup = 'keep_backup'


def get_notes_info(*args, **kwargs):
    q_cols = [Note.cmsNoteId, Note.title, Note.subdetector, Note.submitter, Note.type, Note.status, Note.submitter,
              Note.authors, Note.submitDate, Note.submitterName]
    q = current_db_ssn().query(*q_cols)

    for arg, col in {Attr.note_title: Note.title, Attr.note_type: Note.type, Attr.note_id: Note.cmsNoteId}.items():
        if arg in kwargs and kwargs.get(arg):
            q = q.filter(col.like('%%%s%%' % kwargs.get(arg)))

    return [{col.key: value for col, value in zip(q_cols, row)} for row in q.all()]


def set_note_submitter(*args, **kwargs):
    cms_id = kwargs.get(Attr.cms_id)
    note_id = kwargs.get(Attr.note_id)
    ssn = current_db_ssn()

    new_submitter = ssn.query(Person).filter(Person.cmsId == cms_id).one()
    note = ssn.query(Note).filter(Note.cmsNoteId.like('%%%s%%' % note_id)).one()

    wf_rows = ssn.query(WfData, WfRecord, WfProcess).join(WfRecord, WfData.id == WfRecord.id).\
        join(WfProcess, WfProcess.id == WfRecord.process).filter(WfData.cmsNoteId==note.get(Note.cmsNoteId)).all()

    new_submitter_str = '{name} {surname} ({inst_code})'.format(name=new_submitter.get(Person.firstName),
                                                                surname=new_submitter.get(Person.lastName),
                                                                inst_code=new_submitter.get(Person.instCode))

    if wf_rows:
        for data, record, process in wf_rows:
            if process.get(WfProcess.submitter) != new_submitter.get(Person.cmsId):
                process.set(WfProcess.submitter, new_submitter.cmsId)
                process.set(WfProcess.submitterName, new_submitter_str)
                ssn.add(process)

    note.set(Note.submitter, new_submitter.get(Person.cmsId))
    note.set(Note.submitterName, new_submitter_str)
    ssn.add(note)
    ssn.commit()

    return note


def set_note_subproject(*args, **kwargs):
    note_cms_id = kwargs.get(Attr.note_id)
    subproject = kwargs.get(Attr.subproject)
    ssn = Note.session()
    note = ssn.query(Note).filter(Note.cmsNoteId == note_cms_id).one()
    for data in ssn.query(WfData).filter(WfData.cmsNoteId == note.get(Note.cmsNoteId)).all():
        # because some artificial constructs exist like 'CRSTH' subprojects in workflow for STH subdetector in notes
        data.set(WfData.subproject, data.get(WfData.subproject).replace(note.get(Note.subdetector), subproject))
        ssn.add(data)
    note.set(Note.subdetector, subproject)
    ssn.add(note)
    ssn.commit()
    return note


def _get_path_to_most_recent_pdf(note_id):
    ssn = Note.session()
    # get the last file from NoteFiles entries
    sq = ssn.query(sa.func.max(NoteFile.id).label('ref_id')).join(Note, NoteFile.noteId == Note.id). \
        filter(Note.cmsNoteId == note_id).group_by(NoteFile.noteId).subquery()
    (file_name, _db_type) = ssn.query(NoteFile.fileName, Note.type).join(Note, Note.id == NoteFile.noteId).join(sq, NoteFile.id == sq.c.ref_id).one()

    # duh, the type of the note can be stored in the workflow and then it seems to take precedence when it comes to path

    _wf_type = ssn.query(sa.distinct(WfProcess.type)).join(WfRecord, WfProcess.id == WfRecord.process).join(WfData, WfData.id == WfProcess.id).filter(WfData.cmsNoteId==note_id).one_or_none()

    _path_patterns = [
        '/data/iCMSfiles/Notes/{year}/{type}/{filename}',
        '/data/iCMSfiles/Notes/Drafts/{filename}',
        '/data/iCMSfiles/Notes/{year}/{db_type}/{filename}',
    ]
    if _wf_type:
        _path_patterns.append('/data/iCMSfiles/Notes/{year}/{wf_type}/{filename}')

    m = re.match(r'CMS ([A-Z]+)-(\d{4})/\d+', note_id)
    if m and len(m.groups()) == 2:
        _type = m.group(1)
        _year = m.group(2)

    _paths = {_s.format(filename=file_name, type=_type, year=_year, db_type=_db_type, wf_type=_wf_type) for _s in _path_patterns}
    host = config.get_old_icms_host()
    for _path in _paths:
        logging.debug('trying to find the file at %s (on host %s)' % (_path, host))
        p = Popen(['ssh', host, 'ls %s' % _path], stdout=PIPE, stdin=PIPE, stderr=PIPE)
        output, err = p.communicate()
        if p.returncode == 0:
            logging.debug('Seems like we found the file at %s' % output)
            return _path
        else:
            logging.debug(err)

    logging.debug('The last resort: trying to find the file')
    cmd = 'find /data/iCMSfiles/Notes/ -type f -name %s' % file_name
    p = Popen(['ssh', host, cmd], stdout=PIPE, stderr=PIPE, stdin=PIPE)
    output, err = p.communicate()
    if p.returncode == 0:
        return output.strip()
    else:
        logging.debug(err)
    return None


def get_pdf(*args, **kwargs):
    cms_note_id = kwargs.get(Attr.note_id)
    cms_note_id = cms_note_id.startswith('CMS ') and cms_note_id or 'CMS %s' % cms_note_id

    _path = _get_path_to_most_recent_pdf(cms_note_id)
    if not _path:
        raise Exception('Failed to find the file for CMS Note with ID %s' % cms_note_id)
    _handle, _filepath = tempfile.mkstemp(prefix='stitch_', suffix='.pdf', dir='/tmp/')
    p = Popen(['scp', '{host}:{path}'.format(host=config.get_old_icms_host(), path=_path), _filepath], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    _out, _err = p.communicate()
    if p.returncode == 0:
        logging.debug('Copied well into %s' % _filepath)
        _resp = send_file(_filepath)
        os.remove(_filepath)
        return _resp
    else:
        logging.error(_err)
        raise Exception(_err)


def restitch_note(cms_note_id, local_source, keep_a_copy=True):
    cms_note_id = cms_note_id.startswith('CMS ') and cms_note_id or 'CMS %s' % cms_note_id
    _path = _get_path_to_most_recent_pdf(cms_note_id)

    if not _path:
        raise Exception('Failed to find the path to the most recent PDF of %s' % cms_note_id)
    logging.debug('Target file resides at: %s' % _path)

    _host = config.get_old_icms_host()
    _replacement = _path + '_new'
    _intermediate = _path + '_tmp'

    p = Popen(['scp', local_source, _host + ':' + _intermediate], stderr=PIPE, stdout=PIPE, stdin=PIPE)
    out, err = p.communicate()
    if p.returncode != 0:
        raise Exception('Fail: %s' % err or out)

    _commands = [
        'pdftk O={original} R={intermediate} cat O1 R output {replacement}'.
            format(original=_path, replacement=_replacement, intermediate=_intermediate)
    ]

    if keep_a_copy:
        _commands.append('mv {src} {dst}'.format(src=_path, dst=(_path + '_stitch_bkp')))
    _commands.append('mv {src} {dst}'.format(src=_replacement, dst=_path))
    _commands.append('rm {intermediate}'.format(intermediate=_intermediate))

    for _cmd in _commands:
        logging.debug('Trying to remotely execute: %s' % _cmd)
        p = Popen(['ssh', _host, _cmd], stdin=PIPE, stderr=PIPE, stdout=PIPE)
        out, err = p.communicate()
        if p.returncode != 0:
            raise Exception('Fail: %s' % (err or out))

    return 0


def find_likely_subeditors(*args, **kwargs):
    """
    Finds the past subeditors for notes of the same type and subproject as the note denoted by provided CMS Note ID
    :param args:
    :param kwargs:
    :return:
    """
    cms_note_id = kwargs.get(Attr.note_id)
    cms_note_id = cms_note_id.startswith('CMS ') and cms_note_id or 'CMS %s' % cms_note_id

    ssn = WfData.session()

    sq = ssn.query(WfData.subproject.label('subproject'), WfProcess.type.label('type')).\
        join(WfRecord, WfData.id == WfRecord.id).join(WfProcess, WfRecord.process == WfProcess.id).\
        filter(WfData.cmsNoteId == cms_note_id).distinct().subquery()

    _group_cols = [WfData.subeditor, WfData.subeditorName, WfProcess.type, WfData.subproject]
    _cols = _group_cols + [sa.func.count(WfData.id).label('cases'), sa.func.max(WfData.id).label('maxId')]

    _data = ssn.query(*_cols).group_by(*_group_cols)\
        .join(WfRecord, WfData.id == WfRecord.id).join(WfProcess, WfRecord.process == WfProcess.id) \
        .join(sq, sa.and_(WfData.subproject == sq.c.subproject, WfProcess.type == sq.c.type)).all()

    return [{_c.key: _row[_i] for _i, _c in enumerate(_cols)} for _row in _data]


