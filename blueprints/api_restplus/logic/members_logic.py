from icms_orm.cmspeople import Person, MemberActivity, Institute, MoData
import sqlalchemy as sa
import re
from datetime import date


class Arg(object):
    cms_id = 'cms_id'
    inst_code = 'inst_code'
    country = 'country'
    inst_status = 'inst_status'
    ind_status = 'ind_status'
    project = 'project'
    only_cms = 'only_cms'


def get_institutes_info(*args, **kwargs):
    q_cols = [Institute.code, Institute.name, Institute.country, Institute.cmsStatus]
    q = Institute.session().query(*q_cols)
    filter_funcs = {
        Arg.country: lambda _q: _q.filter(Institute.country.ilike('%%%s%%' % kwargs.get(Arg.country))),
        Arg.inst_code: lambda _q: _q.filter(Institute.code.ilike('%%%s%%' % kwargs.get(Arg.inst_code))),
        Arg.inst_status: lambda _q: _q.filter(Institute.cmsStatus.ilike('%%%s%%' % kwargs.get(Arg.inst_status)))
    }
    for key in kwargs.keys():
        if kwargs.get(key) is not None:
            q = filter_funcs.get(key)(q)
    return [{k.key: v for k, v in zip(q_cols, vals)} for vals in q.all()]
