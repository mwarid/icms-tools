from icms_orm.epr import TimeLineUser as TLU, Category
from datetime import datetime


def __categories():
    ssn = Category.session()
    if not hasattr(__categories, 'stored'):
        setattr(__categories, 'stored', {c.name: c.id for c in ssn.query(Category).all()})
    return getattr(__categories, 'stored')


def __q_cols():
    return TLU.ia_list() + [Category.name]


def __apply_tlu_query_filters(q, id, cms_id, inst_code, year):
    if id:
        q = q.filter(TLU.id == id)
    if cms_id:
        q = q.filter(TLU.cmsId == cms_id)
    if inst_code:
        q = q.filter(TLU.instCode == inst_code)
    if year:
        q = q.filter(TLU.year == year)
    return q


def __convert_payload_to_tlu_ia_dict(params):
    categories = __categories()
    param_name_map = {a.key: a for a in TLU.ia_list()}
    result = dict()
    for param_name, value in params.items():
        # get the param based on its name
        param = param_name_map.get(param_name)
        # process the value according to some pre-defined rules
        value = {
            TLU.category.key: lambda v: categories.get(v),
            # todo: hard-coded format that presently happens to be the same one as used by datetime model field
            TLU.timestamp.key: lambda v: datetime.strptime(v, '%Y-%m-%dT%H:%M:%S')}. \
            get(param_name,lambda v: v)(value)
        result[param] = value
    return result


def get_epr_time_lines(id=None, cms_id=None, inst_code=None, year=None):
    ssn = TLU.session()
    q = ssn.query(*__q_cols()).join(Category, Category.id == TLU.category).order_by(TLU.timestamp)
    q = __apply_tlu_query_filters(q, id, cms_id, inst_code, year)
    q = q.filter(TLU.yearFraction > 0)
    return [{col.key: value for col, value in zip(__q_cols(), row)} for row in q.all()]


def update_epr_time_line(id, cms_id, inst_code, year, params):
    ssn = TLU.session()
    q = ssn.query(TLU, Category).join(Category, TLU.category == Category.id).filter(TLU.id == id)
    q = __apply_tlu_query_filters(q, id, cms_id, inst_code, year)
    tlu, category = q.one()
    diff_set = {}
    for param, value in __convert_payload_to_tlu_ia_dict(params).items():
        if tlu.get(param) != value:
            diff_set[param.key] = value
            tlu.set(param, value)
    if diff_set:
        ssn.add(tlu)
        ssn.commit()
    return diff_set


def create_new_epr_time_line(params):
    ssn = TLU.session()
    entry = TLU.from_ia_dict(__convert_payload_to_tlu_ia_dict(params))
    ssn.add(entry)
    ssn.commit()
    result = entry.to_dict()
    result[Category.name.key] = {v:k for k, v in __categories().items()}.get(result.get(TLU.category.key))
    return result
