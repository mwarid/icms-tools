class QueryTools(object):
    @staticmethod
    def apply_filter_funcs(q, filter_funcs_dict, filter_values_dict):
        """
        :param q: the query
        :param filter_funcs_dict: dict {param_name: lambda q: q.filter(...)}
        :param filter_values_dict: dict of filter values received by the method
        :return:
        """
        for key in filter_values_dict.keys():
            if filter_values_dict.get(key) is not None:
                q = (filter_funcs_dict.get(key) or (lambda _q: _q))(q)
        return q

    @staticmethod
    def apply_join_funcs(q, join_funcs):
        for f in join_funcs:
            q = f(q)
        return q

    @staticmethod
    def result_as_list_of_maps(q, q_cols):
        return [{k.key: v for k, v in zip(q_cols, row)} for row in q.all()]
