from datetime import date
from typing import List
import warnings

import sqlalchemy as sa

from icms_orm.cmsanalysis import (
    ARC,
    ARCMember,
    ARCsMembersRel,
    AnalysisStateChanges,
    Analysis,
)
from icms_orm.cmspeople import Person

from blueprints.api_restplus.exceptions import BadRequestException


CADI_LINE_STATUSES = [
    "ACCEPT",
    "ARC-GreenLight",
    "AWG",
    "CWR",
    "CWR-ended",
    "Completed",
    "FinalReading",
    "Free",
    "GoingToPreApp",
    "Inactive",
    "PAS-PUB",
    "PAS-only-PUB",
    "PAS-readyForPUB",
    "PHYS-APP",
    "PRE-APP",
    "PUB",
    "Planned",
    "ReSubmitted",
    "ReadyForCWR",
    "ReadyForFR",
    "ReadyForSub",
    "RefComments",
    "SUB",
    "SUPERSEDED",
    "Started",
    "Thesis-Approved",
]


def get_arcs(ssn, page=1, per_page=10, desc=False):
    return (
        ssn.query(ARC)
        .order_by((desc and sa.desc or sa.asc)(ARC.id))
        .paginate(page, per_page, error_out=False)
        .items
    )


def get_arcs_by_awg_and_year(ssn, awg_name, year=None, page=1, per_page=10):
    q = ssn.query(ARC)
    if not year:
        q = q.filter(ARC.name.ilike("ARC-%s-%%" % awg_name))
    else:
        q = q.filter(ARC.name.ilike("ARC-%s-%d-%%" % (awg_name, year % 100)))
    return q.all()


def get_arc_by_analysis_code(ssn, code):
    return ssn.query(ARC).filter(ARC.name == ("ARC-%s" % code)).one()


def get_arc_members(ssn, page=1, per_page=10, awg=None, year=None, an_code=None):
    cols = [
        ARCMember.cmsId,
        ARCMember.status,
        ARCMember.propstatus,
        ARCMember.startDate,
        ARC.name,
        Person.lastName,
        Person.firstName,
    ]
    q = (
        ssn.query(*cols)
        .join(ARCsMembersRel, ARCMember.id == ARCsMembersRel.memberId)
        .join(ARC, ARC.id == ARCsMembersRel.id)
        .join(Person, Person.cmsId == ARCMember.cmsId)
    )

    pattern = "ARC-%s" % (
        an_code
        or "{awg}-{year}-%%".format(
            awg=(awg or "%%"), year=(year and year % 100 or "%%")
        )
    )
    q = q.filter(ARC.name.ilike(pattern))

    items = q.paginate(page, per_page, error_out=False).items
    items = [{c.key: item[i] for i, c in enumerate(cols)} for item in items]

    return items


def get_updated_analyses(
    year: int = None,
    awg: str = None,
    to_status: List[str] = None,
    current_status: List[str] = None,
    start_date: date = None,
    end_date: date = None,
):
    """Return the codes of the `awg`-`year` analyses whose status changed to `to_status`
    during the requested period"""
    query = (
        AnalysisStateChanges.session()
        .query(AnalysisStateChanges)
        .order_by(AnalysisStateChanges.changeDate.desc())
    )
    if year:
        if len(str(year)) != 4:
            raise BadRequestException(f"Invalid year: {year}")
        year_code = str(year)[2:]
    code_like = ""
    if year and awg:
        code_like = f"%{awg}-{year_code}-%"
    elif awg:
        code_like = f"%{awg}-%"
    elif year:
        code_like = f"%-{year_code}-%"
    if code_like:
        query = query.filter(AnalysisStateChanges.code.like(code_like))
    if start_date:
        query = query.filter(AnalysisStateChanges.changeDate >= start_date)
    if end_date:
        query = query.filter(AnalysisStateChanges.changeDate <= end_date)
    if to_status:
        for status in to_status:
            if status not in CADI_LINE_STATUSES:
                raise BadRequestException(f"Invalid status: {status}")
        query = query.filter(AnalysisStateChanges.newValue.in_(to_status))
    if current_status:
        for status in current_status:
            if status not in CADI_LINE_STATUSES:
                raise BadRequestException(f"Invalid status: {status}")
        query = query.join(Analysis, Analysis.id == AnalysisStateChanges.lineId).filter(
            Analysis.status.in_(current_status)
        )
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", message=r".*Truncated incorrect date.*")
        results = query.all()
    return results
