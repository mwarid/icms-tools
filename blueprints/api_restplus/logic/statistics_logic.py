from icms_orm.cmspeople import Person, Institute, User, MemberActivity
from icms_orm.common import PrehistoryTimeline, PersonStatus
from icms_orm.epr import TimeLineInst as TLI, TimeLineUser as TLU
from icmsutils.businesslogic import paperstats
import sqlalchemy as sa

def get_people_info():
    ssn = Person.session()
    q_cols = [Person.cmsId, Person.hrId, User.sex, Person.birthDate, Person.birthDate, Person.project]
    q = ssn.query(*q_cols).join(User, Person.cmsId == User.cmsId)
    results = q.all()
    results = [{k.key: v for (k, v) in zip(q_cols, row)} for row in results]
    return results


def get_people_status_info(date):
    PH = PrehistoryTimeline
    PS = PersonStatus
    ssn = PH.session()

    sq_tlu = ssn.query(TLU.isAuthor.label('epr_is_author'), TLU.cmsId.label('cms_id')).filter(sa.and_(TLU.year == date.year, TLU.timestamp <= date)).\
        order_by(TLU.cmsId, sa.desc(TLU.timestamp)).distinct(TLU.cmsId).subquery()

    q_cols = [PH.cms_id, PH.status_cms, PH.inst_code, PH.activity_cms, sa.func.coalesce(sq_tlu.c.epr_is_author, PS.is_author).label('is_author')]

    q = ssn.query(*q_cols).filter(PH.start_date <= date).filter(sa.or_(PH.end_date > date, PH.end_date == None)). \
        join(PS,
             sa.and_(PS.cms_id == PH.cms_id, PS.start_date <= date, sa.or_(PS.end_date == None, PS.end_date > date)),
             isouter=True). \
        join(sq_tlu, sq_tlu.c.cms_id == PH.cms_id, isouter=True)

    results = q.all()
    results = [{k.key: v for (k, v) in zip(q_cols, row)} for row in results]
    return results


def get_institutes_status_info(date):
    ssn = TLI.session()
    q_cols = [TLI.code, TLI.cmsStatus]
    # sq to find the latest time line for given year and preceeding the indicated date
    sq = ssn.query(TLI.code.label('ic'), sa.func.max(TLI.timestamp).label('ts')).\
        filter(TLI.year == date.year).filter(TLI.timestamp <= date).group_by(TLI.code).subquery()
    data = ssn.query(*q_cols).filter(TLI.year == date.year).join(sq,
            sa.and_(sq.c.ic == TLI.code, sq.c.ts == TLI.timestamp)).all()
    return [{k.key: v for (k, v) in zip(q_cols, row)} for row in data]


def get_institutes_info():
    ssn = Institute.session()
    q_cols = Institute.code, Institute.country, Institute.country
    results = q = ssn.query(*q_cols).all()
    results = [{k.key: v for (k, v) in zip(q_cols, row)} for row in results]
    return results


def get_authorship_stats(year):
    return [{'cmsId': key, 'authorLists': value.get(year)} for key, value in
            paperstats.signed_papers_count_map_by_cms_id_and_year(year=year).items()]
