import flask
from util import constants as const

api_blueprint = flask.Blueprint(
    const.BP_NAME_API, __name__, template_folder='templates')
