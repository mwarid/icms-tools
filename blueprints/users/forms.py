from flask_wtf import FlaskForm as Form
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired


class LoginPasswordForm(Form):
    login = StringField('Login', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Submit')


class LoginIgnorePasswordForm(LoginPasswordForm):
    password = PasswordField('Password', validators=[])
    # without overriding the submit ends up rendered between password and login - looks so-so
    submit = SubmitField('Submit (ignores password)')
