from typing import Optional
from webapp.webapp import Webapp
import flask
from flask.globals import current_app
from flask.wrappers import Request
from flask_login import LoginManager, UserMixin
from models.icms_people import Institute, Person, User as PersonUser, PersonHistory, MoData, PersonCernData, Project, ProjectManagers, PeopleFlagsAssociation, Flag as DbFlag
from models.icms_common import Person as NewPerson, Affiliation as NewAffiliation, PersonStatus as NewPersonStatus, InstituteLeader
from util import constants as const
from sqlalchemy import or_
from datetime import date
import sqlalchemy as sa
import logging
from icmsutils.businesslogic.flags import Flag
from icmsutils.ldaputils import LdapPerson, LdapProxy
from util.trivial import current_db_ssn
from blueprints.api_restplus.api_units import UserInfoApiCall
from icmsutils.funcutils import Cache
from blueprints.users.user_class import User
from util.structs import Credentials


users_blueprint = flask.Blueprint(
    const.BP_NAME_USERS, __name__, template_folder='templates', static_folder='static')
login_manager = LoginManager()
login_manager.login_message_category = const.FLASK_FLASH_WARNING


@login_manager.user_loader
@Cache.expiring(timeout=3 * 60)
def load_person(cms_id_or_login):
    logging.debug('Trying to reload a user with CMSid = %s [type %s]' % (
        str(cms_id_or_login), str(type(cms_id_or_login))))
    person = None
    if isinstance(cms_id_or_login, int) or isinstance(cms_id_or_login, str) and cms_id_or_login.isdigit():
        return User.create_from_api_info(cms_id_or_login)
    else:
        person = make_shell_person_from_ldap_data(login_id=cms_id_or_login)
    if person:
        return User(person=person)
    logging.warning('Loading user failed for CMSid = %s. Type of argument: %s' % (
        str(cms_id_or_login), str(type(cms_id_or_login))))
    return None


@login_manager.request_loader
def load_user_by_header_value(request: Request):
    header_name = current_app.config.get('AUTH_HEADER_NAME')
    if header_name in request.headers.keys() and request.headers.get(header_name):
        header_value = request.headers.get(header_name)
        logging.info(f'Logging based on header\'s value: {header_value}')
        user = get_user_for_credentials(Credentials.from_string(header_value))
        return user
    return None


def make_shell_person_from_ldap_data(ldap_user=None, login_id=None) -> Person:
    ldap_user = ldap_user or LdapProxy.get_instance(ldap_uri_override=flask.current_app.config.get(
        'LDAP_URI', None)).get_person_by_login(login_id, account_types=LdapProxy.AccountType.ANY)

    retval = Person.from_ia_dict({
        Person.lastName: ldap_user.familyName,
        Person.firstName: ldap_user.firstName or ldap_user.fullName.replace(ldap_user.familyName, '').strip(),
        Person.loginId: ldap_user.login,
        Person.niceLogin: ldap_user.login,
        Person.cmsId: -1 * ldap_user.hrId,
        Person.hrId: ldap_user.hrId,
        Person.status: 'NOTCMS'
    })
    assert isinstance(retval, Person)
    return retval


def is_bypass_enabled() -> bool:
    return True if flask.current_app.config[const.OPT_NAME_IGNORE_PASSWORD] else False


def obtain_credentials(headers, form) -> Optional[Credentials]:
    sso_token = headers.get('User', None)
    if sso_token:
        return Credentials.from_string(sso_token)
    else:
        if form.validate_on_submit():
            return Credentials.for_username_password(form.login.data, form.password.data)


def get_user_for_credentials(credentials: Credentials) -> User:
    """
    There are four possibilities, wrt to different loging and running modes:
    - header with bypass (unusual development combination, useful for testing though)
    - form with bypass (typical development configuration)
    - header without bypass (typical deployment production, no need for LDAP auth)
    - form without bypass (not actively used, could be set up for some development checks, needs LDAP authentication)
    While it masks the actual decision variables, email only comes with the header while username through the form.
    Email needs to be resolved to the username - in bypass mode it's just extracted from the string, otherwise LDAP is used.
    """
    ldap: LdapProxy = Webapp.current_app().get_ldap_proxy()
    ldap_person: Optional[LdapPerson] = None
    if credentials.email is not None:
        ldap_person = ldap.get_person_by_mail(
            credentials.email, account_types=LdapProxy.AccountType.ANY)
        credentials = Credentials(
            ldap_person.login, credentials.password, credentials.email)
    elif credentials.username is not None:
        ldap_person = ldap.get_person_by_login(
            credentials.username, account_types=LdapProxy.AccountType.ANY)
    # below is the check assuming that email parameter comes only from a TRUSTED header while login can come from a text input field
    if credentials.email is not None or ldap.authenticate(credentials.username, credentials.password):
        api_info = UserInfoApiCall.get_for_username(
            credentials.username, nullable=True)
        if api_info is not None:
            logging.debug(
                f'Creating user object from API info for {credentials.username}')
            return User.create_from_api_info(cms_id=None, api_info=api_info)
        logging.debug( f'Username {credentials.username} not found in iCMS. Continuing in guest mode. apiInfo: {api_info}')
        return User(person=make_shell_person_from_ldap_data(ldap_user=ldap_person), ldap_person=ldap_person)
