Dear {{ name }},

your account "{{ login }}" is now in the CMS (zh) group{{ removedFromOther }}.

It will still take a while for this info to propagate for the services needed, so please be patient.

Some services, like the ones which will give you access to the protected CMS twiki and CMS indico pages, will only be updated over night, so this will work only tomorrow (CERN time).

You can manage some properties of the account, as well as pass the obligatory security course and sign the Cern computing usage rules via the web:

   https://account.cern.ch/account

Thanks,
  cheers, andreas
