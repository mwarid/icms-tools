from blueprints.users.blueprint import users_blueprint, User, login_manager
from blueprints.users import views
from util import constants as const

login_manager.login_view = '%s.%s' % (const.BP_NAME_USERS, views.route_sign_in.__name__)
