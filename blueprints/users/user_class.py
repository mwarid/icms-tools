import logging
from datetime import date
from typing import Optional, Set
from webapp.webapp import Webapp

from icms_orm.toolkit import CmsWeek, RoomRequest, Status
import sqlalchemy as sa
from flask_login import UserMixin
import sqlalchemy

from blueprints.api_restplus.api_units import UserInfoApiCall
from icmsutils.businesslogic.flags import Flag
from icmsutils.funcutils import Cache
from icmsutils.ldaputils import LdapPerson, LdapProxy
from models.icms_common import Affiliation as NewAffiliation
from models.icms_common import InstituteLeader
from models.icms_common import Person as NewPerson
from models.icms_common import PersonStatus as NewPersonStatus
from models.icms_people import Flag as DbFlag
from models.icms_people import MoData, Person, PersonCernData, PersonHistory
from models.icms_people import User as PersonUser
from util import constants as const
from icmsutils.classutils import DictalikeMixin


class User(UserMixin, DictalikeMixin):
    @classmethod
    def create_from_api_info(cls, cms_id: Optional[int], api_info=None) -> "User":
        """
        Method added to ease the phasing out of the old DB - we'll create a mock structure to imitate the old DB
        entirely from the API call's response - which in turn uses only the new DB.
        """
        api_user_info = api_info or UserInfoApiCall.get_for_cms_id(cms_id)

        class EmulatedPerson(Person):
            """
            We need to implement lazy getters for some related entities that might need to be fetched from the old db
            """

            def __init__(self):
                super(EmulatedPerson, self).__init__()
                self._cernData = None
                self._modata = None
                self._user = None
                self._history = None

            @property
            def history(self):
                if self._history is None:
                    self._history = (
                        Person.session()
                        .query(PersonHistory)
                        .filter(PersonHistory.cmsId == self.cmsId)
                        .one()
                    )
                    Person.session().expunge(self._history)
                return self._history

            @property
            def modata(self):
                if self._modata is None:
                    self._modata = (
                        Person.session()
                        .query(MoData)
                        .filter(MoData.cmsId == self.cmsId)
                        .one()
                    )
                    Person.session().expunge(self._modata)
                return self._modata

            @property
            def cernData(self):
                if self._cernData is None:
                    self._cernData = (
                        Person.session()
                        .query(PersonCernData)
                        .filter(PersonCernData.cmsId == self.cmsId)
                        .one()
                    )
                    Person.session().expunge(self._cernData)
                return self._cernData

            @property
            def user(self):
                if self._user is None:
                    self._user = (
                        Person.session()
                        .query(PersonUser)
                        .filter(PersonUser.cmsId == self.cmsId)
                        .one()
                    )
                    Person.session().expunge(self._user)
                return self._user

        # creating a mock person object
        p = EmulatedPerson.from_ia_dict({Person.flags: []})

        for _dict_key, attr in [
            (NewPerson.cms_id.key, Person.cmsId),
            (NewPerson.hr_id.key, Person.hrId),
            (NewPerson.login.key, Person.loginId),
            (NewPerson.login.key, Person.niceLogin),
            (NewPerson.first_name.key, Person.firstName),
            (NewPerson.last_name.key, Person.lastName),
            (NewAffiliation.inst_code.key, Person.instCode),
            (NewPersonStatus.is_author.key, Person.isAuthor),
            (NewPersonStatus.status.key, Person.status),
        ]:
            p.set(attr, api_user_info[_dict_key])

        for _flag_code in api_user_info["flags"] or []:
            p.flags.append(
                DbFlag.from_ia_dict({DbFlag.id: _flag_code, DbFlag.desc: _flag_code})
            )

        _user = cls(person=p)
        _user.team_duties = api_user_info["team_duties"] or dict()
        _user._managed_unit_ids = api_user_info.get("managed_unit_ids", set())
        return _user

    def __init__(
        self,
        cms_id: int = None,
        person: Optional[Person] = None,
        ldap_person: Optional[LdapPerson] = None,
    ):
        self._person = None
        self._managed_unit_ids = set()

        if isinstance(person, Person):
            self._person = person
        elif cms_id:
            _api_info = UserInfoApiCall.get_for_cms_id(cms_id)
            _user = User.create_from_api_info(cms_id, _api_info)
            self._person = _user._person
        else:
            self._person = Person.from_ia_dict({})

        self._ldap_person = ldap_person
        self.team_duties = dict()
        self._led_member_cache = {}
        # WARNING:  the lists below are no longer populated as they incurred too much querying time while the features
        #           requiring them are not even used.
        self.managed_projects = []
        self.subedited_projects = []

    def get_id(self):
        return (
            self.person.cmsId is not None
            and self.person.cmsId > 0
            and self.person.cmsId
            or self.person.loginId
        )

    def is_cbi(self):
        return any([_v.lower() == "leader" for _v in self.team_duties.values()])

    def is_cbd(self):
        return any([_v.lower() == "deputy" for _v in self.team_duties.values()])

    def is_inst_representative(self):
        return self.is_cbi() or self.is_cbd()

    def get_represented_institutes_codes(self):
        return {_k: _v.lower() == "leader" for _k, _v in self.team_duties.items()}

    def is_leader_of_member(self, member_cms_id: int) -> bool:
        if not member_cms_id in self._led_member_cache:
            inst_codes_led = set(
                k
                for k, v in self.team_duties.items()
                if v.lower() in ("leader", "deputy")
            )
            if not inst_codes_led:
                return False
            today = date.today()
            member_inst_code = (
                NewAffiliation.session()
                .query(NewAffiliation.inst_code)
                .filter(NewAffiliation.cms_id == member_cms_id)
                .filter(NewAffiliation.is_primary.is_(True))
                .filter(NewAffiliation.start_date <= today)
                .filter(
                    sa.or_(
                        NewAffiliation.end_date.is_(None),
                        NewAffiliation.end_date > today,
                    )
                )
                .one()
            )
            self._led_member_cache[member_cms_id] = (
                member_inst_code[0] in inst_codes_led
            )
        return self._led_member_cache[member_cms_id]

    def get_members(self, item_list) -> list:
        members = []
        if self.is_inst_representative():
            current_user_codes = list(self.get_represented_institutes_codes().keys())
            cms_ids = [item["cms_id"] for item in item_list]

            today = date.today()
            members_query = (
                NewAffiliation.session()
                .query(NewAffiliation.cms_id)
                .filter(NewAffiliation.cms_id.in_(cms_ids))
                .filter(NewAffiliation.inst_code.in_(current_user_codes))
                .filter(NewAffiliation.is_primary.is_(True))
                .filter(NewAffiliation.start_date <= today)
                .filter(
                    sa.or_(
                        NewAffiliation.end_date.is_(None),
                        NewAffiliation.end_date >= today,
                    )
                )
                .all()
            )

            members = [member[0] for member in members_query]
        return members

    def get_flags(self):
        return self.person.flags

    def has_flag(self, flag_id):
        return flag_id in [flag.id for flag in self.person.flags]

    def is_admin(self):
        return self.has_flag(Flag.ICMS_ADMIN)

    def is_secretariat_member(self):
        return self.has_flag(Flag.ICMS_SECR)

    def is_mgt_board_member(self):
        return self.has_flag(Flag.MANAGEMENT_BOARD)

    def is_engagement_office_member(self):
        return self.has_flag(Flag.ENGAGEMENT_OFFICE)

    def is_spokesperson_team_member(self):
        return self.has_flag(Flag.FLAG_SPOKESPERSON_AND_DEPUTIES)

    def get_roles_mask(self):
        """deprecated?"""
        mask = 0x0
        if self.is_admin():
            mask += const.ROLE_ADMIN
        if self.is_cbi():
            mask += const.ROLE_CBI
        if self.is_cbd():
            mask += const.ROLE_CBD
        if self.is_secretariat_member():
            mask += const.ROLE_SECRETARIAT
        if self.is_mgt_board_member():
            mask += const.ROLE_MGT_BOARD
        return mask

    def is_project_manager(self):
        return len(self.managed_projects) > 0

    def get_managed_projects(self):
        return self.managed_projects

    def is_subeditor(self):
        return len(self.subedited_projects) > 0

    def get_subedited_projects(self):
        return self.subedited_projects

    def is_in_egroup(self, egroup_name):
        self._ldap_person = (
            self._ldap_person
            or Webapp.current_app()
            .get_ldap_proxy()
            .get_person_by_login(self._person.loginId)
        )
        if not self._ldap_person:
            self._ldap_person = (
                Webapp.current_app()
                .get_ldap_proxy()
                .get_person_by_login(
                    self._person.loginId, account_types=LdapProxy.AccountType.ANY
                )
            )
        if not self._ldap_person:
            return False
        return self._ldap_person.is_in_egroup(egroup_name)

    @property
    def is_diversity_office_chair(self):
        """Diversity office chair egroup members"""
        return self.is_in_egroup("cms-diversity-office-chair")

    @property
    def is_award_admin(self):
        """Award admins can perform restricted awards-related actions"""
        return (self.username in ["pfeiffer", "tbawej"]) or self.is_in_egroup(
            "cms-award-admins"
        )

    @property
    def is_job_nomination_committee(self):
        """Job Nomination Committee can perform restricted nomination-related actions"""
        return self.is_in_egroup("job-nomination-committee")

    @property
    def is_cms_jobop_admins(self):
        return self.is_in_egroup("cms-jobop-admins")

    @property
    def is_authenticated(self) -> bool:
        """Anyone with a username is considered to be authenticated upstream (SSO)"""
        return self.username is not None

    @property
    def is_cms(self) -> bool:
        return self.cms_id is not None and self.cms_id > 0

    @property
    def person(self) -> Optional[Person]:
        return isinstance(self._person, Person) and self._person or None

    @property
    def cms_id(self) -> Optional[int]:
        return isinstance(self._person, Person) and self._person.cmsId or None

    @property
    def username(self) -> Optional[str]:
        return (
            isinstance(self._person, Person)
            and (self._person.loginId or self._person.niceLogin)
            or isinstance(self._ldap_person, LdapPerson)
            and self._ldap_person.login
            or None
        )

    @property
    def inst_code(self) -> Optional[str]:
        return isinstance(self._person, Person) and self._person.instCode or None

    @property
    def flags(self):
        return set([f.id for f in self.get_flags()])

    @property
    def managed_unit_ids(self):
        return self._managed_unit_ids

    @property
    def team_subordinate_cms_ids(self):
        return _find_subordinate_cms_ids_set(self.cms_id)

    @property
    def owned_booking_ids(self):
        return _find_owned_booking_ids(self.cms_id)

    @property
    def represented_institutes(self):
        return self.get_represented_institutes_codes()


@Cache.expiring(timeout=10 * 60)
def _find_subordinate_cms_ids_set(leader_cms_id):
    ssn = NewAffiliation.session()
    _today = date.today()
    q = (
        ssn.query(NewAffiliation.cms_id)
        .join(InstituteLeader, NewAffiliation.inst_code == InstituteLeader.inst_code)
        .filter(NewAffiliation.is_primary == True)
        .filter(NewAffiliation.start_date <= _today)
        .filter(InstituteLeader.start_date <= _today)
        .filter(
            sa.or_(NewAffiliation.end_date == None, NewAffiliation.end_date > _today)
        )
        .filter(
            sa.or_(InstituteLeader.end_date == None, InstituteLeader.end_date > _today)
        )
        .filter(InstituteLeader.cms_id == leader_cms_id)
    )
    result = {x[0] for x in q.all()}
    logging.info(f"Found {len(result)} subordinates for {leader_cms_id}")
    return result


@Cache.expiring(timeout=3)
def _find_owned_booking_ids(cms_id: int) -> Set[int]:
    session = RoomRequest.session()
    q = session.query(RoomRequest.id).join(
        CmsWeek, CmsWeek.id == RoomRequest.cms_week_id
    )
    q = q.filter(CmsWeek.date >= date.today()).filter(
        RoomRequest.status.notin_([Status.DELETED, Status.CANCELLED, Status.DONE])
    )
    q = q.filter(
        sqlalchemy.or_(
            RoomRequest.cms_id_by == cms_id, RoomRequest.cms_id_for == cms_id
        )
    )
    found = set(r[0] for r in q.all())
    logging.info(f"Found {len(found)} bookings owned by {cms_id}")
    return found
