import abc
from typing import List, Set
from icmsutils.domain_object_model import VotingParticipant, VotingEntity, Person


class AbstractVotingListModel(abc.ABC):

    def __init__(self):
        self._voting_entities = []
        self._nearly_voting_entities = []

    def _add_voter(self, person: Person, entity: VotingEntity, votes: int = 1, remarks: str = None, delegate=None):
        assert isinstance(person, Person)
        assert isinstance(entity, VotingEntity)
        assert delegate is None or isinstance(delegate, Person)
        if votes == 0:
            self._add_near_voter(person, entity, votes, remarks, delegate)
        else:
            self._voting_entities.append(VotingParticipant(person, entity, votes, remarks, delegate))

    def _add_near_voter(self, person: Person, entity: VotingEntity, votes: int = 1, remarks: str = None, delegate=None):
        assert isinstance(person, Person)
        assert isinstance(entity, VotingEntity)
        assert delegate is None or isinstance(delegate, Person)
        self._nearly_voting_entities.append(VotingParticipant(person, entity, votes, remarks, delegate))

    @property
    def participants(self) -> List[VotingParticipant]:
        return self._voting_entities

    @property
    def near_participants(self) -> List[VotingParticipant]:
        return self._nearly_voting_entities

    @property
    def weighted_votes(self):
        return len({v.votes for v in self.participants}) > 1

    @property
    @abc.abstractmethod
    def voting_date(self):
        pass

    @property
    @abc.abstractmethod
    def list_closing_date(self):
        pass

    def is_voting(self, cms_id):
        return cms_id in self.get_voting_cms_ids()

    def get_voting_cms_ids(self) -> Set[int]:
        result: Set[int] = set()
        for _p in self.participants:
            assert isinstance(_p, VotingParticipant)
            result.add((_p.delegate or _p.person).cms_id)
        return result

    def get_entitled_cms_ids(self) -> Set[int]:
        """
        Ignores delegations
        """
        result = set()
        for _p in self.participants:
            assert isinstance(_p, VotingParticipant)
            result.add(_p.person.cms_id)
        return result

    def is_entitled(self, cms_id):
        return cms_id in self.get_entitled_cms_ids()
