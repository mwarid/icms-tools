from sqlalchemy.orm import aliased
from blueprints.voting.voting_lists.abstract_voting_list_model import AbstractVotingListModel
from blueprints.voting.voting_lists.info_providers import VotingInfoProvider
from icms_orm.toolkit import Voting, VotingMerger as VM, VotingMergerMember as VMM, VotingListEntry as VLE
from icms_orm.common import Person as DbPerson, Institute as DbInst, Country
from icms_orm.cmspeople import Institute as OldDbInst, Person as OldDbPerson, InstStatusValues
from icmsutils.domain_object_model import Institute, Person, VotingEntity, VotingCluster, VotingInstitute, VotingCountry
import sqlalchemy as sa


class InstInfoProvider(object):
    """
    This one exists as a separate class so that we can keep using the country naming like in the old DB
    """
    def __init__(self):
        q = OldDbInst.session().query(OldDbInst.code, OldDbInst.shortName, OldDbInst.country).\
            filter(OldDbInst.cmsStatus == InstStatusValues.YES)
        self._data = {code: Institute(code=code, name=name, country=country) for code, name, country in q.all()}

    def get_inst_by_code(self, code: str) -> Institute:
        return self._data.get(code)


class PersistedVotingListModel(AbstractVotingListModel):

    def __init__(self, voting_code):
        """
        With the right magic commands to sqlalchemy all the necessary entities could probably be loaded with just one q
        :param voting_code: the code
        """
        super().__init__()
        self._voting_info = VotingInfoProvider(code=voting_code)
        self._inst_info = InstInfoProvider()

        ssn = Voting.session()
        Delegator = aliased(DbPerson)

        _sq = VM.session().query(VMM.merger_id.label('merger_id'), sa.func.json_agg(VMM.inst_code).label('codes')).group_by(VMM.merger_id).subquery()

        _data = ssn.query(VLE, DbPerson, Delegator, VM, _sq.c.codes).\
            join(DbPerson, VLE.cms_id == DbPerson.cms_id).\
            join(Delegator, VLE.delegated_by_cms_id == Delegator.cms_id, isouter=True).\
            join(VM, VLE.represented_merger_id == VM.id, isouter=True).\
            join(_sq, VM.id == _sq.c.merger_id, isouter=True).\
            filter(VLE.code == voting_code).\
            all()

        for vle, db_person, delegator, vm, clustered_inst_codes in _data:
            assert isinstance(vle, VLE)
            assert delegator is None or isinstance(delegator, DbPerson)
            assert vm is None or isinstance(vm, VM)

            person = Person.from_data_object(delegator or db_person)
            entity = None
            delegate = Person.from_data_object(db_person) if delegator is not None else None

            if vle.represented_inst_code is not None:
                entity = VotingInstitute(institute=self._inst_info.get_inst_by_code(vle.represented_inst_code))
            elif clustered_inst_codes:
                entity = VotingCluster(*[self._inst_info.get_inst_by_code(code) for code in clustered_inst_codes])
            else:
                entity = VotingCountry(country=vm.country)

            self._add_voter(person=person, entity=entity, remarks=vle.remarks, votes=int(vle.weight), delegate=delegate)

    @property
    def list_closing_date(self):
        return self._voting_info.list_closing_date

    @property
    def voting_date(self):
        return self._voting_info.voting_date



