import flask
import logging
from .blueprint import dashboard_blueprint

log: logging.Logger = logging.getLogger(__name__)


@dashboard_blueprint.route('/cms/tenures_by_unit', methods=['GET'])
def tenures_by_unit():
    return flask.render_template('_stripped_down_base.html')
