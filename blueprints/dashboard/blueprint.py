from flask import Blueprint
from util import constants as const

dashboard_blueprint = Blueprint(const.BP_NAME_DASHBOARD, __name__, template_folder='templates', static_folder='static')