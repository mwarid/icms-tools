# -*- coding: utf-8 -*-
from datetime import date, datetime, timedelta

import flask
import logging
from flask_login import current_user
from flask_login import login_required
from icms_orm.cmsanalysis import PaperAuthor, PaperAuthorHistory
from icms_orm.cmspeople import Person
from icmsutils.businesslogic.flags import Flag
from icmsutils.ldaputils import LdapProxy
from icmsutils.prehistory import PersonAndPrehistoryWriter
from sqlalchemy import func
from sqlalchemy import or_, and_

from .blueprint import admin_blueprint
from blueprints.users import views as user_views
from util import constants as const
from util import decorators
from util.structs import Table, Link, PreformatedText, Checkbox, InputField


@admin_blueprint.route('/checkDateEndSign')
@login_required
@decorators.any_flag_required([Flag.ICMS_ADMIN, Flag.AUTHORSHIP_BOARD, Flag.ICMS_SECR])
def route_check_date_end_sign():
    statuses = ['EXMEMBER', 'CMSEXTENDED']
    ssn = PaperAuthor.session()
    sq = ssn.query(PaperAuthor.cmsid.label('cmsId'), func.max(func.str_to_date(PaperAuthorHistory.date, '%d/%m/%Y')).
                   label('last_sign_date'))\
        .join(PaperAuthorHistory, PaperAuthor.code == PaperAuthorHistory.code)\
        .join(Person, Person.cmsId == PaperAuthor.cmsid).filter(Person.status.in_(statuses))\
        .filter(PaperAuthorHistory.action == 'ListGen').filter(PaperAuthor.status.ilike('%in%')).\
        group_by(PaperAuthor.cmsid).subquery()

    t_0 = date(2010, 1, 1)
    rows = ssn.query(Person.cmsId, Person.hrId, Person.lastName, Person.firstName, Person.status,
                     Person.dateEndSign, Person.dateCreation, Person.remarks, Person.remarksSecr,
                     Person.isAuthor, Person.exDate,
                     sq.c.last_sign_date).filter(Person.status.in_(statuses)). \
        filter(or_(and_(Person.isAuthor2009 == 1, Person.dateCreation < t_0), Person.status == 'CMSEXTENDED')).\
        filter(Person.exDate != None).filter(func.str_to_date(Person.exDate, '%Y-%m-%d') >
                                             date(year=date.today().year-5, month=date.today().month, day=date.today().day)).\
        outerjoin(sq, Person.cmsId == sq.c.cmsId).all()
    logging.debug('Found %d potentially affected people' % len(rows))

    td_recoverable = Table.get_empty(
        keys=['Person', 'e-Groups', 'Date Start', 'Date Exit', 'Last Sign',  'End Sign', 'Computed End', 'Extra Months', 'Discrepancy', 'Remarks'])
    td_outdated = Table.get_empty(keys=[k for k in td_recoverable.keys])
    td_correct = Table.get_empty(keys=[k for k in td_recoverable.keys])
    td_not_signing_anyway = Table.get_empty(
        keys=[k for k in td_recoverable.keys])

    td_recoverable.keys.append('Fix')

    hrids = {getattr(row, Person.hrId.key) for row in rows}
    ldap_info = LdapProxy.get_instance(ldap_uri_override=flask.current_app.config.get(
        'LDAP_URI', None)).find_people_by_hrids(hrids)
    egroup_info = dict()
    for person in ldap_info:
        egroup_info[person.hrId] = egroup_info.get(person.hrId, set())
        for egroup in person.cernEgroups + person.cmsEgroups:
            if egroup in {'zp', 'z2', 'z5'}:
                egroup_info[person.hrId].add(egroup)

    hidden_details = {}

    # just to make sure the object from the loop above is not accidentally accessed later
    person = None

    for row in rows:

        end_sign, start_date, remarks, remarks_secr, cms_id, hr_id, last_name, first_name, is_author = (getattr(row, x.key) for x in
                                                                                                        [Person.dateEndSign, Person.dateCreation, Person.remarks, Person.remarksSecr, Person.cmsId, Person.hrId, Person.lastName, Person.firstName, Person.isAuthor])

        end_sign = None if end_sign == '0000-00-00' else end_sign
        assert end_sign is None or isinstance(
            end_sign, date), 'Date end sign is something weird: {0}'.format(end_sign)

        last_sign_date = row.last_sign_date
        exit_date = (lambda x: x and datetime.strptime(
            x, '%Y-%m-%d').date())(getattr(row, Person.exDate.key))
        start_date = start_date.date()
        days_before = (t_0 - start_date).days
        extra_months = max(min(4 * days_before // 365, 48), 0)
        projected_end = exit_date + \
            timedelta(round((12 + extra_months) * 365 // 12))

        table = (not last_sign_date or (exit_date - last_sign_date).days > 21) and td_not_signing_anyway \
            or end_sign and abs((end_sign - projected_end).days) < 30 and td_correct \
            or projected_end >= date.today() and td_recoverable \
            or td_outdated

        if remarks or remarks_secr:
            details = {}
            if remarks:
                details['Remarks'] = PreformatedText(remarks)
            if remarks_secr:
                details['Secretariat Remarks'] = PreformatedText(remarks_secr)
            hidden_details['remarks_%d' % cms_id] = details

        table.vals.append({
            'Person': Link.link_to_user_profile(cms_id=cms_id, label='%s %s [#%d]' % (
                last_name, first_name, cms_id)),
            'Author?': is_author,
            'e-Groups': ', '.join(egroup_info.get(hr_id, {})),
            'Date Start': Link(label=start_date, href=flask.url_for(
                '%s.%s' % (const.BP_NAME_USERS, user_views.route_modal_history.__name__), cms_id=cms_id),
                type=Link.Type.MODAL_PASSIVE),
            'Date Exit': exit_date,
            'Last Sign': last_sign_date,
            'End Sign': end_sign,
            'Computed End': table != td_recoverable and projected_end or InputField(type='date',
                                                                                    name='end_sign_%d' % cms_id,
                                                                                    value=projected_end),
            'Extra Months': extra_months,
            'Discrepancy': (projected_end - (end_sign or exit_date + timedelta(days=365))).days / (365 / 12.0),
            'Remarks': (remarks or remarks_secr) and Link.link_reveal_hidden(
                hidden_element_id='remarks_%d' % cms_id, label='show', title='Remarks for %s %s' %
                (first_name, last_name)) or None
        })

        if 'Fix' in table.keys:
            table.vals[-1]['Fix'] = Checkbox(name='fix_%d' %
                                             cms_id, label='Fix')

    td_recoverable.form_action = flask.url_for('%s.%s' % (
        const.BP_NAME_ADMIN, route_fix_date_end_sign.__name__))
    return flask.render_template('generic_page.html', title='DateEndSign check',
                                 table_data={'Fixable': td_recoverable, 'Outdated': td_outdated, 'Correct': td_correct,
                                             'NotSigning': td_not_signing_anyway}, hidden_data=hidden_details)


@admin_blueprint.route('/fix_date_end_sign', methods=['POST'])
@login_required
@decorators.any_flag_required([Flag.ICMS_ADMIN, Flag.AUTHORSHIP_BOARD, Flag.ICMS_SECR])
def route_fix_date_end_sign():
    form = flask.request.form
    new_dates = {}
    for field, value in form.items():
        if field.startswith('fix_') and field[4:].isdigit():
            cms_id = int(field[4:])
            new_dates[cms_id] = datetime.strptime(
                form['end_sign_%d' % cms_id], '%Y-%m-%d').strftime('%Y-%m-%d')

    affected_people = {p.cmsId: p for p in Person.session().query(
        Person).filter(Person.cmsId.in_(new_dates.keys())).all()}

    for cms_id, date_end in new_dates.items():
        pw = PersonAndPrehistoryWriter(Person.session(), actor_person=current_user.person,
                                       object_person=affected_people[cms_id])
        pw.set_new_value(Person.dateEndSign, date_end)
        pw.apply_changes(do_commit=False)

    Person.session().commit()
    flask.flash(
        'New DateEndSign was set for %d %s.' % (len(affected_people), len(
            affected_people) < 2 and 'person' or 'people'),
        const.FLASK_FLASH_SUCCESS)

    return flask.redirect(flask.url_for('%s.%s' % (const.BP_NAME_ADMIN, route_check_date_end_sign.__name__)))


@admin_blueprint.route('/cds_papers')
@login_required
@decorators.admin_required
def route_cds_papers():
    from icmsutils.businesslogic.cadi import get_cds_papers
    data = get_cds_papers()
    return flask.render_template('generic_page.html', title='What\'s up CDS?', data_maps={('CDS papers', 'cds_papers'): data})
