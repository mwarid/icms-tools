from flask.app import Flask
import pytest
from tests.fixtures.basic_fixtures import app
from tests.fixtures.fakedata_fixtures import FakePerson
from flask import Flask
from datetime import date, datetime


@pytest.fixture(scope="class")
def check_applications(app: Flask, request):
    from scripts import check_authorship_application
    params_dict = request.param
    check_authorship_application.main(**params_dict)

