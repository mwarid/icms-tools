from typing import Dict
from flask.testing import FlaskClient
from flask import current_app
import os
import logging
from flask.wrappers import Response

log: logging.Logger = logging.getLogger(__name__)


class IcmsTestClient(FlaskClient):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dump_counter = 0

    def post(self, *args, **kwargs):
        response = super().post(*args, **kwargs)
        response_dump_dir = current_app.config.get('TEST_RESPONSE_DUMP_DIR')
        if response_dump_dir is not None:
            self.__dump_counter += 1
            with open(file=os.path.join(response_dump_dir, 'sample{:03d}.html'.format(self.__dump_counter)), mode='wb+') as dst:
                dst.write(response.data)
        return response

    def request(self, path=None) -> 'RequestBuilder':
        return RequestBuilder(client=self, path=path)


class RequestBuilder():
    def __init__(self, client: FlaskClient, path: str = None):
        self._client = client
        self._path = path or '/'
        self._method = 'GET'
        self._headers = dict()
        self._payload = dict()
        self._follow_redirects = True
        self._form_data = None

    def follow_redirects(self, value: bool) -> 'RequestBuilder':
        self._follow_redirects = value
        return self

    def path(self, path: str) -> 'RequestBuilder':
        self._path = path
        return self

    def header(self, key: str, value: str) -> 'RequestBuilder':
        self._headers[key] = value
        return self

    def auth_header(self, value: str) -> 'RequestBuilder':
        headername = self._client.application.config.get('AUTH_HEADER_NAME')
        return self.header(headername, value)

    def headers(self, headers: Dict[str, str]) -> 'RequestBuilder':
        self._headers = headers
        return self

    def payload(self, payload: Dict[str, object]) -> 'RequestBuilder':
        self._payload = payload
        return self

    def form_data(self, data: Dict[str, object]) -> 'RequestBuilder':
        self._form_data = data
        return self

    def perform(self) -> Response:
        """
        https://werkzeug.palletsprojects.com/en/1.0.x/test/#werkzeug.test.EnvironBuilder
        class werkzeug.test.EnvironBuilder(
            path='/',
            ase_url=None, query_string=None,
            method='GET', input_stream=None,
            content_type=None, content_length=None,
            errors_stream=None, multithread=False,
            multiprocess=False, run_once=False,
            headers=None, data=None,
            environ_base=None,
            environ_overrides=None, charset='utf-8',
            mimetype=None, json=None)¶

        Additional parameters introduced by Flask:

    as_tuple – Returns a tuple in the form (environ, result)
    buffered – Set this to True to buffer the application run. This will automatically close the application for you as well.
    follow_redirects – Set this to True if the Client should follow HTTP redirects.

        """
        result: Response = self._client.open(path=self._path, method=self._method,
                                             headers=self._headers, follow_redirects=self._follow_redirects,
                                             **({'data': self._form_data} if self._form_data else {'json': self._payload}))
        if result.json:
            log.info(f'Response yields JSON: {result.json}')
        return result

    def post(self) -> Response:
        self._method = 'POST'
        return self.perform()

    def get(self) -> Response:
        self._method = 'GET'
        return self.perform()

    def put(self) -> Response:
        self._method = 'PUT'
        return self.perform()

    def delete(self) -> Response:
        self._method = 'DELETE'
        return self.perform()

    def patch(self) -> Response:
        self._method = 'PATCH'
        return self.perform()

