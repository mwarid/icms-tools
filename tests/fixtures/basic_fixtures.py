import os
import tempfile
from typing import Dict, List, Optional
import pytest
from app_profiles import ProfileTest
import flask
from flask import Flask
from flask.testing import FlaskClient
import logging
from icmsutils.icmstest import ddl
from icmscommon.governors import DbGovernor
from .testkit import IcmsTestClient
import icms_orm
import webapp
import time


class TestappSingletonFactory(object):
    """
    It looks like the session-scoped fixture actually runs multiple times (virtue of importing perhaps)
    so the caching below has been put in place to prevent weird lockups.
    """
    __the_app = None

    @classmethod
    def get_web_app(cls) -> Optional[Flask]:
        if cls.__the_app is None:
            cls.__the_app = webapp.Webapp(
                __name__, root_path='./webapp', profile=ProfileTest)
            cls.__the_app.test_client_class = IcmsTestClient

        return cls.__the_app


@pytest.fixture(scope='session')
def app() -> Optional[Flask]:
    yield TestappSingletonFactory.get_web_app()


@pytest.fixture()
def client(app: Flask) -> FlaskClient:
    with app.test_client() as client:
        yield client


@pytest.fixture()
def patch_app_config(app: Flask, request) -> Flask:
    """
    Takes a dictionary of parameters to be overwritten in application's config (passed through request.param, virtue of indirect parameterization)
    Restores the initial values upon completion
    """
    params_to_patch: Dict[str, str]
    params_to_patch = request.param
    params_backup = dict()
    for key, value in params_to_patch.items():
        params_backup[key] = app.config[key]
        app.config[key] = value
    yield app
    for key, value in params_backup.items():
        app.config[key] = value


@pytest.fixture()
def sign_in(client: FlaskClient, request):
    yield client.post('/users/signin', data=dict(login=request.param), follow_redirects=True)
    client.get('users/signout', follow_redirects=True)


@pytest.fixture(scope='class')
def reset_db_people():
    ddl.recreate_people_db()


@pytest.fixture(scope='class')
def reset_db_icms_public():
    ddl.recreate_common_db()


@pytest.fixture(scope='class')
def reset_db_toolkit():
    ddl.recreate_toolkit_db()


@pytest.fixture(scope='class')
def reset_old_notes_dbs():
    ddl.recreate_old_notes_dbs()


@pytest.fixture(scope='class')
def reset_old_cadi_db():
    ddl.recreate_old_cadi_db()


@pytest.fixture(scope='class')
def reset_dbs_icms():
    manager = DbGovernor.get_db_manager()
    manager.drop_all(bind=icms_orm.epr_bind_key())
    manager.drop_all(bind=icms_orm.toolkit_bind_key())
    manager.drop_all(bind=icms_orm.cms_common_bind_key())
    manager.create_all(bind=icms_orm.cms_common_bind_key())
    manager.create_all(bind=icms_orm.toolkit_bind_key())
    manager.create_all(bind=icms_orm.epr_bind_key())


@pytest.fixture(scope='class')
def recycle_dbs(app) -> float:
    bind_keys: List[str]
    bind_keys = [
        icms_orm.epr_bind_key(),
        icms_orm.toolkit_bind_key(),
        icms_orm.cms_common_bind_key(),
        icms_orm.cms_people_bind_key(),
    ]
    manager = DbGovernor.get_db_manager()
    for _bk in bind_keys:
        manager.drop_all(bind=_bk)
    for _bk in reversed(bind_keys):
        manager.create_all(bind=_bk)
    return time.time()
