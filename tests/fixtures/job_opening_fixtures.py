from typing import List
from dataclasses import dataclass, field
from icms_orm.cmspeople.people import MoData, User
from icms_orm.toolkit import JobOpening, JobQuestionnaire, JobOpenPosition
from icms_orm.common import Position, OrgUnit, OrgUnitType, Person
from icms_orm.common.common_schema_views import PersonView
from icms_orm.custom_classes import PseudoEnum
import pytest
from icmsutils.businesslogic.flags import Flag as FlagDef
from datetime import date, datetime
import time
from tests.fixtures.tenures_fixtures import (
    FakePositions,
    FakeOrgUnitTypes,
    FakeOrgUnits,
    FakeOrgUnitTypes,
)
from tests.fixtures.fakedata_fixtures import FakePerson
from util import constants as const

@dataclass
class FakeJobQuestionnaire:
    id: int
    title: str
    questions: str
    date_created: datetime
    date_updated: datetime


class FakeJobQuestionnaires(PseudoEnum):
    questionnaire1 = FakeJobQuestionnaire(
        4, "Questionnaire One", "Questions", datetime.utcnow(), datetime.utcnow()
    )
    questionnaire2 = FakeJobQuestionnaire(
        5, "Questionnaire Two", "Questions", datetime.utcnow(), datetime.utcnow()
    )
    questionnaire3 = FakeJobQuestionnaire(
        6, "Questionnaire Three", "Questions", datetime.utcnow(), datetime.utcnow()
    )
    questionnaire4 = FakeJobQuestionnaire(
        7, "Questionnaire Four", "Questions", datetime.utcnow(), datetime.utcnow()
    )


@dataclass
class FakeJobOpening:
    id: int
    title: str
    description: str
    requirements: str
    positions: list
    job_unit_id: int
    questionnaire_id: int
    start_date: datetime
    end_date: datetime
    nominations_deadline: datetime
    status: str
    comment: str
    days_for_nominee_response: int
    nominee_no_reply: str
    send_mail_to_nominee: bool
    questionnaire_uri: str


class FakeJobOpenings(PseudoEnum):
    jobopening1 = FakeJobOpening(
        4,
        "job opening1",
        "Job as Spokesperson - not as easy as it looks ...",
        "Excellent management and communication skills",
        42,
        17,
        4,
        datetime.utcnow(),
        datetime.utcnow(),
        datetime.utcnow(),
        const.JOB_STATUS_ACTIVE,
        "Spokesperson response",
        5,
        None,
        True,
        "Questionnaire link1",
    )
    jobopening2 = FakeJobOpening(
        5,
        "job opening2",
        "Job as Spokesperson - not as easy as it looks ...",
        "Excellent management and communication skills",
        42,
        17,
        5,
        datetime.utcnow(),
        datetime.utcnow(),
        datetime.utcnow(),
        const.JOB_STATUS_ACTIVE,
        "Spokesperson response",
        3,
        None,
        True,
        "Questionnaire link2",
    )
    jobopening3 = FakeJobOpening(
        6,
        "job opening3",
        "Job as Spokesperson - not as easy as it looks ...",
        "Excellent management and communication skills",
        42,
        17,
        6,
        datetime.utcnow(),
        datetime.utcnow(),
        datetime.utcnow(),
        const.JOB_STATUS_ACTIVE,
        "Spokesperson response",
        2,
        None,
        True,
        "Questionnaire link3",
    )
    jobopening4 = FakeJobOpening(
        7,
        "job opening4",
        "Job as Spokesperson - not as easy as it looks ...",
        "Excellent management and communication skills",
        42,
        17,
        7,
        datetime.utcnow(),
        datetime.utcnow(),
        datetime.utcnow(),
        const.JOB_STATUS_ACTIVE,
        "Spokesperson response",
        2,
        None,
        True,
        "Questionnaire link4",
    )


@dataclass
class FakeJobOpenPosition:
    id: int
    position_id: int
    job_unit_id: int
    job_opening_id: int    
    status: str
    start_date: datetime
    end_date: datetime
    nominations_deadline: datetime


class FakeJobOpenPositions(PseudoEnum):
    jobopenposition1 = FakeJobOpenPosition(1, 42, 17, 4, const.JOB_STATUS_ACTIVE, datetime.utcnow(), datetime.utcnow(), datetime.utcnow())
    jobopenposition2 = FakeJobOpenPosition(2, 42, 17, 5, const.JOB_STATUS_ACTIVE, datetime.utcnow(), datetime.utcnow(), datetime.utcnow())
    jobopenposition3 = FakeJobOpenPosition(3, 42, 17, 6, const.JOB_STATUS_ACTIVE, datetime.utcnow(), datetime.utcnow(), datetime.utcnow())
    jobopenposition4 = FakeJobOpenPosition(4, 42, 17, 7, const.JOB_STATUS_ACTIVE, datetime.utcnow(), datetime.utcnow(), datetime.utcnow())


@pytest.fixture(scope="class")
def insert_job_opening():
    ssn = JobOpening.session()
    jobopening: FakeJobOpening
    for jobopening in FakeJobOpenings.values():
        db_job_opening = JobOpening.from_ia_dict(
            {
                JobOpening.id: jobopening.id,
                JobOpening.title: jobopening.title,
                JobOpening.description: jobopening.description,
                JobOpening.requirements: jobopening.requirements,
                JobOpening.start_date: jobopening.start_date,
                JobOpening.end_date: jobopening.end_date,
                JobOpening.nominations_deadline: jobopening.nominations_deadline,
                JobOpening.questionnaire_id: jobopening.questionnaire_id,
                JobOpening.status: jobopening.status,
                JobOpening.comment: jobopening.comment,
                JobOpening.days_for_nominee_response: jobopening.days_for_nominee_response,
                JobOpening.nominee_no_reply: jobopening.nominee_no_reply,
                JobOpening.send_mail_to_nominee: jobopening.send_mail_to_nominee,
                JobOpening.questionnaire_uri: jobopening.questionnaire_uri,
            }
        )
        ssn.add(db_job_opening)
    ssn.commit()


@pytest.fixture(scope="class")
def insert_questionnaire():
    ssn = JobQuestionnaire.session()
    questionnaire: FakeJobQuestionnaire
    for questionnaire in FakeJobQuestionnaires.values():
        db_job_questionnaire = JobQuestionnaire.from_ia_dict(
            {
                JobQuestionnaire.id: questionnaire.id,
                JobQuestionnaire.title: questionnaire.title,
                JobQuestionnaire.questions: questionnaire.questions,
                JobQuestionnaire.date_created: questionnaire.date_created,
                JobQuestionnaire.date_updated: questionnaire.date_updated,
            }
        )
        ssn.add(db_job_questionnaire)
    ssn.commit()


@pytest.fixture(scope="class")
def insert_job_open_position():
    ssn = JobOpenPosition.session()
    jobopenposition: FakeJobOpenPosition
    for jobopenposition in FakeJobOpenPositions.values():
        db_job_open_position = JobOpenPosition.from_ia_dict(
            {
                JobOpenPosition.id: jobopenposition.id,
                JobOpenPosition.position_id: jobopenposition.position_id,
                JobOpenPosition.job_unit_id: jobopenposition.job_unit_id,
                JobOpenPosition.job_opening_id: jobopenposition.job_opening_id,
                JobOpenPosition.status: jobopenposition.status,
                JobOpenPosition.start_date: jobopenposition.start_date,
                JobOpenPosition.end_date: jobopenposition.end_date,
                JobOpenPosition.nominations_deadline: jobopenposition.nominations_deadline,                
                
            }
        )
        ssn.add(db_job_open_position)
    ssn.commit()
