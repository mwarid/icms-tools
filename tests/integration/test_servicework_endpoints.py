from icms_orm.common.common_schema_misc_tables import ApplicationAsset
from icms_orm.epr.epr_schema_models import Category, EprInstitute
import pytest
import json
import logging
from io import BytesIO
from typing import Any, Dict, List, Optional
from flask.wrappers import Response
from icms_orm import PseudoEnum
from mockito import unstub, when
from mockito.matchers import captor
from requests import Response as ReqResponse
from requests import Session
from requests.sessions import PreparedRequest
from tests.fixtures.fakedata_fixtures import FakeActivities, FakeActivity, FakeInst, FakeInsts, FakePeople, FakePerson
from tests.fixtures.testkit.test_client import IcmsTestClient, RequestBuilder
from icmsutils.icmstest.assertables import count_in_db
from icms_orm.epr import EprUser, TimeLineUser
from icmsutils.businesslogic.servicework import AssetNames

log: logging.Logger = logging.getLogger(__name__)


@pytest.fixture(scope='class')
def insert_epr_dues(populate_dbs):
    person: FakePerson
    inst: FakeInst
    activity: FakeActivity

    ApplicationAsset.session.add(ApplicationAsset.from_ia_dict({
        ApplicationAsset.name: AssetNames.EPR_AUTHOR_REDUCTION_REASONS,
        ApplicationAsset.application: None,
        ApplicationAsset.data: [{"value": "phd", "label": "PHD"}, {"value": "maternity", "label": "Maternity"}, {"value": "paternity", "label": "Paternity"}, {"value": "parenting", "label": "Part time work due to parenting"}, {"value": "mo_ab", "label": "M & O A / B"}, {"value": "convener_l012_pog_pag", "label": "L0 / L1 / L2 / POG / PAG"}, {"value": "illness", "label": "Serious illness"}, {"value": "force_majeure", "label": "Force majeure"}, {"value": "other", "label": "Other"}],
    }))

    for activity in FakeActivities.values():
        Category.session.add(
            Category(activity.name, activity.old_id in [2, 3, 6, 9]))

    epr_institutes: List[EprInstitute] = []
    for inst in FakeInsts.values():
        epr_institutes.append(EprInstitute(len(epr_institutes) + 13, inst.name, inst.country.name,
                                           inst.code, inst.status.name, 1986, 'We are unreal!'))
        EprInstitute.session.add(epr_institutes[-1])
    EprInstitute.session.commit()
    # The ids should now be in place

    inst_map: Dict[str, EprInstitute] = {i.code: i for i in epr_institutes}
    log.info(f'Voila, the inst map: {inst_map}')
    for person in FakePeople.values():

        epr_inst: EprInstitute = inst_map[person.inst.code]
        u = EprUser(person.login, person.last_name, person.hr_id, person.cms_id, epr_inst.id, person.is_author,
                    person.status, person.is_suspended, person.activity.name, 1986, epr_inst.id, 'I am not real!', None)
        tlu = TimeLineUser.from_ia_dict({
            TimeLineUser.dueAuthor: 4,
            TimeLineUser.dueApplicant: 0,
            TimeLineUser.year: 2020,
            TimeLineUser.yearFraction: 1,
            TimeLineUser.instCode: person.inst.code,
            TimeLineUser.cmsId: person.cms_id,
        })
        TimeLineUser.session.add(u)
        TimeLineUser.session.flush()
        TimeLineUser.session.add(tlu)

    TimeLineUser.session.commit()


class TestUri(PseudoEnum):
    epr_waiver = '/restplus/epr/waive_author_due'
    corrections = '/restplus/epr/due_corrections'


@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs', 'insert_epr_dues')
class TestServiceWorkEndpoints():
    @pytest.mark.parametrize('officer, beneficiary', [(FakePeople.tbawej, FakePeople.grossi)])
    def test_applying_epr_reduction(self, client: IcmsTestClient, officer: FakePerson, beneficiary: FakePerson):
        correction_payload = {
            'cms_id': beneficiary.cms_id,
            'year': 2020,
            'inst_code': beneficiary.inst.code,
            'correction': -2,
            'remarks': 'None',
            'reason': 'PHD'
        }
        assert 1 == count_in_db(
            TimeLineUser, {TimeLineUser.dueAuthor: 4, TimeLineUser.year: 2020, TimeLineUser.cmsId: beneficiary.cms_id})
        resp: Response = client.request(TestUri.epr_waiver).auth_header(
            officer.login).payload(correction_payload).post()
        assert 200 == resp.status_code
        assert 1 == count_in_db(
            TimeLineUser, {TimeLineUser.dueAuthor: 2, TimeLineUser.year: 2020, TimeLineUser.cmsId: beneficiary.cms_id})

        resp = client.request(TestUri.corrections).auth_header(
            officer.login).get()
        assert 200 == resp.status_code
        by_id = {e['author_cms_id']: e for e in resp.json}
        assert beneficiary.cms_id in by_id.keys()
        assert -2 == by_id[beneficiary.cms_id]['correction']
        assert 2020 == by_id[beneficiary.cms_id]['year']
