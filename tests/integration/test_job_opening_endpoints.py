from datetime import date, datetime, timedelta
from flask.wrappers import Response
from tests.fixtures import FakePerson, FakePeople
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
import logging
from icms_orm import PseudoEnum
from tests.testkit import AbstractPayloadTest
from tests.fixtures.job_opening_fixtures import FakeJobQuestionnaires, FakeJobOpenings
from tests.fixtures.tenures_fixtures import (
    FakePositions,
    FakeOrgUnits,
    FakeOrgUnitTypes,
    insert_tenure_related_data,
    insert_org_unit_types,
    insert_org_units,
    insert_positions,
    insert_tenures,
    insert_ex_officio_mandates,
)
from tests.fixtures.job_opening_fixtures import insert_questionnaire, insert_job_opening
from util import constants as const


class TestUri(PseudoEnum):
    job_opening = "/restplus/org_chart/job_openings"
    get_job_opening = "/restplus/org_chart/job_opening?id="
    put_job_opening = "/restplus/org_chart/job_openings"
    delete_job_opening = "/restplus/org_chart/job_openings"


@pytest.mark.usefixtures(
    "recycle_dbs",
    "populate_dbs",
    "insert_tenure_related_data",
    "insert_questionnaire",
    "insert_job_opening",
)
class TestJobOpening(AbstractPayloadTest):
    person: FakePerson = FakePeople.tbawej
    position1: FakePositions = FakePositions.member
    position2: FakePositions = FakePositions.secretary
    position3: FakePositions = FakePositions.leader
    joborgunit1: FakeOrgUnits = FakeOrgUnits.physics
    joborgunit2: FakeOrgUnits = FakeOrgUnits.publications
    joborgunit3: FakeOrgUnits = FakeOrgUnits.executive
    joborgunit4: FakeOrgUnits = FakeOrgUnits.trigger
    questionnaire: FakeJobQuestionnaires = FakeJobQuestionnaires.questionnaire1
    jobopening: FakeJobOpenings = FakeJobOpenings.jobopening1
    today = datetime.now().date().strftime("%Y-%m-%d")
    weekdate = (datetime.now() + timedelta(days=7)).date().strftime("%Y-%m-%d")
    tomorrow = (datetime.now() + timedelta(days=1)).date().strftime("%Y-%m-%d")
    nextweekdate = (datetime.now() + timedelta(days=14)).date().strftime("%Y-%m-%d")
    yesterday = (datetime.now() + timedelta(days=-1)).date().strftime("%Y-%m-%d")
    post_payload: dict = {
        "title": "test",
        "description": "Job as Spokesperson - not as easy as it looks ...",
        "requirements": "Excellent management and communication skills",
        "start_date": today,
        "end_date": weekdate,
        "nominations_deadline": weekdate,
        "job_unit_id": joborgunit4.id,
        "positions": [
            {
                "position_id": position1.id,
                "job_unit_id": joborgunit1.id,
                "status": const.JOB_STATUS_ACTIVE,
                "start_date": weekdate,
                "end_date": weekdate,
                "nominations_deadline": weekdate,
            },
            {
                "position_id": position2.id,
                "job_unit_id": joborgunit2.id,
                "status": const.JOB_STATUS_ACTIVE,
                "start_date": weekdate,
                "end_date": weekdate,
                "nominations_deadline": weekdate,
            },
            {
                "position_id": position3.id,
                "job_unit_id": joborgunit3.id,
                "status": const.JOB_STATUS_ACTIVE,
                "start_date": weekdate,
                "end_date": weekdate,
                "nominations_deadline": weekdate,
            },
        ],
        "questionnaire_id": questionnaire.id,
        "status": const.JOB_STATUS_ACTIVE,
        "comment": "Spokesperson response",
        "days_for_nominee_response": 2,
        "nominee_no_reply": True,
        "send_mail_to_nominee": True,
        "questionnaire_uri": "Questionnaire link",
    }

    update_payload: dict = {
        "title": "test-udpate",
        "description": "Updated Job as Spokesperson - not as easy as it looks ...",
        "requirements": "Excellent management and communication skills",
        "start_date": today,
        "job_unit_id": joborgunit4.id,
        "positions": [
            {
                "position_id": position1.id,
                "job_unit_id": joborgunit4.id,
                "status": "open",
                "start_date": weekdate,
                "end_date": weekdate,
                "nominations_deadline": weekdate,
            }
        ],
        "questionnaire_id": questionnaire.id,
        "status": const.JOB_STATUS_ACTIVE,
    }

    delete_payload: dict = {
        "positions": [{"position_id": position1.id, "job_unit_id": joborgunit1.id}],
    }

    def test_create_multi_job_open_positions(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.post_payload)
        resp: Response = (
            client.request(TestUri.job_opening)
            .auth_header(self.person.email)
            .payload(payload)
            .post()
        )
        assert 200 == resp.status_code
        json = resp.get_json()
        resp: Response = (
            client.request(f"{TestUri.get_job_opening}1")
            .auth_header(self.person.email)
            .get()
        )
        self.assert_success(resp)
        json = resp.get_json()
        assert len(self.post_payload.get("positions")) == len(
            json
        ), f"Expected exactly 3 Job Open Positions created by {self.person.login}"
        logging.info(f"Fetched a list of matching requests: {json}")

    def test_past_startdate_job_opening(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.post_payload)
        payload["start_date"] = self.yesterday
        resp: Response = (
            client.request(TestUri.job_opening)
            .auth_header(self.person.email)
            .payload(payload)
            .post()
        )
        message = resp.get_json()["message"]
        assert 400 == resp.status_code
        assert (
            message == "Entered date cannot be in the past for creation of Job Opening"
        )

    def test_past_enddate_job_opening(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.post_payload)
        payload["end_date"] = self.yesterday
        resp: Response = (
            client.request(TestUri.job_opening)
            .auth_header(self.person.email)
            .payload(payload)
            .post()
        )
        message = resp.get_json()["message"]
        assert 400 == resp.status_code
        assert (
            message == "Entered date cannot be in the past for creation of Job Opening"
        )

    def test_past_nomination_deadline_job_opening(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.post_payload)
        payload["nominations_deadline"] = self.yesterday
        resp: Response = (
            client.request(TestUri.job_opening)
            .auth_header(self.person.email)
            .payload(payload)
            .post()
        )
        message = resp.get_json()["message"]
        assert 400 == resp.status_code
        assert (
            message == "Entered date cannot be in the past for creation of Job Opening"
        )

    def test_startdate_notequals_enddate(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.post_payload)
        payload["start_date"] = payload["end_date"]
        resp: Response = (
            client.request(TestUri.job_opening)
            .auth_header(self.person.email)
            .payload(payload)
            .post()
        )
        message = resp.get_json()["message"]
        assert 400 == resp.status_code
        assert (
            message
            == "Entered startdate and enddate cannot be same for the creation of Job Opening"
        )

    def test_startdate_lessthan_enddate(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.post_payload)
        payload["start_date"] = self.nextweekdate
        resp: Response = (
            client.request(TestUri.job_opening)
            .auth_header(self.person.email)
            .payload(payload)
            .post()
        )
        message = resp.get_json()["message"]
        assert 400 == resp.status_code
        assert (
            message
            == "Entered startdate should be less than enddate for the creation of Job Opening"
        )

    def test_nominationdeadline_lessthanequal_enddate(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.post_payload)
        payload["nominations_deadline"] = self.nextweekdate
        resp: Response = (
            client.request(TestUri.job_opening)
            .auth_header(self.person.email)
            .payload(payload)
            .post()
        )
        message = resp.get_json()["message"]
        assert 400 == resp.status_code
        assert (
            message
            == "Entered nominationdeadline should be less than or equalto enddate for the creation of Job Opening"
        )

    def test_get_job_opening(self, client: IcmsTestClient):
        resp: Response = (
            client.request(f"{TestUri.get_job_opening}1")
            .auth_header(self.person.email)
            .get()
        )
        assert 200 == resp.status_code

    def test_update_job_opening(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.update_payload)
        resp: Response = (
            client.request(f"{TestUri.put_job_opening}/1")
            .auth_header(self.person.email)
            .payload({f: v for f, v in payload.items() if v is not None})
            .put()
        )
        assert 200 == resp.status_code
        json = resp.get_json()
        resp: Response = (
            client.request(f"{TestUri.get_job_opening}1")
            .auth_header(self.person.email)
            .get()
        )
        self.assert_success(resp)
        json = resp.get_json()
        assert "test-udpate" == json[0]["title"]

    def test_delete_job_open_position(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.delete_payload)
        resp: Response = (
            client.request(f"{TestUri.delete_job_opening}/1")
            .auth_header(self.person.email)
            .payload({f: v for f, v in payload.items() if v is not None})
            .delete()
        )

        resp: Response = (
            client.request(f"{TestUri.get_job_opening}1")
            .auth_header(self.person.email)
            .get()
        )
        status = resp.get_json()[0]["status"]
        assert status != const.JOB_STATUS_CLOSED

    def test_delete_job_opening(self, client: IcmsTestClient):
        payload = {}
        resp: Response = (
            client.request(f"{TestUri.delete_job_opening}/1")
            .auth_header(self.person.email)
            .payload({f: v for f, v in payload.items() if v is not None})
            .delete()
        )
        resp: Response = (
            client.request(f"{TestUri.get_job_opening}1")
            .auth_header(self.person.email)
            .get()
        )
        status = resp.get_json()[0]["status"]
        assert status == const.JOB_STATUS_CLOSED
