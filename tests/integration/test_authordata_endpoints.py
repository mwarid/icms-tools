from tests.fixtures.authorization_fixtures import grant_write
from icms_orm.common.common_schema_people_tables import UserAuthorData
from icms_orm.cmspeople import Person as ProtoPerson
from blueprints.api_restplus.api_units.authorship_units.authordata_api_unit import AuthordataApiUnit
import pytest
import logging
from typing import Any, Dict
from flask.wrappers import Response
from icms_orm import PseudoEnum
from tests.fixtures.fakedata_fixtures import FakePeople, FakePerson
from tests.fixtures.testkit.test_client import IcmsTestClient
from icmsutils.icmstest.assertables import count_in_db


log: logging.Logger = logging.getLogger(__name__)


class TestUri(PseudoEnum):
    base = '/restplus/authorship/authordata'


@pytest.fixture(scope="class")
def setup_permissions(populate_dbs, insert_access_classes):
    grant_write(TestUri.base, 'self')
    grant_write(TestUri.base, 'root')
    grant_write(f'{TestUri.base}/<int:cms_id>', 'self')
    grant_write(f'{TestUri.base}/<int:cms_id>', 'root')


@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs', 'setup_permissions')
class TestAuthordataEndpoints():

    @pytest.mark.parametrize('person, orcid, inspire', [
        (FakePeople.tmuller, 'TEEESST-0000', '123 4132 213'),
        (FakePeople.tbawej, 'TEEESST-0001', '123 4132 213'),
        (FakePeople.grossi, 'TEEESST-0012', '123.4132.213'),
        (FakePeople.jdoe, '0123-21445', '123+4132-213'),
    ])
    def test_persisting(self, client: IcmsTestClient, person: FakePerson, orcid: str, inspire: str):

        payload: Dict[str, Any] = {
            AuthordataApiUnit.Field.ORCID_ID.name: orcid,
            AuthordataApiUnit.Field.INSPIRE_ID.name: inspire,
            AuthordataApiUnit.Field.CMS_ID.name: person.cms_id
        }

        resp: Response = client.request(TestUri.base).payload(
            payload).auth_header(FakePeople.tbawej.login).post()
        assert 200 == resp.status_code

        assert 200 != client.request(TestUri.base).payload(
            payload).auth_header(FakePeople.tbawej.login).post().status_code

        assert 1 == count_in_db(UserAuthorData, {
                                UserAuthorData.orcid: orcid, UserAuthorData.inspireid: inspire, UserAuthorData.cmsid: person.cms_id})

        resp = client.request(
            f'{TestUri.base}/{person.cms_id}').auth_header(FakePeople.tbawej.login).get()
        assert 200 == resp.status_code
        for field_name, value in payload.items():
            assert value == resp.json[field_name], f'Field\'s {field_name} value != {value}'

    @pytest.mark.parametrize('person', [FakePeople.jdoe, FakePeople.tbawej])
    def test_updating(self, client: IcmsTestClient, person: FakePerson):
        '''Needs to run after the posting test above, sorry'''
        resp: Response = client.request(
            f'{TestUri.base}/{person.cms_id}').auth_header(FakePeople.tbawej.login).get()
        assert 200 == resp.status_code

        payload = {}
        payload.update(resp.json)
        payload.update(
            {AuthordataApiUnit.Field.INSPIRE_ID.name: f'INSPIRE-{person.hr_id}'})
        resp = client.request(
            f'{TestUri.base}/{person.cms_id}').auth_header(FakePeople.tbawej.login).payload(payload).put()
        assert 200 == resp.status_code

        resp: Response = client.request(
            f'{TestUri.base}/{person.cms_id}').auth_header(FakePeople.tbawej.login).get()
        assert 200 == resp.status_code
        assert payload[AuthordataApiUnit.Field.INSPIRE_ID.name] == resp.json[AuthordataApiUnit.Field.INSPIRE_ID.name]
        assert payload[AuthordataApiUnit.Field.ORCID_ID.name] == resp.json[AuthordataApiUnit.Field.ORCID_ID.name]

    def test_listing(self, client: IcmsTestClient):
        '''This one should run last'''
        resp: Response = client.request(TestUri.base).auth_header(
            FakePeople.tbawej.login).get()
        assert 200 == resp.status_code
        assert 4 == len(resp.json)

    @pytest.mark.parametrize('user,person,inspire,orcid', [(
        FakePeople.tbawej, FakePeople.ptgreat, 'INSPIRE-01234', '123-432',
    )])
    def test_insert_and_update_propagate_to_old_db_and_return_names(self, client: IcmsTestClient, user: FakePerson, person: FakePerson, inspire: str, orcid: str):
        # todo: check that name and surname are returned from the update as well
        payload = {
            AuthordataApiUnit.Field.ORCID_ID.name: orcid,
            AuthordataApiUnit.Field.INSPIRE_ID.name: inspire,
            AuthordataApiUnit.Field.CMS_ID.name: person.cms_id,
        }
        resp: Response = client.request(f'{TestUri.base}').auth_header(
            user.login).payload(payload).post()
        assert 200 == resp.status_code
        assert inspire == resp.json[AuthordataApiUnit.Field.INSPIRE_ID.name]
        assert orcid == resp.json[AuthordataApiUnit.Field.ORCID_ID.name]
        assert person.first_name == resp.json[AuthordataApiUnit.Field.FORENAME.name]
        assert person.last_name == resp.json[AuthordataApiUnit.Field.SURNAME.name]
        assert 1 == count_in_db(
            ProtoPerson, {ProtoPerson.cmsId: person.cms_id, ProtoPerson.authorId: inspire})

        payload[AuthordataApiUnit.Field.ORCID_ID.name] = '999-234'
        payload[AuthordataApiUnit.Field.INSPIRE_ID.name] = 'INSPIRE-53211'

        resp: Response = client.request(
            f'{TestUri.base}/{person.cms_id}').auth_header(user.login).payload(payload).put()
        assert 200 == resp.status_code
        for key, value in payload.items():
            assert value == resp.json[key]
        assert person.first_name == resp.json[AuthordataApiUnit.Field.FORENAME.name]
        assert person.last_name == resp.json[AuthordataApiUnit.Field.SURNAME.name]

        assert 0 == count_in_db(
            ProtoPerson, {ProtoPerson.cmsId: person.cms_id, ProtoPerson.authorId: inspire})
        assert 1 == count_in_db(
            ProtoPerson, {ProtoPerson.cmsId: person.cms_id, ProtoPerson.authorId: payload[AuthordataApiUnit.Field.INSPIRE_ID.name]})

    def test_the_right_to_manage_own_data(self, client: IcmsTestClient):
        person: FakePerson = FakePeople.ksmith
        payload = {
            AuthordataApiUnit.Field.ORCID_ID.name: '42',
            AuthordataApiUnit.Field.INSPIRE_ID.name: '24',
            AuthordataApiUnit.Field.CMS_ID.name: person.cms_id,
        }
        resp: Response = client.request(TestUri.base).auth_header(
            person.login).payload(payload).post()
        assert 200 == resp.status_code

        resp: Response = client.request(
            f'{TestUri.base}/{person.cms_id}').auth_header(person.login).get()
        assert 200 == resp.status_code
        assert '_links' in resp.json
        links: dict = resp.json['_links']
        log.info(links)
        assert len(links) > 0, "_links should not be empty!"

    def test_that_one_can_edit_their_data(self, client: IcmsTestClient):
        person: FakePerson = FakePeople.tholland
        payload = {
            AuthordataApiUnit.Field.ORCID_ID.name: '421',
            AuthordataApiUnit.Field.INSPIRE_ID.name: '124',
            AuthordataApiUnit.Field.CMS_ID.name: person.cms_id,
        }
        resp: Response = client.request(TestUri.base).auth_header(
            person.login).payload(payload).post()
        assert 200 == resp.status_code, 'Submitting one\'s own data should go smoothly'

        payload.update({AuthordataApiUnit.Field.INSPIRE_ID.name: '12456'})
        resp: Response = client.request(f'{TestUri.base}/{person.cms_id}').auth_header(
            person.login).payload(payload).put()
        assert 200 == resp.status_code, 'Editting one\'s own data should go smoothly'

        resp: Response = client.request(
            f'{TestUri.base}/{person.cms_id}').auth_header(person.login).get()
        assert 200 == resp.status_code, 'Retrieving data should go smoothly'
        assert '12456' == resp.json[AuthordataApiUnit.Field.INSPIRE_ID.name]

    def test_that_url_and_payload_cms_ids_must_match(self, client: IcmsTestClient):
        person: FakePerson = FakePeople.ppending
        payload = {
            AuthordataApiUnit.Field.ORCID_ID.name: '42133',
            AuthordataApiUnit.Field.INSPIRE_ID.name: '124333',
            AuthordataApiUnit.Field.CMS_ID.name: person.cms_id,
        }
        resp: Response = client.request(TestUri.base).auth_header(
            person.login).payload(payload).post()
        assert 200 == resp.status_code, 'Submitting one\'s own data should go smoothly'

        payload.update({AuthordataApiUnit.Field.INSPIRE_ID.name: '12456'})
        resp: Response = client.request(f'{TestUri.base}/{FakePeople.jdoe.cms_id}').auth_header(
            person.login).payload(payload).put()
        assert 400 == resp.status_code, 'Conflicting CMS ids should be caught'

        payload.update(
            {AuthordataApiUnit.Field.CMS_ID.name: FakePeople.jdoe.cms_id})
        resp: Response = client.request(f'{TestUri.base}/{person.cms_id}').auth_header(
            person.login).payload(payload).put()
        assert 400 == resp.status_code, 'Conflicting CMS ids should be caught'
