from icms_orm.common.common_schema_misc_tables import EmailMessage
from blueprints.api_restplus.api_units.basics.model_field_types import DateTime
from datetime import date, datetime, timedelta
from os import name
from blueprints.api_restplus.api_units.booking_units import BookingSlotsApiUnit, BookingRequestApiCall
from flask.wrappers import Response
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
import logging
from icms_orm import PseudoEnum
from tests.fixtures.basic_fixtures import recycle_dbs, app, client, patch_app_config
from tests.fixtures.booking_fixtures import FakeBooking, FakeBookings, insert_cms_weeks, insert_rooms
from tests.fixtures.booking_fixtures import FakeWeeks, FakeRooms, insert_bookings
from tests.fixtures.fakedata_fixtures import FakeFlags, FakePeople, FakePerson, FakeProject, FakeProjects, populate_dbs
from tests.fixtures.authorization_fixtures import grant_supervisor_write, grant_booking_owner_write, grant_anyone_write
from tests.fixtures.script_fixtures import check_applications
from tests.fixtures.fakedata_processing_fixtures import bypass_application
from tests.testkit import AbstractPayloadTest
from icmsutils.icmstest.assertables import count_in_db


class TestPath(PseudoEnum):
    weeks = '/restplus/booking/cmsWeeks'
    @staticmethod
    def week(id): return f'/restplus/booking/cmsWeeks{id}'
    slots = '/restplus/booking/slots'
    @staticmethod
    def slot(id): return f'/restplus/booking/slots/{id}'
    rooms = '/restplus/booking/rooms'
    @staticmethod
    def room(id): return f'/restplus/booking/rooms/{id}'
    requests = '/restplus/booking/requests'
    @staticmethod
    def request(id): return f'/restplus/booking/requests/{id}'
    statuses = '/restplus/booking/requests/statuses'
    @staticmethod
    def status(id): return f'/restplus/booking/requests/statuses/{id}'


@pytest.mark.parametrize('grant_booking_owner_write, grant_anyone_write', [(TestPath.request('<int:id>'), TestPath.requests)], indirect=True)
@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs', 'insert_bookings', 'grant_booking_owner_write', 'grant_anyone_write')
class TestRoomBooking(AbstractPayloadTest):

    poster: FakePerson = FakePeople.jdoe
    approver: FakePerson = FakePeople.tbawej
    day: date =FakeWeeks.upcoming_local.date_end
    post_payload: dict = {
        "cms_id_for": poster.cms_id,
        "title": "TestBookingTitle",
        "project": FakeProjects.daq.code,
        "webcast": True,
        "time_start": datetime(year=day.year, month=day.month, day=day.day, hour=10),
        "duration": 60,
        "cms_week_id": FakeWeeks.upcoming_local.id,
        "room_id": FakeRooms.local_500_r_012.id,
        "capacity": 1,
        "official": True,
        "remarks": "please work out",
        "reason": "Testreason"
    }

    approval_payload: dict = {
        "status": "done"
    }

    def test_edit_links_presence_for_the_owner(self, client: IcmsTestClient):
        resp: Response = client.request(f'{TestPath.requests}?cms_id_by={FakePeople.grossi.cms_id}').auth_header(
            FakePeople.grossi.email).get()
        assert 200 == resp.status_code
        json = resp.get_json()
        logging.info(json)
        """
        root:test_room_booking.py:68 [{'id': 1, 'cms_id_by': 1234, 'cms_id_for': 1234, 'title': 'TESTmeet', 'project': 'TEST', 'webcast': False, 'time_start': '2020-11-27T00:00:00', 'duration': 60, 'cms_week_id': 3, 'room_id_preferred': 2, 'room_id': None, 'capacity': 10, 'password': None, 'official': False, 'agenda_url': None, 'remarks': 'Thank you', 'reason': 'To thank you', 'status': 'pending', 'time_created': '2020-10-30T15:07:58.608873', 'time_updated': None, '_links': {}}]
        """
        for entry in json:
            assert 0 < len(entry.get('_links'))
            assert 'update' in entry.get('_links').keys()

    def test_that_others_cannot_see_edit_links(self, client: IcmsTestClient):
        resp: Response = client.request(TestPath.requests).auth_header(
            FakePeople.jdoe.email).get()
        assert 200 == resp.status_code
        json = resp.get_json()
        logging.info(json)
        assert 0 < len(json), 'There should be some data'
        for entry in json:
            assert 0 == len(entry.get('_links'))

    def test_posting_a_booking(self, client: IcmsTestClient):
        
        payload = {}
        payload.update(self.post_payload)

        dt = DateTime()
        payload[BookingRequestApiCall.FIELD_START_TIME.name] = dt.format(payload[BookingRequestApiCall.FIELD_START_TIME.name])
        resp: Response = client.request(TestPath.requests).auth_header(
            self.poster.email).payload(payload).post()
        assert 200 == resp.status_code
        json = resp.get_json()
        self.compare_json_against_payload(
            json, self.post_payload, skip_keys=set(['time_start']))
        assert json.get('time_start').startswith(
            str(payload['time_start']))
        assert json.get('time_start').endswith('10:00:00')
        assert self.poster.cms_id == json.get('cms_id_by')
        assert self.poster.cms_id == json.get('cms_id_for')

        resp: Response = client.request(
            f'{TestPath.requests}?cms_id_by={self.poster.cms_id}').auth_header(self.poster.email).get()
        self.assert_success(resp)
        json = resp.get_json()
        assert 1 == len(
            json), f'Expected exactly 1 booking created by {self.poster.login}'
        logging.info(f'Fetched a list of matching requests: {json}')
        
        assert 1 == count_in_db(EmailMessage, {
            EmailMessage.to: self.poster.email,
            EmailMessage.subject: 'Room request created'
        })

    def test_editing_posted_booking(self, client: IcmsTestClient):
        booking: FakeBooking = FakeBookings.one
        url: str = TestPath.request(booking.id)
        email: str = booking.booked_for.email
        resp: Response = client.request(url).auth_header(email).get()
        self.assert_success(resp)

        payload = resp.get_json()
        del payload['id']
        payload['project'] = FakeProjects.ecal.code

        resp: Response = client.request(url).auth_header(email).payload(
            {f: v for f, v in payload.items() if v is not None}).put()
        self.assert_success(resp)

        resp: Response = client.request(url).auth_header(email).get()
        self.assert_success(resp)

        assert FakeProjects.ecal.code == resp.get_json().get('project')

        assert 1 == count_in_db(EmailMessage, {
            EmailMessage.to: email,
            EmailMessage.subject: 'Room request updated'
        })

    def test_inability_to_edit_others_bookings(self, client):
        booking: FakeBooking = FakeBookings.one
        url: str = TestPath.request(booking.id)
        email: str = FakePeople.jdoe.email
        resp: Response = client.request(url).auth_header(email).get()
        self.assert_success(resp)

        payload = resp.get_json()
        del payload['id']
        payload['project'] = FakeProjects.ecal.code

        resp: Response = client.request(url).auth_header(
            email).payload(payload).put()
        assert 403 == resp.status_code

    def test_inability_to_approve_own_booking(self, client: IcmsTestClient):
        # resp: Response = client.request(f'TestPath')
        pass

    def test_approving_by_secretariat(self, client: IcmsTestClient):
        resp: Response = client.request(TestPath.status(1)).auth_header(
            self.approver.email).payload(self.approval_payload).put()
        self.assert_success(resp, 'Failed to approve booking!')
        resp: Response = client.request(TestPath.request(
            1)).auth_header(self.approver.email).get()
        self.assert_success(resp)
        json = resp.get_json()
        assert "done" == json.get("status")

        assert 1 == count_in_db(EmailMessage, {
            EmailMessage.to: self.poster.email,
            EmailMessage.subject: 'Room request approved'
        })

    def test_inability_to_edit_the_booking_after_approval(self, client: IcmsTestClient):
        booking: FakeBooking = FakeBookings.two
        url: str = TestPath.request(booking.id)
        email: str = booking.booked_for.email
        resp: Response = client.request(url).auth_header(email).get()
        self.assert_success(resp)

        payload = resp.get_json()
        del payload['id']
        payload['project'] = FakeProjects.daq.code

        resp: Response = client.request(url).auth_header(email).payload(
            {k: v for k, v in payload.items() if v is not None}).put()
        assert 403 == resp.status_code

    @pytest.mark.parametrize('colliding', [FakeBookings.four])
    def test_collision_detection(self, client: IcmsTestClient, colliding: FakeBooking):
        """
        """
        payload = {}
        payload.update(self.post_payload)
        payload[BookingRequestApiCall.FIELD_ROOM_ID.name] = colliding.room.id
        payload[BookingRequestApiCall.FIELD_WEEK_ID.name] = colliding.week.id
        payload[BookingRequestApiCall.FIELD_DURATION.name] = 240
        payload[BookingRequestApiCall.FIELD_START_TIME.name] = DateTime().format(colliding.week.date_start + \
            timedelta(days=colliding.start_day, hours=colliding.start_hour + 1))
        resp: Response = client.request(TestPath.requests).auth_header(
            FakePeople.tbawej.email).payload(payload).post()

        assert 409 == resp.status_code

    def test_list_booking_slots(self, client: IcmsTestClient):
        """
        TODO: mock the slot provider so as to simulate the interactions with Indico
        This should be easier the next time an actual booking can be made, allowing to check and mock a real Indico's response.
        """
        resp: Response = client.request(f'{TestPath.slots}?{BookingSlotsApiUnit.FIELD_WEEK_ID.name}={FakeWeeks.upcoming_local.id}').auth_header(
            FakePeople.tbawej.login).get()
        assert 200 == resp.status_code
