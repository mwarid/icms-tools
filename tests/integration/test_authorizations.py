from flask.wrappers import Response
from icms_orm.custom_classes import PseudoEnum
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
from tests.fixtures.basic_fixtures import app, client, recycle_dbs
from tests.fixtures.fakedata_fixtures import FakePerson, populate_dbs, FakePeople
from tests.fixtures.authorization_fixtures import grant_self_write, grant_supervisor_write
from flask.testing import FlaskClient


class TestPaths(PseudoEnum):
    overdues: str = '/restplus/inst/overdue-graduations/status'
    prehistory: str = '/restplus/janitorial/text_history/<int:cms_id>'
    profile: str = '/restplus/icms_public/user_info'


class TestAuthenticationBasics():
    def test_access_restriction(self, recycle_dbs, populate_dbs, client: IcmsTestClient):
        resp1 = client.request(TestPaths.profile).get()
        assert 403 == resp1.status_code

    def test_authenticating_with_header(self, recycle_dbs, populate_dbs, client: IcmsTestClient):
        resp2 = client.request(TestPaths.profile).auth_header(
            FakePeople.tbawej.email).get()
        assert 200 == resp2.status_code

    def test_reporting_team_duties(self, recycle_dbs, populate_dbs, client: IcmsTestClient):
        resp: Response
        resp = client.request(TestPaths.profile).auth_header(
            FakePeople.grossi.email).get()
        assert 200 == resp.status_code
        json = resp.get_json()
        assert 'leader' == json.get('team_duties').get('CERN')


class TestInstSupervisorAccessClass():
    payload = {'cmsId': FakePeople.jdoe.cms_id, "status": "confirmed"}

    def test_protection_without_permission(self, recycle_dbs, populate_dbs, client: IcmsTestClient):
        resp = client.request(TestPaths.overdues).auth_header(
            FakePeople.grossi.email).payload(self.payload).post()
        assert 403 == resp.status_code

    @pytest.mark.parametrize('grant_supervisor_write', [TestPaths.overdues], indirect=True)
    def test_cross_institute_protection(self, recycle_dbs, populate_dbs, grant_supervisor_write, client: IcmsTestClient):
        resp = client.request(TestPaths.overdues).auth_header(
            FakePeople.tmuller.email).payload(self.payload).post()
        assert 403 == resp.status_code

    @pytest.mark.parametrize('grant_supervisor_write', [TestPaths.overdues], indirect=True)
    def test_access_with_permission(self, recycle_dbs, populate_dbs, grant_supervisor_write, client: IcmsTestClient):
        resp = client.request(TestPaths.overdues).auth_header(
            FakePeople.grossi.email).payload(self.payload).post()
        assert 200 == resp.status_code


class TestSelfAccessClass():
    overdue_payload = {'cmsId': FakePeople.jdoe.cms_id, "status": "confirmed"}
    history_payload = {'cms_id': str(FakePeople.jdoe.cms_id), 'history': ''}

    def test_protection_without_permission(self, recycle_dbs, populate_dbs, client: IcmsTestClient):
        resp = client.request(TestPaths.overdues).auth_header(
            FakePeople.jdoe.email).payload(self.overdue_payload).post()
        assert 403 == resp.status_code

    @pytest.mark.parametrize('grant_self_write', [TestPaths.overdues], indirect=True)
    def test_access_with_permission(self, recycle_dbs, populate_dbs, grant_self_write, client: IcmsTestClient):
        resp = client.request(TestPaths.overdues).auth_header(
            FakePeople.jdoe.email).payload(self.overdue_payload).post()
        assert 200 == resp.status_code

    def test_protection_without_permission_2(self, recycle_dbs, populate_dbs, client: IcmsTestClient):
        person: FakePerson
        person = FakePeople.jdoe
        resp = client.request(TestPaths.prehistory.replace('<int:cms_id>', str(person.cms_id))).auth_header(
            FakePeople.jdoe.email).payload(self.history_payload).post()
        assert 403 == resp.status_code

    @pytest.mark.parametrize('grant_self_path_write', [TestPaths.prehistory], indirect=True)
    def test_access_with_permission_2(self, recycle_dbs, populate_dbs, grant_self_path_write, client: IcmsTestClient):
        person: FakePerson
        person = FakePeople.jdoe
        resp = client.request(TestPaths.prehistory.replace('<int:cms_id>', str(person.cms_id))).auth_header(
            FakePeople.jdoe.email).payload(self.history_payload).post()
        assert 200 == resp.status_code
