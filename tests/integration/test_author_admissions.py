from flask.wrappers import Response
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
from icms_orm import PseudoEnum
from tests.fixtures.basic_fixtures import recycle_dbs, app, client, patch_app_config
from tests.fixtures.fakedata_fixtures import FakeFlags, FakePeople, FakePerson, populate_dbs
from tests.fixtures.authorization_fixtures import grant_supervisor_write
from tests.fixtures.script_fixtures import check_applications
from tests.fixtures.fakedata_processing_fixtures import bypass_application


class TestPath(PseudoEnum):
    pending_authors = '/restplus/authorlists/pendingAuthors'


@pytest.mark.parametrize("grant_supervisor_write, bypass_application", [(TestPath.pending_authors, FakePeople.trice), ], indirect=True)
@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs', 'grant_supervisor_write', 'bypass_application')
class TestAdmissions():

    def test_fetching_list_of_pending_authors(self, client: IcmsTestClient):
        resp: Response
        resp = client.request(TestPath.pending_authors).auth_header(
            FakePeople.grossi.email).get()
        assert 200 == resp.status_code
        assert 1 == len([x for x in resp.get_json() if x.get(
            'cmsId') == FakePeople.trice.cms_id])

    def test_unblocking_successful_applicant(self, client: IcmsTestClient):
        # The old DB should get updated but the new one will still require a sync
        resp: Response
        resp = client.request(TestPath.pending_authors).auth_header(FakePeople.grossi.email).\
            payload({'cmsId': FakePeople.trice.cms_id, 'action': 'admit'}).put()
        assert 200 == resp.status_code

        from icms_orm.cmspeople import Person
        applicant: Person
        applicant = Person.session().query(Person).filter(
            Person.cmsId == FakePeople.trice.cms_id).one()

        assert not applicant.isAuthorBlock
        assert not FakeFlags.misc_authorno.code in {
            f.id for f in applicant.flags}

    @pytest.mark.parametrize('user, applicant, action, exp_code', [
        (FakePeople.grossi, FakePeople.ksmith, 'admit', 409), 
        (FakePeople.grossi, FakePeople.ksmith, 'freeze', 409), 
        (FakePeople.grossi, FakePeople.grossi, 'admit', 409),
        (FakePeople.grossi, FakePeople.grossi, 'freeze', 409),
        (FakePeople.tmuller, FakePeople.trice, 'admit', 403),
        (FakePeople.tmuller, FakePeople.trice, 'freeze', 403),
    ])
    def test_illegal_inputs(self, client: IcmsTestClient, user: FakePerson, applicant: FakePerson, action, exp_code):
        """
        Checking that acting on non-successful applicants or already unblocked people yields a 409 response
        """
        resp = client.request(TestPath.pending_authors).auth_header(user.email).\
            payload({'cmsId': applicant.cms_id, 'action': action}).put()
        assert exp_code == resp.status_code


@pytest.mark.parametrize("grant_supervisor_write, bypass_application", [(TestPath.pending_authors, FakePeople.trice), ], indirect=True)
@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs', 'grant_supervisor_write', 'bypass_application')
class TestDeferrals():
    def test_freezing(self, client: IcmsTestClient):
        # TODO: parametrize the input so that freeze/admit can be tested independently
        resp: Response
        resp = client.request(TestPath.pending_authors).auth_header(FakePeople.grossi.email).\
            payload({'cmsId': FakePeople.trice.cms_id, 'action': 'freeze'}).put()
        assert 200 == resp.status_code

        from icms_orm.cmspeople import Person
        applicant: Person
        applicant = Person.session().query(Person).filter(
            Person.cmsId == FakePeople.trice.cms_id).one()

        assert not applicant.isAuthorBlock
        assert FakeFlags.misc_authorno.code in {
            f.id for f in applicant.flags}

