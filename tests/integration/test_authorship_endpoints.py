import logging
from icms_orm.toolkit import ALFileset
from blueprints.api_restplus.api_units.authorlist_units.authorlist_files_api_units import ALFileApiUnit
from typing import Dict
from util import constants
from _pytest.fixtures import SubRequest
from icms_orm.custom_classes import PseudoEnum
import pytest
from flask.wrappers import Response
from tests.fixtures.testkit.test_client import IcmsTestClient
from tests.fixtures.basic_fixtures import app, client, sign_in, reset_dbs_icms, reset_db_people
from tests.fixtures.fakedata_fixtures import FakePerson, populate_icms_db, populate_people_db, FakePeople
from tests.fixtures.basic_fixtures import TestappSingletonFactory
from icmsutils.businesslogic.flags import Flag as FlagDef
import flask
import time
from datetime import date
from os import listdir, makedirs, path
from icmsutils.icmstest.assertables import count_in_db
from blueprints.api_restplus.api_units.authorlist_units import AuthorListUnit
from blueprints.api_restplus.api_units.authorlist_units import ALFileSetApiCall
from blueprints.author_lists.gen_al_views import AuthorListFileType


log: logging.Logger = logging.getLogger(__name__)


class TestUri(PseudoEnum):
    rights_check = '/restplus/authorship/check_rights/'

    @staticmethod
    def rights_check_for(cms_id: int):
        return f'{TestUri.rights_check}{cms_id}'


@pytest.mark.usefixtures('reset_db_people', 'reset_dbs_icms', 'populate_people_db', 'populate_icms_db')
class TestAuthorshipEndpoints():
    @pytest.mark.parametrize('user', list(FakePeople.values()))
    def test_check_signing_rights(self, client: IcmsTestClient,  user: FakePerson):
        uri = TestUri.rights_check_for(user.cms_id)
        resp: Response = client.request(uri).auth_header(
            FakePeople.tbawej.login).get()
        assert 200 == resp.status_code
        assert user.is_author == resp.json['is_author']

