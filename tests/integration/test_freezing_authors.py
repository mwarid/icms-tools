from flask.wrappers import Response
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
from icms_orm import PseudoEnum
from tests.fixtures.basic_fixtures import recycle_dbs, app, client, patch_app_config
from tests.fixtures.fakedata_fixtures import FakeFlags, FakePeople, FakePerson, populate_dbs
from tests.fixtures.authorization_fixtures import grant_supervisor_write
from tests.fixtures.script_fixtures import check_applications
from tests.fixtures.fakedata_processing_fixtures import bypass_application


class TestPath(PseudoEnum):
    authorships = '/restplus/authorlists/memberAuthors'


@pytest.mark.parametrize('grant_supervisor_write', [(TestPath.authorships),], indirect=True)
@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs', 'grant_supervisor_write')
class TestAuthorshipManagement():
    
    @pytest.mark.parametrize('user', [FakePeople.grossi, FakePeople.tmuller])
    def test_loading_the_list(self, client: IcmsTestClient, user: FakePerson):
        resp = client.request(TestPath.authorships).auth_header(user.email).get()
        assert 200 == resp.status_code

    @pytest.mark.parametrize('user, member, input', [
        (FakePeople.tmuller, FakePeople.tmuller, 'authorno'),
        (FakePeople.tmuller, FakePeople.tmuller, None),
        (FakePeople.grossi, FakePeople.jdoe, 'authorno'),
        (FakePeople.grossi, FakePeople.jdoe, None),
    ])
    def test_authorship_freeze_unfreeze(self, client: IcmsTestClient, user: FakePerson, member: FakePerson, input: bool):
        resp = client.request(TestPath.authorships).auth_header(user.email).payload(
            {'cmsId': member.cms_id, 'flagId': input}).put()
        assert 200 == resp.status_code

        from icms_orm.cmspeople import Person
        person: Person
        person = Person.session().query(Person).filter(Person.cmsId == member.cms_id).one()
        flag_count = len([f for f in person.flags if f.id == FakeFlags.misc_authorno.code])
        assert (input is not None) == flag_count, 'For non-null input a MISC_authorno flag should be assigned'


    @pytest.mark.parametrize('user, member, input, exp_code', [
        (FakePeople.tmuller, FakePeople.jdoe, 'authorno', 403),
        (FakePeople.tmuller, FakePeople.grossi, None, 403),
        (FakePeople.grossi, FakePeople.tmuller, None, 403),
    ])
    def test_illegal_inputs(self, client: IcmsTestClient, user: FakePerson, member: FakePerson, input, exp_code):
        resp = client.request(TestPath.authorships).auth_header(user.email).payload(
            {'cmsId': member.cms_id, 'flagId': input}).put()
        assert exp_code == resp.status_code