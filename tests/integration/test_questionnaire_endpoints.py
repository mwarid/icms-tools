from datetime import date, datetime, timedelta
from flask.wrappers import Response
from tests.fixtures import FakePerson, FakePeople
from tests.fixtures.testkit.test_client import IcmsTestClient
from tests.fixtures.authorization_fixtures import (
    grant_supervisor_write,
    grant_anyone_write,
)
import pytest
import logging
from icms_orm import PseudoEnum
from tests.testkit import AbstractPayloadTest
from tests.fixtures.job_opening_fixtures import FakeJobQuestionnaires
from tests.fixtures.job_opening_fixtures import insert_questionnaire


class TestUri(PseudoEnum):
    job_questionnaire = "/restplus/org_chart/job_questionnaire"
    get_job_questionnaire = "/restplus/org_chart/job_questionnaire"
    put_job_questionnaire = "/restplus/org_chart/job_questionnaire"


@pytest.mark.usefixtures("recycle_dbs", "populate_dbs", "insert_questionnaire")
class TestJobQuestionnaire(AbstractPayloadTest):
    person: FakePerson = FakePeople.tbawej
    questionnaire: FakeJobQuestionnaires = FakeJobQuestionnaires.questionnaire1
    today = datetime.now().date()
    weekdate = (datetime.now() + timedelta(days=7)).date()
    post_payload: dict = {
        "title": "Spokesperson",
        "questions": "Are you interested in this job opening",
        "date_created": today,
        "date_updated": weekdate,
    }
    update_payload: dict = {
        "title": "Spokesperson-test",
        "questions": "Are you interested in this job opening",
    }

    def test_create_job_questionnaire(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.post_payload)
        resp: Response = (
            client.request(TestUri.job_questionnaire)
            .auth_header(self.person.email)
            .payload(payload)
            .post()
        )
        assert 200 == resp.status_code
        json = resp.get_json()
        resp: Response = (
            client.request(f"{TestUri.get_job_questionnaire}/1")
            .auth_header(self.person.email)
            .get()
        )
        self.assert_success(resp)
        json = resp.get_json()
        assert (
            1 == json["id"]
        ), f"Expected exactly 1 Job Questionnaire created by {self.person.login}"
        logging.info(f"Fetched a list of matching requests: {json}")

    def test_get_job_questionnaire(self, client: IcmsTestClient):
        resp: Response = (
            client.request(f"{TestUri.get_job_questionnaire}/1")
            .auth_header(self.person.email)
            .get()
        )
        assert 200 == resp.status_code

    def test_update_job_questionnaire(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.update_payload)

        resp: Response = (
            client.request(f"{TestUri.put_job_questionnaire}/1")
            .auth_header(self.person.email)
            .payload({f: v for f, v in payload.items() if v is not None})
            .put()
        )
        assert 200 == resp.status_code
        json = resp.get_json()
        resp: Response = (
            client.request(f"{TestUri.get_job_questionnaire}/1")
            .auth_header(self.person.email)
            .get()
        )
        self.assert_success(resp)
        json = resp.get_json()
        assert "Spokesperson-test" == json["title"]
