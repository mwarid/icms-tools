from tests.fixtures.basic_fixtures import TestappSingletonFactory
from flask.wrappers import Response
from tests.fixtures.testkit.test_client import IcmsTestClient
import random
import string
import pytest
import os
import copy

from icms_orm import PseudoEnum
from tests.fixtures.fakedata_fixtures import FakeInst, FakeInsts, FakePeople, FakePerson


class TestPath(PseudoEnum):
    egroup_info = '/restplus/egroups/egroup'
    members_info = '/restplus/egroups/members'
    cadi_egroup_info = '/restplus/egroups/cadi_egroup'


_egNameTest = "apf-test-CI%s" % ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(5))

_egParams = {
  "group_name": _egNameTest,
  "hr_ids": "",
  "egroups": "",
  "accounts": "",
  "emails": "",
  "admin_egroup": "icms-support-admins",
  "privacy_info": "",
  "email_properties": ""
}


@pytest.mark.skipif( os.environ.get('EGROUP_USER', None) is None, reason='Not running eGroup API tests without EGROUP_USER set in environment.' )
class TestEgroupCADIInfo():

    @classmethod
    def setup_class(cls):
        config = TestappSingletonFactory.get_web_app().config

    @classmethod
    def teardown_class(cls):
        config = TestappSingletonFactory.get_web_app().config

    @pytest.mark.skipif( os.environ.get('TEST_EG_CADI_CREATE', None) is None, reason='Tests for eGroup CADI API presently disabled.' )
    def test_egroup_cadi_create(self, client: IcmsTestClient):
        egName = 'cms-XXX-22-042' # use a targeted eGroup name as it will be checked 
        data =  copy.deepcopy( _egParams )
        data['group_name'] = egName
        data['hr_ids'] = "1,2,3,4,5"
        resp = ( client.request(f'{TestPath.cadi_egroup_info}')
                        .auth_header(FakePeople.tbawej.email)
                        .payload( data )
                        .post()
        )
        assert resp.status_code == 200
        assert resp.get_json() == { 'status': 'OK', 'payload': '1,2,3,4,5' }


@pytest.mark.skipif( os.environ.get('EGROUP_USER', None) is None, reason='Not running eGroup API tests without EGROUP_USER set in environment.' )
class TestEgroupInfo():

    @classmethod
    def setup_class(cls):
        config = TestappSingletonFactory.get_web_app().config

    @classmethod
    def teardown_class(cls):
        config = TestappSingletonFactory.get_web_app().config


    def test_egroup_create(self, client: IcmsTestClient):
        data =  _egParams
        resp = ( client.request(f'{TestPath.egroup_info}')
                        .auth_header(FakePeople.tbawej.email)
                        .payload( data )
                        .post()
        )
        assert resp.status_code == 200
        assert resp.get_json() == {
                                    'group_name': _egNameTest,
                                    'accounts': '[]',
                                    'egroups': '[]',
                                    'emails': '[]',
                                    'hr_ids': '[]',
                                    'admin_egroup': None,
                                    'email_properties': None,
                                    'privacy_info': None
                                  }


    def test_egroup_info(self, client: IcmsTestClient):
        data =  { "group_name": _egNameTest }
        resp = ( client.request(f'{TestPath.egroup_info}')
                        .auth_header(FakePeople.tbawej.email)
                        .payload( data )
                        .get()
        )

        assert resp.status_code == 200
        assert resp.get_json() == { 'group_name': _egNameTest, 
                                   'hr_ids': '[]', 
                                   'egroups': "[]", 
                                   'accounts': "[]", 
                                   'emails': "[]", 
                                   'admin_egroup': 'icms-support-admins', 
                                   'privacy_info': "{'Selfsubscription': 'Closed', 'Privacy': 'Members'}", 
                                   'email_properties': "{'MailPostingRestrictions': {'PostingRestrictions': 'OwnerAdminsAndOthers', 'OtherRecipientsAllowedToPost': []}, 'SenderAuthenticationEnabled': 'False', 'WhoReceivesDeliveryErrors': 'Sender', 'MaxMailSize': 10, 'ArchiveProperties': 'DoesNotExist'}"}

    # we should not be able to create the same eGroup a second time
    def test_egroup_create_fail(self, client: IcmsTestClient):
        data =  _egParams
        resp = ( client.request(f'{TestPath.egroup_info}')
                        .auth_header(FakePeople.tbawej.email)
                        .payload( data )
                        .post()
        )
        assert resp.status_code == 409
        print( f'GOT: {resp} ')
        print( f'GOT: json: {resp.get_json()} ')
        assert resp.get_json() == {'message': f"ERROR: eGroup '{_egNameTest}' already exists! Nothing done."}


    def test_egroup_updateMembers(self, client: IcmsTestClient):
        data = {  "group_name": _egNameTest, "hr_ids": "422405,42", "egroups": "icms-support-admins, eg2", "accounts":"icmswww,andreasp" }
        resp = ( client.request(f'{TestPath.members_info}')
                        .auth_header(FakePeople.tbawej.email)
                        .payload( data )
                        .post()
        )
        assert resp.status_code == 200
        assert resp.get_json() == { "group_name": _egNameTest, "hr_ids": "[422405]", "egroups": "['icms-support-admins']", "accounts": "['ANDREASP', 'ICMSWWW']", "emails": "[]" }

    def test_egroup_getMembers(self, client: IcmsTestClient):
        data =  { "group_name": _egNameTest }
        resp = ( client.request(f'{TestPath.members_info}')
                        .auth_header(FakePeople.tbawej.email)
                        .payload( data )
                        .get()
        )
        assert resp.status_code == 200
        assert resp.get_json() == { "group_name": _egNameTest, "hr_ids": "[422405]", "egroups": "['icms-support-admins']", "accounts": "['ANDREASP', 'ICMSWWW']", "emails": "[]" }

    def test_egroup_removeMembers(self, client: IcmsTestClient):
        # try to remove some members, even some which are not in the group ('eg2')
        data = { "group_name": _egNameTest, "hr_ids": "422405", "egroups": "eg2", "accounts": "andreasp" }
        resp = ( client.request(f'{TestPath.members_info}')
                        .auth_header(FakePeople.tbawej.email)
                        .payload( data )
                        .delete()
        )
        assert resp.status_code == 200
        assert resp.get_json() == { "group_name": _egNameTest, "hr_ids": [], "egroups": [ "icms-support-admins" ], "accounts": [ "ICMSWWW" ], "emails": [] }
        

    def test_egroup_delete(self, client: IcmsTestClient):
        data =  _egParams
        resp = ( client.request(f'{TestPath.egroup_info}')
                        .auth_header(FakePeople.tbawej.email)
                        .payload( data )
                        .delete()
        )
        assert resp.status_code == 200
        assert resp.get_json() == {'status': 'OK', 'message': f'eGroup {_egNameTest} successfully deleted.'}
