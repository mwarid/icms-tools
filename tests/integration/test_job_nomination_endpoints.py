from datetime import date, datetime, timedelta
from blueprints.api_restplus.api_units.tenures_units import (
    JobNominationApiCall,
    JobNominationStatusApiCall,
)
from flask.wrappers import Response
from tests.fixtures import FakePerson, FakePeople
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
import logging
from icms_orm import PseudoEnum
from tests.testkit import AbstractPayloadTest
from tests.fixtures.job_opening_fixtures import (
    FakeJobQuestionnaires,
    FakeJobOpenings,
    FakeJobOpenPositions,
    insert_job_opening,
    insert_job_open_position,
    insert_questionnaire,
)
from tests.fixtures.tenures_fixtures import (
    FakePositions,
    FakeOrgUnits,
    FakeOrgUnitTypes,
    insert_tenure_related_data,
    insert_org_unit_types,
    insert_org_units,
    insert_positions,
    insert_tenures,
    insert_ex_officio_mandates,
)
from util import constants as const


class TestUri(PseudoEnum):
    job_nomination = "/restplus/org_chart/job_nomination"
    get_job_nomination = "/restplus/org_chart/job_nomination"
    put_job_nomination = "/restplus/org_chart/job_nomination"


@pytest.mark.usefixtures(
    "recycle_dbs",
    "populate_dbs",
    "insert_tenure_related_data",
    "insert_questionnaire",
    "insert_job_opening",
    "insert_job_open_position",
)
class TestNomination(AbstractPayloadTest):
    person: FakePerson = FakePeople.tbawej
    position: FakePositions = FakePositions.member
    joborgunit: FakeOrgUnits = FakeOrgUnits.physics
    questionnaire: FakeJobQuestionnaires = FakeJobQuestionnaires.questionnaire1
    jobopening: FakeJobOpenings = FakeJobOpenings.jobopening1
    jobopenposition: FakeJobOpenPositions = FakeJobOpenPositions.jobopenposition1
    today = datetime.now().date()
    weekdate = (datetime.now() + timedelta(days=7)).date()
    post_payload: dict = {
        "job_id": jobopening.id,
        "questionnaire_id": questionnaire.id,
        "questions": "Are you interested for the nomination",
        "nominee_id": person.cms_id,
        "questionnaire_answers": "am interested in this position",
        "position_id": position.id,
        "job_unit_id": joborgunit.id,
    }
    update_payload: dict = {
        "job_id": jobopening.id,
        "questionnaire_id": questionnaire.id,
        "questions": "Are you interested for the nomination",
        "actor_id": person.cms_id,
        "questionnaire_answers": "am interested in this position",
        "actor_remarks": None,
        "status": const.JOB_STATUS_ACTIVE,
    }

    def test_create_job_nomination(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.post_payload)
        resp: Response = (
            client.request(TestUri.job_nomination)
            .auth_header(self.person.email)
            .payload(payload)
            .post()
        )
        assert 200 == resp.status_code
        json = resp.get_json()
        resp: Response = (
            client.request(f"{TestUri.get_job_nomination}/1")
            .auth_header(self.person.email)
            .get()
        )
        self.assert_success(resp)
        json = resp.get_json()
        assert 3 == len(
            json
        ), f"Expected exactly 1 Job Nomination created by {self.person.login}"
        assert payload.get("job_id") == json[0].get("job_opening_id")
        assert const.JOB_STATUS_ACTIVE == json[0].get("statuses")[0].get("status")
        assert payload.get("nominee_id") == json[0].get("statuses")[0].get("actor_id")
        logging.info(f"Fetched a list of matching requests: {json}")

    def test_validate_to_existing_nominee(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.post_payload)
        resp: Response = (
            client.request(TestUri.job_nomination)
            .auth_header(self.person.email)
            .payload(payload)
            .post()
        )
        message = resp.get_json()["message"]
        assert 400 == resp.status_code
        assert message == "Nominee already proposed for this position by you"

    def test_get_job_nomination(self, client: IcmsTestClient):
        resp: Response = (
            client.request(f"{TestUri.get_job_nomination}/1")
            .auth_header(self.person.email)
            .get()
        )
        assert 200 == resp.status_code

    def test_update_job_nomination(self, client: IcmsTestClient):
        payload = {}
        payload.update(self.update_payload)
        resp: Response = (
            client.request(f"{TestUri.put_job_nomination}/1")
            .auth_header(self.person.email)
            .payload({f: v for f, v in payload.items() if v is not None})
            .put()
        )
        json = resp.get_json()
        print(json)
        assert 200 == resp.status_code
        assert payload.get("status") == json[0].get("statuses")[0].get("status")
        assert payload.get("actor_remarks") == json[0].get("statuses")[0].get(
            "actor_remarks"
        )
        assert payload.get("actor_id") == json[0].get("statuses")[0].get("actor_id")
