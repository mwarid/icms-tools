import pytest, json
from tests.fixtures.basic_fixtures import (
    app,
    client,
    sign_in,
    reset_dbs_icms,
    reset_db_people,
)
from tests.fixtures.fakedata_fixtures import populate_icms_db, populate_people_db
from flask.testing import FlaskClient


def test_that_test_app_is_alive(client: FlaskClient):
    response = client.get("/", follow_redirects=True)
    assert b"iCMS toolkit" in response.data
    assert b"Login" in response.data
    assert b"ignores password" in response.data


@pytest.mark.parametrize("sign_in", ["tbawej"], indirect=True)
def test_login(
    reset_db_people,
    reset_dbs_icms,
    populate_people_db,
    populate_icms_db,
    client,
    sign_in,
):
    response = sign_in
    assert b"Logout" in response.data
    assert (b"User " + b"tbawej") in response.data

    response = client.get("restplus/icms_public/user_info")
    assert response.json["cms_id"] == 9981
    assert response.json["inst_code"] == "CERN"


@pytest.mark.parametrize("sign_in", ["tbawej"], indirect=True)
def test_non_existent_user(
    client,
    sign_in,
):
    response = (
        client.request("restplus/icms_public/user_info")
        .payload({"cms_id": "17007", "login": "non-existent"})
        .get()
    )

    assert response.status_code == 404
    assert response.json == {"message": "The requested user (17007/non-existent) was not found"}
