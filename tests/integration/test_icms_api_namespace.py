from flask.wrappers import Response
from icms_orm.common.common_schema_people_tables import GenderValues
from sqlalchemy.orm.attributes import InstrumentedAttribute
from tests.fixtures.fakedata_fixtures import FakeInst, FakeInsts, FakePeople, FakePerson
from blueprints.api_restplus.api_units.basics.model_field_types import Integer, String
from flask_restx.api import Api
import pytest
from blueprints.api_restplus.api_units.basics.builders import ModelFieldDefinition
from typing import Any, Dict, List, Type
from tests.fixtures.testkit.test_client import IcmsTestClient
from blueprints.api_restplus.api_units.basics.crud import AbstractCRUDUnit, Deleteless, Listless, Postless, Putless
from icms_orm.common import Person, Institute


class MockPersonUnit(AbstractCRUDUnit):

    FIELD_ID = ModelFieldDefinition.builder(
        Integer).column(Person.cms_id).id().build()
    FIELD_FORENAME = ModelFieldDefinition.builder(String).column(
        Person.first_name).name('forename').build()
    FIELD_SURNAME = ModelFieldDefinition.builder(
        String).column(Person.last_name).name('surname').build()
    FIELD_GENDER = ModelFieldDefinition.builder(String).column(
        Person.gender).enum(GenderValues.values()).build()
    FIELD_LOGIN = ModelFieldDefinition.builder(
        String).column(Person.login).build()
    # A dummy derived value added on top of what's pulled straight from the DB
    FIELD_LOGIN_LENGTH = ModelFieldDefinition.builder(
        Integer).readonly().name('login_length').build()

    @classmethod
    def get_model_name(cls: Type['MockPersonUnit']) -> str:
        return 'PersonMockModel'

    @classmethod
    def get_model_fields_list(cls: Type['MockPersonUnit']) -> List[ModelFieldDefinition]:
        return [cls.FIELD_ID, cls.FIELD_FORENAME, cls.FIELD_SURNAME, cls.FIELD_GENDER, cls.FIELD_LOGIN, cls.FIELD_LOGIN_LENGTH]

    @classmethod
    def row_to_dict(cls: Type['MockPersonUnit'], row: List[Any], cols_list: List[InstrumentedAttribute]):
        retval: Dict[str, Any] = super().row_to_dict(row, cols_list)
        retval[cls.FIELD_LOGIN_LENGTH.name] = len(retval[cls.FIELD_LOGIN.name])
        return retval


class MockInstApiUnit(AbstractCRUDUnit, Listless, Putless, Postless, Deleteless):
    # Ignoring the ID to test that string-ID'ed resources work okay
    FIELD_CODE = ModelFieldDefinition.builder(
        String).column(Institute.code).id().build()
    FIELD_NAME = ModelFieldDefinition.builder(
        String).column(Institute.name).build()

    @classmethod
    def get_model_name(cls: Type['MockInstApiUnit']) -> str:
        return 'Mock Institute'

    @classmethod
    def get_model_fields_list(cls: Type['MockInstApiUnit']) -> List[ModelFieldDefinition]:
        return [cls.FIELD_CODE, cls.FIELD_NAME]


@pytest.fixture(scope='class')
def register_crud():
    from blueprints.api_restplus import SingletonApiFactory
    from blueprints.api_restplus.icms_api_namespace import IcmsApiNamespace
    api: Api = SingletonApiFactory.get_api()
    mock_ns = IcmsApiNamespace(
        'mock_ns', description='Foo namespace', ordered=False)
    mock_ns.register_crud_handler_class(api, MockPersonUnit, '/mock_ppl')
    mock_ns.register_crud_handler_class(api, MockInstApiUnit, '/mock_insts')
    api.add_namespace(ns=mock_ns, path=f'/{mock_ns.name}')


@pytest.mark.usefixtures('reset_db_people', 'reset_dbs_icms', 'populate_people_db', 'populate_icms_db', 'register_crud')
class TestMockApiUnits:
    @pytest.mark.parametrize('person', [FakePeople.tbawej, FakePeople.tmuller])
    def test_fetching_db_info_by_existing_ids(self, client: IcmsTestClient, person: FakePerson):
        resp: Response = client.request(
            f'/restplus/mock_ns/mock_ppl/{person.cms_id}').auth_header(FakePeople.tbawej.login).get()
        assert 200 == resp.status_code
        assert person.first_name == resp.json[MockPersonUnit.FIELD_FORENAME.name]

    @pytest.mark.parametrize('person', [FakePeople.tbawej, FakePeople.tmuller])
    def test_generating_resource_links(self, client: IcmsTestClient, person: FakePerson):
        resp: Response = client.request(
            f'/restplus/mock_ns/mock_ppl/{person.cms_id}').auth_header(FakePeople.tbawej.login).get()
        assert 200 == resp.status_code
        assert '_links' in resp.json
        links = resp.json['_links']
        assert 0 < len(links)
        assert 'update' in links
        assert 'delete' in links
        assert f'/restplus/mock_ns/mock_ppl/{person.cms_id}' == links['update']['url']
        assert f'/restplus/mock_ns/mock_ppl/{person.cms_id}' == links['delete']['url']

    @pytest.mark.parametrize('person', [FakePeople.tbawej, FakePeople.tmuller])
    def test_fetching_derived_info_by_existing_ids(self, client: IcmsTestClient, person: FakePerson):
        resp: Response = client.request(
            f'/restplus/mock_ns/mock_ppl/{person.cms_id}').auth_header(FakePeople.tbawej.login).get()
        assert 200 == resp.status_code
        assert person.first_name == resp.json[MockPersonUnit.FIELD_FORENAME.name]
        assert len(
            person.login) == resp.json[MockPersonUnit.FIELD_LOGIN_LENGTH.name]

    @pytest.mark.parametrize('name, hits', [(FakePeople.tbawej.last_name, 1), (FakePeople.jdoe.last_name, 2)])
    def test_searching_by_last_name(self, client: IcmsTestClient, name: str, hits: int):
        resp: Response = client.request(
            f'/restplus/mock_ns/mock_ppl?{MockPersonUnit.FIELD_SURNAME.name}={name}').auth_header(FakePeople.tbawej.login).get()
        assert 200 == resp.status_code
        assert hits == len(resp.json)

    @pytest.mark.parametrize('inst', list(FakeInsts.values()))
    def test_fetching_by_non_int_ids(self, client: IcmsTestClient, inst: FakeInst):
        resp: Response = client.request(
            f'/restplus/mock_ns/mock_insts/{inst.code}').auth_header(FakePeople.tbawej.login).get()
        assert 200 == resp.status_code
        assert inst.name == resp.json[MockInstApiUnit.FIELD_NAME.name]
        assert inst.code == resp.json[MockInstApiUnit.FIELD_CODE.name]

    def test_methods_masking(self, client: IcmsTestClient):
        resp: Response = client.request(
            '/restplus/mock_ns/mock_insts').auth_header(FakePeople.tbawej.login).payload({}).post()
        assert 405 == resp.status_code
        assert 405 == client.request(f'/restplus/mock_ns/mock_insts/{FakeInsts.cern.code}').auth_header(
            FakePeople.tbawej.email).delete().status_code, 'DELETE supposed to be masked-out'
        assert 405 == client.request(f'/restplus/mock_ns/mock_insts/{FakeInsts.cern.code}').auth_header(
            FakePeople.tbawej.email).payload({}).put().status_code, 'PUT supposed to be masked-out'
        resp = client.request(
            f'/restplus/mock_ns/mock_insts').auth_header(FakePeople.tbawej.email).get()
        assert 405 == resp.status_code or resp.json is None, 'GET (collection) supposed to be masked-out'
