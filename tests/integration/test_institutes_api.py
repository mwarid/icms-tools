from flask.wrappers import Response
from icms_orm import PseudoEnum
from tests.fixtures.fakedata_fixtures import FakeInst, FakeInsts, FakePeople

from tests.testkit import AbstractPayloadTest
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest

class TestUri(PseudoEnum):
    inst_list = '/restplus/members/institutes'
    inst_info = '/restplus/members/institute?code='

@pytest.mark.usefixtures('reset_db_people', 'reset_dbs_icms', 'populate_people_db', 'populate_icms_db')
class TestInstitutesApi(AbstractPayloadTest):
    @pytest.mark.parametrize('inst', list(FakeInsts.values()))
    def test_single_inst_info(self, client: IcmsTestClient, inst: FakeInst):
        resp: Response = client.request(TestUri.inst_info + inst.code).auth_header(FakePeople.tbawej.login).get()
        assert 200 == resp.status_code
        assert inst.code == resp.json['code']
        assert inst.name == resp.json['name']
        assert inst.country.code == resp.json['country_code']
        assert inst.status.name == resp.json['status']
        assert inst.country.name == resp.json['country_name']
        assert len(inst.leaders) == len(resp.json['management'])

    def test_listing_institutes(self, client: IcmsTestClient):
        resp: Response = client.request(
            TestUri.inst_list).auth_header(FakePeople.jdoe.login).get()
        assert 200 == resp.status_code
        assert len(FakeInsts.values()) == len(resp.json)

    def test_empty_institute_details(self, client:IcmsTestClient):
        resp: Response = client.request(TestUri.inst_info + FakeInsts.void_inst.code).auth_header(FakePeople.tbawej.login).get()
        assert 200 == resp.status_code
        assert 0 == len(resp.json['management'])
