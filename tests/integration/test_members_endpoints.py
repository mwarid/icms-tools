from collections import defaultdict
from tests.fixtures.basic_fixtures import TestappSingletonFactory
from flask.wrappers import Response
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
import logging
from datetime import date
from icms_orm import PseudoEnum
from tests.fixtures.fakedata_fixtures import (
    FakeInst,
    FakeInsts,
    FakePeople,
    FakePerson,
    FakeRegion,
    FakeRegions,
    FakeCountry,
    FakeCountries,
)


class TestPath(PseudoEnum):
    members_count = "/restplus/statistics/members_count"
    users_info = "/restplus/members/users_info"
    assignments = "/restplus/members/assignments"


@pytest.mark.usefixtures("recycle_dbs", "populate_dbs")
class TestMembersCount:
    _backed_up_threshold = None

    @classmethod
    def setup_class(cls):
        config = TestappSingletonFactory.get_web_app().config
        cls._backed_up_threshold = config["STATS_PRIVACY_THRESHOLD"]
        config["STATS_PRIVACY_THRESHOLD"] = -1

    @classmethod
    def teardown_class(cls):
        config = TestappSingletonFactory.get_web_app().config
        config["STATS_PRIVACY_THRESHOLD"] = cls._backed_up_threshold

    def test_by_inst_authors_counts_per_year(self, client: IcmsTestClient):
        resp = (
            client.request(
                f"{TestPath.members_count}?grouping_column=inst_code&is_author=true"
            )
            .auth_header(FakePeople.tbawej.email)
            .get()
        )
        assert resp.status_code == 200

        data = defaultdict(int)
        entries = resp.get_json()["stats_entries_by_year"]

        for entry in entries:
            if entry["year"] == 2016:
                for label in entry["entries"]:
                    data[label["labels"]["inst_code"]] = label["count"]
                assert data[FakeInsts.desy.code] == 2
            elif entry["year"] == 2017:
                for label in entry["entries"]:
                    data[label["labels"]["inst_code"]] = label["count"]
                assert data[FakeInsts.desy.code] == 0
                assert data[FakeInsts.cern.code] == 2
                assert data[FakeInsts.fermilab.code] == 1
            elif entry["year"] == 2018:
                for label in entry["entries"]:
                    data[label["labels"]["inst_code"]] = label["count"]
                assert data[FakeInsts.cern.code] == 0
                assert data[FakeInsts.fermilab.code] == 0
            elif entry["year"] == 2020:
                for label in entry["entries"]:
                    data[label["labels"]["inst_code"]] = label["count"]
                assert data[FakeInsts.void_inst.code] == 1
            elif entry["year"] == 2024:
                for label in entry["entries"]:
                    data[label["labels"]["inst_code"]] = label["count"]
                assert data[FakeInsts.desy.code] == 4
                assert data[FakeInsts.cern.code] == 6
                assert data[FakeInsts.fermilab.code] == 2

    def test_by_inst_members_counts_per_year(self, client: IcmsTestClient):
        resp = (
            client.request(f"{TestPath.members_count}?grouping_column=inst_code")
            .auth_header(FakePeople.tbawej.email)
            .get()
        )
        assert 200 == resp.status_code

        data = defaultdict(int)
        entries = resp.get_json()["stats_entries_by_year"]

        for entry in entries:
            if entry["year"] == 2016:
                for label in entry["entries"]:
                    data[label["labels"]["inst_code"]] = label["count"]
                assert data[FakeInsts.desy.code] == 2
            elif entry["year"] == 2017:
                for label in entry["entries"]:
                    data[label["labels"]["inst_code"]] = label["count"]
                assert data[FakeInsts.desy.code] == 0
                assert data[FakeInsts.cern.code] == 2
                assert data[FakeInsts.fermilab.code] == 1
            elif entry["year"] == 2018:
                for label in entry["entries"]:
                    data[label["labels"]["inst_code"]] = label["count"]
                assert data[FakeInsts.cern.code] == 0
                assert data[FakeInsts.fermilab.code] == 0
            elif entry["year"] == 2020:
                for label in entry["entries"]:
                    data[label["labels"]["inst_code"]] = label["count"]
                assert data[FakeInsts.void_inst.code] == 1
            elif entry["year"] == 2024:
                for label in entry["entries"]:
                    data[label["labels"]["inst_code"]] = label["count"]
                assert data[FakeInsts.desy.code] == 6
                assert data[FakeInsts.cern.code] == 14
                assert data[FakeInsts.fermilab.code] == 4
                assert data[FakeInsts.void_inst.code] == 2

    def test_by_region_members_counts_per_year(self, client: IcmsTestClient):
        resp = (
            client.request(f"{TestPath.members_count}?grouping_column=region")
            .auth_header(FakePeople.tbawej.email)
            .get()
        )
        assert 200 == resp.status_code

        data = defaultdict(int)
        entries = resp.get_json()["stats_entries_by_year"]

        for entry in entries:
            if entry["year"] == 2016:
                for label in entry["entries"]:
                    data[label["labels"]["region"]] = label["count"]
                assert data[FakeRegions.germany.name] == 2
            elif entry["year"] == 2017:
                for label in entry["entries"]:
                    data[label["labels"]["region"]] = label["count"]
                assert data[FakeRegions.germany.name] == 0
                assert data[FakeRegions.cern.name] == 2
                assert data[FakeRegions.usa.name] == 1
            elif entry["year"] == 2018:
                for label in entry["entries"]:
                    data[label["labels"]["region"]] = label["count"]
                assert data[FakeRegions.cern.name] == 0
                assert data[FakeRegions.usa.name] == 0
            elif entry["year"] == 2020:
                for label in entry["entries"]:
                    data[label["labels"]["region"]] = label["count"]
                assert data[FakeRegions.switzerland.name] == 1
            elif entry["year"] == 2024:
                for label in entry["entries"]:
                    data[label["labels"]["region"]] = label["count"]
                assert data[FakeRegions.germany.name] == 6
                assert data[FakeRegions.cern.name] == 14
                assert data[FakeRegions.usa.name] == 4
                assert data[FakeRegions.switzerland.name] == 2

    @pytest.mark.parametrize(
        "user, threshold",
        [(FakePeople.tbawej, 5), (FakePeople.tbawej, 1), (FakePeople.tbawej, 10)],
    )
    def test_anonymized_members_count(
        self, client: IcmsTestClient, user: FakePerson, threshold: int
    ):
        TestappSingletonFactory.get_web_app().config[
            "STATS_PRIVACY_THRESHOLD"
        ] = threshold
        resp: Response = (
            client.request(f"{TestPath.members_count}?grouping_column=inst_code")
            .auth_header(user.login)
            .get()
        )
        assert 200 == resp.status_code
        counts = defaultdict(int)
        entries = resp.get_json()["stats_entries_by_year"]
        last_entry = entries[len(entries) - 1]
        for year_data in last_entry["entries"]:
            counts[year_data["labels"]["inst_code"]] = year_data["count"]
        inst: FakeInst
        for inst in FakeInsts.values():
            members = inst.members
            if len(members) > threshold:
                assert len(members) == counts[inst.code]
        TestappSingletonFactory.get_web_app().config["STATS_PRIVACY_THRESHOLD"] = -1


@pytest.mark.parametrize(
    "grant_supervisor_write", [TestPath.assignments], indirect=True
)
@pytest.mark.usefixtures("recycle_dbs", "populate_dbs", "grant_supervisor_write")
class TestMembersInfo:
    @pytest.mark.parametrize(
        "inst", [FakeInsts.cern, FakeInsts.desy, FakeInsts.fermilab]
    )
    def test_fetching_inst_members(self, client: IcmsTestClient, inst: FakeInst):
        user = FakePeople.tbawej
        resp: Response
        resp = (
            client.request(f"{TestPath.users_info}?inst_code={inst.code}")
            .auth_header(user.email)
            .get()
        )
        assert 200 == resp.status_code
        json = resp.get_json()
        for entry in json:
            assert (
                inst.code
                == [k for k, v in entry.get("affiliations").items() if v == "primary"][
                    0
                ]
            )
