from flask.wrappers import Response
from icms_orm.toolkit import RestrictedResource
from tests.fixtures.testkit.test_client import IcmsTestClient
import pytest
from icms_orm import PseudoEnum
from tests.fixtures.basic_fixtures import recycle_dbs, app, client, patch_app_config
from tests.fixtures.fakedata_fixtures import FakeFlags, FakePeople, FakePerson, populate_dbs
from tests.fixtures.authorization_fixtures import insert_access_classes, insert_dummy_resources
from tests.fixtures.script_fixtures import check_applications
from tests.fixtures.fakedata_processing_fixtures import bypass_application
from webapp.webapp import Webapp
from tests.testkit import AbstractPayloadTest


class TestPath(PseudoEnum):
    resources = '/restplus/admin/permissions/resources'
    classes = '/restplus/admin/permissions/classes'
    rights = '/restplus/admin/permissions/rights'
    stores = '/restplus/admin/permissions/stores'


@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs')
class TestParameterStores():
    @pytest.mark.parametrize('user, path', zip([FakePeople.tbawej] * len(TestPath.values()), TestPath.values()))
    def test_listing_param_stores(self, client: IcmsTestClient, user: FakePerson, path):
        resp: Response
        resp = client.request(path).auth_header(user.email).get()
        assert 200 == resp.status_code


@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs')
class TestResourcesManagement(AbstractPayloadTest):
    payloads = [{
        'type': 'endpoint',
        'key': '/restplus/mo/steps',
        'filters': {}
    }, {
        'type': 'endpoint',
        'key': '/restplus/fake/path',
        'filters': {'r.cmsId': 1988}
    }]

    update_payload = {
        'type': 'endpoint',
        'key': '/restplus/fake/steps',
        'filters': {'u.cms_id': 90210}
    }

    @pytest.mark.parametrize('user, path, payload', [
        (FakePeople.tbawej, TestPath.resources, payloads[0]),
        (FakePeople.tbawej, TestPath.resources, payloads[1])
    ])
    def test_adding_restricted_resource(self, client: IcmsTestClient,  user: FakePerson, path: str, app: Webapp, payload):

        resp: Response
        resp = client.request(path).auth_header(
            user.email).payload(payload).post()
        assert 200 == resp.status_code

        self.compare_json_against_payload(resp.get_json(), payload)

    @pytest.mark.parametrize('user, path, id', [
        (FakePeople.tbawej, TestPath.resources, 1),
        (FakePeople.tbawej, TestPath.resources, 2)
    ])
    def test_retrieving_resource_by_id(self, client: IcmsTestClient,  user: FakePerson, path: str, app: Webapp, id: int):
        resp: Response
        resp = client.request(f'{path}/{id}').auth_header(user.email).get()
        assert 200 == resp.status_code
        self.compare_json_against_payload(resp.get_json(), self.payloads[id-1])

    @pytest.mark.parametrize('user, path', [(FakePeople.tbawej, TestPath.resources)])
    def test_editing_resource_by_id(self, client: IcmsTestClient,  user: FakePerson, path: str, app: Webapp):
        resp: Response
        resp = client.request(
            f'{path}/2').auth_header(user.email).payload(self.update_payload).put()
        assert 200 == resp.status_code

        self.compare_json_against_payload(resp.get_json(), self.update_payload)

        resp = client.request(path).auth_header(user.email).get()
        json = resp.get_json()
        assert 200 == resp.status_code
        assert 2 == len(json)

        json = next(r for r in json if r.get('id') == 2)
        self.compare_json_against_payload(json, self.update_payload)

    @pytest.mark.parametrize('user, path', [(FakePeople.tbawej, TestPath.resources)])
    def test_removing_resource_by_id(self, client: IcmsTestClient,  user: FakePerson, path: str, app: Webapp):
        resp: Response
        resp = client.request(f'{path}/1').auth_header(user.email).delete()
        assert 200 == resp.status_code

        resp = client.request(f'{path}/1').auth_header(user.email).delete()
        assert 404 == resp.status_code

        resp = client.request(path).auth_header(user.email).get()
        assert 200 == resp.status_code
        assert 1 == len(resp.get_json())


@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs', 'insert_access_classes')
class TestAccessClassesManagement(AbstractPayloadTest):
    payloads = [
        {'name': 'chosen-ones',
            'rules': [{'u.cmsId': [1, 4, 18]}, {'masterchef': 'u.flags'}]},
        {'name': 'by-login', 'rules': [{'u.login': ['eve', 'adam']}]}
    ]

    new_ids = []

    @pytest.mark.parametrize('payload', payloads)
    def test_adding_a_new_class(self, payload, client):
        user: FakePerson
        user = FakePeople.tbawej
        resp: Response
        resp = client.request(TestPath.classes).auth_header(
            user.email).payload(payload).post()
        assert 200 == resp.status_code

        json = resp.get_json()
        self.compare_json_against_payload(json, payload)
        self.new_ids.append(json.get('id'))


@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs', 'insert_access_classes', 'insert_dummy_resources')
class TestPermisssionsManagement(AbstractPayloadTest):
    payloads = [
        {
            'resource_id': 1,
            'access_class_id': 2,
            'action': "read"
        }, {
            'resource_id': 2,
            'access_class_id': 3,
            'action': "write"
        }, {
            'resource_id': 3,
            'access_class_id': 1,
            'action': "read"
        }
    ]

    @pytest.mark.parametrize('payload', payloads)
    def test_adding_new_permissions(self, payload, client: IcmsTestClient):
        user: FakePerson
        user = FakePeople.tbawej
        resp: Response
        resp = client.request(TestPath.rights).auth_header(
            user.email).payload(payload).post()
        assert 200 == resp.status_code

        json = resp.get_json()
        self.compare_json_against_payload(json, payload)

    @pytest.mark.parametrize('id', range(1, len(payloads)+1))
    def test_fetching_by_id(self, id: int, client: IcmsTestClient):
        user: FakePerson
        user = FakePeople.tbawej
        resp: Response
        resp = client.request(f'{TestPath.rights}/{id}').auth_header(
            user.email).get()
        assert 200 == resp.status_code
        self.compare_json_against_payload(resp.get_json(), self.payloads[id-1])

    @pytest.mark.parametrize('id', range(1, len(payloads)+1))
    def test_deleting_by_id(self, id: int, client: IcmsTestClient):
        user: FakePerson
        user = FakePeople.tbawej
        resp: Response
        resp = client.request(f'{TestPath.rights}/{id}').auth_header(
            user.email).delete()
        assert 200 == resp.status_code

        resp = client.request(f'{TestPath.rights}/{id}').auth_header(
            user.email).delete()
        assert 404 == resp.status_code