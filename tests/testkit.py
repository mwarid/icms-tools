from flask.wrappers import Response


class AbstractPayloadTest():

    @classmethod
    def assert_success(cls, resp: Response, message=None) -> Response:
        assert 200 == resp.status_code, message or f'Expected code 200, received {resp.status_code}'
        return resp

    @classmethod
    def compare_json_against_payload(cls, json: dict, payload: dict, skip_keys=None):
        skip_keys = skip_keys or set()
        for key in payload.keys():
            if key in skip_keys:
                continue
            assert json.get(key) == payload[key], f'values differ for {key}: {json.get(key)} vs {payload[key]}'