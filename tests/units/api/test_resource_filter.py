from icms_orm.common.common_schema_people_tables import GenderValues
from blueprints.api_restplus.api_units.api_util_classes import ResourceFilter
from icms_orm.common import Person

class TestResourceFilter():
    def test_iteration(self):
        res_filter: ResourceFilter = ResourceFilter(Person.first_name, 'Jane')
        res_filter = ResourceFilter(Person.last_name, 'Doe', res_filter)
        res_filter = ResourceFilter(Person.gender, GenderValues.FEMALE, res_filter)
        
        assert 3 == len(list(res_filter))
        assert 'Doe' == list(res_filter)[1].value
