from flask.app import Flask
from icmsutils.permissions.wrappers import AccessRuleWrapper
from webapp.permissions.restricted_resource import ToolkitRestrictedResource
from icmsutils.permissions.authorization_context import AuthorizationContext
import pytest
from webapp.permissions import PermissionsManager, RestrictedResourcesManager, ToolkitAccessRulesProvider
from tests.fixtures.basic_fixtures import app, recycle_dbs

"""
Not quite "unit" tests as their dependencies including a set up DB connection.
Nevertheless we're checking the intermediate responses here rather than eg. the final API response status code.
"""

@pytest.mark.usefixtures(app.__name__, recycle_dbs.__name__)
class TestToolkitResourcesManager():
    def test_anonymous_resource_gets_returned_for_unmatched_keys(self, app, recycle_dbs):
        resource: ToolkitRestrictedResource
        resource = RestrictedResourcesManager.get_resource_by_key_and_context(
            key='/hypthetical/url/<int:id>', context=AuthorizationContext())
        assert resource.id is None


class TestToolkitAccessRulesProvider():
    @pytest.mark.parametrize('action', ['read', 'write', 'delete'])
    def test_default_rules_for_untracked_resources(self, app: Flask, recycle_dbs, action: str):
        with app.app_context():
            rsc = ToolkitRestrictedResource(None)
            rules = ToolkitAccessRulesProvider.get_rules_for_resource_and_action(
                rsc, action)
            config_rule = app.config['DEFAULT_PERMISSION_RULES'][action]
            assert len(rules) == 1, 'Default rules list should only contain 1 rule'
            assert config_rule == rules[0].rule
