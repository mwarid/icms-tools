'''
This test file groups some of the deprecated or should-be-deprecated views without much proper testing actually
'''
from datetime import datetime
import mockito
from mockito.mocking import mock
from mockito.mockito import when2
from util.EgroupHandler import EgroupHandler
from icmsutils.ldaputils import LdapPerson, LdapProxy
import pytest
import json
import logging
from io import BytesIO
from typing import Any, Dict, Optional
from flask.wrappers import Response
from icms_orm import PseudoEnum
from mockito import unstub, when
from mockito.matchers import captor
from requests import Response as ReqResponse
from requests import Session
from requests.sessions import PreparedRequest
from tests.fixtures.fakedata_fixtures import FakePeople
from tests.fixtures.testkit.test_client import IcmsTestClient, RequestBuilder
from icmsutils.icmstest.assertables import count_in_db

log: logging.Logger = logging.getLogger(__name__)


class TestUri(PseudoEnum):
    cadi_dashboard = '/cadi/dashboard'
    cadi_approved_lines = '/cadi/approvedCadilines'
    cadi_updated_lines = '/cadi/updatedCadilines'
    account_status = '/users/account_status/<int:hr_id>'
    account_status = '/users/account_status/43'
    check_new_users = '/users/check_new_users'
    modal_history = '/users/modal_history/<int:cms_id>'
    request_group_removal = '/users/request_removal_other/<string:grp>/<int:hr_id>'
    add_to_zh = '/users/addToZH/<int:hr_id>'
    mail_account_added = '/users/sendMailAccountAdded/<int:hr_id>'
    mail_account_disabled = '/users/sendMailAccountDisabled/<int:hr_id>'
    mail_reg_needed = '/users/sendMailRegNeeded/<int:hr_id>'

@pytest.fixture(scope='class')
def tune_data():
    from icms_orm.cmspeople import Person
    target: Person = Person.query.filter(
        Person.cmsId == FakePeople.tbawej.cms_id).one()
    target.dateCreation = datetime.today()
    Person.session().add(target)
    Person.session().commit()


@pytest.mark.usefixtures('recycle_dbs', 'populate_dbs', 'tune_data')
class TestAncientEndpoints():
    @classmethod
    def setup_class(cls):
        '''
        Stubbing the LdapProxy and some bits of EgroupHandler to let the client code run past it
        '''
        ldap_person: LdapPerson = LdapPerson({
            'memberOf': ['CN=zh', ],
            'displayName': [FakePeople.tbawej.login.encode('utf-8')],
            'division': ['FOO'.encode('utf8')],
            'employeeID': [FakePeople.tbawej.hr_id],
            'givenName': [FakePeople.tbawej.first_name.encode('utf-8')],
            'unixHomeDirectory': ['/root'.encode('utf8')],
            'telephoneNumber': [''.encode('utf8')],
            'sn': [''.encode('utf8')],
            'cernSection': [''.encode('utf8')],
            'gidNumber': ['42'.encode('utf8')],
            'mail': [FakePeople.tbawej.email.encode('utf8')],
            'postOfficeBox': [''.encode('utf8')],
            'userAccountControl': [''.encode('utf8')],
            'mobile': [''.encode('utf8')],
            'name': [FakePeople.tbawej.last_name.encode('utf-8')]
        })
        when(LdapProxy).find_people_by_filter(...).thenReturn([ldap_person])
        # when2(LdapProxy.get_person_by_hrid, 43, ...).thenReturn(None)
        # when2(LdapProxy.get_person_by_hrid,
        #   FakePeople.tbawej, ...).thenReturn(ldap_person)
        # when(EgroupHandler).__init__().the
        when2(EgroupHandler.__init__).thenReturn(None)
        # when2(EgroupHandler.client).thenReturn(None)
        when(EgroupHandler).addMemberPerson(...).thenReturn(None)
        when2(EgroupHandler.findGroup, mockito.any(str)).thenReturn(
            (True, mock({'Members': []})))

    @classmethod
    def teardown_class(cls):
        unstub()

    @ pytest.mark.parametrize('path', TestUri.values())
    def test_opening_pages(self, client: IcmsTestClient, path: str):
        path = path.replace('<int:cms_id>', str(FakePeople.tbawej.cms_id))
        path = path.replace('<int:hr_id>', str(FakePeople.tbawej.hr_id))
        path = path.replace('<string:grp>', 'z5')
        log.debug(f'Endpoint to be visited: {path}')
        resp: Response = client.request(path).auth_header(
            FakePeople.tbawej.login).get()
        assert 200 == resp.status_code
