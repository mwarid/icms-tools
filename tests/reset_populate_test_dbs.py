from typing import List

import pytest
from sqlalchemy.engine import Engine
from sqlalchemy.engine.reflection import Inspector
from sqlalchemy.schema import (
    DropConstraint, DropTable, MetaData, Table, ForeignKeyConstraint, _DropView
)

import icms_orm
from icms_orm import OrmManager
from icmscommon.governors import DbGovernor

from tests.fixtures.basic_fixtures import app
from tests.fixtures.fakedata_fixtures import populate_dbs
from tests.fixtures.booking_fixtures import insert_bookings

from tests.fixtures.job_opening_fixtures import insert_questionnaire, insert_job_opening

from tests.fixtures.authorization_fixtures import (
    insert_access_classes, insert_dummy_resources
)
from tests.fixtures.tenures_fixtures import (insert_tenure_related_data, insert_org_unit_types,
                                             insert_org_units, insert_positions, insert_tenures, insert_ex_officio_mandates)


def custom_drop_all(engines: List[Engine]):
    """Drop all foreign key constraints, then views, then tables.

    See https://github.com/pallets/flask-sqlalchemy/issues/722
    """
    engine_tables = {engine: [] for engine in engines}
    engine_foreign_keys = {engine: [] for engine in engines}
    engine_views = {engine: [] for engine in engines}
    view_to_delete_first = 'view_person'
    for engine in engines:
        inspector = Inspector.from_engine(engine)
        metadata = MetaData()
        with engine.begin() as connection:
            for table_name in inspector.get_table_names():
                foreign_keys = [
                    ForeignKeyConstraint((), (), name=foreign_key['name'])
                    for foreign_key in inspector.get_foreign_keys(table_name)
                    if foreign_key['name']
                ]
                engine_tables[engine].append(Table(table_name, metadata, *foreign_keys))
                engine_foreign_keys[engine].extend(foreign_keys)
            view_names = inspector.get_view_names()
            if view_to_delete_first in view_names:
                view_names.remove(view_to_delete_first)
                view_names.insert(0, view_to_delete_first)
            engine_views[engine] = [
                Table(view_name, metadata)
                for view_name in view_names
            ]
    for engine in engines:
        with engine.begin() as connection:
            for foreign_key in engine_foreign_keys[engine]:
                connection.execute(DropConstraint(foreign_key))
            for view in engine_views[engine]:
                connection.execute(_DropView(Table(view, metadata)))
    for engine in engines:
        with engine.begin() as connection:
            for table in engine_tables[engine]:
                connection.execute(DropTable(table))


@pytest.fixture(scope='class')
def db_manager() -> OrmManager:
    manager = DbGovernor.get_db_manager()
    # print('Using the following DB binds:\n', manager.binds)
    return manager


@pytest.fixture(scope='class')
def reset_dbs(db_manager: OrmManager):
    new_bind_keys = [
        icms_orm.epr_bind_key(),
        icms_orm.toolkit_bind_key(),
        icms_orm.cms_common_bind_key(),
    ]
    old_bind_keys = [
        icms_orm.cms_people_bind_key(),
    ]
    bind_keys = new_bind_keys + old_bind_keys
    new_engines = [
        db_manager.get_engine(bind=bind_key)
        for bind_key in new_bind_keys
    ]
    old_engines = [
        db_manager.get_engine(bind=bind_key)
        for bind_key in old_bind_keys
    ]
    custom_drop_all(new_engines)
    custom_drop_all(old_engines)
    for bind_key in reversed(bind_keys):
        db_manager.create_all(bind_key)


@pytest.mark.usefixtures(
    'app',
    'reset_dbs',
    'populate_dbs',
    'insert_bookings',
    'insert_access_classes', 'insert_dummy_resources',
    'insert_tenure_related_data',
    'insert_questionnaire',
    'insert_job_opening'
)
def test_reset_populate_dbs():
    """Reset configured DBs and populate with fake data.

    Invoke from the project's root directory as such:
    ICMS_LEGACY_USER=root PYTHONPATH=./ pytest tests/reset_populate_test_dbs.py -p tests.prestart_plugin
    """
    pass
