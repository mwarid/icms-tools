
import os
import datetime
import time
import netrc

from flask import current_app
from util import constants as const

import sqlalchemy as sa

from .sendMails import Message
from util.accountCheck import send_mail_db

from icmsutils.ldaputils import LdapProxy

subjArcGL = "[PC] Please post publication plan for %(code)s."
mailArcGL = """ Dear ARC chair and analysis contact person, and PAG conveners,

  The ARC green light has been set for %(code)s.

  This is a reminder that, if you have not already done so, you should at this time review and then post to the hypernews the publication plan for this analysis.
  
Best wishes,
Roberto, Maurizio, and Greg
"""


subjStalled = '[PC] Please update status for stalled analysis %(code)s.'
mailStalled = """Dear ARC chair and analysis contact person,

The analysis %(code)s has been in the %(status)s status, without any change since %(lastUpdate)s (i.e. for %(nDays)s days).

Please post a message to the HN summarising the status of the analysis and/or its review. If updates to the status have been discussed on HN during the last weeks, a very short statement is sufficient (please note that this is an automated message).

Best wishes,

Roberto and Maurizio (Physics Coordinators)
"""

mailStalledSummary = """Dear PC,

The following analyses have been without any change since a sufficent long time.

%(summaryTable)s

Best wishes,
Roberto and Maurizio (Physics Coordinators)
"""

class CadiReminder(object):

    def __init__(self, userName = None):
        self.greenLights = []
        self.stalled = []
        (self.login, self.account, self.password) = (None, None, None)
        self.eMailMap = { }

        try:
            (self.login, self.account, self.password) = netrc.netrc('/data/secrets/.netrc').authenticators('icmsweb.mail')
        except Exception as e:
            print(f'Failed to grab the secrets with: {e}')    

        # From Bill:
        # PRE-APP,PHYS-APP,PAS-PUB,PUB-Draft/ReadyForLE,CWR-ended,FinalReading,Ref comments rcvd

        self.statusList = [ 'GoingToPreApp', 'PRE-APP', 'PHYS-APP', 'PAS-PUB', 'PUB-Draft', # /ReadyForLE',
                            'CWR-ended', 'FinalReading', 'RefComments' ]

        dbUrl = current_app.config[ 'SQLALCHEMY_BINDS' ][ const.OPT_NAME_BIND_LEGACY ]
        self.pplEngine = sa.create_engine( dbUrl )
        self.anaEngine = sa.create_engine( dbUrl.replace( 'CMSPEOPLE', 'CMSAnalysis' ) )

        self.messages = {}

    def queryDB(self, statusList, timeRange):

        baseQuery = "select code,status,updaterDate from Analysis where status in ('%s') and updaterDate like '%s';"

        query = baseQuery % ("','".join(statusList), timeRange)

        # print( "query: '%s'" % query )

        result = []
        with self.anaEngine.connect( ) as con :
            rs = con.execute( query )
            # clean up the results a bit
            for item in rs:
                if item is None: continue
                entry, status, lastUpdate = item
                lastUpdate = [int(x) for x in reversed(lastUpdate.split('/'))]
                result.append( [ entry,
                                 status,
                                 datetime.datetime( lastUpdate[0], lastUpdate[1], lastUpdate[2] )
                               ] )

        # add entries where the updaterDate is not set, use the creatorDate instead:

        baseQuery = "select code,status,creatorDate from Analysis where status in ('%s') and updaterDate = '';"
        query = baseQuery % ("','".join(statusList),)
        # print( "query: '%s'" % query )
        with self.anaEngine.connect( ) as con :
            rs = con.execute( query )
            # clean up the results a bit
            for item in rs:
                if item is None: continue
                entry, status, lastUpdate = item
                lastUpdate = [int(x) for x in reversed(lastUpdate.split('/'))]
                result.append( [ entry,
                                 status,
                                 datetime.datetime( lastUpdate[0], lastUpdate[1], lastUpdate[2] )
                               ] )

        return result

    def getHistory(self, code, status, lastUpdate):

        query = """select updaterDate, field, newValue from CMSAnalysis.History
        where forId in
        (select id from CMSAnalysis.Analysis where code = '%s')
        and newValue not in ('IN WORK', 'Proposed', 'Accepted', 'Waiting') /* ignore changes in status of Committee */
;
""" % ( code, status)

        # print( query )

        result = []
        with self.anaEngine.connect( ) as con :
            rs = con.execute( query )
            for item in rs.fetchall():
                if item is None: continue
                updDateStr, field, newValue = item
                try:
                    updDate = datetime.datetime.strptime(updDateStr, '%a, %d %b %Y %H:%M:%S %Z')
                except ValueError:
                    print( "ERROR converting date from '%s'" % updDateStr )
                    continue

                # ignore updates older than "lastUpdate"
                if updDate < lastUpdate: continue

                result.append( [updDate,
                                field,
                                newValue] )

        return result

    def getStatChangeFromHistory( self, code, status, lastUpdate ) :

            query = """select updaterDate, field, newValue from CMSAnalysis.History
            where forId in
            (select id from CMSAnalysis.Analysis where code = '%s')
            /* and newValue not in ('IN WORK', 'Proposed', 'Accepted', 'Waiting') */
            and newValue = '%s'
    ;
    """ % (code, status)

            # print( query )

            result = [ ]
            with self.anaEngine.connect( ) as con :
                rs = con.execute( query )
                for item in rs.fetchall( ) :
                    if item is None : continue
                    updDateStr, field, newValue = item
                    try :
                        updDate = datetime.datetime.strptime( updDateStr, '%a, %d %b %Y %H:%M:%S %Z' )
                    except ValueError :
                        print( "ERROR converting date from '%s'" % updDateStr )
                        continue

                    # ignore updates older than "lastUpdate"
                    if updDate < lastUpdate : continue

                    # ignore updates if they change the status to the same value
                    # if field == 'status' and newValue == status: continue

                    result.append( [ updDate,
                                     field,
                                     newValue ] )

            return result

    def getGreenLightDateFromHistory( self, code, status, lastUpdate ) :

            query = """select updaterDate, field, newValue from CMSAnalysis.History
            where forId in
            (select id from CMSAnalysis.Analysis where code = '%s')
            /* and newValue not in ('IN WORK', 'Proposed', 'Accepted', 'Waiting') */
            and newValue = 'ARC-GreenLight' and tbl = 'Analysis'
            order by updaterDate
    ;
    """ % (code, )

            # print( query )

            result = [ ]
            with self.anaEngine.connect( ) as con :
                rs = con.execute( query )
                for item in rs.fetchall( ) :
                    if item is None : continue
                    updDateStr, field, newValue = item
                    try :
                        updDate = datetime.datetime.strptime( updDateStr, '%a, %d %b %Y %H:%M:%S %Z' )
                    except ValueError :
                        print( "ERROR converting date from '%s'" % updDateStr )
                        continue
                    # remember only the last value
                    result =[ updDate, field, newValue ]

            return result[0]

    def getInfoFromDB(self):

        timeRange = '%%/20%%'
        arcGLs = self.queryDB(statusList=['ARC-GreenLight'], timeRange=timeRange)
        self.notifyListArcGL = set()
        for code, status, lastUpdate in arcGLs:

            # lastUpdate = self.getLastUpdate( code, lastUpdate, status )
            lastUpdate = self.getGreenLightDateFromHistory( code, status, lastUpdate )

            # ignore if the change happened before yesterday:
            if lastUpdate < datetime.datetime.today() - datetime.timedelta(days=1) : continue
            print( "GreenLight for %s on %s" % (code, lastUpdate) )

            self.notifyListArcGL.add( code )
            self.greenLights.append( [code, status, lastUpdate] )

        stalledList = []
        if datetime.datetime.today().day == 1: # do this only we really need it ...
            stalledList = self.queryDB( self.statusList, timeRange=timeRange)

        self.notifyListOther = set()
        for code, status, lastUpdate in stalledList:

            lastUpdate = self.getLastUpdate( code, lastUpdate, status )

            # ignore "old" entries, cutoff date at present is Jul 1, 2016
            if lastUpdate < datetime.datetime(2016,7,1) : continue

            # ignore if the latest change is less than one month ago:
            if lastUpdate > datetime.datetime.today()- datetime.timedelta(days=31) : continue

            self.notifyListOther.add( code )
            self.stalled.append( [code, status, lastUpdate] )

        # get emails for the GreenLights:
        self.eMailMap = { }
        for code, status, lastUpdate in self.greenLights:
            if code in self.eMailMap :
                print( "ERROR: code %s already in eMailMap " % code )
            else :
                self.eMailMap[ code ] = self.getEmails( code )

    def showInfo(self):
        msgGL = ''
        msgGL += "\nCadiLines presently in state 'ARC-GreenLight':"
        msgGL += '%10s    | %10s \n' % ("code", 'lastUpdate')
        for code, status, lastUpdate in self.greenLights:
            msgGL +=  " - %10s | %10s \n" % ( code[1:] if code.startswith('d') else code, lastUpdate.isoformat().split('T')[0] )

        msgOld = ''
        msgOld += "\n %s CadiLines where last status update more than 31 days ago:\n" % len(self.notifyListOther)
        msgOld += '%10s    | %10s | %14s | %s \n' % ("code", 'lastUpdate', 'status', 'email')
        for code, status, lastUpdate in self.stalled:
            codeName = code[1:] if code.startswith('d') else code
            msgOld +=  " - %10s | %10s | %14s | %s \n" % ( codeName,
                                                   lastUpdate.isoformat().split('T')[0],
                                                   status,
                                                   'hn-cms-%s@cern.ch' % codeName
                                                )

        msgPpl = ''
        msgPpl +=  "\nPeople which will get notified for greenlighted CadiLines:"
        msgPpl +=  '%10s    | %s \n' % ("code", 'eMails')
        for code in sorted(self.notifyListArcGL):
            msgPpl += " - %10s | %s \n" % ( code[1:] if code.startswith('d') else code,
                                      ', '.join(self.eMailMap[code]) )

        print( msgGL )
        print( msgOld )
        print( msgPpl )

        self.messages = { 'greelight': msgGL, 'stalled': msgOld, 'pplForGreenlight': msgPpl }

    def getLastUpdate( self, code, lastUpdate, status ) :

        statChgHist = self.getStatChangeFromHistory( code, status, lastUpdate )

        # print( "\ngetLastUpdate: code %s lastUpdate %s status %s - statChgHist %s " % (code, lastUpdate, status, statChgHist) )

        # look for the most recent change-date for this status
        for updDate, field, newVal in statChgHist :
            if updDate > lastUpdate :  # was updated more recently than "lastUpdate" in Analysis entry
                # print( "%s (%s) updated after %s : %s - %s %s" % (code, status, lastUpdate, updDate, field, newVal) )
                lastUpdate = updDate

        return lastUpdate

    def getEmails(self, code):

        pplList = self.getPeopleToNotify(code)

        result = set()
        with self.pplEngine.connect( ) as con :
            idString = str([int(x) for x in pplList])
            idString = idString.replace('[','').replace(']','')
            query = "select loginid from PeopleData where CMSid in (%s);" % ( idString, )
            res = con.execute( query ).fetchall()
            if len(res) != len(pplList):
                print( "[%s] ERROR inconsistent number of emails found %d - expected %d" % (code, len(res), len(pplList)) )
                print( "[%s] ... pplList: %s" % (code, ','.join([str(x) for x in pplList]) ) )
                print( "[%s] ... res    : %s" % (code, ','.join([str(x) for x in res]) ) )
            for login, in res:
                eMail = LdapProxy().get_person_by_login(login).mail
                if eMail and type(eMail) is not type(True):
                    result.add( eMail )
                else: # if not eMail or type(eMail) is type(True):
                    print( "[%s] ==> No email found for login %s " % (code, login) )

        return result

    def getPeopleToNotify(self, code):
        result = set()
        with self.anaEngine.connect( ) as con :
            committeeId, awg = None, None

            anId,committeeId,awg = con.execute( "select id,committee, awg from Analysis where code='%s';" % code ).fetchall( )[0]
            print( "found analysis id %s for code %s " % (anId, code) )

            # Analysis CADI contact:
            try:
                res = con.execute( "select cmsid from AnalysisAnalysts where analysis='%s';" % anId ).fetchall()
                print( "got res: ", res )
                contact, = res[0]
                result.add( contact )
                print( "[%s] found contact: %s" % (code, contact) )
            except ValueError :
                print( "ERROR when trying to get email for contact (updater) for %s " % code )

            # ARC:
            if committeeId:
                try :
                    arc = con.execute( "select cmsid from CommitteeMembers where status='Accepted' and memberId in (select memberId from CommitteesMembersRel where id=%d);" % int(committeeId) ).fetchall()
                    result |= set( [ x[0] for x in arc] )
                    print( "[%s] found ARC: %s" % (code, ','.join( [x[0] for x in arc] ) )  )
                except ValueError :
                    print( "ERROR when trying to get email for contact for %s " % code )

            # ... and Conveners
            if awg:
                try :
                    conv = con.execute( "select cmsid from AwgConveners where status='Active' and awg=%d;" % int(awg) ).fetchall()
                    result |= set( [ x[ 0 ] for x in conv ] )
                    print( "[%s] found conveners: %s" % (code, ','.join( [x[0] for x in conv] ) )  )
                except ValueError :
                    print( "ERROR when trying to get email for contact for %s " % code )

        return result

    def sendEmails(self, what, db):

        if 'greenlights' in what.lower():
            for codeRaw, status, lastUpdated in self.greenLights:
                code = codeRaw[1:] if codeRaw.startswith('d') else codeRaw
                msg = Message()
                msg.fromAddress = 'cms-physics-coordinator@cern.ch'
                msg.toAddresses = list(self.eMailMap[codeRaw])
                msg.bccAddresses = ['icms-support@cern.ch']
                msg.subject = subjArcGL % { "code" : code }
                msg.body = mailArcGL % { "code" : code }
                print( "going to send mail for GreenLight %s:\n%s" % (code, msg) )
                if not msg.isValid( ) :
                    print( ( 'sendEmails> invalid message found (%s), not sending mail ... ' % str( msg ) ) )
                    continue

                # SMTP( ).sendEmail( msg.subject, msg.body, msg.fromAddress, msg.toAddresses,
                #                    bccAddresses=msg.bccAddresses )
                send_mail_db( db=db,
                              fromAddr=msg.fromAddress,
                              toAddrs=msg.toAddresses,
                              ccAddrs=[msg.fromAddress],
                              bccAddrs=msg.bccAddresses,
                              subject=msg.subject,
                              msgBody=msg.body,
                              )

        # on Feb 1, 2017 from my laptop mail sending crashed after the first 49 mails - likely blocked by mail server ...
        alreadySent = [] # [ 'FSQ-12-001', 'FSQ-12-004', 'B2G-12-016', 'TOP-12-031', 'TOP-12-039', 'BPH-13-002', 'FSQ-13-007', 'SMP-13-008', 'HIN-13-005', 'BPH-13-008', 'HIN-14-005', 'TOP-14-008', 'EXO-14-007', 'TOP-14-013', 'SMP-14-010', 'BPH-14-009', 'HIN-15-001', 'TOP-15-006', 'BPH-15-001', 'BPH-15-002', 'TOP-15-008', 'SMP-15-003', 'HIG-15-009', 'EXO-15-007', 'BTV-15-002', 'HIN-15-008', 'EXO-15-009', 'BPH-15-008', 'SMP-15-009', 'HIN-15-015', 'SUS-15-012', 'B2G-15-007', 'BPH-15-009', 'B2G-15-008', 'HIG-15-013', 'TOP-16-001', 'TAU-16-002', 'EXO-16-011', 'EXO-16-012', 'SMP-16-005', 'SMP-16-004', 'HIN-16-002', 'FSQ-16-004', 'BTV-16-001', 'HIN-16-003', 'HIN-16-004', 'HIN-16-005', 'HIN-16-006', 'HIN-16-007' ]

        # on Feb 2, 2017 from my laptop mail sending crashed several times (general authentication issues on the server side) ...
        # alreadySent =  ['PFT-10-001', 'EGM-10-001', 'PFT-10-002', 'EGM-10-003', 'EGM-10-004', 'EGM-10-005', 'PFT-10-003', 'PFT-10-004', 'EGM-10-006', 'BPH-10-019', 'EWK-11-005', 'FSQ-12-008', 'HIN-12-009', 'B2G-12-007', 'B2G-12-008', 'B2G-12-017', 'FSQ-12-033', 'TOP-12-030', 'FSQ-13-003', 'JME-13-001', 'JME-13-002', 'JME-13-005', 'JME-13-007', 'TOP-13-007',]
        # alreadySent += ['EXO-13-005', 'FSQ-13-009', 'LUM-13-001', 'LUM-13-002', 'B2G-14-001', 'TOP-14-004', 'B2G-14-003', 'HIN-14-008', 'HIN-14-014', 'JME-14-001']
        # alreadySent += ['JME-14-003', 'JME-14-002', 'TOP-14-009', 'FTR-14-015', 'TOP-14-014', 'TOP-14-019', 'TOP-14-020', 'SUS-14-017', 'JME-14-005', 'SMP-14-021']
        # alreadySent += ['FSQ-15-003', 'SMP-15-002', 'HIN-15-004']

        print( "found %d mails already sent ... " % len(alreadySent) )
        if 'monthly' in what.lower():
            for codeRaw, status, lastUpdated in self.stalled:
                code = codeRaw[1:] if codeRaw.startswith('d') else codeRaw
                if code in alreadySent:
                    print( 'mail already sent for code %s -- ignoring' % code )
                    continue
                nDays = (datetime.datetime.now() - lastUpdated).days
                msg = Message()
                msg.fromAddress = 'cms-physics-coordinator@cern.ch'
                msg.toAddresses = [ 'hn-cms-%s@cern.ch' % code ]
                msg.bccAddresses = ['icms-support@cern.ch']
                msg.subject = subjStalled % { "code" : code }
                msg.body = mailStalled % { "code" : code,
                                           'status' : status,
                                           'lastUpdate' : lastUpdated.isoformat().split('T')[0],
                                           'nDays' : nDays,
                                         }
                print( "going to send mail for stalled %s:\n%s" % (code, msg) )
                if not msg.isValid( ) :
                    print( ( 'sendEmails> invalid message found (%s), not sending mail ... ' % str( msg ) ) )
                    continue
                # SMTP( ).sendEmail( msg.subject, msg.body, msg.fromAddress, msg.toAddresses,
                #                    bccAddresses=msg.bccAddresses )
                send_mail_db( db=db,
                              fromAddr=msg.fromAddress,
                              toAddrs=msg.toAddresses,
                              ccAddrs=[msg.fromAddress],
                              bccAddrs=msg.bccAddresses,
                              subject=msg.subject,
                              msgBody=msg.body,
                              )
                time.sleep(8)


        if 'pcsummary' in what.lower():
            msg = Message()
            msg.fromAddress = 'icms-support@cern.ch'
            msg.toAddresses =  [ 'cms-physics-coordinator@cern.ch' ]
            msg.bccAddresses = ['icms-support@cern.ch']
            msg.subject = '[PC] Summary of stalled analyses'
            msg.body = mailStalledSummary % { "summaryTable" : self.messages['stalled'] }
            print( "going to send mail for stalled summary " )
            if not msg.isValid( ) :
                print( ( 'sendEmails> invalid message found (%s), NOT sending mail ... ' % str( msg ) ) )
            else:
                # SMTP( ).sendEmail( msg.subject, msg.body, msg.fromAddress, msg.toAddresses,
                #                    bccAddresses=msg.bccAddresses )
                send_mail_db( db=db,
                                fromAddr=msg.fromAddress,
                                toAddrs=msg.toAddresses,
                                ccAddrs=msg.toAddresses, # [msg.fromAddress],
                                bccAddrs=msg.toAddresses, # msg.bccAddresses,
                                subject=msg.subject,
                                msgBody=msg.body,
                                )
